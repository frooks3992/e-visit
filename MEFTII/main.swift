//
//  main.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 02/02/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import Foundation
import UIKit

UIApplicationMain(
    CommandLine.argc,
    CommandLine.unsafeArgv,
    NSStringFromClass(MFApplication.self),
    NSStringFromClass(AppDelegate.self)
)

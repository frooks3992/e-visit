//
//  UIImage+base64.swift
//  MEFTII
//
//  Created by Farhan on 16/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import Foundation
import UIKit


class Base64Image{
      static var shared = Base64Image()
    
    func imageToBase64(imageToDecode: UIImage!) -> String {
      //  let imageData = imageToDecode.jpegData(compressionQuality: 0.8)
        
     
           let imageData = imageToDecode.jpegData(compressionQuality: 1)
        return (imageData?.base64EncodedString(options:
            Data.Base64EncodingOptions.init(rawValue: 0)))!
          
    }
    
     func  convertImageToBase64String(image : UIImage ) -> String
       {
           let strBase64 =  image.pngData()?.base64EncodedString()
           return strBase64!
       }
    
    
  
    
}
public enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

extension UIImage {
    public func toBase64(format: ImageFormat) -> String? {
        var imageData: Data?

        switch format {
        case .png:
            imageData = self.pngData()
        case .jpeg(let compression):
            imageData = self.jpegData(compressionQuality: compression)
        }

        return imageData?.base64EncodedString()
    }
    
   
}

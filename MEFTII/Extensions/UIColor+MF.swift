//
//  UIcolor+MF.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 12/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    
    class var  MFDarkRed: UIColor {
        //rgba(222, 53, 53, 1)
        return UIColor(red: 222.0 / 255.0, green: 53.0 / 255.0, blue: 53.0 / 255.0, alpha: 1.0)
    }
    
    class var  MFRed: UIColor {
        //rgba(255, 58, 58, 1)
        return UIColor(red: 255.0 / 255.0, green: 58.0 / 255.0, blue: 58.0 / 255.0, alpha: 1.0)
    }
    
    class var MFLightBlue: UIColor {
        //rgba(51, 153, 255, 1)
        return UIColor(red: 51.0 / 255.0, green: 153.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }
    
    class var MFBlue: UIColor {
           //rgba(51, 153, 255, 1)
           return UIColor(red: 0 / 255.0, green: 122.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
       }
    
    class var MFGreen: UIColor {
              //rgba(51, 153, 255, 1)
              return UIColor(red: 0 / 255.0, green: 178.0 / 255.0, blue: 0 / 255.0, alpha: 1.0)
          }
    
    class var MFLightGray: UIColor {
        //rgba(233, 233, 233, 1)
        return UIColor(red: 233.0 / 255.0, green: 233.0 / 255.0, blue: 233.0 / 255.0, alpha: 1.0)
    }
    
    class var  MFLightBlack: UIColor {
        //rgba(0, 0, 0, 1)
        return UIColor(red: 0 / 255.0, green: 0 / 255.0, blue: 0 / 255.0, alpha: 0.5)
    }
    
    class var  MFCalenderGreen: UIColor {
        return #colorLiteral(red: 0, green: 0.6980392157, blue: 0, alpha: 1)
    }
    
    class var  MFBlack60: UIColor {
        return UIColor(red: 0 / 255.0, green: 0 / 255.0, blue: 1 / 255.0, alpha: 0.5)
    }
    
    
}


extension UIColor {
    /// Create color from RGB
    convenience init(absoluteRed: Int, green: Int, blue: Int) {
        self.init(
            absoluteRed: absoluteRed,
            green: green,
            blue: blue,
            alpha: 1.0
        )
    }
    
    /// Create color from RGBA
    convenience init(absoluteRed: Int, green: Int, blue: Int, alpha: CGFloat) {
        let normalizedRed = CGFloat(absoluteRed) / 255
        let normalizedGreen = CGFloat(green) / 255
        let normalizedBlue = CGFloat(blue) / 255
        
        self.init(
            red: normalizedRed,
            green: normalizedGreen,
            blue: normalizedBlue,
            alpha: alpha
        )
    }
    
    // Color from HEX-Value
    convenience init(hexValue:Int) {
        self.init(
            absoluteRed: (hexValue >> 16) & 0xff,
            green: (hexValue >> 8) & 0xff,
            blue: hexValue & 0xff
        )
    }
    
    // Color from HEX-String
    convenience init(hexString:String) {
        var normalizedHexString = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (normalizedHexString.hasPrefix("#")) {
            normalizedHexString.remove(at: normalizedHexString.startIndex)
        }
        //  normalizedHexString = normalizedHexString.replacingOccurrences(of: "#", with: "")
        
        // Convert to hexadecimal integer
        var hexValue:UInt32 = 0
        Scanner(string: normalizedHexString).scanHexInt32(&hexValue)
        
        self.init(
            hexValue:Int(hexValue)
        )
    }
}

//
//  Post+LocalNotifications.swift
//  MEFTII
//
//  Created by Farhan on 01/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import Foundation
extension Notification.Name {
    static let isFirstNameEmpty = Notification.Name("isFirstNameEmpty")
    static let isLastNameEmpty = Notification.Name("isLastNameEmpty")
    static let completedLengthyDownload = Notification.Name("completedLengthyDownload")
}

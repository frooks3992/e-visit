//
//  UIView+Additions.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 24/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    func addTransition(_ duration:CFTimeInterval, fromLeft:Bool) {
        
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        animation.subtype = fromLeft ? .fromLeft:.fromRight
        animation.duration = duration
        
        //layer.add(animation, forKey: CATransitionType.fade.rawValue)
        layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
    
    func addRightShadow() {
        
        let shadowSize : CGFloat = 2.0
        let shadowPath = UIBezierPath(rect: CGRect(x: self.frame.size.width,
                                                   y: 0,
                                                   width: shadowSize,
                                                   height: self.frame.size.height))
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowRadius = 2.0
        self.layer.shadowPath = shadowPath.cgPath
    }
}

extension UIView {
    func findViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.findViewController()
        } else {
            return nil
        }
    }
}

extension UIView {
    func getParentScrolView() -> UIScrollView? {
        if let view = self.superview {
            if view is UIScrollView {
                return view as? UIScrollView
            }
            return view.getParentScrolView()
        }
        else {
            return nil
        }
        
    }
}




extension Decodable {
  init(from: Any) throws {
    let data = try JSONSerialization.data(withJSONObject: from, options: .prettyPrinted)
    let decoder = JSONDecoder()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:sszzz"
    decoder.dateDecodingStrategy = .formatted(dateFormatter)
    self = try decoder.decode(Self.self, from: data)
  }
}

extension Encodable {
    
    func getJsonObject() -> Any {
        var jsonObj:Any = ""
        do {
            let data = try JSONEncoder().encode(self)
            jsonObj = try JSONSerialization.jsonObject(with: data, options: [])
        }
        catch {
            jsonObj = ""
        }
        return jsonObj
    }
    
}

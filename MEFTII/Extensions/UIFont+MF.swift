//
//  UIFont+MF.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 12/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    

    class func AppFont(ofSize size: Float = 15.0) -> UIFont {
        return UIFont(name: "Montserrat-Regular", size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size))
    }
    
    class func AppBoldFont(ofSize size: Float = 15.0) -> UIFont {
        return UIFont(name: "Montserrat-Bold", size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size), weight: UIFont.Weight.bold)
    }
    
    class func AppSemiBoldFont(ofSize size: Float = 15.0) -> UIFont {
        return UIFont(name: "Montserrat-SemiBold", size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size), weight: UIFont.Weight.bold)
    }

    class func AppMediumFont(ofSize size: Float = 15.0) -> UIFont {
        return UIFont(name: "Montserrat-Medium", size: CGFloat(size)) ?? UIFont.systemFont(ofSize: CGFloat(size), weight: UIFont.Weight.bold)
    }
    
    class func printAllFonts() {
        for family in UIFont.familyNames {
            print("Font family Name: \(family)")
            print("-------------------------------")
            
            for name in UIFont.fontNames(forFamilyName: family) {
                print("   \(name)")
            }
            print("-------------------------------")
        }
    }
}


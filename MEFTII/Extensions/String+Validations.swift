//
//  String+Validations.swift
//  MEFTII
//
//  Created by Farhan on 10/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import Foundation

extension String {
    
    public func isValidPassword() -> Bool {
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{6,}"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    
    
    public func isValidEmail() -> Bool {
        let emailRegex = "[a-zA-Z0-9._%+-]+@[a-zA-Z0-9]+\\.[a-zA-Z]{2,5}(\\.[a-zA-Z]{2,5}){0,1}"
        
         return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
     }
    
    
    
    
}
extension String {
    var containsWhitespace : Bool {
        return(self.rangeOfCharacter(from: .whitespacesAndNewlines) != nil)
    }
}


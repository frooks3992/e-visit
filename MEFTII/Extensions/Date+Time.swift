//
//  Date+Time.swift
//  MEFTII
//
//  Created by Farhan on 02/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import Foundation

class Date_Time{
    
    public static var shared = Date_Time()
    
    
    
    func dateFormatter(date:Date)->String{
           
           let dateFormatter = DateFormatter()
                 dateFormatter.dateFormat = "MM-dd-yyyy"
                 let dateString = dateFormatter.string(from: date)
           return dateString
       }
       
    
    
    
    
}

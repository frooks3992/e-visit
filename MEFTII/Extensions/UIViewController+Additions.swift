//
//  UIViewController+Additions.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 20/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications



extension UIViewController {
    
    
    class func instantiateFromStoryboard(_ name: String = "Main") -> Self {
        return instantiateFromStoryboardHelper(name)
    }
    
    class func instantiateVCFromStoryboard(_ name: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: name)
    }
    
    
    fileprivate class func instantiateFromStoryboardHelper<T>(_ name: String) -> T {
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let identifier = String(describing: self)
        let controller = storyboard.instantiateViewController(withIdentifier: identifier) as! T
        return controller
    }
    
    
    
    func isUserEnabledNotification(completionHandler: @escaping (Bool) -> Void) {
        
        
        if #available(iOS 10.0, *) {
            
            var isEnabled : Bool = false
            let current = UNUserNotificationCenter.current()
            
            current.getNotificationSettings(completionHandler: { (settings) in
                
                if settings.authorizationStatus == .authorized {
                    // Notification permission was already granted
                    isEnabled = true
                    
                } else if settings.authorizationStatus == .notDetermined {
                    // Notification permission has not been asked yet, go for it!
                    isEnabled = false
                    
                } else if settings.authorizationStatus == .denied {
                    // Notification permission was previously denied, go to settings & privacy to re-enable
                    isEnabled = false
                    
                }
                completionHandler(isEnabled)
                
            })
            
        } else {
            
            var isEnabled : Bool = false
            // Fallback on earlier versions
            if let notificationType = UIApplication.shared.currentUserNotificationSettings?.types {
                if notificationType == [] {
                    
                    isEnabled = false
                } else {
                    isEnabled = true
                }
            } else {
                isEnabled = false
            }
            
            completionHandler(isEnabled)
        }
    }
    
    func setDummyImage(gender:String) -> UIImage {
          
          switch gender.lowercased() {
          case "female":
              
              return #imageLiteral(resourceName: "ic_female_placeholder")
          default:
                  return #imageLiteral(resourceName: "ic_male_placeholder")
          }
          
      }
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.bounds.width/2 - 150, y: self.view.bounds.height-120, width: 300, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.numberOfLines = 0
        toastLabel.preferredMaxLayoutWidth = 300
        toastLabel.font = UIFont(name: "Roboto-Regular", size: 12.0)
        toastLabel.text = message
        let size = toastLabel.sizeThatFits(CGSize(width: 300, height: 35))
        if size.height > 35 {
            toastLabel.sizeToFit()
        }
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 7;
        toastLabel.clipsToBounds  =  true
        toastLabel.center = CGPoint(x: self.view.center.x, y: toastLabel.center.y)
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 1.5, options: .curveEaseIn, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    
    //    func showAlertWithTitleAndMessage(title:String?, message: String?,_ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
    //
    //        if let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertMessageVC") as? AlertMessageVC {
    //            alert.modalPresentationStyle = .overCurrentContext
    //            alert.modalTransitionStyle = .crossDissolve
    //
    //            alert.setAlertWith(title: title ?? "", description: message ?? "", okBtnClickedBlock: actionBlock)
    //            self.navigationController?.present(alert, animated: true, completion: nil)
    //        } else {
    //            let alertViewController = UIAlertController(title: title ?? "", message: message ?? "", preferredStyle: .alert)
    //
    //            let okAction = UIAlertAction(title: Localized("BtnTitle_OK"), style: .default) { (action) in
    //
    //            }
    //
    //            alertViewController.addAction(okAction)
    //            self.present(alertViewController, animated: true, completion: nil)
    //        }
    //    }
    //    func showAlertWithMessage(message: String?, actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
    //
    //        self.showAlertWithTitleAndMessage(title: Localized("Title_Attention"), message: message, actionBlock)
    //    }
    //    func showErrorAlertWithMessage(message: String?, _ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
    //        self.showAlertWithTitleAndMessage(title: Localized("Title_Error"), message: message, actionBlock)
    //    }
    //    func showSuccessAlertWithMessage(message: String?, _ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
    //        self.showAlertWithTitleAndMessage(title: Localized("Title_successful"), message: message, actionBlock)
    //    }
    //
    //    func showAccessAlert(message: String?) {
    //
    //        let alertViewController = UIAlertController(title: Localized("Title_Alert"), message: message ?? "", preferredStyle: .alert)
    //
    //        let cancelAction = UIAlertAction(title: Localized("BtnTitle_CANCEL"), style: .cancel) { (action) in }
    //
    //        let settingAction = UIAlertAction(title: Localized("BtnTitle_SETTING"), style: .default) { (action) in
    //
    //            if let url = URL(string: UIApplicationOpenSettingsURLString ), UIApplication.shared.canOpenURL(url) {
    //                if #available(iOS 10, *) {
    //                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
    //                } else {
    //                    UIApplication.shared.openURL(url)
    //                }
    //            } else {
    //                self.showErrorAlertWithMessage(message: Localized("Message_UnexpectedError"))
    //            }
    //        }
    //
    //        alertViewController.addAction(cancelAction)
    //        alertViewController.addAction(settingAction)
    //
    //        self.present(alertViewController, animated: true, completion: nil)
    //
    //    }
    //
    //
    //    func setAttributedTextWithManatSign(WithText messageString: String?) -> NSMutableAttributedString {
    //
    //        // Return Empty string is text is emty
    //        if messageString?.isBlank == true {
    //            return NSMutableAttributedString(string: "" ,attributes: [ NSAttributedStringKey.font: UIFont.MBArial(fontSize: 14)])
    //        }
    //
    //        let amountAttributedText = NSMutableAttributedString(string: "\(messageString ?? "0") ₼" ,attributes: [ NSAttributedStringKey.font: UIFont.MBArial(fontSize: 14)])
    //
    //        amountAttributedText.addAttribute(NSAttributedStringKey.font,
    //                                          value: UIFont.systemFont(ofSize: 12),
    //                                          range: NSRange(location: (amountAttributedText.length - 1), length: 1))
    //
    //        amountAttributedText.addAttribute(NSAttributedStringKey.baselineOffset,
    //                                          value:2.00,
    //                                          range: NSRange(location:(amountAttributedText.length - 1),length:1))
    //
    //        return amountAttributedText
    //    }
    //}
    //
    //
    //extension UITabBarController {
    //
    //    var myStoryBoard: UIStoryboard {
    //
    //        if let currentStoryBoard = self.storyboard  {
    //            return currentStoryBoard
    //        } else {
    //            return UIStoryboard(name: "Main", bundle: nil)
    //        }
    //    }
    //
    //    func showAlertWithTitleAndMessage(title:String?, message: String?,_ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
    //
    //        if let alert = self.myStoryBoard.instantiateViewController(withIdentifier: "AlertMessageVC") as? AlertMessageVC {
    //            alert.modalPresentationStyle = .overCurrentContext
    //            alert.modalTransitionStyle = .crossDissolve
    //
    //            alert.setAlertWith(title: title ?? "", description: message ?? "", okBtnClickedBlock: actionBlock)
    //            self.navigationController?.present(alert, animated: true, completion: nil)
    //        } else {
    //            let alertViewController = UIAlertController(title: title ?? "", message: message ?? "", preferredStyle: .alert)
    //
    //            let okAction = UIAlertAction(title: Localized("BtnTitle_OK"), style: .default) { (action) in
    //
    //            }
    //
    //            alertViewController.addAction(okAction)
    //            self.present(alertViewController, animated: true, completion: nil)
    //        }
    //    }
    //    func showAlertWithMessage(message: String?, actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
    //
    //        self.showAlertWithTitleAndMessage(title: Localized("Title_Attention"), message: message, actionBlock)
    //    }
    //    func showErrorAlertWithMessage(message: String?, _ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
    //        self.showAlertWithTitleAndMessage(title: Localized("Title_Error"), message: message, actionBlock)
    //    }
    //    func showSuccessAlertWithMessage(message: String?, _ actionBlock : @escaping MBButtonCompletionHandler = { _ in }) {
    //        self.showAlertWithTitleAndMessage(title: Localized("Title_successful"), message: message, actionBlock)
    //    }
}

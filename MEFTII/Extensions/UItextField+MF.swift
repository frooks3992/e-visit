//
//  UItextField+MF.swift
//  MEFTII
//
//  Created by Schaheer Saleem on 10/21/19.
//  Copyright © 2019 Mian Waqas Umar. All rights reserved.
//

import UIKit

extension UITextField {
    
    func isEmailValid(candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
     
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
}

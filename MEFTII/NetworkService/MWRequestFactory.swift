//
//  MWRequestFactory.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 15/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Alamofire

class MWRequestFactory: SessionManager {
    
    //DEV :
    
    //STG :
    
    
    private var baseURL: URL
    private var baseURL_NEW: URL
    private var defaultTimeoutInterval = 30.0
    public static let shared: MWRequestFactory = {
    let type = Constants.getDevUrl(type: "dev")
    var urlString = "http://\(type):8080/mefti" 
//        var urlString = "http://173.255.217.231:8080/mefti"
        
        if urlString.hasSuffix("/") {
            urlString = String(urlString.dropLast())
        }
        
        let baseURL = URL(string: urlString)!
        let baseURL_NEW =  URL(string: "http://\(type):8080/doctor-evisit/")!
        
        let serverTrustPolicy = ServerTrustPolicy.disableEvaluation
        
        let serverTrustPolicies = [baseURL.host ?? "": serverTrustPolicy]
        let serverTrustPolicyManager = ServerTrustPolicyManager(policies: serverTrustPolicies)
    
        
        return MWRequestFactory(baseUrl: baseURL,baseURL_NEW: baseURL_NEW, timeoutInterval: 30.0, serverTrustPolicyManager: serverTrustPolicyManager)
    }()

    
    
    // MARK: - Lifecycle
    
    /// Creates an instance with the specified `baseUrl` and `serverTrustPolicyManager`.
    ///
    /// - parameter baseUrl:            The configuration used to construct the managed session.
    ///                                       `URLSessionConfiguration.default` by default.
    /// - parameter serverTrustPolicyManager: The server trust policy manager to use for evaluating all server trust
    ///                                       challenges. `nil` by default.
    ///
    /// - returns: The new `MWRequestFactory` instance.
    public init(
        baseUrl url: URL,baseURL_NEW url_new : URL,
        timeoutInterval timeout:TimeInterval,
        serverTrustPolicyManager: ServerTrustPolicyManager? = nil)
    {
        
        self.baseURL = url
        self.baseURL_NEW = url_new
        self.defaultTimeoutInterval = timeout
        
        let configration = URLSessionConfiguration.default
        configration.timeoutIntervalForRequest = defaultTimeoutInterval
        configration.allowsCellularAccess = true
        
        var headers = SessionManager.defaultHTTPHeaders
        headers["content-type"] = "application/json"
        headers["channel"] = "iOS"
        headers["deviceid"] = "unknown"
        
        configration.httpAdditionalHeaders = headers
        
        super.init(configuration: configration, serverTrustPolicyManager: serverTrustPolicyManager)
        
        self.startRequestsImmediately = false
    }
    
    deinit {
        
        cancelAllRequests()
        
        session.invalidateAndCancel()
    }
    
    func cancelAllRequests() {
        
        session.getAllTasks { tasks in
            
            for task in tasks {
                task.cancel()
            }
        }
        
    }
    
    func createRequest(endPoint: String,
                          parameters: [String : AnyObject]? ,
                          isPostRequest: Bool = true,
                          headers: [String : String]?) -> MWNetworkRequest {
        
        var methodName = endPoint
        if methodName.hasPrefix("/") {
            methodName.remove(at: methodName.startIndex)
        }
        let url = baseURL.appendingPathComponent(methodName)
        
        let HTTPCallMethod : HTTPMethod = isPostRequest ? .post:.get
        
        var originalRequest = URLRequest(url: url, cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: self.defaultTimeoutInterval)
        originalRequest.allHTTPHeaderFields = headers
        originalRequest.httpMethod = HTTPCallMethod.rawValue
        
        
        if let parm = parameters {
            do {
                if #available(iOS 11.0, *) {
                    let rqstBodyDta = try JSONSerialization.data(withJSONObject: parm, options: .sortedKeys)
                    originalRequest.httpBody = rqstBodyDta
                } else {
                    let rqstBodyDta = try JSONSerialization.data(withJSONObject: parm, options: [])
                    originalRequest.httpBody = rqstBodyDta
                }
            }
            catch {
                return MWNetworkRequest(dataRequest: request(originalRequest))
            }
        }
        
        
        return MWNetworkRequest(dataRequest: request(originalRequest))
    }
    
    func createRequest_New(endPoint: String,
                             parameters: [String : AnyObject]? ,
                             isPostRequest: Bool = true,
                             headers: [String : String]?) -> MWNetworkRequest {
         
          let email =   DataManager.shared.getcurrentEmail()
        
           //UserDefaults.standard.value(forKey: "patient_email") as? String
        
        let headers = ["Content-Type":"application/json",
                       "email":"\(email)",
                       "language":"english",
                       "channel":"ios",
                       "version":"1.0.0"]
           var methodName = endPoint
           if methodName.hasPrefix("/") {
               methodName.remove(at: methodName.startIndex)
           }
           let url = baseURL_NEW.appendingPathComponent(methodName)
           
           let HTTPCallMethod : HTTPMethod = isPostRequest ? .post:.get
           
           var originalRequest = URLRequest(url: url, cachePolicy: .reloadRevalidatingCacheData, timeoutInterval: self.defaultTimeoutInterval)
           originalRequest.allHTTPHeaderFields = headers
           originalRequest.httpMethod = HTTPCallMethod.rawValue
           
           
           if let parm = parameters {
               do {
                   if #available(iOS 11.0, *) {
                       let rqstBodyDta = try JSONSerialization.data(withJSONObject: parm, options: .sortedKeys)
                       originalRequest.httpBody = rqstBodyDta
                   } else {
                       let rqstBodyDta = try JSONSerialization.data(withJSONObject: parm, options: []) 
                       originalRequest.httpBody = rqstBodyDta
                   }
               }
               catch {
                   return MWNetworkRequest(dataRequest: request(originalRequest))
               }
           }
           
           
           return MWNetworkRequest(dataRequest: request(originalRequest))
       }
       
    
}


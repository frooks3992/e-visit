//
//  MWNetworkRequest.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 15/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import Alamofire

typealias NetworkCallSuccessBlock = (_ code: Int, _ description: String, _ response : [String:AnyObject?]) -> Void
typealias NetworkCallFailureBlock = (_ error: Error) -> Void

class MWNetworkRequest: NSObject {
    
    let request:DataRequest

    init(dataRequest:DataRequest) {
        request = dataRequest
        super.init()

    }
    
    func start(vc:BaseViewController?, successBlock:@escaping NetworkCallSuccessBlock, failureBlock: NetworkCallFailureBlock?) {

        request.resume()
        
        vc?.showLoadingView(progress: self.request.progress)
        request.validate(statusCode: 200..<600).responseJSON { response in
            
            vc?.hideLoadingView()
            
            let rsp = response.value as? [String:AnyObject?]
            let isSuccess = (rsp?["callStatus"] as? String ?? "") == "true"
            let description = rsp?["resultDesc"] as? String
            let code = Int(rsp?["resultCode"] as? String ?? "")
            
            switch response.result {
            case .success:
                self.showRequestDetailForSuccess(responseObject: response)
                if isSuccess {
                    successBlock(code ?? 0,description ?? "",rsp!)
                }
                else {
                    let err = MWNetworkRequest.createErrorObject(code: code, desription: description)
                    //vc?.showToast(message: err.localizedDescription)
                    vc?.showAlertWith(title: "Failure", message: err.localizedDescription, buttonText: "Okay", callBack: {
                                   
                                                            })
                    failureBlock?(err)
                }
                break
            case .failure(let error):
                
                vc?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Okay", callBack: {
                
                                         })
               // vc?.showToast(message: error.localizedDescription)
                self.showRequestDetailForFailure(responseObject: response, error as NSError)
                failureBlock?(error)
                break
            }
            
        }
    }
    
    class func createErrorObject(code:Int?, desription:String?) -> NSError {
        let erCode = code ?? 1077
        let erDesc = desription ?? "Unable to communicate with server!"
        let userInfo : [String: Any] = [NSLocalizedDescriptionKey : erDesc]
        return NSError(domain:"com.meftii.networking", code:erCode, userInfo:userInfo)
    }
    
    
    func showRequestDetailForSuccess(responseObject response : DataResponse<Any>) {
        
        #if DEBUG
        
        print("\n\n\n🔹🔹🔹 ------- Success Response Start ------- 🔹🔹🔹\n")
        print(""+(response.request?.url?.absoluteString ?? ""))
        print("\n=========   allHTTPHeaderFields   ========== \n")
        print(response.request!.allHTTPHeaderFields!)
        
        if let bodyData : Data = response.request?.httpBody {
            let bodyString = String(data: bodyData, encoding: String.Encoding.utf8)
            print("\n=========   Request httpBody   ========== \n" + (bodyString ?? ""))
        } else {
            print("\n=========   Request httpBody   ========== \n" + "Found Request Body Nil")
        }
        
        if let responseData : Data = response.data {
            let responseString = String(data: responseData, encoding: String.Encoding.utf8)
            print("\n=========   Response Body   ========== \n" + (responseString ?? ""))
        } else {
            print("\n=========   Response Body   ========== \n" + "Found Response Body Nil")
        }
        print("\n🔹🔹🔹 ------- Success Response End ------- 🔹🔹🔹\n\n\n")
        
        
        #endif
        
    }
    
    func showRequestDetailForFailure(responseObject response : DataResponse<Any>, _ error: Error) {
        
        
        #if DEBUG
        
        print("\n\n\n♦️♦️♦️ ------- Failure Response Start ------- ♦️♦️♦️\n")
        print(""+(response.request?.url?.absoluteString ?? ""))
        print("\n=========   allHTTPHeaderFields   ========== \n")
        print("%@",response.request!.allHTTPHeaderFields!)
        
        print("\n=========   Failure Reason   ========== \n" + error.localizedDescription )
        
        if let bodyData : Data = response.request?.httpBody {
            let bodyString = String(data: bodyData, encoding: String.Encoding.utf8)
            print("\n=========   Request httpBody   ========== \n" + (bodyString ?? ""))
        } else {
            print("\n=========   Request httpBody   ========== \n" + "Found Request Body Nil")
        }
        
        if let responseData : Data = response.data {
            let responseString = String(data: responseData, encoding: String.Encoding.utf8)
            print("\n=========   Response Body   ========== \n" + (responseString ?? ""))
        } else {
            print("\n=========   Response Body   ========== \n" + "Found Response Body Nil")
        }
        print("\n♦️♦️♦️ ------- Failure Response End ------- ♦️♦️♦️\n\n\n")
        
        
        #endif
        
        
    }
    
    
}

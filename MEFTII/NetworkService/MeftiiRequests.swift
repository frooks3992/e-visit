//
//  MeftiiRequests.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 16/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation


class MeftiiRequests {
    
    //0
    class func loginApi(userName:String, password: String) -> MWNetworkRequest {
        
        let service = "api/user/login"
        
        let headers : [String : String]? = nil
        
        let params = ["username": userName, "password": password]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    //0
    class func loginPatient(userName:String, password: String) -> MWNetworkRequest {
        
        let service = "/patient/login"
        let fcmKey = DataManager.shared.getFcmToken()
        let headers : [String : String]? = nil
        
        let params = ["password": password,"fcmKey":fcmKey]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //0
    class func loginDoctor(userName:String, password: String) -> MWNetworkRequest {
        
        let service = "/doctor/login"
        
        let headers : [String : String]? = nil
        let fcmKey = DataManager.shared.getFcmToken()
        let params = ["password": password,"fcmKey":fcmKey]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    //1
    class func updatePatientProfile(_ profile:PatientProfile) -> MWNetworkRequest {
        
        let service = "api/patient/profile/update"
        
        let headers : [String : String]? = nil
        
        
        let params = ["patientProfile": profile.getJsonObject() ]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    class func updatePatientProfile2(_ profile:Profile_p2) -> MWNetworkRequest {
        
        let service = "api/patient/profile/update"
        
        let headers : [String : String]? = nil
        
        
        let params = ["patientProfile": profile.getJsonObject() ]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //2
    class func updatePasswordSecndStep(email:String, role:String, password:String, confirmPass:String, otp: String) -> MWNetworkRequest {
        
        let service = "api/user/updatepassword"
        
        let headers : [String : String]? = nil
        
        let params = ["email": email, "role": role, "password": password, "confirmPassword": confirmPass, "otp" : otp]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //3
    class func getPatientProfile(username:String) -> MWNetworkRequest {
        
        let service = "api/patient/profile/get"
        
        let headers : [String : String]? = nil
        
        let params = ["username": username]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    
    
    //4
    class func forgotPasswordStepOne(email:String, role:String) -> MWNetworkRequest {
        
        let service = "api/user/forgotpassword"
        
        let headers : [String : String]? = nil
        
        let params = ["email": email, "role":role]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //5
    class func registerPatientApi(patientProfile profile:Profile_p) -> MWNetworkRequest {
        
        let service = "/patient/registerprofile"
        
        let headers : [String : String]? = nil
        
        profile.email = nil
        let params = profile.getJsonObject()
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as? [String : AnyObject], headers: headers)
        
    }
    
    class func updatePatientApi(patientProfile profile:Profile_p2) -> MWNetworkRequest {
        
        DataManager.shared.saveLoggedInPatient(profile: profile)
              
            
              
              let data = profile
                  //  data.city = nil
                    // data.state = nil
        
       // data.stateId = data.stateId
        //data.cityId = data.cityId
       if data.stateId == nil {
                 data.stateId = profile.state?.id
             }
             if data.cityId == nil {
                 data.cityId = profile.city?.id
                    }
        
        
        let service = "/patient/updateprofile"
        
        let headers : [String : String]? = nil
        
        //let params = profile.getJsonObject()
      
      //  print(params)
        var foodandAllergy : [Int] = []
        let foodAllergy = data.foodAndDrugAllergies
        foodAllergy?.forEach({ (food) in
            foodandAllergy.append(food.id!)
        })
        let params = ["cityId": data.cityId, "copay":data.copay,"dateOfBirth":data.dateOfBirth,"employer":data.employer,"firstName":data.firstName,"foodAndDrugAllergies":foodandAllergy,"gender":data.gender,"id":data.id,"insuranceCompany":data.insuranceCompany,"isPregnant":data.isPregnant,"lastName":data.lastName,"policyNumber":data.policyNumber,"primaryInsuredName":data.primaryInsuredName,"socialSecurityNumber":data.socialSecurityNumber,"stateId":data.stateId,"street":data.street,"zipCode":data.zipCode,"profilePicture":data.profilePicture] as [String : Any]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as? [String : AnyObject], headers: headers)
        
    }
    
    class func updateDoctorApi(patientProfile profile:Profile_d2) -> MWNetworkRequest {
     
        DataManager.shared.saveLoggedInDoctor(profile: profile)
        
           let data = profile
        
        if data.stateId == nil {
            data.stateId = profile.state?.id
        }
        if data.cityId == nil {
            data.cityId = profile.city?.id
               }
               data.city = nil
               data.state = nil
              // data.stateId = 1
               //data.cityId = 1
               data.id = nil
               data.email = nil
        
           let service = "/doctor/updateprofile"
           
           let headers : [String : String]? = nil
           
           let params = data.getJsonObject()
           print(params)
           
           return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as? [String : AnyObject], headers: headers)
           
       }
    //5
    class func getavailableappointmentdates(doctorId:Int,startDate:String,endDate:String) -> MWNetworkRequest {
        
        let service = "/appointment/getavailableappointmentdates"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId":doctorId,"startDate":startDate,"endDate":endDate] as [String:AnyObject]
        
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    
    class func getscheduledatescolor(doctorId:Int,startDate:String,endDate:String) -> MWNetworkRequest {
        
        let service = "/doctorschedule/getdatescolor"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId":doctorId,"startDate":startDate,"endDate":endDate] as [String:AnyObject]
        
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    
    
    //5
    class func getschedule(doctorId:Int,date:String) -> MWNetworkRequest {
        
        let service = "/appointment/getschedule"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId":doctorId,"date":date] as [String:AnyObject]
        
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    class func  getdoctorschedule(doctorId:Int,date:String) -> MWNetworkRequest {
        
        let service = "/doctorschedule/get"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId":doctorId,"date":date] as [String:AnyObject]
        
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    
    class func  Savedoctorschedule(doctorId: Int, defaultTemplateFlag: String,date:String,timeSlots: [timeSlot] ) -> MWNetworkRequest {
        
        let service = "/doctorschedule/save"
        
        let headers : [String : String]? = nil
        var slots = [[String:Any]]()
        
        timeSlots.forEach { (timeslot) in
            
            slots.append(timeslot.jsonData())
            
        }
        let params = ["doctorId":doctorId,"defaultTemplateFlag":defaultTemplateFlag,"date":date,"scheduleList":slots] as [String : AnyObject]
        
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    
    class func getdatescolor(doctorId:Int,startDate:String,endDate:String) -> MWNetworkRequest {
        
        let service = "/doctorschedule/getdatescolor"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId":doctorId,"startDate":startDate,"endDate":endDate] as [String:AnyObject]
        
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    
    class func getgroupdoctorschedule(groupId:Int,doctorId:Int,startDate:String,endDate:String
    ) -> MWNetworkRequest {
        
        let service = "/oncallgroup/getgroupdoctorschedule"
        let headers : [String : String]? = nil
        
        let params = ["groupId":groupId,"doctorId":doctorId,"startDate":startDate,"endDate":endDate] as [String:AnyObject]
        
        
        
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    
    class func getpatientappointments(patientId:Int
      ) -> MWNetworkRequest {
          
          let service = "/appointment/getall"
          let headers : [String : String]? = nil
          
          let params = ["patientId":patientId] as [String:AnyObject]
         print(params)
          
          return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
          
      }
    
    class func updategroupdocschedule(params:[String:Any]) -> MWNetworkRequest {
        
        let service = "/oncallgroup/updategroupdoctorschedule"
        
        let headers : [String : String]? = nil
        
        let params = params as [String:AnyObject]
        
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    class func insertAppointment(doctorId:Int,patientId:Int,appointmentSlotId:Int) -> MWNetworkRequest {
        
        let service = "/appointment/insert"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId":doctorId,"patientId":patientId,"appointmentSlotId":appointmentSlotId] as [String:AnyObject]
        
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    class func addAppointment(appointmentId:Int,temperature:String,respiratoryRate:String,heartRate:String,oxygenSaturation:String,commaSeperatedLabReportsURLs:String,symptomsList:[Int],medicalConditionList:[Int]) -> MWNetworkRequest {

        let service = "/appointment/reasonforvisit"

        let headers : [String : String]? = nil

        let params = ["appointmentId":appointmentId,"temperature":temperature,"respiratoryRate":respiratoryRate,"heartRate":heartRate,"oxygenSaturation":oxygenSaturation,"commaSeperatedLabReportsURLs":commaSeperatedLabReportsURLs,"symptomsList":symptomsList,"medicalConditionList":medicalConditionList] as [String:AnyObject]

        print(params)

        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)

    }
    
    
    //    class func imageuploadd(image:String,useCase:String,userType:String){
    //        var semaphore = DispatchSemaphore (value: 0)
    //        let  params = ["image":image.data(using: .utf8),
    //                         "useCase":useCase,
    //                         "usertype":userType] as [String:Any]
    //         let chosenImage = imageView.image
    //         let imageData = UIImageJPEGRepresentation(chosenImage!, 1)
    //         let uploadUrlString = "https://api.imgur.com/3/upload"
    //         let uploadUrl = URL(string: uploadUrlString)
    //
    //         var postRequest = URLRequest.init(url: uploadUrl!)
    //         postRequest.addValue("Client-ID XXXXXX", forHTTPHeaderField: "Authorization")
    //         postRequest.httpMethod = "POST"
    //         postRequest.httpBody = imageData
    //
    //         let uploadSession = URLSession.shared
    //         let executePostRequest = uploadSession.dataTask(with: postRequest as URLRequest) { (data, response, error) -> Void in
    //             if let response = response as? HTTPURLResponse
    //             {
    //                 print(response.statusCode)
    //             }
    //             if let data = data
    //             {
    //                 let json = String(data: data, encoding: String.Encoding.utf8)
    //                 print("Response data: \(String(describing: json))")
    //             }
    //         }
    //         executePostRequest.resume()
    //    }
    //
    //
    
    
    //    class func registerPatientApi(patientProfile profile:PatientProfile) -> MWNetworkRequest {
    //
    //           let service = "api/patient/profile/register"
    //
    //           let headers : [String : String]? = nil
    //
    //           let params = ["patientProfile": profile.getJsonObject()]
    //
    //           return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
    //
    //       }
    //5_2
    class func getstatesList(type:Int,state_id:Int) -> MWNetworkRequest {
        var service:String!
        var params:[String:AnyObject]!
        switch type {
        case 1:
            service = "/dropdown/getstateslist"
            
            params = nil
        default:
            service = "/dropdown/getcitieslist"
            
            params = ["stateId":state_id] as [String:AnyObject]
        }
        
        
        
        let headers : [String : String]? = nil
        
        
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    //5_2
    class func getsymptomsList(type:Int) -> MWNetworkRequest {
        var service:String!
        var params:[String:AnyObject]!
        switch type {
        case 2:
            service = "/dropdown/getsymptomslist"
            
            
            params = nil
        default:
            service = "/dropdown/getmedicalconditionslist"
            
            params = nil
        }
        
        let headers : [String : String]? = nil
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    
    
    //5_2
    class func getFoodAndAllergies() -> MWNetworkRequest {
        
        let service = "/dropdown/getfoodanddrugallergieslist"
        
        
        let headers : [String : String]? = nil
        
        
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: nil, headers: headers)
        
    }
    
    //upload image
    
    class func uploadImage(image:String,useCase:String,userType:String) -> MWNetworkRequest {
        
        
        let service = "/image/uploadimage"
        let  params = ["image":image,
                       "useCase":useCase,
                       "userType":userType] as [String:AnyObject]
        
        let headers : [String : String]? = nil
        
        print(params)
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    
    //generateOTP
    
    class func generateOTP(msisdn:String,userTypeId:Int) -> MWNetworkRequest {
        
        
        let service = "/otp/generate"
        let  params = ["msisdn":msisdn,
                       "userTypeId":userTypeId] as [String:AnyObject]
        
        let headers : [String : String]? = nil
        
        
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    
    //VerifyOTP
    
    class func verifyOTP(msisdn:String,otp:String) -> MWNetworkRequest {
        
        
        let service = "/otp/verify"
        let  params = ["msisdn":msisdn,
                       "otp":otp] as [String:AnyObject]
        
        let headers : [String : String]? = nil
        
        
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params, headers: headers)
        
    }
    
    //6
    class func updateDoctorProfile(username:String, doctorProfile profile:DoctorProfile ) -> MWNetworkRequest {
        
        let service = "api/doctor/profile/update"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorProfile": profile.getJsonObject(), "username":username] as [String : Any]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //7
    class func registerDoctorProfile(_ profile:Profile_d ) -> MWNetworkRequest {
        
        let service = "doctor/registerprofile"
        
        let headers : [String : String]? = nil
        profile.email = nil
        let params = profile.getJsonObject()
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as? [String : AnyObject], headers: headers)
        
    }
    //    //7
    //     class func registerDoctorProfile(_ profile:DoctorProfile ) -> MWNetworkRequest {
    //
    //         let service = "api/doctor/profile/register"
    //
    //         let headers : [String : String]? = nil
    //
    //         let params = ["doctorProfile": profile.getJsonObject()] as [String : Any]
    //
    //         return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
    //
    //     }
    
    
    //8
    class func getDoctorProfile(userName:String ) -> MWNetworkRequest {
        
        let service = "api/doctor/profile/get"
        
        let headers : [String : String]? = nil
        
        let params = ["username": userName] as [String : Any]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //9
    class func getAllDoctorList(userName:String ) -> MWNetworkRequest {
        
        let service = "api/doctor/profile/get/all"
        
        let headers : [String : String]? = nil
        
        let params = ["username": userName] as [String : Any]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    
    
    
    class func getAllDoctorList2(userName:Int ) -> MWNetworkRequest {
        
        let service = "/yourdoctor/getsuggesteddoctors"
        
        let headers : [String : String]? = nil
        
        let params = ["patientId": userName] as [String : Any]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
     class func getOnCallSuggestedDocs(userName:Int ) -> MWNetworkRequest {
         
         let service = "/oncallgroup/getsuggesteddctors"
         
         let headers : [String : String]? = nil
         
         let params = ["doctorId": userName] as [String : Any]
         
         return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
         
     }
    class func addDoctors(userName:Int,doctorIdsList:[Int] ) -> MWNetworkRequest {
        
        let service = "/yourdoctor/add"
        
        let headers : [String : String]? = nil
        
        let params = ["patientId": userName,"doctorIdsList":doctorIdsList] as [String : Any]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    
    class func getaddeddoctors(userName:Int ) -> MWNetworkRequest {
        
        let service = "/yourdoctor/getaddeddoctors"
        
        let headers : [String : String]? = nil
        
        let params = ["patientId": userName] as [String : Any]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    
    class func getUrgentaddeddoc(userName:Int ) -> MWNetworkRequest {
          
          let service = "/urgentcall/geturgentcalldoctor"
          
          let headers : [String : String]? = nil
          
          let params = ["doctorId": userName] as [String : Any]
          
          return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
          
      }
      
    
    //send request
    
    class func sendrequest(doctorId:Int,patientId:Int,patientName:String) -> MWNetworkRequest {
          
          let service = "/urgentcall/sendrequest"
          
          let headers : [String : String]? = nil
          
        let params = ["doctorId": doctorId,"patientId":patientId,"patientName":patientName] as [String : Any]
          
          return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
          
      }
    
    //get sent request
       
       class func getsentrequests(patientId:Int) -> MWNetworkRequest {
             
             let service = "/urgentcall/getrequests"
             
             let headers : [String : String]? = nil
             
           let params = ["patientId":patientId] as [String : Any]
             
             return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
             
         }
       
    
   
    //Fetch on call groups
    
    
    class func getoncallgroups(doctorId:Int ) -> MWNetworkRequest {
        
        let service = "/oncallgroup/getgroup"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId": doctorId] as [String : Any]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    
    //get patient sent request
          
          class func getsentpatientrequests(doctorId:Int) -> MWNetworkRequest {
                
                let service = "/urgentcallrequests/get"
                
                let headers : [String : String]? = nil
                
              let params = ["doctorId":doctorId] as [String : Any]
                
                return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
                
            }
    
    //get patient sent request
             
    class func acceptdeclinerequest(requestId:Int,status:String,patientId:Int,doctorId:Int,doctorName:String) -> MWNetworkRequest {
                   
                   let service = "/urgentcallrequests/update"
                   
                   let headers : [String : String]? = nil
                   
                 let params = ["requestId":requestId,"status":status,"patientId":patientId,"doctorName":doctorName] as [String : Any]
                   
                   return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
                   
               }
    
   
          
    
    class func addgroup(groupName:String,speciality:String,startTime:String,endTime:String,doctorId:Int,doctors:[[String:Any]]) -> MWNetworkRequest {
        
        let service = "/oncallgroup/addgroup"
        
        let headers : [String : String]? = nil
        
        let params = [ "groupName": groupName,
                       "speciality": speciality ,
                       "startTime": startTime,
                       "endTime": endTime,
                       "doctorId": doctorId,"doctors":doctors] as [String : Any]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    class func updategroup(groupName:String,speciality:String,startTime:String,endTime:String,groupId:Int) -> MWNetworkRequest {
        
        let service = "/oncallgroup/updategroup"
        
        let headers : [String : String]? = nil
        
        let params = [ "groupName": groupName,
                       "speciality": speciality ,
                       "startTime": startTime,
                       "endTime": endTime,
                       "groupId": groupId] as [String : Any]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    class func deletegroupDocs(groupId:Int,doctorId:Int) -> MWNetworkRequest {
        
        let service = "/oncallgroup/deletegroupdoctor"
        
        let headers : [String : String]? = nil
        
        let params = ["groupId": groupId, "doctorId":doctorId] as [String : Any]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    class func deleteOnCallGroup(groupId:Int) -> MWNetworkRequest {
        
        let service = "/oncallgroup/deletegroup"
        
        let headers : [String : String]? = nil
        
        let params = ["groupId":groupId,]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //10
    class func updateDoctorReviewRating(userName:String, rating:Int, review: Review ) -> MWNetworkRequest {
        
        let service = "api/doctor/profile/ratingreview/update"
        
        let headers : [String : String]? = nil
        
        let params = ["username": userName, "rating": rating, "review": review.getJsonObject()] as [String : Any]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //11
    class func getAvailableSlotsForDate(_ date:String, userName:String, docId: String ) -> MWNetworkRequest {
        
        let service = "api/appointment/availableslots"
        
        let headers : [String : String]? = nil
        
        let params = ["username": userName, "date": date, "docId": docId] as [String : Any]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //12
    class func getAppointmentList(id:Int, forDoctor:Bool) -> MWNetworkRequest {
        
        let service = forDoctor ? "api/appointment/doctor/getlist":"api/appointment/patient/getlist"
        
        let headers : [String : String]? = nil
        
        let params = forDoctor ? ["doctorId": id]:["patientId": id]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //13
    class func regiterAppointment(username:String, appointment: Appointment) -> MWNetworkRequest {
        
        let service = "api/appointment/insert"
        
        let headers : [String : String]? = nil
        
        let params = ["username": username, "appointment":appointment.getJsonObject()
            ] as [String : Any]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //14
    class func updateAppointmentStatusByDoctor(username:String, appointmentId: String, status:String) -> MWNetworkRequest {
        
        let service = "api/appointment/update/status"
        
        let headers : [String : String]? = nil
        
        let appointment = ["id": appointmentId, "status": status]
        
        let params = ["username": username, "appointment":appointment] as [String : Any]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //15
    class func updateAppointmentHistoryStatusAfterCall(username:String, appointmentId: String, historyStatus:String) -> MWNetworkRequest {
        
        let service = "api/appointment/update/historystatus"
        
        let headers : [String : String]? = nil
        
        let appointment = ["id": appointmentId, "historyStatus": historyStatus]
        
        let params = ["username": username, "appointment":appointment] as [String : Any]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //16
    class func AddEndCallDiagnosis(model:EndCallModel) -> MWNetworkRequest {
        
        let service = "api/appointment/update/disgnosis/prescription"
        
        let headers : [String : String]? = nil
        
        let params = ["appointment":model] as [String : Any]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //17
    class func getPrescriptionByAppointment(id:Int) -> MWNetworkRequest {
        
        let service = "api/appointment/disgnosis/get"
        
        let headers : [String : String]? = nil
        
        let params = ["appointmentId":id] as [String : Any]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //18
    class func getPharmacyList() -> MWNetworkRequest {
        
        let service = "api/general/pharmacy/list"
        
        let headers : [String : String]? = nil
        
        let params : [String : AnyObject]? = [:]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params, headers: headers)
        
    }
    
    //19
    class func getDropDownList() -> MWNetworkRequest {
        
        let service = "api/general/dropdown/list"
        
        let headers : [String : String]? = nil
        
        let params : [String : AnyObject]? = nil
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params, isPostRequest:false, headers: headers)
        
    }
    
    //20
    class func getTokensForCall(sender:String, receiver:String) -> MWNetworkRequest {
        
        let service = "api/twilio/getvideotoken"
        
        let headers : [String : String]? = nil
        
        let params = ["senderIdentity":sender, "receiverIdentity":receiver] 
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //21
    class func getOnCallDoctors(for doctorId:String, patient userName:String) -> MWNetworkRequest {
        
        let service = "api/doctor/profile/get/myoncalldoctors"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId":doctorId, "username":userName]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //22
    class func addOnCallDoctor(for doctorId:String, onCallDocId:String) -> MWNetworkRequest {
        
        let service = "api/doctor/profile/add/oncalldoctors"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId":doctorId, "onCallDoctorId":onCallDocId]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    class func onCallGroupDoctors(groupId:Int) -> MWNetworkRequest {
        
        let service = "oncallgroup/getgroupdoctors"
        
        let headers : [String : String]? = nil
        
        let params = ["groupId":groupId]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    class func addonCallGroupDoctors(groupId:Int,doctorId:Int,schedulesDates:[[String:Any]]) -> MWNetworkRequest {
           
           let service = "oncallgroup/addgroupdoctor"
           
           let headers : [String : String]? = nil
           
        let doctor = schedulesDates.first! as [String:Any]
        
        let params = ["groupId":groupId,"doctor":doctor] as [String : Any]
           
           return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
           
       }
    
    //23
    class func deleteOnCallDoctor(for doctorId:String, onCallDocId:String) -> MWNetworkRequest {
        
        let service = "api/doctor/profile/delete/oncalldoctors"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId":doctorId, "onCallDoctorId":onCallDocId]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    class func deleteOnCallDoctor2(for patient_id:Int, onCallDocId:Int) -> MWNetworkRequest {
        
        let service = "/yourdoctor/delete"
        
        let headers : [String : String]? = nil
        
        let params = ["patientId":patient_id, "doctorId":onCallDocId]
        
        return MWRequestFactory.shared.createRequest_New(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    //24
    class func updateTimingFor(for doctorId:String, onCallDocId:String, timings:[Timings]) -> MWNetworkRequest {
        
        let service = "api/doctor/profile/update/oncalldoctors"
        
        let headers : [String : String]? = nil
        
        let params = ["doctorId":doctorId, "onCallDoctorId":onCallDocId, "timings":timings.getJsonObject() ]
        
        return MWRequestFactory.shared.createRequest(endPoint: service, parameters: params as [String : AnyObject], headers: headers)
        
    }
    
    
    
}

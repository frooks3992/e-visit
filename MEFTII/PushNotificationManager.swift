//
//  PushNotificationManager.swift
//  FindHelp
//
//  Created by Mian Waqas Umar on 29/10/2019.
//  Copyright © 2019 Waqas Creations. All rights reserved.
//

import Foundation
import Firebase
import FirebaseMessaging
import UIKit
import UserNotifications


class PushNotificationManager: NSObject, MessagingDelegate {
    
    static let shared = PushNotificationManager()
    
    private(set) var userName : String = ""

    private override init() {
        
        super.init()
    }
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: { [weak self] granted, error in
                    
                    print("Permission granted: \(granted)")
                    guard granted else { return }
                    self?.getNotificationSettings()
            })
            
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            
            UIApplication.shared.registerUserNotificationSettings(settings)
            
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        
    }
    
    func getNotificationSettings() {
      UNUserNotificationCenter.current().getNotificationSettings { settings in
        print("Notification settings: \(settings)")
        
        guard settings.authorizationStatus == .authorized else { return }
        DispatchQueue.main.async {
          UIApplication.shared.registerForRemoteNotifications()
        }
        
      }
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        subscribeToMyTopic()
    }
    
    
    func subscribeToMyTopic() {
        
        if let _ = DataManager.shared.userName {
            var uName = (DataManager.shared.userName ?? "").lowercased()
            uName = uName.components(separatedBy: CharacterSet.letters.inverted).joined()
            self.userName = uName
            Messaging.messaging().subscribe(toTopic: userName) { error in
              print("Subscribed to \"\(self.userName)\" topic")
            }
            
        }

    }
    
    
    func sendPushNotification(to topic: String, body:String, isForCall:Bool, token:String) {
        let urlString = "https://fcm.googleapis.com/fcm/send"
        let url = NSURL(string: urlString)!
        
        var senderName = ""
        
        var uName = (topic).lowercased()
        uName = uName.components(separatedBy: CharacterSet.letters.inverted).joined()
        
        
        if DataManager.shared.currentRole == "doctor" {
            senderName = DataManager.shared.currentDoctor?.completeName ?? ""
        }
        else {
            senderName = DataManager.shared.currentPatient?.completeName ?? ""
        }
        
        var dataDict = ["user_name" : userName, "text" : body, "priority": "high", "sender_name" : senderName, "isForCall": "NO"]
        
        if isForCall {
            dataDict = ["user_name" : userName,
                        "text" : body,
                        "priority": "high",
                        "sender_name" : senderName,
                        "isForCall": "YES",
                        "room_name": body,
                        "recv_token": token]
        }
        
        let paramString: [String : Any] = ["to" : "/topics/\(uName)",
            "notification" : ["title" : senderName, "body" : (isForCall ? "calling you. Tap to view.":body)],
                                           "data" : dataDict
        ]
        
        let request = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "POST"
        request.httpBody = try? JSONSerialization.data(withJSONObject:paramString, options: [.prettyPrinted])
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAAZg_y_Ts:APA91bGOFab7RCDycw_tXelLHTd6T51JMbP0e-bPR6xqrtnld3ZEm1huhosUyD9CemyqOConNPzvFob7txQF0yau01vJsp8p9Bsdo-FG-vbvaTT9np1CZj0oZFtkL6p2O1b3CjsEjCzu", forHTTPHeaderField: "Authorization")
        let task =  URLSession.shared.dataTask(with: request as URLRequest)  { (data, response, error) in
            do {
                if let jsonData = data {
                    if let jsonDataDict  = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.allowFragments) as? [String: AnyObject] {
                        NSLog("Received data:\n\(jsonDataDict))")
                    }
                }
            } catch let err as NSError {
                print(err.debugDescription)
            }
        }
        task.resume()
    }
    
}


extension PushNotificationManager : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        

        // Print full message.
        print(userInfo)
        
        let isForCall = userInfo["isForCall"] as? String
        let userName = userInfo["user_name"] as? String
        let senderName = userInfo["sender_name"] as? String
        let text = userInfo["text"] as? String
        
        if (isForCall == "YES") {
            
            let room_name = userInfo["room_name"] as? String
            let recv_token = userInfo["recv_token"] as? String

            if let viewController = UIApplication.shared.delegate?.window??.rootViewController as? MFNavigationController {
                if let room = room_name, let tkn = recv_token {
                    viewController.presentCallController(forIncoming: true, roomName: room, token: tkn)
                }
            }
            completionHandler([.sound, .alert])
        }
        else {
            
            let msg = MWChatMessage()
            msg.text = text ?? ""
            msg.userId = ""
            msg.senderUserName = userName ?? ""
            msg.userDisplayName = senderName ?? ""
            msg.isReceivedViaPush = true
            
            MWChatHandler.shared.messageReceived(msg)
            
            completionHandler([.sound])
        }
        
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo

        
        // Print full message.
        print(userInfo)
        
        let isForCall = userInfo["isForCall"] as? String
        let userName = userInfo["user_name"] as? String
        let senderName = userInfo["sender_name"] as? String
        let text = userInfo["text"] as? String
        
        
        if isForCall == "YES" {
            let room_name = userInfo["room_name"] as? String
            let recv_token = userInfo["recv_token"] as? String
            
            if let viewController = UIApplication.shared.delegate?.window??.rootViewController as? MFNavigationController {
                if let room = room_name, let tkn = recv_token {
                    viewController.presentCallController(forIncoming: true, roomName: room, token: tkn)
                }
            }
            
        }
        else {
            let msg = MWChatMessage()
            msg.text = text ?? ""
            msg.userId = ""
            msg.senderUserName = userName ?? ""
            msg.userDisplayName = senderName ?? ""
            msg.isReceivedViaPush = true
            MWChatHandler.shared.messageReceived(msg)
        }
        
        completionHandler()
    }
}

//
//  City.swift
//  MEFTII
//
//  Created by Farhan on 15/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import Foundation

class Profile_p : Codable {
    var id : Int?
    var email : String?
    var password : String?
    var gender : String?
    var profilePicture : String?
    var dateOfBirth : String?
    var street : String?
    var cityId : Int?
    var stateId : Int?
    var zipCode : String?
    var copay : String?
    var socialSecurityNumber : String?
    var phone : String?
    var employer : String?
    var primaryInsuredName : String?
    var policyNumber : String?
    var insuranceCompany : String?
    var isPregnant : String?
    var firstName : String?
    var lastName : String?
    var foodAndDrugAllergies : [Int]?
    var isFingerPrintOn : Bool?
    var completeName : String {
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
    
    init() {
        
    }
    
    func replica() -> Profile_p {
        let newObj = Profile_p()
        newObj.id = self.id
        newObj.email = self.email
        newObj.password = self.password
        newObj.gender = self.gender
        newObj.profilePicture = self.profilePicture
        newObj.dateOfBirth = self.dateOfBirth
        newObj.street = self.street
        newObj.cityId = self.cityId
        newObj.stateId = self.stateId
        newObj.zipCode = self.zipCode
        
        newObj.copay = self.copay
        newObj.socialSecurityNumber = self.socialSecurityNumber
        newObj.phone = self.phone
        newObj.employer = self.employer
        newObj.primaryInsuredName = self.primaryInsuredName
        newObj.policyNumber = self.policyNumber
        newObj.insuranceCompany = self.insuranceCompany
        newObj.isPregnant = self.isPregnant
 
        newObj.firstName = self.firstName
        newObj.lastName = self.lastName
        newObj.foodAndDrugAllergies = self.foodAndDrugAllergies
        
        
        
        
        //newObj.user_id = self.user_id
      
   
        return newObj
    }

}


class Profile_p2 : Codable {
    var id : Int?
    var email : String?
    var password : String?
    var gender : String?
    var profilePicture : String?
    var dateOfBirth : String?
    var street : String?
    var city : city?
    var state : city?
    var zipCode : String?
    var copay : String?
    var socialSecurityNumber : String?
    var phone : String?
    var employer : String?
    var primaryInsuredName : String?
    var policyNumber : String?
    var insuranceCompany : String?
    var isPregnant : String?
    var firstName : String?
    var lastName : String?
    var foodAndDrugAllergies : [foodDrugAllergies]?
    var cityId : Int?
    var stateId : Int?
    var isFingerPrintOn : Bool?
    var completeName : String {
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
    
    init() {
        
    }
    
    func replica() -> Profile_p2 {
        let newObj = Profile_p2()
        newObj.id = self.id
        newObj.email = self.email
        newObj.password = self.password
        newObj.gender = self.gender
        newObj.profilePicture = self.profilePicture
        newObj.dateOfBirth = self.dateOfBirth
        newObj.street = self.street
        newObj.city = self.city
        newObj.state = self.state
        newObj.zipCode = self.zipCode
        newObj.cityId = self.cityId ?? DataManager.shared.currentPatient?.cityId
        newObj.stateId = self.stateId ?? DataManager.shared.currentPatient?.stateId
        
        newObj.copay = self.copay
        newObj.socialSecurityNumber = self.socialSecurityNumber
        newObj.phone = self.phone
        newObj.employer = self.employer
        newObj.primaryInsuredName = self.primaryInsuredName
        newObj.policyNumber = self.policyNumber
        newObj.insuranceCompany = self.insuranceCompany
        newObj.isPregnant = self.isPregnant
        
        // newObj.role = self.role
        // newObj.status = self.status
        newObj.firstName = self.firstName
        newObj.lastName = self.lastName
        newObj.foodAndDrugAllergies = self.foodAndDrugAllergies ?? []
        
        
        
        
        //newObj.user_id = self.user_id
      
   
        return newObj
    }

}


struct foodallergy {
 var id : Int?
    var name : String?
    
}

class foodDrugAllergies : Codable {
 var id : Int?
 var name : String?

    init(id:Int,name:String) {
     
        self.id = id
        self.name = name
 }
 
// func replica() -> foodDrugAllergies {
//     let newObj = foodDrugAllergies()
//     newObj.id = self.id
//     newObj.name = self.name
//  
//     return newObj
// }
}

class city : Codable {
 var id : Int?
 var name : String?

 init() {
     
 }
 
 func replica() -> city {
     let newObj = city()
     newObj.id = self.id
     newObj.name = self.name
  
     return newObj
 }
}
class state : Codable {
 var id : Int?
 var name : String?

 init() {
     
 }
 
 func replica() -> state {
     let newObj = state()
     newObj.id = self.id
     newObj.name = self.name
  
     return newObj
 }
}

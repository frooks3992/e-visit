//
//  City.swift
//  MEFTII
//
//  Created by Farhan on 15/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

    import Foundation

    class City {
        
        var id: Int
        var name: String

        init(id: Int, name: String) {
            self.id = id
            self.name = name
         
            
        }
        
        init(json: [String:Any]) {
            self.id = json["id"] as? Int ?? 0
            self.name = json["name"] as? String ?? ""
         
        }
        
    }


class Condition {
      
      var id: Int
      var name: String
    var isEmergency:Bool

    init(id: Int, name: String,isEmergency:Bool) {
          self.id = id
          self.name = name
        self.isEmergency = isEmergency
       
          
      }
      
      init(json: [String:Any]) {
          self.id = json["id"] as? Int ?? 0
          self.name = json["name"] as? String ?? ""
          self.isEmergency = json["isEmergency"] as? Bool ?? false
       
      }
      
  }

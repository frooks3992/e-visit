//
//  suggestedDoctors.swift
//  MEFTII
//
//  Created by Farhan on 09/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import Foundation

class suggestedDoctors : Codable {
    var doctorId : Int?
    var firstName: String?
    var speciality : String?
    var profilePicture : String?
   
    
    
    
    func replica() -> suggestedDoctors {
        let newObj = suggestedDoctors()
        newObj.doctorId = self.doctorId
        newObj.firstName = self.firstName
        newObj.speciality = self.speciality
        newObj.profilePicture = self.profilePicture
   
       return newObj
    }
    
}
extension suggestedDoctors : Hashable {
    
    static func == (lhs: suggestedDoctors, rhs: suggestedDoctors) -> Bool {
        return lhs.doctorId == rhs.doctorId
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(doctorId)
    }
    
}

//
//  timeSlot.swift
//  MEFTII
//
//  Created by Farhan on 24/04/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import Foundation
import UIKit
 class timeSlot {
     
     var id: Int
     var status: String
     var timeSlot:String
    var isSelected:Bool

    init(id: Int, status: String,timeSlot:String) {
         self.id = id
         self.status = status
         self.timeSlot = timeSlot
          self.isSelected = false
      
         
     }
     
     init(json: [String:Any]) {
         self.id = json["id"] as? Int ?? 0
         self.status = json["status"] as? String ?? ""
         self.timeSlot = json["timeSlot"] as? String ?? ""
        self.isSelected = false
      
     }
    
    func jsonData() -> [String:Any] {
        
        
        return [ "id" : id,
                 "status" : status,
                 "timeSlot":timeSlot
        ]
        
    }
     
 }

class DaysColorsList {
    
    var date: String
    var color: UIColor

   init(date: String, color: UIColor) {
        self.date = date
        self.color = color
  
     
        
    }
    
    init(json: [String:Any]) {
        self.date = json["date"] as? String ?? ""
     
           let clr = json["color"] as? String ?? ""
        
        switch clr {
        case "green":
            self.color = UIColor.MFGreen
        default:
            self.color = UIColor.MFRed
        }
        
        
          
    }
    
}


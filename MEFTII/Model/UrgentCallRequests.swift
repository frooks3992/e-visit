//
//  UrgentCallRequests.swift
//  MEFTII
//
//  Created by Farhan on 13/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import Foundation

class UrgentCallRequests{
    
    var requestId: Int
    var doctorId: Int
    var patientId: Int
    var doctorName:String
    var status:String
    var patientName: String
    var patientProfilePic: String
    var date:String
    var time:String
    
    init(requestId: Int, doctorId: Int,patientId:Int,doctorName:String,status:String,patientName: String,patientProfilePic:String,date:String,time:String) {
        self.requestId = requestId
        self.doctorId = doctorId
        self.patientId = patientId
        self.doctorName = doctorName
        self.status = status
        self.patientName = patientName
        self.patientProfilePic = patientName
        self.date = date
        self.time = time
    }
    
    init(json: [String:Any]) {
        self.requestId = json["id"] as? Int ?? json["rquestId"] as? Int ?? 0
        
        self.doctorId = json["doctorId"] as? Int ?? 0
        self.patientId = json["patientId"] as? Int ?? 0
        self.doctorName = json["doctorName"] as? String ?? ""
        self.status = json["status"] as? String ?? ""
        self.patientProfilePic = json["patientProfilePic"] as? String ?? ""
        self.patientName = json["patientName"] as? String ?? ""
        self.date = json["doctorName"] as? String ?? ""
        self.time = json["status"] as? String ?? ""
        
        
    }
}


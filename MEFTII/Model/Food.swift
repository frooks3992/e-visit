//
//  City.swift
//  MEFTII
//
//  Created by Farhan on 15/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

    import Foundation

    class Food {
        
        var id: Int
        var name: String

        init(id: Int, name: String) {
            self.id = id
            self.name = name
         
            
        }
        
        init(json: [String:Any]) {
            self.id = json["id"] as? Int ?? 0
            self.name = json["name"] as? String ?? ""
         
        }
        
    }



/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation


class DoctorProfile : Codable {
    
    var id : String?
    var username : String?
    var password : String?
    var role : String?
    var status : String?
    var firstname : String?
    var lastname : String?
    var email : String?
    var dea : String?
    var stl : String?
    var npi : String?
    var hospital : String?
    var address : Address?
    var phone : String?
    var gender : String?
    var unscheduledCalls : String?
    var pricePerSession : String?
    var timings : [Timings]?
    var user_id : Int?
    var rating : Int?
    var ratingCount : Int?
    var reviews : [Review]?
    var latitude : String?
    var longitude : String?
    var profilePicture : String?
    var isFingerPrintOn : Bool?
    var addressTwo: String?
    var dateOfBirth: String?
    var speciality: String?
    var ssn: String?
    var differentTimings: String?
    
    var completeName : String {
        return "\(firstname ?? "") \(lastname ?? "")"
    }
    
    var avgRating : Double {
        let rat = Double(rating ?? 0)
        var count = Double(ratingCount ?? 0)
        count = count < 1 ? 1:count
        return rat/count
    }
    
    init() {
        
    }
    
    func replica() -> DoctorProfile {
        let newObj = DoctorProfile()
        newObj.id = self.id
        newObj.username = self.username
        newObj.password = self.password
        newObj.role = self.role
        newObj.status = self.status
        newObj.firstname = self.firstname
        newObj.lastname = self.lastname
        newObj.email = self.email
        newObj.user_id = self.user_id
        newObj.email = self.email
        newObj.dea = self.dea
        newObj.stl = self.stl
        newObj.npi = self.npi
        newObj.hospital = self.hospital
        newObj.address = self.address?.replica()
        newObj.phone = self.phone
        newObj.gender = self.gender
        newObj.unscheduledCalls = self.unscheduledCalls
        newObj.pricePerSession = self.pricePerSession
        newObj.rating = self.rating
        newObj.ratingCount = self.ratingCount
        newObj.latitude = self.latitude
        newObj.longitude = self.longitude
        newObj.profilePicture = self.profilePicture
        newObj.dateOfBirth = self.dateOfBirth
        newObj.speciality = self.speciality
        newObj.ssn = self.ssn
        newObj.differentTimings = self.differentTimings
        if let timgns = self.timings {
            newObj.timings = []
            for item in timgns {
                newObj.timings?.append(item.replica())
            }
        }
        newObj.reviews = self.reviews
        return newObj
    }

}

extension DoctorProfile : Hashable {
    
    static func == (lhs: DoctorProfile, rhs: DoctorProfile) -> Bool {
        return lhs.username == rhs.username && lhs.user_id == rhs.user_id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(username)
        hasher.combine(user_id)
    }
    
}



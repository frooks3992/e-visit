//
//  State.swift
//  MEFTII
//
//  Created by Farhan on 15/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import Foundation

struct State : Codable {
    let id : Int?
    let name : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }

}

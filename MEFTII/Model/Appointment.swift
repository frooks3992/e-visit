/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

class Appointment : Codable {
    
	var from : String?
	var patientId : Int?
	var doctorId : Int?
	var symptoms : ConditionSymptom?
	var condition : ConditionSymptom?
	var temperature : String?
	var heartRate : String?
	var oxygenSaturation : String?
	var respirationRate : String?
	var labReports : [LabReports]?
    var pictureReports : [LabReports]?
	var status : String?
	var historyStatus : String?
    
    var doctorProfile : DoctorProfile?
    var patientProfile : PatientProfile?
    var diagnosis : String?
    var subjective : String?
    var objective : String?
    var assessment : String?
    var plan : String?
    var prescriptions : String?
    
    var doctorName : String? {
        get {
            return doctorProfile?.completeName
        }
    }
    var doctorImage : String? {
        get {
            return doctorProfile?.profilePicture
        }
    }
    
    init() { }
    
    
}

struct LabReports : Codable {
    
    let imageData : String?
    let ext : String?
    
    enum CodingKeys: String, CodingKey {
        
        case imageData = "imageData"
        case ext = "ext"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        imageData = try values.decodeIfPresent(String.self, forKey: .imageData)
        ext = try values.decodeIfPresent(String.self, forKey: .ext)
    }
    
    init(data:String, exten:String) {
        imageData = data
        ext = exten
    }
    
}

struct ConditionSymptom : Codable {
    
    let id : Int?
    let name : String?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
    
    init(id: Int?, name: String?) {
        self.id = id
        self.name = name
    }
    
}


class appoitments : Codable {
    var doctorId : Int?
    var firstName : String?
    var lastName : String?
    var profilePicture : String?
    var timeSlot : String?
    var date : String?
    var status : String?
    var completeName : String {
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
    
    init() {
        
    }
    
    func replica() -> appoitments {
        let newObj = appoitments()
        newObj.doctorId = self.doctorId
        newObj.firstName = self.firstName
        newObj.lastName = self.lastName
        newObj.profilePicture = self.profilePicture
        newObj.timeSlot = self.timeSlot
        newObj.date = self.date
        newObj.status = self.status

           return newObj
    }

}

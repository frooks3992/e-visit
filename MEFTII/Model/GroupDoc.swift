//
//  GroupDoc.swift
//  MEFTII
//
//  Created by Farhan on 05/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import Foundation
class GroupDoc {
    
    var doctorId: Int
    var firstName: String
    var lastName:String
    var speciality:String
    var profilePicture :String

    init(doctorId: Int,firstName: String,lastName:String,speciality:String,profilePicture:String) {
        
        self.doctorId = doctorId
        self.firstName = firstName
        self.lastName = lastName
        self.speciality = speciality
        self.profilePicture = profilePicture
        
       
    }
    
    init(json: [String:Any]) {
        self.doctorId = json["doctorId"] as? Int ?? 0
        self.firstName = json["firstName"] as? String ?? ""
        self.speciality = json["speciality"] as? String ?? ""
        self.lastName = json["lastName"] as? String ?? ""
        self.profilePicture = json["profilePicture"] as? String ?? ""
     
        
    }
    
 
}
extension GroupDoc : Hashable {
     
     static func == (lhs: GroupDoc, rhs: GroupDoc) -> Bool {
         return lhs.doctorId == rhs.doctorId
     }
     
     func hash(into hasher: inout Hasher) {
         hasher.combine(doctorId)
     }
     
 }

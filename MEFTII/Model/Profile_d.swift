//
//  City.swift
//  MEFTII
//
//  Created by Farhan on 15/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import Foundation

class Profile_d : Codable {
    
    var email : String?
    var password : String?
    var gender : String?
    var profilePicture : String?
    var dateOfBirth : String?
    var street : String?
    var cityId : Int?
    var stateId : Int?
    var zipCode : String?
    var speciality : String?
    var socialSecurityNumber : String?
    var phone : String?
    var deaNumber : String?
    var nationalProviderIdentifierNumber : String?
    var hospitalAffiliation : String?
    var stateLicenseNumber : String?
    //var isPregnant : String?
    var firstName : String?
    var lastName : String?
    //var foodAndDrugAllergies : [Int]?
    var isFingerPrintOn : Bool?
    var completeName : String {
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
    
    init() {
        
    }
    
    func replica() -> Profile_d {
        let newObj = Profile_d()
        //newObj.username = self.username
        newObj.email = self.email
        newObj.password = self.password
        newObj.gender = self.gender
        newObj.profilePicture = self.profilePicture
        newObj.dateOfBirth = self.dateOfBirth
        newObj.street = self.street
        newObj.cityId = self.cityId
        newObj.stateId = self.stateId
        newObj.zipCode = self.zipCode
        
        newObj.speciality = self.speciality
        newObj.socialSecurityNumber = self.socialSecurityNumber
        newObj.phone = self.phone
        newObj.deaNumber = self.deaNumber
        newObj.nationalProviderIdentifierNumber = self.nationalProviderIdentifierNumber
        newObj.hospitalAffiliation = self.hospitalAffiliation
        newObj.stateLicenseNumber = self.stateLicenseNumber
        // newObj.isPregnant = self.isPregnant
        
        // newObj.role = self.role
        // newObj.status = self.status
        newObj.firstName = self.firstName
        newObj.lastName = self.lastName
        //  newObj.foodAndDrugAllergies = self.foodAndDrugAllergies
        
        
        
        
        //newObj.user_id = self.user_id
        
        
        return newObj
    }
    
}

class Profile_d2 : Codable {
    var id : Int?
    var doctorId:Int?
    var email : String?
    var password : String?
    var gender : String?
    var profilePicture : String?
    var dateOfBirth : String?
    var street : String?
    var city : city?
      var state : city?
    var cityId : Int?
    var stateId : Int?
    var zipCode : String?
    var speciality : String?
    var socialSecurityNumber : String?
    var phone : String?
  
    var deaNumber : String?
    var nationalProviderIdentifierNumber : String?
    var hospitalAffiliation : String?
    var stateLicenseNumber : String?
    //var isPregnant : String?
    var firstName : String?
    var lastName : String?
    var isSelected :String?
    var urgentCallFlag: String?
   // var foodAndDrugAllergies : [Int]?
    var isFingerPrintOn : Bool?
    var completeName : String {
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
    
    init() {
        
    }
    
    func replica() -> Profile_d2 {
        let newObj = Profile_d2()
        newObj.id = self.id
        newObj.doctorId = self.doctorId
        newObj.email = self.email
        newObj.password = self.password
        newObj.gender = self.gender
        newObj.profilePicture = self.profilePicture
        newObj.dateOfBirth = self.dateOfBirth
        newObj.street = self.street
       
        newObj.zipCode = self.zipCode
        newObj.state = self.state
        newObj.city = self.city
        newObj.speciality = self.speciality
        newObj.cityId = self.city?.id
        newObj.stateId = self.state?.id
        newObj.socialSecurityNumber = self.socialSecurityNumber
        newObj.urgentCallFlag = self.urgentCallFlag ?? "0"
        newObj.deaNumber = self.deaNumber
        newObj.nationalProviderIdentifierNumber = self.nationalProviderIdentifierNumber
        newObj.hospitalAffiliation = self.hospitalAffiliation
        newObj.stateLicenseNumber = self.stateLicenseNumber
        // newObj.isPregnant = self.isPregnant
        
        newObj.phone = self.phone
        // newObj.status = self.status
        newObj.firstName = self.firstName
        newObj.lastName = self.lastName
        //  newObj.foodAndDrugAllergies = self.foodAndDrugAllergies
        
        
        
        
        //newObj.user_id = self.user_id
        
        
        return newObj
    }
    
}
extension Profile_d2 : Hashable {
    
    static func == (lhs: Profile_d2, rhs: Profile_d2) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
}

class Profile_d3 : Codable {
    var id : Int?
    var doctorId:Int?
    var email : String?
    var password : String?
    var gender : String?
    var profilePicture : String?
    var dateOfBirth : String?
    var street : String?
    var cityId : Int?
    var stateId : Int?
    var zipCode : String?
    var speciality : String?
    var socialSecurityNumber : String?
    var phone : String?
    var city : String?
    var state : String?
    var deaNumber : String?
    var nationalProviderIdentifierNumber : String?
    var hospitalAffiliation : String?
    var stateLicenseNumber : String?
    //var isPregnant : String?
    var firstName : String?
    var lastName : String?
    var isSelected :String?
    var urgentCallFlag: String?
   // var foodAndDrugAllergies : [Int]?
    var isFingerPrintOn : Bool?
    var completeName : String {
        return "\(firstName ?? "") \(lastName ?? "")"
    }
    
    
    init() {
        
    }
    
    func replica() -> Profile_d3 {
        let newObj = Profile_d3()
        newObj.id = self.id
        newObj.doctorId = self.doctorId
        newObj.email = self.email
        newObj.password = self.password
        newObj.gender = self.gender
        newObj.profilePicture = self.profilePicture
        newObj.dateOfBirth = self.dateOfBirth
        newObj.street = self.street
        newObj.cityId = self.cityId
        newObj.stateId = self.stateId
        newObj.zipCode = self.zipCode
        newObj.state = self.state
        newObj.city = self.city
        newObj.speciality = self.speciality
        newObj.socialSecurityNumber = self.socialSecurityNumber
        newObj.urgentCallFlag = self.urgentCallFlag ?? "0"
        newObj.deaNumber = self.deaNumber
        newObj.nationalProviderIdentifierNumber = self.nationalProviderIdentifierNumber
        newObj.hospitalAffiliation = self.hospitalAffiliation
        newObj.stateLicenseNumber = self.stateLicenseNumber
        // newObj.isPregnant = self.isPregnant
        
        newObj.phone = self.phone
        // newObj.status = self.status
        newObj.firstName = self.firstName
        newObj.lastName = self.lastName
        //  newObj.foodAndDrugAllergies = self.foodAndDrugAllergies
        
        
        
        
        //newObj.user_id = self.user_id
        
        
        return newObj
    }
    
}
extension Profile_d3 : Hashable {
    
    static func == (lhs: Profile_d3, rhs: Profile_d3) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
}

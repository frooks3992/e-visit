//
//  Group.swift
//  MEFTII
//
//  Created by Farhan on 05/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import Foundation

class Group {
    
    var groupId: Int
    var groupName: String
    var speciality:String
    var startTime:String
    var endTime:String
    var doctorId: Int
    var timing:String!
    
    
    init(groupId: Int,groupName: String,speciality:String,startTime:String,endTime:String, doctorId: Int,timing:String) {
        
        self.groupId = groupId
        self.groupName = groupName
        self.speciality = speciality
        self.startTime = startTime
        self.endTime = endTime
        self.doctorId = doctorId
        self.timing = timing
        
       
    }
    
    init(json: [String:Any]) {
        self.groupId = json["groupId"] as? Int ?? 0
        self.groupName = json["groupName"] as? String ?? ""
        self.speciality = json["speciality"] as? String ?? ""
        self.startTime = json["startTime"] as? String ?? ""
        self.endTime = json["endTime"] as? String ?? ""
        self.doctorId = json["doctorId"] as? Int ?? 0
       self.timing = startTime+" - "+endTime
     
        
    }
    
 
}
extension Group : Hashable {
     
     static func == (lhs: Group, rhs: Group) -> Bool {
         return lhs.groupId == rhs.groupId
     }
     
     func hash(into hasher: inout Hasher) {
         hasher.combine(groupId)
     }
     
 }

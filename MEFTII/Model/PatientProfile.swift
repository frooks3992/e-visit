/* 
Copyright (c) 2019 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

class PatientProfile : Codable {
    
	var id : String?
	var username : String?
	var password : String?
	var role : String?
	var status : String?
	var firstname : String?
	var lastname : String?
	var email : String?
	var dob : String?
	var gender : String?
	var allergy : String?
	var employer : String?
	var insuranceName : String?
	var insuranceCompany : String?
	var policyNumber : String?
	var address : Address?
    var phone : String?
	var user_id : Int?
    var profilePicture : String?
    var isFingerPrintOn : Bool?
    var addressTwo: String?
    var ssn: String?
    var coPay: String?
    
    var completeName : String {
        return "\(firstname ?? "") \(lastname ?? "")"
    }
    
    
    init() {
        
    }
    
    func replica() -> PatientProfile {
        let newObj = PatientProfile()
        newObj.id = self.id
        newObj.username = self.username
        newObj.password = self.password
        newObj.role = self.role
        newObj.status = self.status
        newObj.firstname = self.firstname
        newObj.lastname = self.lastname
        newObj.email = self.email
        newObj.dob = self.dob
        newObj.gender = self.gender
        newObj.allergy = self.allergy
        newObj.employer = self.employer
        newObj.insuranceName = self.insuranceName
        newObj.insuranceCompany = self.insuranceCompany
        newObj.policyNumber = self.policyNumber
        newObj.address = self.address?.replica()
        newObj.user_id = self.user_id
        newObj.phone = self.phone
        newObj.profilePicture = self.profilePicture
        newObj.ssn = self.ssn
        newObj.coPay = self.coPay
        newObj.ssn = self.ssn

        return newObj
    }

}

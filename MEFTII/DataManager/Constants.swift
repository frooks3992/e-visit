//
//  Constants.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 21/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit
final class Constants {
    
    private(set) static var cities : [ConditionSymptom] = []
    private(set) static var states : [ConditionSymptom] = []
    private(set) static var dosages : [ConditionSymptom] = []
    private(set) static var genders : [ConditionSymptom] = []
    private(set) static var symptoms : [ConditionSymptom] = []
    private(set) static var conditions : [ConditionSymptom] = []
    private(set) static var allergies : [ConditionSymptom] = []
    
    static var selctedFileNameArray : [String] = []
    static var DoctorImage : UIImage = UIImage()
    static var PatientImage : UIImage = UIImage()
    static var selctedFileImagesArray : [UIImage] = []
    static var collectionViewsHeghtForfile :CGFloat = 125
    static let kAPIFormat = "yyyyMMdd"
    static let kHomeDateFormate = "yyyy-MM-dd HH:mm:ss"
     static var filterArray: [String] = ["New York", "Los Angeles", "Chicago", "Houston", "Phoenix", "Philadelphia", "San Antonio"]
    static func setDropDownLists(lists:DropDownLists) {
        
        if let arr = lists.cities {
            self.cities = arr
        }
        
        if let arr = lists.states {
            self.states = arr
        }
        
        if let arr = lists.dosages {
            self.dosages = arr
        }
        
        if let arr = lists.genders {
            self.genders = arr
        }
        
        if let arr = lists.symptoms {
            self.symptoms = arr
        }
        
        if let arr = lists.conditions {
            self.conditions = arr
        }
        
        if let arr = lists.allergies {
            self.allergies = arr
        }
        
    }
    
    static func getProfileImageUrl(userName:String) -> String {
        
        return "http://66.175.218.158:8080/images-meftii/profiles/\(userName)"
        //        return "http://173.255.217.231:8080/images-meftii/profiles/\(userName).jpg"
        
    }
    
    static func getReportImageUrl(userName:String) -> String {
        
        return "http://66.175.218.158:8080/images-meftii/reports/\(userName).jpg"
        //        return "http://173.255.217.231:8080/images-meftii/reports/\(userName).jpg"
        
    }
    
    static func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    static func getDevUrl(type:String) -> String {
        
        
        switch type {
        case "dev":
            return "66.175.218.158"
            
        default:
            return "173.255.217.231"
        }
        
    }
    
    
}

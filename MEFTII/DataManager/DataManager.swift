//
//  DataManager.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 18/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

final class DataManager: NSObject {
    
    // MARK: Properties
    private(set) var currentRole : String?
    private(set) var userName : String?
    
    private(set) var allDoctors : Set<Profile_d2>?
    private(set) var currentPatient : Profile_p2?
    private(set) var currentDoctor : Profile_d2?
    private(set) var showIntroductionVc = false
    private(set) var favoriteDoctors = [Int]()
    var isPatientUpdating = false
    var isDoctorUpdating = false
    var isLegalInfoUpdating = false
    
    var currentPatientId : Int {
        return self.currentPatient?.id ?? 0
    }
    
    var currentDoctorId : Int {
        return self.currentDoctor?.id ?? 0
    }
    
    
    
    // MARK: Shared Instance
    static let shared = DataManager()
    
    override private init() {
        super.init()
        
        currentRole = UserDefaults.standard.string(forKey: "userRole")
        userName = UserDefaults.standard.string(forKey: "userName")
        showIntroductionVc = !UserDefaults.standard.bool(forKey: "showIntroductionVc")
        if let favArray = UserDefaults.standard.array(forKey: "favoriteDoctors") as? [Int], favArray.count > 0 {
            favoriteDoctors.append(contentsOf: favArray)
        }
        
        
        if let patientData = UserDefaults.standard.data(forKey: "loggedInPatient") {
            do {
                currentPatient = try JSONDecoder().decode(Profile_p2.self, from: patientData)
            }
            catch {
                currentPatient = nil
            }
        }
        
        if let doctorData = UserDefaults.standard.data(forKey: "loggedInDoctor") {
            do {
                currentDoctor = try JSONDecoder().decode(Profile_d2.self, from: doctorData)
            }
            catch {
                currentDoctor = nil
            }
        }
        
    }
    
    func setFCMtoken(token:String){
        
        UserDefaults.standard.set(token, forKey: "fcmToken")
        
    }
    
    func getFcmToken()->String{
        
        return UserDefaults.standard.value(forKey: "fcmToken") as! String
    }
    
    
    
    func setcurrentEmail(email:String){
        
        UserDefaults.standard.set(email, forKey: "current_email")
        
    }
    func getcurrentEmail()->String{
        
        if let email = UserDefaults.standard.value(forKey: "current_email") as? String {
            
              return email
            
        }
        else {
            
            return "email"
            
        }
    }
    
    func setcurrentPass(pass:String){
           
           UserDefaults.standard.set(pass, forKey: "current_pass")
           
       }
    
    func getcurrentPass()->String{
          
          return UserDefaults.standard.value(forKey: "current_pass") as! String
      }
    // MARK: Utility Methods
    
    func setUserLoggedIn(userName name: String, role: String) {
        
        self.currentRole = role
        self.userName = name
        
        UserDefaults.standard.set(currentRole, forKey: "userRole")
        UserDefaults.standard.set(userName, forKey: "userName")
        UserDefaults.standard.synchronize()
        
    }
    
    func setAllDoctorsProfile(_ array:[Profile_d2]) {
        allDoctors = Set<Profile_d2>.init(array)
    }
    
    func saveLoggedInPatient(profile:Profile_p2) {
        if let _ = profile.id {
            currentPatient = profile
            do {
                let data = try JSONEncoder().encode(profile)
                UserDefaults.standard.set(data, forKey: "loggedInPatient")
            }
            catch {
                currentPatient = nil
                UserDefaults.standard.removeObject(forKey: "loggedInPatient")
            }
        }
        else {
            currentPatient = nil
            UserDefaults.standard.removeObject(forKey: "loggedInPatient")
        }
        currentDoctor = nil
        UserDefaults.standard.synchronize()
    }
    
    func setState(state:String){
        
         UserDefaults.standard.set(state, forKey: "state")
    }
    
    func setCity(city:String){
          
           UserDefaults.standard.set(city, forKey: "city")
      }
    
    func getState()->String{
        
        if let state = UserDefaults.standard.value(forKey: "state") as? String {
            
            return state
        }
        else {
            return ""
        }
    }
    
    func getCity()->String{
        if let city = UserDefaults.standard.value(forKey: "city") as? String {
            
            return city
        }
        else {
            return ""
        }

    }
    func saveLoggedInDoctor(profile:Profile_d2) {
       if let _ = profile.id {
            currentDoctor = profile
            do {
                let data = try JSONEncoder().encode(profile)
                UserDefaults.standard.set(data, forKey: "loggedInDoctor")
                
            }
            catch {
                currentDoctor = nil
                UserDefaults.standard.removeObject(forKey: "loggedInDoctor")
            }
  
        }
        else {
            currentDoctor = nil
            UserDefaults.standard.removeObject(forKey: "loggedInDoctor")
        }
        currentPatient = nil
        UserDefaults.standard.synchronize()
    }
    
    func logoutCurrentUser(isForceLogout:Bool = false) {
        isPatientUpdating = false
        isDoctorUpdating = false
        if isForceLogout {
            allDoctors = nil
            currentDoctor = nil
            currentPatient = nil
            
            UserDefaults.standard.removeObject(forKey: "loggedInDoctor")
            UserDefaults.standard.removeObject(forKey: "loggedInPatient")
            
        }
        else if let patient = currentPatient {
            //            let isOnFinger = patient.isFingerPrintOn ?? false
            //            if !isOnFinger {
            //                currentPatient = nil
            //                UserDefaults.standard.removeObject(forKey: "loggedInPatient")
            //            }
        }
        else if let doctor = currentDoctor {
            let isOnFinger = doctor.isFingerPrintOn ?? false
            if !isOnFinger {
                currentDoctor = nil
                UserDefaults.standard.removeObject(forKey: "loggedInDoctor")
            }
        }
        UserDefaults.standard.synchronize()
        
        
        
        let delegate = UIApplication.shared.delegate;
        if let vc = delegate?.window??.rootViewController as? UINavigationController {
            

            let transition = CATransition()
                         transition.duration = 0.5
                      transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
                      transition.type = CATransitionType.push
                      transition.subtype = CATransitionSubtype.fromLeft
                     vc.view.layer.add(transition, forKey: nil)
                      vc.view.layer.add(transition, forKey: nil)
                      let vx = ChoosePatientDoctorVC.instantiateFromStoryboard()
            vc.pushViewController(vx, animated: true)
            
//
//            vc.dismiss(animated: false, completion: nil)
//            vc.topViewController?.dismiss(animated: false, completion: nil)
//            vc.popToRootViewController(animated: false)
        
        
        
        }
//        
    }
    
    func restLoginData() {
        
        currentRole = nil
        userName = nil
        UserDefaults.standard.removeObject(forKey: "userRole")
        UserDefaults.standard.removeObject(forKey: "userName")
        UserDefaults.standard.synchronize()
        
    }
    
    func setFingerPrintOnOff(isOn:Bool) {
        
        if let patient = currentPatient {
            // patient.isFingerPrintOn = isOn
            saveLoggedInPatient(profile:patient)
        }
        
        if let doctor = currentDoctor {
            doctor.isFingerPrintOn = isOn
            saveLoggedInDoctor(profile: doctor)
        }
        
    }
    
    func setIntroductionVcShown() {
        showIntroductionVc = false
        UserDefaults.standard.set(true, forKey: "showIntroductionVc")
        UserDefaults.standard.synchronize()
    }
    
    func markDoctorFavorite(_ doctorId:Int) {
        
        if favoriteDoctors.contains(doctorId) {
            return
        }
        else {
            favoriteDoctors.append(doctorId)
        }
        UserDefaults.standard.set(favoriteDoctors, forKey: "favoriteDoctors")
        UserDefaults.standard.synchronize()
    }
    
    func isDoctorFavorite(_ doctorId:Int) -> Bool {
        return favoriteDoctors.contains(doctorId)
    }
    
    func removeFavoriteDoctor(_ doctorId:Int) {
        favoriteDoctors.removeAll { (element) -> Bool in
            return element == doctorId
        }
        UserDefaults.standard.set(favoriteDoctors, forKey: "favoriteDoctors")
        UserDefaults.standard.synchronize()
    }
    
    func doctorlogin(userName:String, password:String) {
          
          
          //  UserDefaults.standard.set(userName, forKey: "doctor_email")
         DataManager.shared.setcurrentEmail(email: userName)
          let request = MeftiiRequests.loginDoctor(userName: userName, password: password)
          
          request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
              
              if code == 0{
                  
                  
                  let data = resp["data"] as! [String:Any]
                  let patient = data
                  do {
                      let profile = try Profile_d2.init(from: patient)
                      //                              profile.isFingerPrintOn = self?.biometricEnabled
                      //                              profile.addressTwo = self?.address
                    DataManager.shared.setCity(city: (profile.city?.name!) ?? "")
                    DataManager.shared.setState(state: (profile.state?.name!) ?? "")
                      DataManager.shared.saveLoggedInDoctor(profile: profile)

                  }
                  catch {
                      
                  }
                  
                  
              }
              else {
                  
              }
          
              DataManager.shared.setUserLoggedIn(userName: userName, role: "doctor")
              
              
              }, failureBlock: { [weak self] (error) in
                
          })
          
      }
    
    
    
    func patientlogin(userName:String, password:String) {
        // UserDefaults.standard.set(userName, forKey: "patient_email")
        
       DataManager.shared.setcurrentEmail(email: userName)
        DataManager.shared.setcurrentPass(pass: password)
        let request = MeftiiRequests.loginPatient(userName: userName, password: password)

        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
             if code == 0{
                
                
                let data = resp["data"] as! [String:Any]
                let patient = data
                do {
                    let profile = try Profile_p2.init(from: patient)
                    DataManager.shared.setCity(city: (profile.city?.name!)!)
                    DataManager.shared.setState(state: (profile.state?.name!)!)
                    
                    DataManager.shared.saveLoggedInPatient(profile: profile)

                }
                catch {
                    
                }
                
                
            }
            else {
                
            }
           
            DataManager.shared.setUserLoggedIn(userName: userName, role: "patient")
           
            
            
            
            }, failureBlock: { [weak self] (error) in
              
        })
        
    }
    
}

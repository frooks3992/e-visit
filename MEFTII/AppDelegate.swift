//
//  AppDelegate.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 10/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Intents
import Firebase
import DropDown
import UserNotifications
import FirebaseInstanceID
import FirebaseMessaging


class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let color = UIColor.MFDarkRed
        let font = UIFont.AppBoldFont(ofSize: 15)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor:color], for: UIControl.State.normal)
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font : font, NSAttributedString.Key.foregroundColor:color], for: UIControl.State.highlighted)
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        
        DropDown.appearance().cellHeight = 60
        DropDown.appearance().textFont = UIFont.AppFont(ofSize: 15)
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().separatorColor = UIColor.lightGray.withAlphaComponent(0.3)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 45.0
        
        FirebaseApp.configure()
        TimeOutManager.shared.resetTimer()
        
        
        if #available(iOS 10.0, *) {
                  // For iOS 10 display notification (sent via APNS)
                  UNUserNotificationCenter.current().delegate = self
                  let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                  UNUserNotificationCenter.current().requestAuthorization(
                      options: authOptions,
                      completionHandler: {_, _ in })
                  // For iOS 10 data message (sent via FCM
                  Messaging.messaging().delegate = self
              } else {
                  let settings: UIUserNotificationSettings =
                      UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
                  application.registerUserNotificationSettings(settings)
              }
              
              application.registerForRemoteNotifications()
              Messaging.messaging().shouldEstablishDirectChannel = true

        
        return true
    }

  

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        
        guard let viewController = window?.rootViewController as? MFNavigationController, let interaction = userActivity.interaction else {
            return false
        }

        var personHandle: INPersonHandle?

        if let startVideoCallIntent = interaction.intent as? INStartVideoCallIntent {
            personHandle = startVideoCallIntent.contacts?[0].personHandle
        } else if let startAudioCallIntent = interaction.intent as? INStartAudioCallIntent {
            personHandle = startAudioCallIntent.contacts?[0].personHandle
        }

        if let personHandle = personHandle {
            viewController.presentCallController(forIncoming: false, roomName: personHandle.value ?? "", token: "TWILIO_ACCESS_TOKEN")
        }

        return true
    }

    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        Messaging.messaging().apnsToken = deviceToken
        PushNotificationManager.shared.subscribeToMyTopic()
        
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("Failed to register: \(error)")
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        TimeOutManager.shared.resetTimer()
        super.touchesBegan(touches, with: event)
    }

}

extension AppDelegate:UNUserNotificationCenterDelegate,MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
           
          print("Token: \(fcmToken)")
           
         DataManager.shared.setFCMtoken(token: fcmToken)
           
       }

     func application(received remoteMessage: MessagingRemoteMessage) {
         print(remoteMessage.appData)
     }
     
     
     
     
     func applicationWillResignActive(_ application: UIApplication) {
        // FBSDKAppEvents.activateApp()
     }
     
     func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
         
         print(userInfo)
     }
     
     
     func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
         print(notification)
         
         completionHandler([.alert, .badge, .sound])
     }
     
     
    

    
}

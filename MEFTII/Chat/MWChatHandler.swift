//
//  MWChatHandler.swift
//  FindHelp
//
//  Created by Mian Waqas Umar on 29/10/2019.
//  Copyright © 2019 Waqas Creations. All rights reserved.
//

import Foundation
import Realm
import RealmSwift


class MWChatHandler: NSObject {


    static let shared = MWChatHandler()
    
    
    private  override init() {
        super.init()
    }
    
    
    func sendMessage(to userName:String, message:MWChatMessage) {
        PushNotificationManager.shared.sendPushNotification(to: userName, body: message.text, isForCall: false, token: "")
        
        let realm = try! Realm()
        let results = realm.objects(MWConversation.self).filter("conversationId == %@", userName)
        
        if let convo = results.first {
            try! realm.write {
                convo.chatMessages.append(message)
            }
        }
        else {
            let conversation = MWConversation()
            conversation.conversationId = userName
            //conversation.displayName = "\(faker.name.firstName()) \(faker.name.lastName())"
            
            try! realm.write {
                realm.add(conversation, update: .all)
                conversation.chatMessages.append(message)
            }
        }
    }
    
    func messageReceived(_ message:MWChatMessage) {
        
        let fromUserName = message.senderUserName
        
        let realm = try! Realm()
        let results = realm.objects(MWConversation.self).filter("conversationId == %@", fromUserName)
        
        if let convo = results.first {
            try! realm.write {
                convo.chatMessages.append(message)
            }
        }
        else {
            let conversation = MWConversation()
            conversation.conversationId = fromUserName
            conversation.displayName = message.userDisplayName.capitalized
            
            try! realm.write {
                realm.add(conversation, update: .all)
                conversation.chatMessages.append(message)
            }
        }
    }

}

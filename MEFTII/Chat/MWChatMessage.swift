//
//  MWChatMessage.swift
//  FindHelp
//
//  Created by Mian Waqas Umar on 29/10/2019.
//  Copyright © 2019 Waqas Creations. All rights reserved.
//

import Foundation
import RealmSwift
import ROCController

@objcMembers class MWChatMessage: ROCChatMessage {
    
    public dynamic var isReceivedViaPush: Bool = false
    public dynamic var senderUserName: String = ""
    
    override var isIncoming: Bool {
        if isReceivedViaPush {
            return true
        }
        return senderUserName != (PushNotificationManager.shared.userName)
    }
    
}

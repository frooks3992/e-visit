//
//  MWConversation.swift
//  FindHelp
//
//  Created by Mian Waqas Umar on 29/10/2019.
//  Copyright © 2019 Waqas Creations. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class MWConversation: Object {
    
    dynamic var conversationId: String = UUID().uuidString
    dynamic var timestamp: Date = Date()
    dynamic var summary: String = ""
    dynamic var displayName: String = ""
    dynamic var imageUrl: String? = nil
    
    let chatMessages = List<MWChatMessage>()
    
    override static func primaryKey() -> String? {
        return "conversationId"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["friendlyTimestampString"]
    }
    
    var friendlyTimestampString: String {
        let dateFormatter = DateFormatter()
    
        if NSCalendar.current.isDateInToday(timestamp){
            dateFormatter.dateFormat = ""
        } else if NSCalendar.current.isDateInYesterday(timestamp) {
            return "Yesterday"
        } else {
            dateFormatter.dateFormat = "MM/dd"
        }
        return dateFormatter.string(from: timestamp)
    }
    
}

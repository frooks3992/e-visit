//
//  doctorViewReportTableCell.swift
//  MEFTII
//
//  Created by Awais Malik on 17/06/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import UIKit

class doctorViewReportTableCell: UITableViewCell {

    @IBOutlet weak var collectionViews: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setDelegate(){
        collectionViews.delegate = self
        collectionViews.dataSource = self
    }
}
extension doctorViewReportTableCell: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "doctorViewReportcolCell", for: indexPath) as! ReportCell
        cell.reportImageView.image = #imageLiteral(resourceName: "home_pic_4")
        return cell
    }
    
    
}

extension doctorViewReportTableCell: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = collectionView.frame.size.height / 2.0
        let width = collectionView.frame.size.width / 2.0
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}

//
//  MFOnCallGroupCell.swift
//  MEFTII
//
//  Created by Farhan on 05/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import UIKit

class MFOnCallGroupCell: UITableViewCell {

    @IBOutlet weak var leftConstraint: NSLayoutConstraint!
    @IBOutlet var title: UILabel!
     @IBOutlet var des: UILabel!
     @IBOutlet var timing: UILabel!
    @IBOutlet weak var editBTn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  MFTableViewCell.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 03/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import FloatRatingView

class MFTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView?
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDateTime: UILabel?
    @IBOutlet weak var btnCall: MFButton?
    @IBOutlet weak var lblValue: UILabel?
    @IBOutlet weak var textField: MFTextField?
    @IBOutlet weak var lblUnreadCount: UILabel?
    @IBOutlet weak var btndelete: UIButton?
    @IBOutlet weak var medicineField: MFTextField?
    @IBOutlet weak var dosageDp: MFDropDownField?
    @IBOutlet weak var directionsField: MFTextField?
    @IBOutlet weak var timeDp: MFDropDownField?
    @IBOutlet weak var toggleSw: UISwitch?
    @IBOutlet weak var ratingVw: FloatRatingView?
    @IBOutlet weak var bottomLineView: UIView?
    @IBOutlet weak var strongLineView: UIView?
    @IBOutlet weak var toButton: UIButton?
    @IBOutlet weak var fromButton: UIButton?
    @IBOutlet weak var toField: UITextField?
    @IBOutlet weak var fromField: UITextField?
    
    //Callback
    var imageTapped: (() -> ())?
    var makeAppointmentTapped: (() -> ())?
    weak var profileViewController: BaseViewController?
    weak var timing: Timings?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgProfile?.isUserInteractionEnabled = false
        imgProfile?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imageViewTapped)))
    }
    
    @objc func imageViewTapped() {
        if let imageTapped = imageTapped {
            imageTapped()
        }
    }
    
    @IBAction func makeAnAppointment(_ sender: MFButton) {
        if let makeAppointmentTapped = makeAppointmentTapped {
            makeAppointmentTapped()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setProfileCell(label:String, value:String?, row: Int) {
        
        if lblName != nil {
            self.lblName.text = label
        }
        self.textField?.text = value
        self.textField?.placeholder = label
        self.textField?.tag = row
        self.lblValue?.text = value
    }
    
    func setTimingForCell(timing tmg:Timings, day:String?, vc:BaseViewController?) {
        timing = tmg
        profileViewController = vc
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        var to = ""
        var from = ""
        
        let apiFormat = DateFormatter()
        apiFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        apiFormat.dateFormat = "HH:mm"
        
        if let toDate = apiFormat.date(from: tmg.to ?? "") {
            to = formatter.string(from: toDate)
        }
        
        if let fromDate = apiFormat.date(from: tmg.from ?? "") {
            from = formatter.string(from: fromDate)
        }
        toField?.text = to
        fromField?.text = from
        if lblName != nil {
            lblName.text = day
        }
        textField?.isUserInteractionEnabled = false
        lblValue?.text = "\(from) - \(to)"
    }
    
    
    @IBAction func toTimePressed(_ sender: UIButton) {
        if let vc = profileViewController {
            vc.showChoseDateDialog(minDate: nil, maxDate: nil, isForTime: true) { [weak self] (date, strDate) in
                if let date = date {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "h:mm a"
                    self?.toField?.text = formatter.string(from: date)
                    formatter.dateFormat = "HH:mm"
                    self?.timing?.to = formatter.string(from: date)
                }
            }
        }
    }
    
    
    
    
    @IBAction func fromBtnTimePressed(_ sender: UIButton) {
        if let vc = profileViewController {
            vc.showChoseDateDialog(minDate: nil, maxDate: nil, isForTime: true) { [weak self] (date, strDate) in
                if let date = date {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "h:mm a"
                    self?.fromField?.text = formatter.string(from: date)
                    formatter.dateFormat = "HH:mm"
                    self?.timing?.from = formatter.string(from: date)
                }
            }
        }
    }
    
}

//
//  UXSwifich.swift
//  Doctore E visit
//
//  Created by Awais Malik on 08/06/2020.
//  Copyright © 2020 Awais. All rights reserved.
//

import Foundation
import UIKit
@IBDesignable
class UXSwitch: UISwitch {
    @IBInspectable var size: CGFloat = 0 {
        didSet {
            self.transform = CGAffineTransform(scaleX: size, y: size)
           }
       }
}

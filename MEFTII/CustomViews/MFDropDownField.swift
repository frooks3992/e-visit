//
//  MFDropDownField.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 17/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import DropDown
import IQKeyboardManagerSwift

@IBDesignable
class MFDropDownField: UIView {
    
    
    @IBInspectable var placeholderText : String = "" {
        didSet {
            textField?.placeholder = placeholderText
            textField?.prepareForInterfaceBuilder()
        }
    }
    @IBInspectable var isEditable : Bool = false
    @IBInspectable var isSearchEnabled : Bool = false
    
    private var textField: MFTextField?
    private var button: UIButton?
    
    private var isShowingDropdown = false
    private var dropDown = DropDown()
    
    var dataStrings : [String] = [] {
        didSet {
            selectedIndex = nil
            stringsToShow = dataStrings
            dropDown.dataSource = stringsToShow
            dropDown.reloadAllComponents()
        }
    }
    
    private var stringsToShow : [String] = []
    
    var selectedValue:String? {
        didSet (newValue) {
            if let _ = newValue {
                textField?.text = selectedValue
            }
        }
    }
    var selectedBorder:UIColor? {
        didSet (newValue) {
            if let _ = newValue {
                textField?.borderColor = selectedBorder ?? UIColor.lightGray
            }
        }
    }
    
    var selectedIndex:Int? {
        didSet {
            if let idx = selectedIndex {
                selectedValue = dataStrings[idx]
                textField?.text = selectedValue
            }
            else {
                selectedValue = nil
                //textField?.placeholder = placeholderText
                textField?.text = nil
            }
        }
    }
    
    var closure : SelectionClosure?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    
    func commonInit() {
        
        for view in subviews {
            view.removeFromSuperview()
        }
        
        dropDown.anchorView = self
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y: self.bounds.height)
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            if item == "Nothing Matched" {
                self.selectedIndex = nil
            }
            else {
                self.selectedIndex = index
                self.textField?.text = item
                self.selectedValue = item
                
            }
            self.isShowingDropdown = false
            self.closure?(index,item)
            self.textField?.resignFirstResponder()
            self.bringSubviewToFront(self.button!)
        }
        
        dropDown.cancelAction = { [unowned self] in
            self.bringSubviewToFront(self.button!)
            self.isShowingDropdown = false
            self.textField?.resignFirstResponder()
            
            if self.isEditable {
                self.selectedValue = self.textField?.text
                self.closure?(0,self.selectedValue ?? "")
            }
            else {
                self.textField?.text = nil
                self.selectedIndex = nil
            }
        }
        
        dropDown.dataSource = dataStrings
        dropDown.reloadAllComponents()
        
        //backgroundColor = UIColor.clear
        textField?.removeFromSuperview()
        textField = MFTextField(frame: self.bounds)
        textField?.addTarget(self, action: #selector(textChanged(_:)), for: .editingChanged)
        textField?.placeholder = placeholderText
        textField?.text = selectedValue
        textField?.textColor = UIColor.black
        //textField?.textFieldDelegate = self
        //textField?.backgroundColor = backgroundColor
        self.addSubview(textField!)
        
        
//        //let imgVw = UIImageView(image: #imageLiteral(resourceName: "dropdown"))
//        let imgVw = UIImageView(frame: CGRect(x: bounds.width - 75, y: 17.5, width: 15, height: 15))
//        imgVw.image = UIImage(named: "dropdown")
//        imgVw.contentMode = .center
//        self.addSubview(imgVw)
        
        button?.removeFromSuperview()
        button = UIButton(type: .custom)
        button?.frame = self.bounds
        button?.addTarget(self, action: #selector(buttonPressed(_:)), for: .touchUpInside)
        self.addSubview(button!)
        
        /*
        let contentView = Bundle.main.loadNibNamed("MFDropDownField", owner: self, options: nil)![0] as? UIView
        contentView?.frame = bounds
        contentView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView?.translatesAutoresizingMaskIntoConstraints = true
        addSubview(contentView!)
        
        titleTextField?.placeholder = placeholderText
        
        //tapButton?.addTarget(self, action: #selector(genderButtonPressed(_:)), for: .touchUpInside)
        */
        self.bringSubviewToFront(button!)
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    @objc func buttonPressed(_ sender: UIButton) {
        UIApplication.shared.keyWindow?.endEditing(true)
        if isSearchEnabled {
            //self.bringSubviewToFront(self.textField!)
            textField?.isEnabled = true
            textField?.delegate = self
            textField?.becomeFirstResponder()
            
        }
        else {
            textField?.isEnabled = false
        }
        stringsToShow = dataStrings
        isShowingDropdown = true
        dropDown.dataSource = stringsToShow
        dropDown.reloadAllComponents()
        
        if isSearchEnabled {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
                let result = self.dropDown.show()
                if !result.canBeDisplayed, let scVw = self.getParentScrolView() {
                    var offsettt = result.offscreenHeight ?? 20
                    if offsettt > 20 {
                        offsettt = 17
                    }
                    let point = CGPoint(x: scVw.contentOffset.x, y: scVw.contentOffset.y + offsettt + 5)
                    scVw.setContentOffset(point, animated: false)
                    self.dropDown.show()
                }
            }
        }
        else {
            self.dropDown.show()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        dropDown.anchorView = self
        dropDown.bottomOffset = CGPoint(x: 0, y: self.bounds.height)
        commonInit()
        //setNeedsLayout()
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        self.commonInit()
    }
    
    @objc func textChanged(_ sender: UITextField) {
        
        selectedValue = nil
        
        let currentSearchString = textField?.text ?? ""
        
        if currentSearchString.count > 0 {
            let results = dataStrings.filter {  $0.localizedCaseInsensitiveContains(currentSearchString) }
            if results.count > 0 {
                stringsToShow = results
            }
            else {
                if isEditable {
                    stringsToShow = [currentSearchString]
                }
                else {
                    stringsToShow = ["Nothing Matched"]
                }
            }
        }
        else {
            stringsToShow = dataStrings
        }
        
        dropDown.dataSource = stringsToShow
        dropDown.reloadAllComponents()
        
        if let _ = DropDown.VisibleDropDown {
            
        }
        else {
            let result = dropDown.show()
            if !result.canBeDisplayed, let scVw = self.getParentScrolView() {
                let offsettt = result.offscreenHeight ?? 20
                let point = CGPoint(x: scVw.contentOffset.x, y: scVw.contentOffset.y + offsettt + 5)
                scVw.setContentOffset(point, animated: false)
                dropDown.show()
            }
        }
        
    }
    
}

extension MFDropDownField : UITextFieldDelegate {
    
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        //self.bringSubviewToFront(self.textField!)
        
        
        self.selectedBorder = UIColor.MFBlack60
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        //self.bringSubviewToFront(self.button!)
    }
    
}


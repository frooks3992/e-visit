//
//  FilterCell.swift
//  Doctore E visit
//
//  Created by Awais Malik on 08/06/2020.
//  Copyright © 2020 Awais. All rights reserved.
//

import UIKit

class FilterCell: UITableViewCell {

    @IBOutlet var title: UILabel!

    @IBOutlet var img: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  urgentCallRequestCell.swift
//  MEFTII
//
//  Created by Farhan on 09/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import UIKit

class urgentCallRequestCell: UITableViewCell {
    @IBOutlet weak var txt: UILabel!
    @IBOutlet weak var acceptBtn: MFButton!
    @IBOutlet weak var declineBtn : MFButton!
 
    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

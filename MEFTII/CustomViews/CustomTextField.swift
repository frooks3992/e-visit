//
//  CustomTextField.swift
//  MEFTII
//
//  Created by Farhan on 05/04/2020.
//  Copyright © 2020 Farhan Ali All rights reserved.
//

import UIKit
import Foundation
import UIKit

enum ValueType: Int {
    case none
    case onlyLetters
    case onlyNumbers
    case phoneNumber   // Allowed "+0123456789"
    case alphaNumeric
    case fullName
    case alphaNumericSpace// Allowed letters and space
}

class CustomTextField: UITextField {
    private var maxLengths = [UITextField: Int]()
    var bottomBorder = UIView()
    var rightImage = UIImageView()
    var ArrowImage = UIImageView()
    var textFieldDelegate : UITextFieldDelegate?
    
    override func awakeFromNib() {
        
        //Setup Bottom-Border
        self.autocapitalizationType = .words
        self.translatesAutoresizingMaskIntoConstraints = false
        
        bottomBorder = UIView.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        bottomBorder.backgroundColor = UIColor.lightGray // Set Border-Color
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(bottomBorder)
        
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true // Set Border-Strength
        
        
        rightImage = UIImageView.init(frame: CGRect(x:self.bounds.width+70, y: 5, width: 20 , height:20))
        addSubview(rightImage)
        rightImage = UIImageView(frame:
            CGRect(x: 10, y: 0, width: 20, height: 20))
        
        ArrowImage = UIImageView(frame:
            CGRect(x: -5, y: 5, width: 13, height: 13))
        ArrowImage.contentMode  = .scaleAspectFit
        
        let iconContainerView: UIView = UIView(frame:
            CGRect(x: 20, y: 0, width: 20, height: 30))

        iconContainerView.addSubview(rightImage)
        iconContainerView.addSubview(ArrowImage)
        rightView = iconContainerView
        rightViewMode = .always

        
        
    }
    @IBInspectable var maxLength: Int = 0 // Max character length
    var valueType: ValueType = ValueType.none // Allowed characters
    
    /************* Added new feature ***********************/
    // Accept only given character in string, this is case sensitive
    @IBInspectable var allowedCharInString: String = ""
    
    func verifyFields(shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch valueType {
        case .none:
            break // Do nothing
            
        case .onlyLetters:
            let characterSet = CharacterSet.letters
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
            
        case .onlyNumbers:
            let numberSet = CharacterSet.decimalDigits
            if string.rangeOfCharacter(from: numberSet.inverted) != nil {
                return false
            }
            
        case .phoneNumber:
            let phoneNumberSet = CharacterSet(charactersIn: "1-000-000-0000")
            if string.rangeOfCharacter(from: phoneNumberSet.inverted) != nil {
                return false
            }
            
        case .alphaNumeric:
            let alphaNumericSet = CharacterSet.alphanumerics
            
            if string.rangeOfCharacter(from: alphaNumericSet.inverted) != nil {
                return false
            }
        case .alphaNumericSpace:
            var characterSet = CharacterSet.alphanumerics
            characterSet = characterSet.union(CharacterSet(charactersIn: " "))
            
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
            
        case .fullName:
            var characterSet = CharacterSet.letters
            print(characterSet)
            characterSet = characterSet.union(CharacterSet(charactersIn: " "))
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
        }
        
        if let text = self.text, let textRange = Range(range, in: text) {
            let finalText = text.replacingCharacters(in: textRange, with: string)
            if maxLength > 0, maxLength < finalText.utf8.count {
                return false
            }
        }
        
        // Check supported custom characters
        if !self.allowedCharInString.isEmpty {
            let customSet = CharacterSet(charactersIn: self.allowedCharInString)
            if string.rangeOfCharacter(from: customSet.inverted) != nil {
                return false
            }
        }
        
        return true
    }
    
    @IBInspectable var hasError: Bool = false {
        didSet {
            
            if (hasError) {
                

                rightImage.image = UIImage(named:"ic_error")
                bottomBorder.backgroundColor = UIColor.MFDarkRed
            } else {
                
                rightImage.image = nil
                bottomBorder.backgroundColor = UIColor.MFBlack60
                
            }
            
        }
    }
    
    @IBInspectable var hasArrow: Bool = false {
        didSet {
            
            if (hasArrow) {
                

                ArrowImage.image = UIImage(named:"next")
                
                
            } else {
                
                ArrowImage.image = nil
                
                
            }
            
        }
    }
    
    
}



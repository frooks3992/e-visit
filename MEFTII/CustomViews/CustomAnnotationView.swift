//
//  CustomAnnotationView.swift
//  MEFTII
//
//  Created by Schaheer Saleem on 10/5/19.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import MapKit

class CustomAnnotationView: MKAnnotationView {
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(annotation: MKAnnotation?, reuseIdentifier: String?, name: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        //Doctor name at bottom of annotation
        let label = UILabel(frame: CGRect(x: -50, y: (self.frame.maxY) + 40, width: 150, height: 30))
        label.textAlignment = .center
        label.text = name ?? "NO-NAME"
        label.textColor = .MFRed
        label.numberOfLines = 0
        label.font = UIFont.AppSemiBoldFont()
        label.sizeToFit()
        label.frame = CGRect(x: -50, y: (self.frame.maxY) + 40, width: 150, height: 60)
        self.addSubview(label)
    }
    
}

//
//  CustomPointAnnotation.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 02/10/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import MapKit

class CustomPointAnnotation: MKPointAnnotation {
    
    var doctorProfile: DoctorProfile?
    var pinCustomImageName:String = "pin"
    
    var pharmacy: Pharmacies?
    var pharmacyPinCustomImageName: String = "pharmacy_pin"
    
}

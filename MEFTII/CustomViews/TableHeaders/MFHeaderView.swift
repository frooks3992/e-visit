//
//  MFHeaderView.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 27/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class MFHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var toggleSwitch: UISwitch?
    @IBOutlet weak var mainText: UILabel?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.backgroundView = UIView()
        
        if #available(iOS 13.0, *) {
            self.backgroundView?.backgroundColor = UIColor.white
        } else {
            // Fallback on earlier versions
            self.backgroundView?.backgroundColor = .white
        }
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    

}

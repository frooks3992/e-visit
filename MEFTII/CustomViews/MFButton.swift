//
//  MFButton.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 14/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class MFButton: UIButton {
    
    // IB: use the adapter
    @IBInspectable var drawPath: Bool = false {
        didSet {
            if drawPath {
                let path = UIBezierPath(rect: self.bounds)
                path.setLineDash([5.0, 5.0], count: 2, phase: 0.0)
                self.accessibilityPath = path
            }
        }
    }
    
    @IBInspectable var isFilled : Bool = true {
        didSet {
            adjustsButtonLayout()
        }
    }
    
    @IBInspectable var color : UIColor = UIColor.MFDarkRed {
        didSet {
            adjustsButtonLayout()
        }
    }
    
    @IBInspectable var titleSize : Float = 15 {
        didSet {
            adjustsButtonLayout()
        }
    }
    
    @IBInspectable var addBackArrow : Bool = false {
        didSet {
            adjustsButtonLayout()
        }
    }
    
    @IBInspectable var addNextArrow : Bool = false {
        didSet {
            adjustsButtonLayout()
        }
    }
    
    var isOutlineButton = false {
        didSet {
            adjustsButtonLayout()
        }
    }
    

    // MARK: - Life Cycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        adjustsButtonLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        adjustsButtonLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        adjustsButtonLayout()
    }

    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        
        if addBackArrow {
            setTitle(title ?? "", rightImage: nil, leftImage: UIImage(named: "backArrow"), for: state)
        }
        else if addNextArrow {
            setTitle(title ?? "", rightImage: UIImage(named: "nextArrow"), leftImage: nil, for: state)
        }
        else {
            super.setAttributedTitle(nil, for: state)
            super.setTitle(title, for: state)
            self.setTitleShadowColor(UIColor.gray, for: state)
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
        
    }
    
    //MARK: - Functions
    
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
        
        self.adjustsButtonLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //adjustsButtonLayout()
    }
    

    func adjustsButtonLayout() {
        
        let titleColor = isFilled ? UIColor.white:color
        let faceColor = isFilled ? color:UIColor.clear
        let font = isFilled ? UIFont.AppMediumFont(ofSize:titleSize):UIFont.AppBoldFont(ofSize:titleSize)
        
        self.layer.cornerRadius = isFilled ? (frame.size.height * 0.5):0.0
        self.clipsToBounds = true
        self.titleLabel?.font = font
        self.backgroundColor = faceColor
        self.setTitleColor(titleColor, for: UIControl.State.normal)
        
        if isOutlineButton {
            self.layer.borderColor = UIColor.MFDarkRed.cgColor
            self.layer.borderWidth = 1.0
            self.backgroundColor = UIColor.white
            self.setTitleColor(UIColor.MFDarkRed, for: UIControl.State.normal)
        }
        
        if let str = self.title(for: UIControl.State.normal) {
            self.setTitle(str, for: UIControl.State.normal)
        }
        
        
        
    }
    
    
    func setState(isEnable : Bool) {
        
        if isEnable {
            self.isEnabled = true
            self.titleLabel?.isEnabled = true
        } else {
            self.isEnabled = false
            self.titleLabel?.isEnabled = false
        }
    }
    
    func setTitle(_ title:String, rightImage:UIImage?, leftImage:UIImage?, for state: UIControl.State) {
        
        var completeStr : NSMutableAttributedString
        
        let titleColor = isFilled ? UIColor.white:color
        let font = isFilled ? UIFont.AppSemiBoldFont(ofSize:titleSize):UIFont.AppBoldFont(ofSize:titleSize)
        
        let titleString = NSAttributedString(string: title, attributes: [.font : font, .foregroundColor: titleColor])
        
        if leftImage != nil {
            let leftImg = NSTextAttachment()
            leftImg.image = leftImage
            let leftStr = NSAttributedString(attachment: leftImg)
            completeStr = NSMutableAttributedString(attributedString: leftStr)
            completeStr.append(NSAttributedString(string: " "))
            completeStr.append(titleString)
        }
        else {
            completeStr = NSMutableAttributedString(attributedString: titleString)
        }
        
        if rightImage != nil {
            let rightImg = NSTextAttachment()
            rightImg.image = rightImage
            let rightStr = NSAttributedString(attachment: rightImg)
            
            completeStr.append(NSAttributedString(string: " "))
            completeStr.append(rightStr)
        }
        
        super.setAttributedTitle(completeStr, for: state)
        super.setTitle(nil, for: state)
        self.setTitleShadowColor(UIColor.gray, for: state)
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
}

@IBDesignable
class MFButton2: UIButton {
    
    // IB: use the adapter
    @IBInspectable var drawPath: Bool = false {
        didSet {
            if drawPath {
                let path = UIBezierPath(rect: self.bounds)
                path.setLineDash([5.0, 5.0], count: 2, phase: 0.0)
                self.accessibilityPath = path
            }
        }
    }
    
    @IBInspectable var isFilled : Bool = true {
        didSet {
            adjustsButtonLayout()
        }
    }
    
    @IBInspectable var color : UIColor = UIColor.MFDarkRed {
        didSet {
            adjustsButtonLayout()
        }
    }
    
    @IBInspectable var titleSize : Float = 15 {
        didSet {
            adjustsButtonLayout()
        }
    }
    
    @IBInspectable var addBackArrow : Bool = false {
        didSet {
            adjustsButtonLayout()
        }
    }
    
    @IBInspectable var addNextArrow : Bool = false {
        didSet {
            adjustsButtonLayout()
        }
    }
    
    var isOutlineButton = false {
        didSet {
            adjustsButtonLayout()
        }
    }
    

    // MARK: - Life Cycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        adjustsButtonLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        adjustsButtonLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        adjustsButtonLayout()
    }

    
    override func setTitle(_ title: String?, for state: UIControl.State) {
        
        if addBackArrow {
            setTitle(title ?? "", rightImage: nil, leftImage: UIImage(named: "backArrow"), for: state)
        }
        else if addNextArrow {
            setTitle(title ?? "", rightImage: UIImage(named: "nextArrow"), leftImage: nil, for: state)
        }
        else {
            super.setAttributedTitle(nil, for: state)
            super.setTitle(title, for: state)
            self.setTitleShadowColor(UIColor.gray, for: state)
            self.setNeedsLayout()
            self.layoutIfNeeded()
        }
        
    }
    
    //MARK: - Functions
    
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
        
        self.adjustsButtonLayout()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        //adjustsButtonLayout()
    }
    

    func adjustsButtonLayout() {
        
        let titleColor = isFilled ? UIColor.white:color
        let faceColor = isFilled ? color:UIColor.clear
        let font = isFilled ? UIFont.AppMediumFont(ofSize:titleSize):UIFont.AppMediumFont(ofSize:titleSize)
        
        self.layer.cornerRadius = isFilled ? (frame.size.height * 0.5):0.0
        self.clipsToBounds = true
        self.titleLabel?.font = font
        self.backgroundColor = faceColor
        self.setTitleColor(titleColor, for: UIControl.State.normal)
        
        if isOutlineButton {
            self.layer.borderColor = UIColor.MFDarkRed.cgColor
            self.layer.borderWidth = 1.0
            self.backgroundColor = UIColor.white
            self.setTitleColor(UIColor.MFDarkRed, for: UIControl.State.normal)
        }
        
        if let str = self.title(for: UIControl.State.normal) {
            self.setTitle(str, for: UIControl.State.normal)
        }
        
        
        
    }
    
    
    func setState(isEnable : Bool) {
        
        if isEnable {
            self.isEnabled = true
            self.titleLabel?.isEnabled = true
        } else {
            self.isEnabled = false
            self.titleLabel?.isEnabled = false
        }
    }
    
    func setTitle(_ title:String, rightImage:UIImage?, leftImage:UIImage?, for state: UIControl.State) {
        
        var completeStr : NSMutableAttributedString
        
        let titleColor = isFilled ? UIColor.white:color
        let font = isFilled ? UIFont.AppMediumFont(ofSize:titleSize):UIFont.AppMediumFont(ofSize:titleSize)
        
        let titleString = NSAttributedString(string: title, attributes: [.font : font, .foregroundColor: titleColor])
        
        if leftImage != nil {
            let leftImg = NSTextAttachment()
            leftImg.image = leftImage
            let leftStr = NSAttributedString(attachment: leftImg)
            completeStr = NSMutableAttributedString(attributedString: leftStr)
            completeStr.append(NSAttributedString(string: " "))
            completeStr.append(titleString)
        }
        else {
            completeStr = NSMutableAttributedString(attributedString: titleString)
        }
        
        if rightImage != nil {
            let rightImg = NSTextAttachment()
            rightImg.image = rightImage
            let rightStr = NSAttributedString(attachment: rightImg)
            
            completeStr.append(NSAttributedString(string: " "))
            completeStr.append(rightStr)
        }
        
        super.setAttributedTitle(completeStr, for: state)
        super.setTitle(nil, for: state)
        self.setTitleShadowColor(UIColor.gray, for: state)
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
    
}

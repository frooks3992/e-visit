//
//  MFTextField.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 24/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class MFTextField: UITextField {
    
    let border = CALayer()
    var textFieldDelegate : UITextFieldDelegate?
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    
    @IBInspectable var isTransparentBk : Bool = false
    @IBInspectable var maxLength : Int = 250
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.delegate = self
        //self.tintColor = isTransparentBk ? UIColor.white:UIColor.darkText
        //self.textColor = isTransparentBk ? UIColor.white:UIColor.darkText
        self.borderStyle = .none
        self.font = UIFont.AppFont(ofSize: 14.0)
        self.clearButtonMode = .never
        //self.attributedPlaceholder = nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.delegate = self
        //self.tintColor = isTransparentBk ? UIColor.white:UIColor.darkText
        //self.textColor = isTransparentBk ? UIColor.white:UIColor.darkText
        self.borderStyle = .none
        self.font = UIFont.AppFont(ofSize: 14.0)
        self.clearButtonMode = .never
        //self.attributedPlaceholder = nil

    }
    
    
    override var tintColor: UIColor! {
        
        didSet {
            setNeedsDisplay()
        }
        
    }
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        
        let startingPoint   = CGPoint(x: rect.minX, y: rect.maxY)
        let endingPoint     = CGPoint(x: rect.maxX, y: rect.maxY)
        
        let path = UIBezierPath()
        
        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = 1.0
        
        let borderColor = isTransparentBk ? UIColor.white:UIColor.black
        
        borderColor.setStroke()
        
        path.stroke()
    }
    

    @IBInspectable var showRightIcon: Bool = false

    @IBInspectable var rightIcon: UIImage? {
        didSet {
            if showRightIcon {
                let iconView = UIImageView(frame: CGRect(x: 16, y: 5, width: 8, height: 13))
                iconView.image = rightIcon
                iconView.contentMode = .scaleAspectFit
                let iconContainerView: UIView = UIView(frame:
                               CGRect(x: 20, y: 0, width: 30, height: 30))
                iconContainerView.addSubview(iconView)
                rightView = iconContainerView
                rightViewMode = .always
            }
        }
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    // Max character length
    var valueType: ValueType = ValueType.none // Allowed characters

    /************* Added new feature ***********************/
    // Accept only given character in string, this is case sensitive
    @IBInspectable var allowedCharInString: String = ""

    func verifyFields(shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch valueType {
        case .none:
            break // Do nothing
            
        case .onlyLetters:
            let characterSet = CharacterSet.letters
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
            
        case .onlyNumbers:
            let numberSet = CharacterSet.decimalDigits
            if string.rangeOfCharacter(from: numberSet.inverted) != nil {
                return false
            }
            
        case .phoneNumber:
            let phoneNumberSet = CharacterSet(charactersIn: "1-000-000-0000")
            if string.rangeOfCharacter(from: phoneNumberSet.inverted) != nil {
                return false
            }
            
        case .alphaNumeric:
            let alphaNumericSet = CharacterSet.alphanumerics
            
            if string.rangeOfCharacter(from: alphaNumericSet.inverted) != nil {
                return false
            }
            case .alphaNumericSpace:
                var characterSet = CharacterSet.alphanumerics
                 characterSet = characterSet.union(CharacterSet(charactersIn: " "))
                
                if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                    return false
                }

        case .fullName:
            var characterSet = CharacterSet.letters
            print(characterSet)
            characterSet = characterSet.union(CharacterSet(charactersIn: " "))
            if string.rangeOfCharacter(from: characterSet.inverted) != nil {
                return false
            }
        }
        
        if let text = self.text, let textRange = Range(range, in: text) {
            let finalText = text.replacingCharacters(in: textRange, with: string)
            if maxLength > 0, maxLength < finalText.utf8.count {
                return false
            }
        }

        // Check supported custom characters
        if !self.allowedCharInString.isEmpty {
            let customSet = CharacterSet(charactersIn: self.allowedCharInString)
            if string.rangeOfCharacter(from: customSet.inverted) != nil {
                return false
            }
        }
        
        return true
    }
    
}

extension MFTextField : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let del = textFieldDelegate?.textFieldShouldBeginEditing?(textField) {
            return del
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        textFieldDelegate?.textFieldDidBeginEditing?(textField)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool  {
        
        if let del = textFieldDelegate?.textFieldShouldEndEditing?(textField) {
            return del
        }
        return true
    
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)  {
        textFieldDelegate?.textFieldDidEndEditing?(textField)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        
        if textField.text!.containsWhitespace{
            
         if string == " " && textField.text?.last == " "{
                  return false
            }
 
        }
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
            
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        let result =  count <= maxLength
        
        if result {
            if let value = textFieldDelegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) {
                return false
            }
        }
        return result
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if let value = textFieldDelegate?.textFieldShouldClear?(textField) {
            return value
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //textField.resignFirstResponder()
        if let value = textFieldDelegate?.textFieldShouldReturn?(textField) {
            return value
        }
        return true
    }
 
    
}

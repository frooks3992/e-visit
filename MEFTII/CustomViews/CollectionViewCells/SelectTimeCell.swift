//
//  SelectTimeCell.swift
//  MEFTII
//
//  Created by Schaheer Saleem on 4/4/20.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import UIKit

class SelectTimeCell: UICollectionViewCell {

    @IBOutlet weak var scheduleStatusLbl: UILabel!
    @IBOutlet weak var scheduleLbl: UILabel!
    @IBOutlet weak var roundedBackView: UIView!

    override class func awakeFromNib() {
        super.awakeFromNib()

        
    }

    func setup(schedule: timeSlot) {
        scheduleLbl.font = .AppMediumFont(ofSize: 11)
        scheduleLbl.textColor = .MFDarkRed
        roundedBackView.layer.cornerRadius = self.bounds.height / 2

        scheduleStatusLbl.font = .AppMediumFont(ofSize: 7)

        if schedule.status.lowercased() == "booked" {
          
            
            scheduleLbl.textColor = .white
            scheduleStatusLbl.textColor = .white
            roundedBackView.backgroundColor = .MFDarkRed
             roundedBackView.layer.borderColor = UIColor.clear.cgColor
            
        } else {
            
            if schedule.isSelected {
                
//                scheduleLbl.textColor = .white
//                scheduleStatusLbl.textColor = .white
//                //roundedBackView.backgroundColor = .MFCalenderGreen
//                roundedBackView.layer.borderWidth = 1
//                roundedBackView.layer.borderColor = UIColor.MFCalenderGreen.cgColor
                scheduleLbl.textColor = .white
                                     scheduleStatusLbl.textColor = .white
                                     roundedBackView.backgroundColor = .MFGreen
                                     roundedBackView.layer.borderWidth = 1
                                     roundedBackView.layer.borderColor = UIColor.MFGreen.cgColor
                
            }
            
            else {
                
                scheduleLbl.textColor = .MFGreen
                       scheduleStatusLbl.textColor = .MFGreen
                       roundedBackView.backgroundColor = .white
                       roundedBackView.layer.borderWidth = 1
                       roundedBackView.layer.borderColor = UIColor.MFGreen.cgColor
            }
            
       
        }

        scheduleLbl.text = schedule.timeSlot
        scheduleStatusLbl.text = schedule.status.lowercased() == "booked" ? "BOOKED" : "OPEN"
    }
    
    
    func updateApperence(color:UIColor){
        
        roundedBackView.backgroundColor = color
        
    }

//    override var isSelected: Bool {
//        didSet {
//            if self.isSelected {
//                scheduleLbl.textColor = .white
//                roundedBackView.backgroundColor = .MFDarkRed
//            } else {
//                scheduleLbl.textColor = .MFDarkRed
//                roundedBackView.backgroundColor = .white
//            }
//        }
//    }

}

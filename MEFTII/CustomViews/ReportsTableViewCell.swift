//
//  ReportsTableViewCell.swift
//  MEFTII
//
//  Created by Schaheer Saleem on 10/11/19.
//  Copyright © 2019 Mian Waqas Umar. All rights reserved.
//

import UIKit

class ReportsTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
  

    var reports: [UIImage] = []
    
    var onCellClicked: ((Int) -> Void )?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func reloadAllData() {
        let height = Double((collectionView.bounds.width / 3.0) - 4)
        let rows = Double(reports.count)/3.0
        let afterRounding = ceil(rows)
        let heights = (afterRounding * height) + 50
        collectionViewHeight.constant = CGFloat(heights)
        Constants.collectionViewsHeghtForfile = CGFloat(heights)
        collectionView.reloadData()
    }
    
    override func layoutIfNeeded() {

        super.layoutIfNeeded()
    }
    
}

extension ReportsTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constants.selctedFileImagesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let reportCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ReportCell.self), for: indexPath) as? ReportCell else {
            return UICollectionViewCell()
        }
    
        //reportCell.reportImageView.image = reports[indexPath.row]
        reportCell.reportImageView.image = Constants.selctedFileImagesArray[indexPath.row]
        reportCell.btndelete.tag = indexPath.row
    
        return reportCell

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        onCellClicked?(indexPath.item)
        
    }
    
    
    @IBAction func btnDeleteAction(_ sender: UIButton){
        Constants.selctedFileImagesArray.remove(at: sender.tag)
        self.reloadAllData()
    }
    
}

extension ReportsTableViewCell: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthHeight = (collectionView.bounds.width / 3.0) - 4

        return CGSize(width: widthHeight, height: widthHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

}

//extension ReportsVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
//
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
//
//        dismiss(animated: true, completion: nil)
//
//        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
//        reports.append(image)
//        collectionView.reloadData()
//    }
//
//}

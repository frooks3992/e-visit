//
//  MFSearchTextField.swift
//  MEFTII
//
//  Created by Farhan on 02/06/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class MFSearchTextField: UITextField {
    
   
    var textFieldDelegate : UITextFieldDelegate?
    let padding = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    

    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.delegate = self
//        //self.tintColor = isTransparentBk ? UIColor.white:UIColor.darkText
//        //self.textColor = isTransparentBk ? UIColor.white:UIColor.darkText
//        self.borderStyle = .none
//        self.font = UIFont.AppFont(ofSize: 14.0)
//        self.clearButtonMode = .never
//        //self.attributedPlaceholder = nil
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.delegate = self
        //self.tintColor = isTransparentBk ? UIColor.white:UIColor.darkText
        //self.textColor = isTransparentBk ? UIColor.white:UIColor.darkText
        self.borderStyle = .none
        self.font = UIFont.AppFont(ofSize: 14.0)
        self.clearButtonMode = .never
        //self.attributedPlaceholder = nil

    }
    
    

    

    

    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    // Max character length
    var valueType: ValueType = ValueType.none // Allowed characters



    
}

extension MFSearchTextField : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if let del = textFieldDelegate?.textFieldShouldBeginEditing?(textField) {
            return del
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)  {
        textFieldDelegate?.textFieldDidBeginEditing?(textField)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool  {
        
        if let del = textFieldDelegate?.textFieldShouldEndEditing?(textField) {
            return del
        }
        return true
    
    }
    
    func textFieldDidEndEditing(_ textField: UITextField)  {
        textFieldDelegate?.textFieldDidEndEditing?(textField)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        
        
        
        if textField.text!.containsWhitespace{
            
         if string == " " && textField.text?.last == " "{
                  return false
            }
 
        }
        
        return true
      
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if let value = textFieldDelegate?.textFieldShouldClear?(textField) {
            return value
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //textField.resignFirstResponder()
        if let value = textFieldDelegate?.textFieldShouldReturn?(textField) {
            return value
        }
        return true
    }
 
    
}

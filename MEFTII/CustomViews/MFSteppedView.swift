//
//  MFSteppedView.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 20/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

@IBDesignable
class MFSteppedView: UIView {

    @IBInspectable var totalSteps : Int = 3
    @IBInspectable var currentStep : Int = 0
    
    private var stepLength : CGFloat = 10
    private var sliderView : UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        showView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        showView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        showView()
    }
    
    
    private func showView()  {
        stepLength = frame.size.width / CGFloat(totalSteps)
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.size.height/2
        self.backgroundColor = UIColor.MFLightGray
        
        if sliderView != nil {
            sliderView.removeFromSuperview()
            sliderView = nil
        }
        let x = CGFloat(currentStep) * stepLength
        sliderView = UIView(frame: CGRect(x: x, y: 0, width: stepLength, height: frame.size.height))
        sliderView.clipsToBounds = true
        sliderView.layer.cornerRadius = frame.size.height/2
        sliderView.backgroundColor = UIColor.MFDarkRed
        addSubview(sliderView)
    }
    
    
    func moveTo(Step step:Int) -> Void {
        var nextStep = step
        if step >= totalSteps {
            nextStep = totalSteps - 1
        }
        if step < 0 {
            nextStep = 0
        }
        UIView.animate(withDuration: 0.33, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            let x = CGFloat(nextStep) * self.stepLength
            self.sliderView.frame = CGRect(x: x, y: 0, width: self.stepLength, height: self.frame.size.height)
        }, completion: { (done) in
            if done {
                self.currentStep = nextStep
            }
        })
    
    }
    
    func moveTo(Step nStep:Int, fromStep pStep:Int) -> Void {
        var prvStep = pStep
        var nextStep = nStep
        if nStep >= totalSteps {
            nextStep = totalSteps - 1
        }
        if nStep < 0 {
            nextStep = 0
        }
        if pStep < 0 {
            prvStep = 0
        }
        self.currentStep = prvStep
        UIView.animate(withDuration: 0.4, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            let x = CGFloat(nextStep) * self.stepLength
            self.sliderView.frame = CGRect(x: x, y: 0, width: self.stepLength, height: self.frame.size.height)
        }, completion: { (done) in
            if done {
                self.currentStep = nextStep
            }
        })
        
    }
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override func prepareForInterfaceBuilder() {
        
        super.prepareForInterfaceBuilder()
        
        self.showView()
    }

    
}

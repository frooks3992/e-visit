//
//  CustomButton.swift
//  MEFTII
//
//  Created by Schaheer Saleem on 10/12/19.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

@IBDesignable
class CustomButton: UIButton {
    
//    @IBInspectable var drawPath: Bool = false {
//        didSet {
//            if drawPath {
//                self.bounds = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
//
//                let dashedBorder = CAShapeLayer()
//                dashedBorder.strokeColor = UIColor.black.cgColor
//                dashedBorder.lineDashPattern = [7, 3]
//                dashedBorder.frame = self.bounds
//                dashedBorder.fillColor = UIColor.white.cgColor
//                dashedBorder.path = UIBezierPath(rect: self.bounds).cgPath
//                layer.addSublayer(dashedBorder)
//            }
//        }
//    }
    
    override func layoutSubviews() {
    
        self.bounds = CGRect(x: 0, y: 0, width: self.bounds.width, height: self.bounds.height)
        
        let dashedBorder = CAShapeLayer()
        dashedBorder.strokeColor = UIColor.black.cgColor
        dashedBorder.lineDashPattern = [7, 3]
        dashedBorder.frame = self.bounds
        
        dashedBorder.fillColor = UIColor.clear.cgColor
        dashedBorder.path = UIBezierPath(rect: self.bounds).cgPath
        layer.addSublayer(dashedBorder)
        
        super.layoutSubviews()
    }
    
    
}

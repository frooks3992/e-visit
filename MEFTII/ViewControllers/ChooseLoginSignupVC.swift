//
//  ChooseLoginSignupVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 24/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class ChooseLoginSignupVC: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        // Do any additional setup after loading the view.
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
     */
    
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        
        let vc = ChoosePatientDoctorVC.instantiateFromStoryboard()
        vc.nextVcIsLogin = false
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func loginButtonPressed(_ sender: MFButton) {
        
        let vc = ChoosePatientDoctorVC.instantiateFromStoryboard()
        vc.nextVcIsLogin = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}

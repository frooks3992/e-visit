//
//  SelectTimeVC.swift
//  MEFTII
//
//  Created by Schaheer Saleem on 4/4/20.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import UIKit



class DoctorBookSlotsVC: BaseViewController {
    
    var date:String!
    var doctorId:Int!
    var timeSlots:[timeSlot] = []
    var selectedSlot = 0
    var dateSelected:String!
    var defaultTemplateFlag:String!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var successView: UICollectionView!
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd/MM/YYYY"
        return formatter
    }()
    var timings = [Schedule]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.isLoading()
        // GetgetSchedule(doctorId: doctorId, date: date)
        // GetgetSchedule(doctorId: 4, date: dateSelected)
        
        GetgetSchedule(doctorId: DataManager.shared.currentDoctorId, date: dateSelected)
        successView.isHidden = true
        
        
        //        timings.append(Schedule(time: "08:00", booked: false))
        //        timings.append(Schedule(time: "08:15", booked: true))
        //        timings.append(Schedule(time: "08:30", booked: false))
        //        timings.append(Schedule(time: "08:50", booked: false))
        //
        //        timings.append(Schedule(time: "09:00", booked: true))
        //        timings.append(Schedule(time: "09:15", booked: false))
        //        timings.append(Schedule(time: "09:30", booked: false))
        //        timings.append(Schedule(time: "09:45", booked: false))
        
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 20
        layout.itemSize = UICollectionViewFlowLayout.automaticSize
        layout.estimatedItemSize = CGSize(width: (UIScreen.main.bounds.width / 4) - 16, height: 32)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        
        collectionView.allowsMultipleSelection = true
        collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //GetgetSchedule
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        //        let selectedItems = collectionView.indexPathsForSelectedItems
        //
        //        selectedItems?.forEach { indexPath in
        //            print(timings[indexPath.row])
        //        }
        
        
        //   InsertAppointment(doctorId: DataManager.shared.currentDoctorId, patientId: DataManager.shared.currentPatientId, appointmentSlotId: selectedSlot)
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        // again convert your date to string
        let myStringafd = formatter.string(from: date)
        
        SaveSchedule(doctorId: DataManager.shared.currentDoctorId, defaultTemplateFlag: defaultTemplateFlag, date: dateSelected, timeSlots: timeSlots)
        
        //   self.backButtonPressed(UIButton())
    }
    
    
    @IBAction func reasonPressed(_ sender: Any) {
        
        performSegue(withIdentifier: "toReasonAndSymptoms", sender: nil)
        
    }
    
    
    
}

extension DoctorBookSlotsVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeSlots.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SelectTimeCell.self), for: indexPath) as? SelectTimeCell else {
            return UICollectionViewCell()
        }
        
        
        cell.setup(schedule: timeSlots[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SelectTimeCell.self), for: indexPath) as? SelectTimeCell
        
        
        timeSlots[indexPath.row].isSelected = !(timeSlots[indexPath.row].isSelected)
        
        if timeSlots[indexPath.row].status.lowercased() == "booked"{
            timeSlots[indexPath.row].status = "open".uppercased()
        }
        else{
            timeSlots[indexPath.row].status = "booked".uppercased()
        }
        
        selectedSlot = timeSlots[indexPath.row].id
        
        print(timeSlots[indexPath.row].isSelected)
        collectionView.reloadData()
    }
    
    
    
}
//extension DoctorBookSlotsVC{
//    //MARK: - GetstatesList API
//    func InsertAppointment(doctorId: Int, patientId: Int, appointmentSlotId: Int) {
//        self.isLoading()
//        let request = MeftiiRequests.addAppointment(doctorId: doctorId, patientId: DataManager.shared.currentPatientId, appointmentSlotId: appointmentSlotId)
//        
//        
//        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
//            
//            
//            
//            print(code)
//            
//            if code == 0 {
//                
//                DispatchQueue.main.asyncAfter(deadline: .now()+2) {
//                    self?.isLoadingFinished()
//                    self!.successView.isHidden = false
//                }
//                
//                
//            }
//            
//            }, failureBlock: { [weak self] (error) in
//                self?.showToast(message: error.localizedDescription)
//                // self?.isLoadingFinished()
//        })
//        
//    }
//}
extension DoctorBookSlotsVC{
    //MARK: - GetstatesList API
    func GetgetSchedule(doctorId: Int, date: String) {
        
        let request = MeftiiRequests.getdoctorschedule(doctorId: doctorId, date: date)
        
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            print(code)
            
            if code == 0 {
                
                let data = resp["data"] as! [String:Any]
                let dates = data["scheduleList"] as! [[String:Any]]
                let TemplateFlag = data["defaultTemplateFlag"] as! String
                self!.defaultTemplateFlag = TemplateFlag
                
                
                var timeSlotsArr:[timeSlot] = []
                for date in dates {
                    
                    let id = date["id"] as! Int
                    let status = date["status"] as! String
                    let timeslot = date["timeSlot"] as! String
                    
                    let slot = timeSlot.init(id: id, status: status, timeSlot: timeslot)
                    timeSlotsArr.append(slot)
                    
                }
                
                self!.timeSlots  = timeSlotsArr
                
                if timeSlotsArr.count > 0 {
                    
                    self!.collectionView.isHidden = false
                }
                else{
                    self!.collectionView.isHidden = true
                }
                
                
                
                self!.collectionView.reloadData()
                
            }
            
            }, failureBlock: { [weak self] (error) in
              //  self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                                   
                                                                          })
                // self?.isLoadingFinished()
        })
        
    }
}

extension DoctorBookSlotsVC{
    //MARK: - GetstatesList API
    func SaveSchedule(doctorId: Int, defaultTemplateFlag: String,date:String,timeSlots: [timeSlot] ){
        
        let request = MeftiiRequests.Savedoctorschedule(doctorId: doctorId, defaultTemplateFlag: defaultTemplateFlag,date:date,timeSlots: timeSlots)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            print(code)
            
            if code == 0 {
                
                self?.showAlertWith(title: "Success", message: desc, buttonText: "Ok", callBack: {
                                  self?.navigationController?.popViewController(animated: true)
                                                                          })
                
                
                
              //  self!.showToast(message: "Schedule added succesfully.")
             
                // self!.successView.isHidden = false
                
                //               let data = resp["data"] as! [String:Any]
                //            let dates = data["scheduleList"] as! [[String:Any]]
                //            let TemplateFlag = data["defaultTemplateFlag"] as! String
                //            self!.defaultTemplateFlag = TemplateFlag
                //
                //
                //            var timeSlotsArr:[timeSlot] = []
                //            for date in dates {
                //
                //                let id = date["id"] as! Int
                //                 let status = date["status"] as! String
                //                 let timeslot = date["timeSlot"] as! String
                //
                //                let slot = timeSlot.init(id: id, status: status, timeSlot: timeslot)
                //                timeSlotsArr.append(slot)
                //
                //            }
                //
                //            self!.timeSlots  = timeSlotsArr
                //            self!.collectionView.reloadData()
                
            }
            
            }, failureBlock: { [weak self] (error) in
              //  self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Success", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                              self?.navigationController?.popViewController(animated: true)
                                                                                      })
                
                // self?.isLoadingFinished()
        })
        
    }
}

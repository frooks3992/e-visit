//
//  ScheduleVC.swift
//  MEFTII
//
//  Created by Schaheer Saleem on 4/3/20.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import UIKit
import FSCalendar


var last = ""
var first = ""
class PatientAppointmentVC: BaseViewController {
    
    @IBOutlet weak var nextBtn: MFButton!
    @IBOutlet weak var calendarView: FSCalendar!
    var startTime:String!
    var endTime:String!
    var dateSelected:String!
    var doctorId:Int!
    var dates : [String] = []
    var Currdates : [String] = []
    var today:Date!
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd/MM/YYYY"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        setupCalender()
        
        
        if dates.count > 0 && first != "" && last != ""{
            
            dates.removeAll()
            first = ""
            last = ""
            
            if first == "" {
                self.GetSlotst(startTime: dates.first! , endTime: dates.last!)
            }
            
            // self.GetSlotst(startTime: first , endTime: last)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if first == "" {
            self.GetSlotst(startTime: dates.first ?? "" , endTime: dates.last ?? "")
        }
            
        else{
            
        }
    }
    
    
    func setupCalender(){
        
        nextBtn.isHidden = true
        calendarView.appearance.headerTitleFont = .AppMediumFont(ofSize: 18)
        calendarView.appearance.weekdayFont = .AppMediumFont(ofSize: 14)
        calendarView.appearance.headerDateFormat = "MMMM YYYY"
        calendarView.collectionView.register(UINib(nibName: "CalenderCell", bundle: nil), forCellWithReuseIdentifier: "CalenderCell")
    }
    
    
    
    //MARK:- VC next action
    
    @IBAction func nextTapped(_ sender: Any) {
        //        let vc = SelectTimeVC.instantiateFromStoryboard("Schedule")
        //        self.navigationController?.pushViewController(vc, animated: true)
        
        
        performSegue(withIdentifier: "ToSlots", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVc = segue.destination as? PatientBookSlotsVC {
            
            destinationVc.dateSelected = dateSelected
            destinationVc.doctorId = doctorId
            
        }
        
        
        
    }
    
    
    //MARK:- Calendar  previous action
    @IBAction func calendarPreviousTapped(_ sender: Any) {
        //   dates.removeAll()
        dates.removeAll()
        Currdates.removeAll()
        let prevMonth = Calendar.current.date(byAdding: .month, value: -1, to: calendarView.currentPage)
        calendarView.setCurrentPage(prevMonth!, animated: true)
        
        
        calendarView.collectionView.reloadData()
        self.GetSlotst(startTime: first , endTime: last)
        //
    }
    
    @IBAction func calendarNextTapped(_ sender: Any) {
        dates.removeAll()
        Currdates.removeAll()
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: calendarView.currentPage)
        calendarView.setCurrentPage(nextMonth!, animated: true)
        
        
        calendarView.collectionView.reloadData()
        self.GetSlotst(startTime: first , endTime: last)
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        first = ""
        last = ""
        dates.removeAll()
        Currdates.removeAll()
        self.navigationController?.popViewController(animated: true)
    }
    
    
}



extension PatientAppointmentVC: FSCalendarDelegate, FSCalendarDataSource {
    
    //    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
    //        return isDateEnabled(date) ? .black : .lightGray
    //    }
    
    
    
    private func isDateEnabled(_ date: Date, at monthPosition: FSCalendarMonthPosition = .current) -> Bool {
        
        
        let todaydate = Date()
        
        if (date < todaydate){
            let currentDate = self.dateFormatter.string(from: date)
            if dates.contains(currentDate) {
                
                return true
            }
            
            
            return false
        }
        
        
        
        return true
        
    }
    
    
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        
        today = date
        let cell = calendar.cell(for: date, at: monthPosition)
        
        cell?.appearance.selectionColor = .none
        //  cell?.appearance.titleSelectionColor = UIColor.white
        //  cell!.titleLabel.font = UIFont(name: "Montserrat-Medium", size: 15.0)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        
        let myStringafd = formatter.string(from: date)
        
        dateSelected = myStringafd
        
        // nextBtn.isHidden = false
        dates.removeAll()
        dates = Currdates
        performSegue(withIdentifier: "ToSlots", sender: nil)
        //self.calendarView.collectionView.reloadData()
    }
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    
    
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        cell.titleLabel.font = UIFont(name: "Montserrat-Medium", size: 15.0)
        
        
        if isDateEnabled(date){
            
            let dt = self.dateFormatter.string(from: date)
            
            if dates.contains(dt) {
                
                cell.shapeLayer.fillColor = UIColor.MFGreen.cgColor
                cell.titleLabel.textColor = UIColor.white
                 cell.isUserInteractionEnabled = true
            }
            else {
                cell.shapeLayer.fillColor = UIColor.white.cgColor
                cell.titleLabel.textColor = UIColor.gray
                cell.isUserInteractionEnabled = false
            }
        }
            
        else{
            
            cell.titleLabel.textColor = UIColor.lightGray
            // }
            cell.shapeLayer.fillColor = UIColor.clear.cgColor
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        // again convert your date to string
        let myStringafd = formatter.string(from: date)
        
        dates.append(myStringafd)
        // print(monthPosition.rawValue)
    }
    
    
    
    
    
}



extension PatientAppointmentVC{
    //MARK: - GetstatesList API
    func GetSlotst(startTime:String,endTime:String) {
        self.isLoading()
        Currdates.removeAll()
        dates.removeAll()
        let request = MeftiiRequests.getavailableappointmentdates(doctorId: doctorId, startDate: startTime, endDate: endTime)
        
        
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            if code == 0 {
                
                let data = resp["data"] as! [String:Any]
                let datess = data["availableAppointmentDatesList"] as! [String]
                
                
                self!.Currdates = datess
                self?.dates = self!.Currdates
                first = startTime
                last = endTime
                self!.calendarView.collectionView.reloadData()
                self?.isLoadingFinished()
            }
            
            }, failureBlock: { [weak self] (error) in
                // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                    
                })
                // self?.isLoadingFinished()
        })
        
    }
}



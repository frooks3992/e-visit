//
//  SelectTimeVC.swift
//  MEFTII
//
//  Created by Schaheer Saleem on 4/4/20.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import UIKit

struct Schedule {
    var time: String
    var booked: Bool
}

class PatientBookSlotsVC: BaseViewController {
     
    var date:String!
    var doctorId:Int!
    var timeSlots:[timeSlot] = []
    var selectedSlot = 0
    var dateSelected:String!
    var appointmentId:String!
    var appointmentSlotId:Int!
    @IBOutlet weak var collectionView: UICollectionView!
     @IBOutlet weak var successView: UICollectionView!
    
    var timings = [Schedule]()

    override func viewDidLoad() {
        super.viewDidLoad()
      GetgetSchedule(doctorId: doctorId, date: dateSelected)
      //  GetgetSchedule(doctorId: 4, date: dateSelected)
        
      //  GetgetSchedule(doctorId: doctorId, date:dateSelected )
      //  successView.isHidden = true
        
        
//        timings.append(Schedule(time: "08:00", booked: false))
//        timings.append(Schedule(time: "08:15", booked: true))
//        timings.append(Schedule(time: "08:30", booked: false))
//        timings.append(Schedule(time: "08:50", booked: false))
//
//        timings.append(Schedule(time: "09:00", booked: true))
//        timings.append(Schedule(time: "09:15", booked: false))
//        timings.append(Schedule(time: "09:30", booked: false))
//        timings.append(Schedule(time: "09:45", booked: false))

        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 20
        layout.itemSize = UICollectionViewFlowLayout.automaticSize
        layout.estimatedItemSize = CGSize(width: (UIScreen.main.bounds.width / 4) - 16, height: 32)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)

        collectionView.allowsMultipleSelection = true
        collectionView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //GetgetSchedule
    }
    
    @IBAction func nextTapped(_ sender: Any) {
//        let selectedItems = collectionView.indexPathsForSelectedItems
//
//        selectedItems?.forEach { indexPath in
//            print(timings[indexPath.row])
//        }

        
//        InsertAppointment(doctorId: doctorId, patientId: DataManager.shared.currentPatientId, appointmentSlotId: selectedSlot)
        
       // self.successView.isHidden = false
        
     //   self.backButtonPressed(UIButton())
        
        self.insertAppointment(doctorId: doctorId ?? 0, patientId: DataManager.shared.currentPatientId ?? 0, appointmentSlotId: selectedSlot ?? 0)
        
    }
    
    
    @IBAction func reasonPressed(_ sender: Any) {
        
        performSegue(withIdentifier: "toReasonAndSymptoms", sender: nil)
        
    }
    
    
    @IBAction func backPressed(_ sender: Any) {
        
        first = ""
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? AppointmentFourVC {
            
            vc.docId = doctorId
            vc.appointmentSlotId = appointmentSlotId
           // vc.appointmentSlotId = selectedSlot
        }
    }

}

extension PatientBookSlotsVC: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timeSlots.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SelectTimeCell.self), for: indexPath) as? SelectTimeCell else {
            return UICollectionViewCell()
        }

    
        cell.setup(schedule: timeSlots[indexPath.row])

        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: SelectTimeCell.self), for: indexPath) as? SelectTimeCell
        
        
        
        timeSlots[indexPath.row].isSelected = !(timeSlots[indexPath.row].isSelected)
       
        selectedSlot = timeSlots[indexPath.row].id
        
       print(selectedSlot)
        collectionView.reloadData()
    }
    
    
    
}
//extension PatientBookSlotsVC{
//    //MARK: - GetstatesList API
//    func InsertAppointment(doctorId: Int, patientId: Int, appointmentSlotId: Int) {
//           self.isLoading()
//        let request = MeftiiRequests.addAppointment(doctorId: doctorId, patientId: DataManager.shared.currentPatientId, appointmentSlotId: appointmentSlotId)
//        
//      
//        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
//            
//              
//            
//         
//            print(code)
//
//         if code == 0 {
//            
//            DispatchQueue.main.asyncAfter(deadline: .now()+2) {
//                 
//            }
//           
//            
//            
//           
//                      }
//            
//            }, failureBlock: { [weak self] (error) in
//                
//                self?.isLoadingFinished()
//                         self!.successView.isHidden = false
//                self?.showToast(message: error.localizedDescription)
//                // self?.isLoadingFinished()
//        })
//        
//    }
//}
extension PatientBookSlotsVC{
    //MARK: - GetstatesList API
    func GetgetSchedule(doctorId: Int, date: String) {
        
        let request = MeftiiRequests.getschedule(doctorId: doctorId, date: date)
        
         self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
             self?.isLoadingFinished()
            
            print(code)

         if code == 0 {

               let data = resp["data"] as! [String:Any]
            let dates = data["scheduleList"] as! [[String:Any]]
                
            var timeSlotsArr:[timeSlot] = []
            for date in dates {
                
                let id = date["id"] as! Int
                 let status = date["status"] as! String
                 let timeslot = date["timeSlot"] as! String
                
                let slot = timeSlot.init(id: id, status: status, timeSlot: timeslot)
                timeSlotsArr.append(slot)
                
            }

            self!.timeSlots  = timeSlotsArr
            
            if timeSlotsArr.count > 0 {
                             
                             self!.collectionView.isHidden = false
                         }
                         else{
                             self!.collectionView.isHidden = true
                
                         }
            self!.collectionView.reloadData()
            
          }
            
            }, failureBlock: { [weak self] (error) in
               // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                   
                                         })
                // self?.isLoadingFinished()
        })
        
    }
    
    func insertAppointment(doctorId: Int, patientId: Int,appointmentSlotId:Int) {
        
        let request = MeftiiRequests.insertAppointment(doctorId: doctorId, patientId: patientId, appointmentSlotId:appointmentSlotId)
        
         self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
             self?.isLoadingFinished()
            
            print(code)

         if code == 0 {
            
            first = ""
            
            let data =  resp["data"] as? [String:Any]
            self!.appointmentSlotId = data!["appointmentId"] as? Int ?? 0

            self?.showAlertWith(title: "Request Sent", message: "Please tell us the reason for visit ", buttonText: "REASON FOR VISIT", callBack: {
                           
                self!.performSegue(withIdentifier: "toReasonAndSymptoms", sender: nil)
                                                 })
                   
            
             
                
            }
            
         else{
            self?.showAlertWith(title: "Error", message: desc, buttonText: "Ok", callBack: {
                                     
                          
                                                           })
            
            }

            
          
            
            }, failureBlock: { [weak self] (error) in
               // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                   
                                         })
                // self?.isLoadingFinished()
        })
        
    }

}

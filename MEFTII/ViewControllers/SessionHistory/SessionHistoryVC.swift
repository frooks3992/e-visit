//
//  SessionHistoryVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 07/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class SessionHistoryVC: BaseViewController {
    
    @IBOutlet weak var tableVw: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

}

extension SessionHistoryVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFTableViewCell else {
            return UITableViewCell()
        }
        
        let row = indexPath.row
        cell.lblUnreadCount?.isHidden = true
        if row == 0 {
            cell.imgProfile?.image = UIImage(named: "home_pic_1")
            cell.lblName.text = "Dr. Strange"
        }
        else if row == 1 {
            cell.imgProfile?.image = UIImage(named: "home_pic_2")
            cell.lblName.text = "Dr. Daniels"
            
        }
        else if row == 2 {
            cell.imgProfile?.image = UIImage(named: "home_pic_3")
            cell.lblName.text = "Dr. Lee"
        }
        else if row == 3 {
            cell.imgProfile?.image = UIImage(named: "home_pic_4")
            cell.lblName.text = "Dr. House"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = SessionDetailVC.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}




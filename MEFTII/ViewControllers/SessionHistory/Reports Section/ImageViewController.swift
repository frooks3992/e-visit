//
//  ImageViewController.swift
//  MEFTII
//
//  Created by Schaheer Saleem on 10/12/19.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var imageToDisplay: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.image = imageToDisplay
    }
    
    @IBAction func closeTapped(_ sender: Any) {
        dismiss(animated: true)
    }
    
}

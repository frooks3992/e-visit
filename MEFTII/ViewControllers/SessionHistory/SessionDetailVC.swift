//
//  SessionDetailVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 07/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class SessionDetailVC: BaseViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    
    var reports: [UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let nib = UINib(nibName: "MFHeaderView", bundle: nil)
        tableVw.register(nib, forHeaderFooterViewReuseIdentifier: "header")
        tableVw.rowHeight = UITableView.automaticDimension
        tableVw.estimatedRowHeight = 60
    }
    
    func showReportZoomed(index:Int) {
        
        print("item clicked", index)
        let imageViewController = ImageViewController.instantiateFromStoryboard()
        imageViewController.imageToDisplay = reports[index]
//        navigationController?.pushViewController(imageViewController, animated: true)
        present(imageViewController, animated: true)
    }

}

extension SessionDetailVC : UITableViewDelegate, UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.section == 3 {
//            return 110
//        }
//
//        return 60
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 4
        }
        else if section == 1 {
            return 2
        }
        else if section == 2 {
            return 1
        } else if section == 3 { //ReportsTableViewCell
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        else if section == 1 {
            return 70
        }
        else if section == 2 {
            return 70
        } else if section == 3 {
            return 70
        }
        return 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? MFHeaderView
        if section == 1 {
            vw?.mainText?.text = "Medicine Prescribed"
            return vw
        }
        else if section == 2 {
            vw?.mainText?.text = "Doctor's Diagnosis"
            return vw
        } else if section == 3 {
            vw?.mainText?.text = "Reports"
            return vw
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let idfier = indexPath.section == 2 ? "bigTextCell":"normalCell"
        
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: idfier) as? MFTableViewCell else {
            return UITableViewCell()
        }
        let row = indexPath.row
        if indexPath.section == 2 {
            cell.lblValue?.text = "Lorem ipsum dolor sit amet, consectetur adipi scing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
        }
        else if indexPath.section == 1 {
            if row == 0 {
                cell.lblName.text = "Antial - 5mg"
                cell.lblValue?.text = "1 Tablet with lunch"
            }
            else if row == 1 {
                cell.lblName.text = "Xyzal - 5mg"
                cell.lblValue?.text = "1 Tablet twice a day"
            }
        }
        else if indexPath.section == 0 {
            if row == 0 {
                cell.lblName.text = "Speciality"
                cell.lblValue?.text = "Allergy & immunology"
            }
            else if row == 1 {
                cell.lblName.text = "Time"
                cell.lblValue?.text = "06:10 PM"
            }
            else if row == 2 {
                cell.lblName.text = "Date"
                cell.lblValue?.text = "7/15/2019"
            }
            else if row == 3 {
                cell.lblName.text = "Fee"
                cell.lblValue?.text = "$50"
            }
        } else if indexPath.section == 3 {
            let reportCell = tableVw.dequeueReusableCell(withIdentifier: "reportsTableViewCell") as! ReportsTableViewCell
            
            reportCell.reports = reports
            
            reportCell.onCellClicked = { (index) in
                self.showReportZoomed(index:index)
            }
            
            reportCell.reloadAllData()
            
            
            return reportCell
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        
    }
}





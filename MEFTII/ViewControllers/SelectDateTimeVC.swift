//
//  SelectDateTimeVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 21/11/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

class SelectDateTimeVC: UIViewController {

    @IBOutlet weak var datePickerView: UIDatePicker!
    
    var minDate : Date? = Date.init(timeIntervalSinceReferenceDate: 0)
    var maxDate : Date?
    var mode : UIDatePicker.Mode = .date
    var closure : ((Date?,String?) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let minDate = minDate {
            datePickerView.minimumDate = minDate
        
        }
        if let maxDate = maxDate {
            datePickerView.maximumDate = maxDate
                datePickerView.setDate(maxDate, animated: false)
        }
        datePickerView.datePickerMode = mode
        if mode == .time {
            datePickerView.minuteInterval = 10
        }
    }
    
    @IBAction func donePressed(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy"
        let dateString = dateFormatter.string(from: datePickerView.date)
        

        
        self.dismiss(animated: true) {
            self.closure?(self.datePickerView.date,dateString)
        }
    }
    
    @IBAction func cancelPressed(_ sender: Any) {
        
        self.dismiss(animated: true) {
            self.closure?(nil,nil)
        }
        
    }
    
    
}

//
//  ForgotPassVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 24/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class ForgotPassVC: BaseViewController {

    var isDoctor: Bool?
    
    @IBOutlet weak var emailTextField: MFTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func submitButtonPressed(_ sender: MFButton) {
        
        let email = emailTextField.text ?? ""
        
        let isEmailValid = Constants.isValidEmail(emailStr: email)
        let role = isDoctor ?? false ? "Doctor" : "Patient"
        
        if isEmailValid {
            forgotPasswordStepOne(email: email, role: role)
        } else {
            //showToast(message: "Email not valid!")
            self.showAlertWith(title: "Error", message: "Email not valid!", buttonText: "Ok", callBack: {
                                                
                                                })
        }
        
    }
    
    //Mark: - Forgot password API
    func forgotPasswordStepOne(email:String, role:String) {
        
        let request = MeftiiRequests.forgotPasswordStepOne(email: email, role: role)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            if code == 0 {
                let vc = ForgotPassTwoVC.instantiateFromStoryboard()
                vc.emailPassed = email
                vc.isDoctor = self?.isDoctor!
                self?.navigationController?.pushViewController(vc, animated: true)
            }
            
            }, failureBlock: { [weak self] (error) in
                //self?.showToast(message: error.localizedDescription)
               self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                      
                                         })
                self?.isLoadingFinished()
        })
        
    }

}


class ForgotPassTwoVC: BaseViewController {
    
    @IBOutlet weak var otpTextField: MFTextField!
    @IBOutlet weak var confirmPasswordField: MFTextField!
    @IBOutlet weak var passwordTextField: MFTextField!
    
    var emailPassed: String?
    var isDoctor: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func submitButtonPressed(_ sender: MFButton) {
        
        let password = passwordTextField.text ?? ""
        let confirmPassword = confirmPasswordField.text ?? ""
        let email = emailPassed ?? ""
        let role = isDoctor ?? false ? "Doctor" : "Patient"
        let otp = otpTextField.text ?? ""
        
        guard password.count > 0 else {
//            showToast(message: "Password cannot be empty ⚠️")
            
            self.showAlertWith(title: "Warning", message: "Password cannot be empty", buttonText: "Ok", callBack: {
            })
            
            return
        }
        
        guard confirmPassword.count > 0 else {
          //  showToast(message: "Confirm Password cannot be empty ⚠️")
            self.showAlertWith(title: "Warning", message: "Confirm Password cannot be empty", buttonText: "Ok", callBack: {
                      })
            return
        }
        
        if password.count != confirmPassword.count {
          //  showToast(message: "Passwords do not match ⚠️")
            self.showAlertWith(title: "Warning", message: "Passwords do not match", buttonText: "Ok", callBack: {
                               })
            return
        }
        
        guard otp.count > 0 else {
          //  showToast(message: "OTP cannot be empty ⚠️")
               self.showAlertWith(title: "Warning", message: "OTP cannot be empty", buttonText: "Ok", callBack: {
                                        })
            return
        }
        
        updatePasswordSecndStep(email: email, role: role, password: password, confirmPass: confirmPassword, otp: otp)
        
    }
    
    //Mark: - Forgot password step 2 API
    func updatePasswordSecndStep(email:String, role:String, password:String, confirmPass:String, otp: String) {
        
        let request = MeftiiRequests.updatePasswordSecndStep(email: email, role: role, password: password, confirmPass: confirmPass, otp: otp
        )
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            if code == 0 {
                self?.showAlertWith(title: "SUCCESS", message: desc , buttonText: "OK", callBack: {
                    let vcs = self?.navigationController?.viewControllers ?? []
                    let vc = vcs[vcs.count-3]
                    self?.navigationController?.popToViewController(vc, animated: true)
                    
                })
            }
            
            }, failureBlock: { [weak self] (error) in
                self?.showToast(message: error.localizedDescription)
                self?.isLoadingFinished()
        })
        
    }
    
}


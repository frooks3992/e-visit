//
//  TimeoutManager.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 05/12/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation


@objc
@objcMembers
class TimeOutManager : NSObject {
    
    static let shared = TimeOutManager(300)
    
    var interval:TimeInterval
    weak var timer:Timer?
    
    private init(_ timeInterval:TimeInterval) {
        interval = timeInterval
        timer = Timer()
        super.init()
        resetTimer()
    }
    
    
    func resetTimer() {
        
        timer?.invalidate()
        timer = nil
        self.timer = Timer.scheduledTimer(timeInterval: self.interval, target: self,
        selector: #selector(self.gotoLoginViewController(_:)), userInfo: nil, repeats: false)
        
    }
    
    func gotoLoginViewController(_ sender: Any?) {
        
        let role = DataManager.shared.currentRole ?? ""
        
        if role == "doctor", let _ = DataManager.shared.currentDoctor {
            
            DispatchQueue.main.async {
                DataManager.shared.logoutCurrentUser()
            }
            
        }
        else if role == "patient", let _ = DataManager.shared.currentPatient {

            DispatchQueue.main.async {
                DataManager.shared.logoutCurrentUser()
            }
    
        }
        else {
            
            
        }
        
        
    }
    
    

}

//
//  MessageDetailVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 07/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit
import ROCController
import RealmSwift
import IQKeyboardManagerSwift

class ChatDetailVC: ROCBaseController<MWChatMessage> {
    
    let conversation: MWConversation
    var largeTitleState = false
    
    init(conversation: MWConversation){
        
        self.conversation = conversation
        let chatMessages = conversation.chatMessages.sorted(byKeyPath: "timestamp", ascending: true)
        super.init(results: chatMessages)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.conversation.displayName
        // Do any additional setup after loading the view, typically from a nib.
        
        
        if let backItem = self.navigationController?.navigationBar.backItem {
            backItem.title = "BACK"
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        super.viewWillAppear(animated)
        largeTitleState = self.navigationController?.navigationBar.prefersLargeTitles ?? false
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.navigationBar.prefersLargeTitles = largeTitleState
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func sendButtonDidTap(text: String) {
        let sampleChatMessage = MWChatMessage()
        sampleChatMessage.senderUserName = PushNotificationManager.shared.userName
        sampleChatMessage.userId = ""
        sampleChatMessage.text = text
        
        MWChatHandler.shared.sendMessage(to: conversation.conversationId, message: sampleChatMessage)
        
    }
    
    override func attachmentButtonDidTapped() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Take a Picture", style: .default, handler: { [weak self] (_) in
            
        }))
        alertController.addAction(UIAlertAction(title: "Choose from Library", style: .default, handler: { [weak self] (_) in
            
        }))
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }

}

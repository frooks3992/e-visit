//
//  FindDoctorMapVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 07/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

class OnCallDoctorsVC: BaseViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var AddButton: UIButton!
    
    var searchTimer: Timer?
    var doctorlist: [Profile_d3] = []
    var docArrayToShow: [Profile_d3] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableVw.tableFooterView = UIView()
        self.searchField.delegate = self
        self.searchField.addTarget(self, action: #selector(textFieldDidEditingChanged(_:)), for: .editingChanged)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchOnCallDoctorsAndReload()
        
        tableVw.reloadData()
    }
    
    
    func fetchOnCallDoctorsAndReload() {
        
        self.isLoading()
        let request = MeftiiRequests.getaddeddoctors(userName: DataManager.shared.currentPatientId)
        request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            
            self?.isLoadingFinished()
            print(codeTwo)
            print(respTwo)
            
            if codeTwo == 0{
                if let someObj = respTwo["data"] {
                    
                    do {
                        let data = try JSONSerialization.data(withJSONObject: someObj!["doctorProfileList"]!, options: .prettyPrinted)
                        let arr = try JSONDecoder().decode([Profile_d3].self, from: data)
                        self?.docArrayToShow = arr
                        self?.doctorlist = arr
                        
                        print(self!.docArrayToShow.count)
                    }
                    catch {
                        
                    }
                    
                    if self!.docArrayToShow.count > 0 {
                        self!.tableVw.isHidden = false
                    }
                    else{
                        self!.tableVw.isHidden = true
                    }
                    self?.tableVw.reloadData()
                }
            }
            
            }, failureBlock: nil)
    }
    
    
    
    func deleteOnCallDoctor(_ onCallDocId:Int) {
        self.isLoading()
        let regst = MeftiiRequests.deleteOnCallDoctor2(for: DataManager.shared.currentPatientId, onCallDocId: onCallDocId)
        regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            self?.isLoadingFinished()
           // self?.showToast(message: descTwo)
            
            self?.showAlertWith(title: "Success", message: descTwo, buttonText: "OK", callBack: {
                
            })
            }, failureBlock: nil)
          
    }
    
    
    @IBAction func addDoctorButtonPressed(_ sender: UIButton) {
        
        let vc = AddOnCallDoctorsVC.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

extension OnCallDoctorsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docArrayToShow.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFTableViewCell else {
            return UITableViewCell()
        }
        
        let doctor = docArrayToShow[indexPath.row]
        
        let urlll = URL(string: doctor.profilePicture ?? "")
        cell.imgProfile?.sd_setImage(with: urlll, placeholderImage: setDummyImage(gender: doctor.gender!))
        
        cell.lblName.text = "Dr. \(doctor.completeName)"
        cell.lblValue?.text = "Specialty: " + (doctor.speciality ?? "")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        true
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//
//            self.showAlertWith(title: "Confirmation required", message: "Are you sure you want to delete Dr." + "\(docArrayToShow[indexPath.row].completeName)", buttonText: "YES", callBack: {
//                let docToRemove = self.docArrayToShow[indexPath.row]
//                if let idx = self.docArrayToShow.firstIndex(of: docToRemove) {
//                    self.docArrayToShow.remove(at: idx)
//                }
//                if let idx = self.doctorlist.firstIndex(of: docToRemove) {
//
//
//
//                    //  doctorlist.remove(at: idx)
//                }
//                tableView.beginUpdates()
//                tableView.deleteRows(at: [indexPath], with: .automatic)
//                tableView.endUpdates()
//
//
//                self.deleteOnCallDoctor(docToRemove.id ?? 0)
//            }, leftButtonText: "NO") {
//                print("cancel")
//            }
//
//
//
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
//        }
//    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                // delete the item here
              
                
                self.showAlertWith(title: "Confirmation required", message: "Are you sure you want to delete Dr." + "\(self.docArrayToShow[indexPath.row].completeName)", buttonText: "YES", callBack: {
                                let docToRemove = self.docArrayToShow[indexPath.row]
                                if let idx = self.docArrayToShow.firstIndex(of: docToRemove) {
                                    self.docArrayToShow.remove(at: idx)
                                }
                                if let idx = self.doctorlist.firstIndex(of: docToRemove) {
                
                
                
                                    //  doctorlist.remove(at: idx)
                                }
                                tableView.beginUpdates()
                                tableView.deleteRows(at: [indexPath], with: .automatic)
                                tableView.endUpdates()
                
                
                                self.deleteOnCallDoctor(docToRemove.id ?? 0)
                            }, leftButtonText: "NO") {
                                print("cancel")
                                completionHandler(true)
                            }
                
            }
            deleteAction.image = #imageLiteral(resourceName: "trash-delete")
            deleteAction.backgroundColor = UIColor(red: 222/255, green: 53/255, blue: 53/255, alpha: 1)
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            return configuration
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = ViewDoctorVC.instantiateFromStoryboard()
        vc.doctor = docArrayToShow[indexPath.row]
        
        vc.isOnCallDoc = true
        navigationController?.pushViewController(vc, animated: true)
    }
}





extension OnCallDoctorsVC : UITextFieldDelegate {
    
    @objc func textFieldDidEditingChanged(_ textField: UITextField) {
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }
        searchTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text ?? "", repeats: false)
    }
    
    @objc func searchForKeyword(_ timer: Timer) {
        let keyword = ((timer.userInfo as? String) ?? "").lowercased()
        if keyword.count > 0 {
            docArrayToShow = doctorlist.filter({ ($0.completeName.lowercased()).contains(keyword) }) ?? []
        }
        else {
            docArrayToShow = doctorlist
        }
        tableVw.reloadData()
    }
    
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
           
           
           if textField.text!.containsWhitespace{
               
            if string == " " && textField.text?.last == " "{
                     return false
               }
    
        }
        return true
    }
    
}

//
//  SetOnCallDocTimingVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 26/03/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import UIKit

class SetOnCallDocTimingVC: BaseViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    
    var differentTimes = true
    var onCallDoctor: Profile_d3?
    var timings: [Timings] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let nib = UINib(nibName: "ProfileHeaderView", bundle: nil)
//        tableVw.register(nib, forHeaderFooterViewReuseIdentifier: "header")
//        if let timin = onCallDoctor?.timings, timin.count > 6 {
//            timings = timin
//        } else {
          reInitTimingObjects()
//        }
        
        tableVw.reloadData()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        saveOnCallDoctorTimings()
    }

    @objc func togglwSwitcthChanged(_ sender: UISwitch) {
        
        differentTimes = sender.isOn
        tableVw.reloadSections(IndexSet(integer: 0), with: .automatic)
        
    }
    
    func reInitTimingObjects() {
        if differentTimes {
            timings = []
            for i in 0..<7 {
                let tim = Timings()
                tim.from = "2019-12-10 00:01:00"
                tim.to = "2019-12-10 23:55:00"
                tim.from = "00:01"
                tim.to = "23:55"
                switch i {
                case 0:
                    tim.day = "Monday"
                case 1:
                    tim.day = "Tuesday"
                case 2:
                    tim.day = "Wednesday"
                case 3:
                    tim.day = "Thursday"
                case 4:
                    tim.day = "Friday"
                case 5:
                    tim.day = "Saturday"
                default:
                    tim.day = "Sunday"
                }
                timings.append(tim)
            }
        }
        else {
            timings = []
            let tim = Timings()
            tim.from = "00:01"
            tim.to = "23:55"
            tim.day = ""
            for _ in 0..<7 {
                timings.append(tim)
            }
        }
    }
    
    
    func saveOnCallDoctorTimings() {
//        let request = MeftiiRequests.updateTimingFor(for: String(DataManager.shared.currentDoctorId), onCallDocId: onCallDoctor?.id, timings: timings)
//        request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
//            if codeTwo == 0 {
//                self?.showToast(message: "Updated successfully!")
//            }
//        }, failureBlock: nil)
    }

}

extension SetOnCallDocTimingVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if timings.count > 0 {
            return timings.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as! MFHeaderView
        vw.toggleSwitch?.isOn = differentTimes
        vw.toggleSwitch?.isUserInteractionEnabled = true
        vw.toggleSwitch?.addTarget(self, action: #selector(togglwSwitcthChanged(_:)), for: .valueChanged)
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let row = indexPath.row
        
        
        if differentTimes && row < timings.count {
            
            guard let cell =  tableView.dequeueReusableCell(withIdentifier: "editableDayCell") as? MFTableViewCell else {
                return UITableViewCell()
            }
            cell.setTimingForCell(timing: timings[row], day: timings[row].day, vc: self)
            return cell

        }
        else if row == 0 {
            
            guard let cell =  tableView.dequeueReusableCell(withIdentifier: "editableOneDayCell") as? MFTableViewCell else {
                return UITableViewCell()
            }
            cell.setTimingForCell(timing: timings[row], day: "", vc: self)
            return cell
            
        }
        return UITableViewCell()
            
  
        
    }
    
    
}

//
//  FindDoctorMapVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 07/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

class AddOnCallDoctorsVC: BaseViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var searchField: UITextField!
    
    var searchTimer: Timer?
    var doctorlist: [Profile_d3] = []
    var docArrayToShow: [Profile_d3] = []
    var newAdded:[Int] = []
    var status: [String:Bool] = [:]

    @IBOutlet weak var savebtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        //savebtn.isHidden = true
        self.searchField.delegate = self
        self.searchField.addTarget(self, action: #selector(textFieldDidEditingChanged(_:)), for: .editingChanged)
        
        fetchAllDoctorsAndReload()
        tableVw.reloadData()
        
    }
    
    
    func validateCells(rows:Int){
      //  status.removeAll()
        

    }
    
    @IBAction func savePressed(_ sender: Any) {
        
        if newAdded.count > 0 {
            addDoctors()
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
       
      
        
    }
    
    func addDoctors() {
        
        self.isLoading()
        let request = MeftiiRequests.addDoctors(userName: DataManager.shared.currentPatientId, doctorIdsList: newAdded)
          request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
              
              print(respTwo)
              
              if codeTwo == 0{
                 self?.showAlertWith(title: "Success", message: descTwo, buttonText: "OK", callBack: {
                           
                    DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                                       self?.isLoadingFinished()
                                       self?.navigationController?.popViewController(animated: true)
                                   }
                          })
                
               
              }
              
              }, failureBlock: nil)
      }
      
      
    
    func fetchAllDoctorsAndReload() {
        let request = MeftiiRequests.getAllDoctorList2(userName: DataManager.shared.currentPatientId)
        
        self.isLoading()
        request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            
            
            print(codeTwo)
            print(respTwo)
            
            self?.isLoadingFinished()
            
            if codeTwo == 0{
                if let Obj = respTwo["data"] {
                    
                    do {
                        let someObj = Obj!["doctorProfileList"]
                            
                        
                        
                        let data = try JSONSerialization.data(withJSONObject: someObj!, options: .prettyPrinted)
                        let arr = try JSONDecoder().decode([Profile_d3].self, from: data)
                        self?.docArrayToShow = arr
                        self?.doctorlist = arr
                        
                        print(self!.docArrayToShow.count)
                        self?.validateCells(rows: self!.docArrayToShow.count)
                    }
                    catch {
                        
                    }
                    
                    if self!.docArrayToShow.count > 0 {
                        self!.tableVw.isHidden = false
                    }
                    else{
                        self!.tableVw.isHidden = false
                    }
                    
                    self?.tableVw.reloadData()
                }
            }
            
            }, failureBlock: nil)
    }
    
    
    
    func addOnCallDoctor(_ onCallDocId:String) {
        self.isLoading()
        let regst = MeftiiRequests.addOnCallDoctor(for: String(DataManager.shared.currentDoctorId), onCallDocId: onCallDocId)
        regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
           // self?.showToast(message: descTwo)
            self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Okay", callBack: {
                              //                    let vc  = LoginVC.instantiateFromStoryboard()
                              self?.navigationController?.popViewController(animated: true)
                          })
            
            self?.isLoadingFinished()
            }, failureBlock: nil)
    }
    
    
    @objc func addDoctorButtonPressed(_ sender: UIButton) {
        
        savebtn.isHidden = false
        let idx = sender.tag
        let cell = tableVw.cellForRow(at: IndexPath(item: sender.tag, section: 0)) as! MFTableViewCell
        
        let docToAdd = docArrayToShow[idx]
        
        let id = docToAdd.id!
        
        if newAdded.contains(id){
        
              let urlll = URL(string: docToAdd.profilePicture ?? "")
            cell.imgProfile?.sd_setImage(with: urlll, placeholderImage: setDummyImage(gender: docToAdd.gender!))
            cell.btnCall?.backgroundColor = UIColor.MFBlue
                                              cell.btnCall?.setTitle("Add", for: .normal)
            newAdded = newAdded.filter{($0 != id)}
           
        }
            
        else{
           cell.btnCall?.backgroundColor = UIColor.MFDarkRed
                                          cell.btnCall?.setTitle("Added", for: .normal)
            newAdded.append(id)
          
        }
        
        //addOnCallDoctor(String(docToAdd.id ?? 0))
        
        print(newAdded)
        
       // tableVw.reloadData()
        
    }
    
}

extension AddOnCallDoctorsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docArrayToShow.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFTableViewCell else {
            return UITableViewCell()
        }
        
        let doctor = docArrayToShow[indexPath.row]
        
        let urlll = URL(string: doctor.profilePicture ?? "")
        
        cell.imgProfile?.sd_setImage(with: urlll, placeholderImage: setDummyImage(gender: (doctor.gender)!))
        
        cell.lblName.text = "Dr. \(doctor.completeName)"
        cell.lblValue?.text = "Specialty: " + (doctor.speciality ?? "")
        cell.btnCall?.tag = indexPath.row
        
//        if newAdded.count > 0 {
//
//            if newAdded.contains(doctor.id!){
//
//                cell.btnCall?.backgroundColor = UIColor.MFDarkRed
//                cell.btnCall?.setTitle("Added", for: .normal)
//            }
//
////            else{
////                cell.btnCall?.backgroundColor = UIColor.MFBlue
////                cell.btnCall?.setTitle("Add", for: .normal)
////            }
//
//        }
        
        
        cell.btnCall?.addTarget(self, action: #selector(addDoctorButtonPressed(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}





extension AddOnCallDoctorsVC : UITextFieldDelegate {
    
    @objc func textFieldDidEditingChanged(_ textField: UITextField) {
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }
        searchTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text ?? "", repeats: false)
    }
    
    @objc func searchForKeyword(_ timer: Timer) {
        let keyword = ((timer.userInfo as? String) ?? "").lowercased()
        
        if keyword.count > 0 {
            
            docArrayToShow = doctorlist.filter{($0.completeName.lowercased()).contains(keyword)}
        }
        else {
            docArrayToShow = doctorlist
            validateCells(rows: docArrayToShow.count)
        }
        
        tableVw.reloadData()
        
    }
     func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
           
           
           if textField.text!.containsWhitespace{
               
            if string == " " && textField.text?.last == " "{
                     return false
               }
    
        }
        return true
    }
    
}

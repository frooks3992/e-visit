//
//  OTPAuthenticationVC.swift
//  MEFTII
//
//  Created by Farhan on 16/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import UIKit

class OTPAuthenticationVC: BaseViewController {

    @IBOutlet weak var TF1: MFTextField!
    @IBOutlet weak var TF2: MFTextField!
    @IBOutlet weak var TF3: MFTextField!
    @IBOutlet weak var TF4: MFTextField!
    var OTP:String!
    @IBOutlet weak var heading: UILabel!
    var msisdn:String!
    var userType:String!
    var verified = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        heading.text = "Enter the four-digit code we sent you on \( msisdn!)"
        
        msisdn = msisdn.replacingOccurrences(of: "-", with: "")
        print(msisdn)
        
        generateOTP(msisdn: msisdn , userTypeId: 1)
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var submitBtn: MFButton!
    
    func autofillOTP(OTP:String){
        
        
//        OTP[0]
//        let OTPNumbers = OTP.components(separatedBy: "")
        
        print(OTP)
        TF1.text = OTP[0]
        TF2.text = OTP[1]
        TF3.text = OTP[2]
        TF4.text = OTP[3]
        
    }
    
    
    
    @IBAction func submitPressed(_ sender: Any) {
        
        if let otp = OTP{
             verifyOTP(msisdn: msisdn, otp: otp)
        }
        
    }
    
    @IBAction func backpressed(_ sender: Any) {
        
        switch userType {
        case "patient":
            performSegue(withIdentifier: "UnwindFromOTP", sender: nil)
        default:
            performSegue(withIdentifier: "UnwindFromOTP2", sender: nil)
        }
        
    }
    

}

extension OTPAuthenticationVC{
    
    
    
      //MARK: - GetstatesList API
    func generateOTP(msisdn:String,userTypeId:Int) {
            
        let request = MeftiiRequests.generateOTP(msisdn:msisdn,userTypeId:userTypeId)
        self.isLoading()
         //   self.isLoading()
            request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
                
               // self?.isLoadingFinished()
                
              
                self?.isLoadingFinished()
                if code == 0 {
                   let data = resp["data"] as! [String:Any]
                                 let otp = data["otp"] as! String
                                 print(otp)
                                 self!.OTP = otp
                                 self!.autofillOTP(OTP: self!.OTP)

                }
                
                }, failureBlock: { [weak self] (error) in
                   // self?.showToast(message: error.localizedDescription)
                    
                    self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                      
                                             })
                   // self?.isLoadingFinished()
            })
            
        }
    
    
    //MARK: - GetstatesList API
     func verifyOTP(msisdn:String,otp:String) {
             
         let request = MeftiiRequests.verifyOTP(msisdn:msisdn,otp:otp)
             
             self.isLoading()
             request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
                 
                 self?.isLoadingFinished()
                 
              
               
                 if code == 0 {
                    
                    self!.verified = true
                    switch self!.userType {
                          case "patient":
                            self!.performSegue(withIdentifier: "UnwindFromOTP", sender: nil)
                          default:
                            self!.performSegue(withIdentifier: "UnwindFromOTP2", sender: nil)
                          }
                    
                 }
                
                 else{
                    
                    print("not verified")
                    
                }
                 
                 }, failureBlock: { [weak self] (error) in
                    // self?.showToast(message: error.localizedDescription)
                    self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                         
                                                                })
                    // self?.isLoadingFinished()
             })
             
         }
     
    
    
}

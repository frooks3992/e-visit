//
//  DoctorProfileTwoVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 29/11/2019.
//  Copyright © 2019 Mian Waqas Umar. All rights reserved.
//

import UIKit

class DoctorProfileTwoVC: UIViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    
    weak var profileVc:ProfileDoctorVC?
    
    var differentTimes = true
    
    var timings: [Timings] = []
    
    var editPressed :Bool {
        get {
            if let vc = profileVc {
                return vc.editPressed
            }
            return false
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableVw.rowHeight = UITableView.automaticDimension
        tableVw.estimatedRowHeight = 70
        
        
        let nib = UINib(nibName: "ProfileHeaderView", bundle: nil)
        tableVw.register(nib, forHeaderFooterViewReuseIdentifier: "header")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        differentTimes = (profileVc?.profileObj.differentTimings ?? "true").lowercased().contains("t")
//        if let timigs = profileVc?.profileObj.timings, timigs.count > 0 {
//            if differentTimes {
//                if timigs.count > 6 {
//                    timings = timigs
//                }
//                else {
//                    timings = []
//                    for _ in 0..<7 {
//                        timings.append(timigs[0])
//                    }
//                }
//            }
//            else {
//                timings = []
//                for _ in 0..<7 {
//                    timings.append(timigs[0])
//                }
//            }
//        }
//        else {
//            reInitTimingObjects()
//        }
        
        
        tableVw.reloadData()
    }
    

    func reloadAllPage()  {
        if tableVw != nil {
            tableVw.reloadData()
        }
    }
    
    
    @objc func togglwSwitcthChanged(_ sender: UISwitch) {
        
//        differentTimes = sender.isOn
//        profileVc?.profileObj.differentTimings = differentTimes ? "true":"false"
//        profileVc?.profileObj.timings = timings
        tableVw.reloadSections(IndexSet(integer: 0), with: .automatic)
        
    }
    
    func getTimingsString(timing:Timings) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        var to = ""
        var from = ""
        
        let apiFormat = DateFormatter()
        apiFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        apiFormat.dateFormat = "HH:mm"
        
        if let toDate = apiFormat.date(from: timing.to ?? "") {
            to = formatter.string(from: toDate)
        }
        
        if let fromDate = apiFormat.date(from: timing.from ?? "") {
            from = formatter.string(from: fromDate)
        }
        
        return "\(from) - \(to)"
        
    }
    
    @objc func toggleFingerPrint(_ sender: UISwitch) {
        DataManager.shared.setFingerPrintOnOff(isOn: sender.isOn)
    }
    
    @objc func toggleUnscheduleCalls(_ sender: UISwitch) {
       // profileVc?.profileObj.unscheduledCalls = sender.isOn ? "true":"false"
    }
    
    
    func reInitTimingObjects() {
        if differentTimes {
            timings = []
            for i in 0..<7 {
                let tim = Timings()
                tim.from = "2019-12-10 00:01:00"
                tim.to = "2019-12-10 23:55:00"
                tim.from = "00:01"
                tim.to = "23:55"
                switch i {
                case 0:
                    tim.day = "Monday"
                case 1:
                    tim.day = "Tuesday"
                case 2:
                    tim.day = "Wednesday"
                case 3:
                    tim.day = "Thursday"
                case 4:
                    tim.day = "Friday"
                case 5:
                    tim.day = "Saturday"
                default:
                    tim.day = "Sunday"
                }
                timings.append(tim)
            }
        }
        else {
            timings = []
            let tim = Timings()
            tim.from = "00:01"
            tim.to = "23:55"
            tim.day = ""
            for _ in 0..<7 {
                timings.append(tim)
            }
        }
    }
    

}

extension DoctorProfileTwoVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if timings.count > 0 {
//            return timings.count + 3
//        }
//        return 0
        return 6
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let vw = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as! MFHeaderView
//        vw.toggleSwitch?.isOn = differentTimes
//        vw.toggleSwitch?.isUserInteractionEnabled = editPressed
//        vw.toggleSwitch?.addTarget(self, action: #selector(togglwSwitcthChanged(_:)), for: .valueChanged)
//        return vw
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 100
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let row = indexPath.row
        
        let idfier = "normalCell"
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: idfier) as? MFTableViewCell else {
            return UITableViewCell()
        }
        if row == 0{
            cell.setProfileCell(label: "", value: profileVc?.profileObj.speciality ?? "", row: row)
             return cell
        }else if row == 1{
            cell.setProfileCell(label: "", value: profileVc?.profileObj.deaNumber ?? "", row: row)
             return cell
        }else if row == 2{
            cell.setProfileCell(label: "", value: profileVc?.profileObj.nationalProviderIdentifierNumber ?? "", row: row)
             return cell
        }else if row == 3{
            cell.setProfileCell(label: "", value: profileVc?.profileObj.hospitalAffiliation ?? "", row: row)
             return cell
        }else if row == 4{
            cell.setProfileCell(label: "", value: profileVc?.profileObj.stateLicenseNumber ?? "", row: row)
             return cell
        }else if row == 5{
            cell.setProfileCell(label: "", value: profileVc?.profileObj.socialSecurityNumber ?? "", row: row)
             return cell
        }
        return UITableViewCell()
        
        
        
//        if differentTimes {
//
//            if row < timings.count {
//                let idfier = editPressed ? "editableDayCell":"normalCell"
//                guard let cell =  tableView.dequeueReusableCell(withIdentifier: idfier) as? MFTableViewCell else {
//                    return UITableViewCell()
//                }
//                cell.setTimingForCell(timing: timings[row], day: timings[row].day, vc: profileVc)
//                return cell
//            }
//            else if row == timings.count {
//                let idfierr = editPressed ? "editableCell":"priceCell"
//                let priceCell = tableView.dequeueReusableCell(withIdentifier: idfierr) as! MFTableViewCell
//                let ttext = "$\(profileVc?.profileObj.pricePerSession ?? "")"
//                priceCell.setProfileCell(label: "Price per Session", value:ttext, row: row)
//                priceCell.textField?.isUserInteractionEnabled = true
//                priceCell.textField?.isEnabled = editPressed
//                priceCell.textField?.textFieldDelegate = self
//                return priceCell
//            }
//            else if row == timings.count + 1 {
//                let priceCell = tableView.dequeueReusableCell(withIdentifier: "callsCell") as! MFTableViewCell
//                priceCell.lblName.text = "Unscheduled Calls"
//                priceCell.toggleSw?.isOn = (profileVc?.profileObj.unscheduledCalls ?? "false").lowercased().contains("t")
//                priceCell.toggleSw?.addTarget(self, action: #selector(toggleUnscheduleCalls(_:)), for: .valueChanged)
//                priceCell.toggleSw?.isUserInteractionEnabled = editPressed
//                return priceCell
//            }
//            else {
//                return UITableViewCell()
//            }
//        }
//        else {
//            if row == 0 {
//                let idfier = editPressed ? "editableOneDayCell":"normalCell"
//                guard let cell =  tableView.dequeueReusableCell(withIdentifier: idfier) as? MFTableViewCell else {
//                    return UITableViewCell()
//                }
//                cell.setTimingForCell(timing: timings[row], day: "", vc: profileVc)
//                return cell
//            }
//            else if row == 1 {
//                let idfierr = editPressed ? "editableCell":"priceCell"
//                let priceCell = tableView.dequeueReusableCell(withIdentifier: idfierr) as! MFTableViewCell
//                let ttext = "$\(profileVc?.profileObj.pricePerSession ?? "")"
//                priceCell.setProfileCell(label: "Price per Session", value:ttext, row: row)
//                priceCell.textField?.isUserInteractionEnabled = true
//                priceCell.textField?.isEnabled = editPressed
//                priceCell.textField?.textFieldDelegate = self
//                return priceCell
//            }
//            else if row == 2 {
//                let priceCell = tableView.dequeueReusableCell(withIdentifier: "callsCell") as! MFTableViewCell
//                priceCell.lblName.text = "Unscheduled Calls"
//                priceCell.toggleSw?.isOn = (profileVc?.profileObj.unscheduledCalls ?? "false").lowercased().contains("t")
//                priceCell.toggleSw?.addTarget(self, action: #selector(toggleUnscheduleCalls(_:)), for: .valueChanged)
//                priceCell.toggleSw?.isUserInteractionEnabled = editPressed
//                return priceCell
//            }
//            else if row == 3 {
//                let priceCell = tableView.dequeueReusableCell(withIdentifier: "callsCell") as! MFTableViewCell
//                priceCell.lblName.text = "FingerPrint Login"
//                priceCell.toggleSw?.isOn = profileVc?.profileObj.isFingerPrintOn ?? false
//                priceCell.toggleSw?.addTarget(self, action: #selector(toggleFingerPrint(_:)), for: .valueChanged)
//                return priceCell
//            }
//            else {
//                return UITableViewCell()
//            }
//        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}

extension DoctorProfileTwoVC : UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        let str:String = textField.text ?? ""
        let textt = str.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        if differentTimes && textField.tag == timings.count {
           // profileVc?.profileObj.pricePerSession = textt
        }
        else if textField.tag == 1 {
            // profileVc?.profileObj.pricePerSession = textt
        }
    }
 

    
}




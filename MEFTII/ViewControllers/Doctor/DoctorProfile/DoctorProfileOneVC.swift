//
//  DoctorProfileOneVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 29/11/2019.
//  Copyright © 2019 Mian Waqas Umar. All rights reserved.
//

import UIKit
import SDWebImage

class DoctorProfileOneVC: UIViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    
    weak var profileVc:ProfileDoctorVC?
    
    var editPressed : Bool {
        get {
            if let vc = profileVc {
                return vc.editPressed
            }
            return false
        }
    }
    
    var profileImage : UIImage?
    
    let dateFormatter = DateFormatter()
    
    var birthDateString = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableVw.rowHeight = UITableView.automaticDimension
        tableVw.estimatedRowHeight = 60
        
        dateFormatter.dateFormat = "MM-dd-yyyy"
        
        let imageVw = UIImageView()
//        let urlll = URL(string: Constants.getProfileImageUrl(userName: (profileVc?.profileObj.profilePicture ?? "")))
//        let urlll = URL(string: profileVc?.profileObj.profilePicture ?? "")
//        print(Constants.getProfileImageUrl(userName: (profileVc?.profileObj.profilePicture ?? "")))
//        imageVw.sd_setImage(with: urlll) { (image, error, cacheType, url) in
//            self.profileImage = image
//            self.tableVw.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
//        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let imageVw = UIImageView()
     let urlll = URL(string: profileVc?.profileObj.profilePicture ?? "")
        print(Constants.getProfileImageUrl(userName: (profileVc?.profileObj.profilePicture ?? "")))
        imageVw.sd_setImage(with: urlll) { (image, error, cacheType, url) in
            self.profileImage = image
            self.tableVw.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
        }
        
        if let date = dateFormatter.date(from: profileVc?.profileObj.deaNumber ?? "") {
            let formater = DateFormatter()
            formater.dateFormat = Constants.kHomeDateFormate
            birthDateString = formater.string(from: date)
        }
        
        tableVw.reloadData()
    }
    
 

    func reloadAllPage()  {
        if tableVw != nil {
            tableVw.reloadData()
        }
    }
    
    
    @IBAction func UrgentCallSwitch(_ sender: UISwitch) {
        
        if let doc = DataManager.shared.currentDoctor{
        if sender.tag == 10 {
         
            if sender.isOn {
            
            
            doc.urgentCallFlag = "1"
        
            }
            
            else{
                 doc.urgentCallFlag = "0"
            }
            
            DataManager.shared.saveLoggedInDoctor(profile: doc)
            
        }
        }
        
    }
    
    
    

}

extension DoctorProfileOneVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        dismiss(animated: true, completion: nil)
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        profileImage = image
        tableVw.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
    }
    
}


extension DoctorProfileOneVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
         let row = indexPath.row

        if row == 0 {
            let celll = tableView.dequeueReusableCell(withIdentifier: "pictureCell") as! MFTableViewCell
           // celll.lblName.text = "Picture"
            
            
            if let pic = profileVc?.profileObj.profilePicture {
                celll.imgProfile?.sd_setImage(with: URL(string: (profileVc?.profileObj.profilePicture)!), placeholderImage: setDummyImage(gender: (profileVc?.profileObj.gender)!))
            }
            else{
                
                celll.imgProfile?.image = setDummyImage(gender: (profileVc?.profileObj.gender) ?? "")
            }
            
            
       
            celll.imgProfile?.isUserInteractionEnabled = self.editPressed
            celll.imageTapped = {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = .photoLibrary
                
                self.present(imagePickerController, animated: true, completion: nil)
            }
            celll.bottomLineView?.isHidden = self.editPressed
            celll.strongLineView?.isHidden = !self.editPressed
            celll.setNeedsLayout()
            return celll
        }
        else if row == 1{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.firstName ?? "") \(profileVc?.profileObj.lastName ?? "")", row: row)
            return cell
        }
//        else if row == 2{
//             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
//            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.completeName ?? "")", row: row)
//            return cell
//        }
        else if row == 2{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.gender ?? "Male")", row: row)
            return cell
        }
        else if row == 3{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.email ?? "")", row: row)
            return cell
        }
        else if row == 4{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.phone ?? "")", row: row)
            return cell
        }
        else if row == 5{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.dateOfBirth ?? "")", row: row)
            return cell
        }
        else if row == 6{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.street ?? ""),  \(profileVc?.profileObj.city?.name ?? ""), \(profileVc?.profileObj.state?.name ?? "" ), \(profileVc?.profileObj.zipCode ?? "")", row: row)
            return cell
        }
        else if row == 7{
             let cell = tableView.dequeueReusableCell(withIdentifier: "heathLegalInfoId") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "Legal Info", row: row)
            return cell
        }
            
        else if row == 8  || row == 9 || row == 10{
             let cell = tableView.dequeueReusableCell(withIdentifier: "FingerPrintId") as! MFTableViewCell
//            if row == 7{
//                           cell.setProfileCell(label: "", value: "Urgent Call", row: row)
//                       }
            if row == 8{
                cell.setProfileCell(label: "", value: "Fingerprint Login", row: row)
            }
            if row == 9{
                cell.setProfileCell(label: "", value: "Facial Recognition", row: row)
            }
            
            if row == 10{
                               cell.setProfileCell(label: "", value: "Urgent Call", row: row)
                
                if profileVc?.profileObj.urgentCallFlag == "1" {
                    cell.toggleSw?.isOn = true
                }
                else{
                     cell.toggleSw?.isOn = false
                }
                
                cell.toggleSw?.tag = 10
                           }
            return cell
        }
         
         return UITableViewCell()
        
//        let idfier = editPressed ? "editableCell":"normalCell"
//
//        guard let cell =  tableView.dequeueReusableCell(withIdentifier: idfier) as? MFTableViewCell else {
//            return UITableViewCell()
//        }
//
//        let row = indexPath.row
        
//        cell.textField?.isUserInteractionEnabled = true
//        cell.textField?.isEnabled = editPressed
//        cell.textField?.textFieldDelegate = self
//
//        if row == 0 {
//            cell.setProfileCell(label: "First Name", value:profileVc?.profileObj.firstname, row: row)
//        }
//        else if row == 1 {
//            cell.setProfileCell(label: "Last Name", value:profileVc?.profileObj.lastname, row: row)
//        }
//        else if row == 2 {
//            cell.setProfileCell(label: "Username", value:profileVc?.profileObj.username, row: row)
//        }
//        else if row == 3 {
//
//            if editPressed {
//                let dropCell =  tableView.dequeueReusableCell(withIdentifier: "editableDropCell") as! MFTableViewCell
//                dropCell.lblName.text = "Gender"
//                dropCell.dosageDp?.dataStrings = Constants.genders.map{ $0.name ?? "" }
//                dropCell.dosageDp?.selectedValue = profileVc?.profileObj.gender
//                dropCell.dosageDp?.placeholderText = ""
//                dropCell.dosageDp?.closure = { (index,value) in
//                    self.profileVc?.profileObj.gender = value
//                }
//                return dropCell
//            }
//            else {
//                cell.lblName.text = "Gender"
//                cell.lblValue?.text = profileVc?.profileObj.gender
//            }
//        }
//        else if row == 4 {
//            let celll = tableView.dequeueReusableCell(withIdentifier: "pictureCell") as! MFTableViewCell
//            celll.lblName.text = "Picture"
//            celll.imgProfile?.image = self.profileImage
//            celll.imgProfile?.isUserInteractionEnabled = self.editPressed
//            celll.imageTapped = {
//                let imagePickerController = UIImagePickerController()
//                imagePickerController.delegate = self
//                imagePickerController.sourceType = .photoLibrary
//
//                self.present(imagePickerController, animated: true, completion: nil)
//            }
//            celll.bottomLineView?.isHidden = self.editPressed
//            celll.strongLineView?.isHidden = !self.editPressed
//            celll.setNeedsLayout()
//            return celll
//        }
//        else if row == 5 {
//            cell.setProfileCell(label: "Date of Birth", value:self.birthDateString, row: row)
//            cell.textField?.isEnabled = false
//            cell.textField?.isUserInteractionEnabled = false
//        }
//        else if row == 6 {
//            cell.setProfileCell(label: "Address", value:profileVc?.profileObj.address?.street, row: row)
//        }
//        else if row == 7 {
//
//            if editPressed {
//                let dropCell =  tableView.dequeueReusableCell(withIdentifier: "editableDropCell") as! MFTableViewCell
//                dropCell.lblName.text = "City"
//                dropCell.dosageDp?.dataStrings = Constants.cities.map{ $0.name ?? "" }
//                dropCell.dosageDp?.selectedValue = profileVc?.profileObj.address?.city
//                dropCell.dosageDp?.placeholderText = ""
//                dropCell.dosageDp?.closure = { (index,value) in
//                    self.profileVc?.profileObj.address?.city = value
//                }
//                return dropCell
//            }
//            else {
//                cell.lblName.text = "City"
//                cell.lblValue?.text = profileVc?.profileObj.address?.city
//            }
//        }
//        else if row == 8 {
//
//            if editPressed {
//                let dropCell =  tableView.dequeueReusableCell(withIdentifier: "editableDropCell") as! MFTableViewCell
//                dropCell.lblName.text = "State"
//                dropCell.dosageDp?.dataStrings = Constants.states.map{ $0.name ?? "" }
//                dropCell.dosageDp?.selectedValue = profileVc?.profileObj.address?.state
//                dropCell.dosageDp?.placeholderText = ""
//                dropCell.dosageDp?.closure = { (index,value) in
//                    self.profileVc?.profileObj.address?.state = value
//                }
//                return dropCell
//            }
//            else {
//                cell.lblName.text = "State"
//                cell.lblValue?.text = profileVc?.profileObj.address?.state
//            }
//        }
//        else if row == 9 {
//            cell.setProfileCell(label: "Zip Code", value:profileVc?.profileObj.address?.zipcode, row: row)
//        }
        
//        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rowInd = indexPath.row
        if rowInd == 7{
           let name = Notification.Name("HeathAndPolicyNotForDoctor")
           NotificationCenter.default.post(name: name, object: nil)
        }
        tableVw.deselectRow(at: indexPath, animated: false)
        if editPressed {
            if indexPath.row == 4 {
                profileVc?.showChoseDateDialog(minDate: nil, maxDate: Date(), callBack: { (date, string) in
                    self.birthDateString = string ?? ""
                    tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
                })
            }
        }
    }
    
    
}

extension DoctorProfileOneVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            profileVc?.profileObj.firstName = textField.text
        case 1:
            profileVc?.profileObj.lastName = textField.text
        case 2:
            profileVc?.profileObj.firstName = textField.text
        case 6:
            profileVc?.profileObj.street = textField.text //Address textField
        case 9:
            profileVc?.profileObj.zipCode = textField.text
        default:
            print("Check textField tag")
        }
    }
    
}



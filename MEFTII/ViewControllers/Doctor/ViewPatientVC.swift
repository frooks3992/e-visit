//
//  ViewPatientVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 11/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import RealmSwift

class ViewPatientVC: BaseViewController {
    
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var rejectBtn: UIButton!
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var profileImgVw: UIImageView!
    @IBOutlet weak var lblPatientName: UILabel!
    
    
    var pateint : PatientProfile?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let urlll = URL(string: Constants.getProfileImageUrl(userName: (pateint?.username ?? "")))
        profileImgVw.sd_setImage(with: urlll, completed: nil)

        lblPatientName.text = pateint?.completeName
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func accept_Pressed(_ sender: MFButton) {
        
    }
    
    @IBAction func reject_Pressed(_ sender: MFButton) {
        
    }
    
    @IBAction func call_Pressed(_ sender: MFButton) {
        
        if let docterUserName = pateint?.username {
           
            
            let reuser = docterUserName + String(Date().timeIntervalSinceNow)
            
            let tokenRqst = MeftiiRequests.getTokensForCall(sender: DataManager.shared.userName ?? "", receiver: reuser)
            
            tokenRqst.start(vc: self, successBlock: { (code, desc, resp) in
                if code == 0 {
                    let senderToken = resp["senderToken"] as? String
                    let receiverToken = resp["receiverToken"] as? String
                    let roomName = resp["roomName"] as? String
                    
                    if let sToken = senderToken,  let rToken = receiverToken, let rName = roomName {
                        self.initiateCall(toDoctor: docterUserName, receiverTkn: rToken, sendrTkn: sToken, room: rName)
                    }
                }
            }, failureBlock: nil)
        }
        
    }
    
    @IBAction func message_Pressed(_ sender: MFButton) {
       
        if let docterUserName = pateint?.username {
            
            let realm = try! Realm()
            let results = realm.objects(MWConversation.self).filter("conversationId == %@", docterUserName)
            
            if let convo = results.first {
                
                let chatViewController = ChatDetailVC(conversation: convo)
                navigationController?.pushViewController(chatViewController, animated: true)
                
            }
            else {
                let conversation = MWConversation()
                
                var uName = docterUserName.lowercased()
                uName = uName.components(separatedBy: CharacterSet.letters.inverted).joined()
                conversation.conversationId = uName
                let name = (pateint?.completeName ?? "").capitalized
                conversation.displayName = name
                
                try! realm.write {
                    realm.add(conversation, update: .all)
                }
                
                let chatViewController = ChatDetailVC(conversation: conversation)
                navigationController?.pushViewController(chatViewController, animated: true)
            }
            
            
        }
        
    }

}


extension ViewPatientVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 4
        }
        else {
            return 3
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let idfier = indexPath.section == 0 ? "normalCell":"bigTextCell"
        
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: idfier) as? MFTableViewCell else {
            return UITableViewCell()
        }
        let row = indexPath.row
        
        if indexPath.section == 1 {
            if row == 0 {
                cell.lblName.text = "Symptoms"
                cell.lblValue?.text = "Lorem ipsum dolor sit amet, consectetur adipi scing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            }
            else if row == 1 {
                cell.lblName.text = "Food or Drug Allergies"
                cell.lblValue?.text = "Lorem ipsum dolor sit amet, consectetur adipi scing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
            }
            else if row == 2 {
                cell.lblName.text = "Chronic Conditions"
                cell.lblValue?.text = "None."
            }
        }
        else if indexPath.section == 0 {
            
            if row == 0 {
                cell.lblName.text = "Time"
                cell.lblValue?.text = "06:10 PM"
            }
            else if row == 1 {
                cell.lblName.text = "Date"
                cell.lblValue?.text = "7/15/2019"
            }
            else if row == 2 {
                cell.lblName.text = "Gender"
                cell.lblValue?.text = "$50"
            }
            else if row == 3 {
                cell.lblName.text = "Speciality"
                cell.lblValue?.text = "Allergy & immunology"
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
}






//
//  HomeDoctorVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 07/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import SDWebImage
import FloatRatingView

class HomeDoctorVC: BaseViewController,UIViewControllerTransitioningDelegate, UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var doctorRatingView: FloatRatingView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var sideMenuTableVw: UITableView!
    @IBOutlet weak var constraintSideMenu: NSLayoutConstraint!
    @IBOutlet weak var constraintTransparentVwWidth: NSLayoutConstraint!
    @IBOutlet weak var Tv: UITableView!
    @IBOutlet weak var sideMenu: UIView!
    
    @IBOutlet weak var PlaceholderLbl: UILabel!
    
     var Sentrequests:[UrgentCallRequests] = []
    var leftMenuOpen = false
    var appointments = [appoitments]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sideMenu.isHidden = true
        lblHeading.text = "Hi Dr. \(DataManager.shared.currentDoctor?.lastName ?? "")!"
        lblProfileName.text = "Dr. " + "\(DataManager.shared.currentDoctor?.firstName ?? "") \(DataManager.shared.currentDoctor?.lastName ?? "")"
        
        Tv.removeExtraRows()
        self.PlaceholderLbl.text = ""
        doctorRatingView.rating = 4 ?? 0
        
        /*
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            
            if let vc = self.navigationController?.topViewController as? HomeDoctorVC {
                vc.showAlertWith(title: "Complete Profile", message: "Just a few more steps and your profile will be complete.", buttonText: "PROFILE", callBack: {
                    let vc = ProfileDoctorVC.instantiateFromStoryboard()
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                })
                
            }
            
        }
         */
        sideMenuTableVw.removeExtraRows()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        getAppointmentList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let doc = DataManager.shared.currentDoctor {
          
            imgProfile.sd_setImage(with: URL(string: (DataManager.shared.currentDoctor?.profilePicture ?? "")), placeholderImage: setDummyImage(gender: doc.gender!))
            
            if let email = doc.email{
                
            }
            else{
                DataManager.shared.currentDoctor?.email = DataManager.shared.getcurrentEmail()
            }
            
        }
        
      
    }
    
    @IBAction func notificationPressed(_ sender: UIButton) {
        
        let vc = UrgentCallRequestVC.instantiateFromStoryboard("UrgentCall")
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
    
    func gotoPage(indexNumber:Int) {
        
        switch indexNumber {
        case 0:
            hideSideMeny(animated: false)
            let vc = ProfileDoctorVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 1:
//            hideSideMeny(animated: false)
//            let vc = MessagesVC.instantiateFromStoryboard()
//            self.navigationController?.pushViewController(vc, animated: true)
            hideSideMeny(animated: false)
            
           
            let vc = DoctorScheduleVC.instantiateFromStoryboard("Schedule")
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
        case 2:
            hideSideMeny(animated: false)
            
//           let vc = UrgentCallAddedDoctors.instantiateFromStoryboard("UrgentCall")
//            self.navigationController?.pushViewController(vc, animated: true)
        let vc = UrgentCallRequestVC.instantiateFromStoryboard("UrgentCall")
             
             self.navigationController?.pushViewController(vc, animated: true)

            break
            
        case 3:
             hideSideMeny(animated: false)
            let vc = DoctorOnCallGroupVC.instantiateFromStoryboard("OnCall")
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
//        case 5:
//            hideSideMeny(animated: false)
////            let vc = DoctorScheduleVC.instantiateFromStoryboard("Schedule")
////            self.navigationController?.pushViewController(vc, animated: true)
//            break
//
//
//            case 4:
//                      hideSideMeny(animated: false)
//                self.performSegue(withIdentifier: "toEprescription", sender: nil)
//
//                break
        case 4:
            hideSideMeny(animated: false)
            DataManager.shared.logoutCurrentUser(isForceLogout: true)
            
//           performSegue(withIdentifier: "logoutDoctor", sender: nil)
//            let transition = CATransition()
//            transition.duration = 0.5
//            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//            transition.type = CATransitionType.fade
            
//            let transition = CATransition()
//               transition.duration = 0.5
//            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
//            transition.type = CATransitionType.push
//            transition.subtype = CATransitionSubtype.fromLeft
//            self.navigationController!.view.layer.add(transition, forKey: nil)
//            self.navigationController?.view.layer.add(transition, forKey: nil)
//            let vc = ChoosePatientDoctorVC.instantiateFromStoryboard()
//            self.navigationController?.pushViewController(vc, animated: true)
            break
        default:
            hideSideMeny(animated: true)
        }
        
    }
    

    
    @IBAction func menuPressed(_ sender: UIButton) {
        showSideMenu(animated:true)
    }
    
    @IBAction func crossPressed(_ sender: UIButton) {
        hideSideMeny(animated: true)
    }
    
    
    func hideSideMeny(animated:Bool) {
          sideMenu.isHidden = true
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.constraintTransparentVwWidth.constant = 0
                self.constraintSideMenu.constant = 0
                self.view.layoutIfNeeded()
            }) { (completed) in
                self.leftMenuOpen = false
                self.sideMenu.layer.masksToBounds = true
            }
        }
        else {
            self.sideMenu.layer.masksToBounds = true
            self.constraintTransparentVwWidth.constant = 0
            self.constraintSideMenu.constant = 0
            self.leftMenuOpen = false
        }
        
    }
    
    func showSideMenu(animated:Bool) {
        sideMenu.isHidden = false
        sideMenu.addRightShadow()
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.constraintTransparentVwWidth.constant = 500
                self.constraintSideMenu.constant = 300
                self.view.layoutIfNeeded()
            }) { (completed) in
                self.leftMenuOpen = true
            }
        }
        else {
            self.constraintTransparentVwWidth.constant = 500
            self.constraintSideMenu.constant = 300
            self.leftMenuOpen = true
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func transparentViewTapped(_ sender: Any) {
        hideSideMeny(animated: true)
    }
    
}

extension HomeDoctorVC{

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView === Tv {
            return Sentrequests.count
        }
        else
        if tableView === sideMenuTableVw  {
            return 5
        }
        return 0
    }


    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if
            tableView === sideMenuTableVw  {
            return 60
        }
        else {
            return 115
        }
          
       }
       
       func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
       }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {


//        if tableView === appointmentTableVw {
//
//            let appointment = appointments[indexPath.row]
//
//            let status = (appointment.status ?? "").lowercased()
//
//            let identifier = status == "new" ? "newCell":"cell"
//
//            guard let cell =  tableView.dequeueReusableCell(withIdentifier: identifier) as? MFTableViewCell else {
//                return UITableViewCell()
//            }
//
//            let url = Constants.getProfileImageUrl(userName: appointment.patientProfile?.profilePicture ?? "")
//
//            cell.imgProfile?.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "home_pic_1"), options: SDWebImageOptions(), context: nil)
//
//            cell.lblName.text = "\(appointment.patientProfile?.completeName ?? "")"
//
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "MM/dd/yyyy HH:mm"
//            let fromDate = dateFormatter.date(from: appointment.from ?? "") ?? Date()
//
//            let boldFont = UIFont.AppBoldFont(ofSize: 10)
//            let normalFont = UIFont.AppFont(ofSize: 10)
//
//            let timeFormatter = DateFormatter()
//            timeFormatter.dateFormat = "HH:mm"
//
//            let timeStr = timeFormatter.string(from: fromDate)
//
//            let datFormatter = DateFormatter()
//            datFormatter.dateFormat = "MM/dd/yyyy"
//
//            let datePartStr = datFormatter.string(from: fromDate)
//
//            let attrString = NSMutableAttributedString(string: "Time: ", attributes: [NSAttributedString.Key.font : normalFont] )
//
//            let stringDate = NSAttributedString(string: " Date: ", attributes: [NSAttributedString.Key.font : normalFont])
//
//
//            let timeAttrString = NSAttributedString(string: timeStr, attributes: [NSAttributedString.Key.font : boldFont])
//
//
//            let dateAttrString = NSAttributedString(string: datePartStr, attributes: [NSAttributedString.Key.font : boldFont])
//
//            attrString.append(timeAttrString)
//            attrString.append(stringDate)
//            attrString.append(dateAttrString)
//
//            cell.lblDateTime?.attributedText = attrString
//
//
//            switch status {
//
//            case "approved":
//
//                let result = Date().timeIntervalSince(fromDate)
//                let difference = (result / 1000) / 3600
//
//                if difference > 1 {
//                    cell.btnCall?.setTitle("Call", for: UIControl.State.normal)
//                    cell.btnCall?.isFilled = true
//                    cell.btnCall?.color = UIColor.MFLightBlue
//                } else {
//                    cell.btnCall?.setTitle("Approved", for: UIControl.State.normal)
//                    cell.btnCall?.isFilled = false
//                    cell.btnCall?.color = UIColor.MFGreen
//                }
//
//            default:
//                print("No such status")
//            }
//
//            return cell
//
//        }
        
        if tableView === Tv{
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "urgentCallRequestCell", for: indexPath) as! urgentCallRequestCell
                   
                   let status = Sentrequests[indexPath.row].status
                   cell.acceptBtn.tag = indexPath.row
                   cell.declineBtn.tag = indexPath.row
                   
                    
                   
                   if status == "NEW" {
                       
                    cell.txt.text =  Sentrequests[indexPath.row].patientName
                    
                   
                       //.patientName + " is requesting urgent virtual visit."
                   }
                   else{
                       cell.txt.text = status
                      
                   }
            
            cell.img.sd_setImage(with: URL(string: (Sentrequests[indexPath.row].patientProfilePic)), placeholderImage: #imageLiteral(resourceName: "ic_male_placeholder"))
            
            return cell
        }
        
        
      else
        if tableView === sideMenuTableVw  {

            guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFTableViewCell else {
                return UITableViewCell()
            }

            let row = indexPath.row

            cell.btnCall?.isHidden = true
            if row == 0 {
                cell.lblName.text = "Profile"
            }
            else if row == 1 {
                cell.lblName.text = "Schedule"
//                cell.btnCall?.isHidden = false
            }

            else if row == 2 {
                              cell.lblName.text = "Urgent Call Request"
                          }
//            else if row == 3 {
//                cell.lblName.text = "On Call Doctors"
//            }
            else if row == 3 {
                cell.lblName.text = "On Call Group"
            }
//          else if row == 4 {
//             cell.lblName.text = "ePrescription"
//
//          }
//            else if row == 5 {
//               cell.lblName.text = "SOAP Note"
//
//            }
//
//
//            else if row == 6 {
//                cell.lblName.text = "Invite People"
//            }
//            else if row == 7 {
//                cell.lblName.text = "Contact Us"
//            }
            
            else if row == 4{
                           cell.lblName.text = "Logout"
                       }
            return cell
        }

        return UITableViewCell()

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView === sideMenuTableVw {
            gotoPage(indexNumber:indexPath.row)
        }
//        else if tableView === appointmentTableVw {
//
//            let appointment = appointments[indexPath.row]
//            if let patientProf = appointment.patientProfile {
//                let vc = ViewPatientVC.instantiateFromStoryboard()
//                vc.pateint =  patientProf
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
//
//        }
    }
}
//extension HomeDoctorVC{
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "urgentCallRequestCell", for: indexPath) as! urgentCallRequestCell
//        
//        let status = Sentrequests[indexPath.row].status
//        cell.acceptBtn.tag = indexPath.row
//        cell.declineBtn.tag = indexPath.row
//        
//         
//        
//        if status == "NEW" {
//            
//            cell.txt.text =  Sentrequests[indexPath.row].patientName + " is requesting urgent virtual visit."
//        
//            
//        }
//        else{
//            cell.txt.text = status
//           
//        }
//        
//        return cell
//    }
//    
//}


extension HomeDoctorVC {
        
        func fetchrequests(doctorId:Int) {
               
               self.isLoading()
            let regst = MeftiiRequests.getsentpatientrequests(doctorId: doctorId )
               regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
                   
                   self?.isLoadingFinished()
                var reqs:[UrgentCallRequests] = []
                   if let data = respTwo["data"] as? [String:Any]{
                     
                    if let message = data["message"] as? String {
                        
                        self!.PlaceholderLbl.text = message
                    }
                    else {
                        
                         self!.PlaceholderLbl.text = "No urgent call requests found"
                    }
                    let requests = data["urgentCallRequestList"] as? [[String:Any]] ?? []
                    
                       do {
                           
                        requests.forEach { (request) in
                        reqs.append(UrgentCallRequests.init(json: request))
                            
                        }
                       
                        self!.Sentrequests = reqs
                        
                       
                        
                        if self!.Sentrequests.count > 0 {
                                   self?.Tv.isHidden = false
                            self!.PlaceholderLbl.isHidden = true
                               }
                               else{
                                   self?.Tv.isHidden = true
                            self!.PlaceholderLbl.isHidden = false
                               }
                        
                        
                        
                        self!.Tv.reloadData()
                        
                       }
                       catch {
                           print()
                       }
                       
                       
                   }
                   
                   }, failureBlock: nil)
           }
        
        
        func acceptdeclinerequest(requestId:Int,status:String,patientId:Int,doctorId:Int,doctorName:String) {
                  
                  self.isLoading()
               let regst = MeftiiRequests.acceptdeclinerequest(requestId: requestId, status: status, patientId: patientId, doctorId: doctorId, doctorName: doctorName)
                  regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
                      
                      self?.isLoadingFinished()
                   
                       if codeTwo == 0 {
                          
                       
                       // self!.showAlertWithMsg(Msg: "Request " + status + " successfully")
                          
                        
                        self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Ok", callBack: {
                                          
                                                                })
                        
                        
                      }
                    
                       else{
                        
                    //    self!.showToast(message: "An error occured. Pls try again")
                        
                    self?.showAlertWith(title: "Error", message: descTwo, buttonText: "Ok", callBack: {
                                                              
                                                                                    })
                        
                    }
                      
                      }, failureBlock: nil)
              }
        
        @IBAction func acceptPressed(_ sender: UIButton) {
            
            
            let idx = sender.tag
                   let cell = Tv.cellForRow(at: IndexPath(item: sender.tag, section: 0)) as! urgentCallRequestCell
                   
            let req = Sentrequests[idx]
                   
            acceptdeclinerequest(requestId: req.requestId, status: "accepted", patientId: req.patientId, doctorId: req.doctorId, doctorName: req.doctorName)
              
        }
        
        func showAlertWithMsg(Msg:String){
            
          //  successView.isHidden = false
            // create the alert
                   let alert = UIAlertController(title: "", message: Msg, preferredStyle: UIAlertController.Style.alert)

                   // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "DONE", style: .destructive, handler: { (UIAlertAction) in
                
               // self.successView.isHidden = true
            }))

                   // show the alert
        
                   self.present(alert, animated: true, completion: nil)
        }
        
        @IBAction func declinePressed(_ sender: UIButton) {
            
            let idx = sender.tag
                          let cell = Tv.cellForRow(at: IndexPath(item: sender.tag, section: 0)) as! urgentCallRequestCell
                          
                   let req = Sentrequests[idx]
                          
                   acceptdeclinerequest(requestId: req.requestId, status: "rejected", patientId: req.patientId, doctorId: req.doctorId, doctorName: req.doctorName)
            
        }
    
}

extension HomeDoctorVC {
    
    func getAppointmentList() {
          fetchrequests(doctorId: DataManager.shared.currentDoctorId)
//        let id  = DataManager.shared.currentDoctor?.id ?? 0
//
//        let apointmentRqst = MeftiiRequests.getAppointmentList(id: id, forDoctor: true)
//
//        self.isLoading()
//
//        apointmentRqst.start(vc: nil, successBlock: { [weak self] (code, desc, resp) in
//
//            let obj = resp["appointments"]
//            do {
//                let data = try JSONSerialization.data(withJSONObject: obj!!, options: .prettyPrinted)
//                let arr = try JSONDecoder().decode([Appointment].self, from: data)
//                self?.appointments = arr
//
//            }
//            catch {
//
//            }
//
//            self?.appointmentTableVw.reloadData()
//            self?.isLoadingFinished()
//
//        }, failureBlock: {[weak self] (error) in
//            self?.isLoadingFinished()
//            self?.showToast(message: error.localizedDescription)
//        })
    }
    
}

//
//  EndCallTwoVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 13/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class EndCallTwoVC: BaseViewController {

    var endCall: EndCallModel?
    
    @IBOutlet weak var planField: UITextField!
    @IBOutlet weak var assessmentField: UITextField!
    @IBOutlet weak var objectiveField: UITextField!
    @IBOutlet weak var subjectiveField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    
    @IBAction func submitTapped(_ sender: MFButton) {
        
        let subjective = subjectiveField.text ?? ""
        let objective = objectiveField.text ?? ""
        let assessment = assessmentField.text ?? ""
        let plan = planField.text ?? ""
        
        guard subjective.count > 0 else {
            showToast(message: "Subjective cannot be empty ⚠️")
            return
        }
        
        guard objective.count > 0 else {
            showToast(message: "Objective cannot be empty ⚠️")
            return
        }
        
        guard assessment.count > 0 else {
            showToast(message: "Assessment cannot be empty ⚠️")
            return
        }
        
        guard plan.count > 0 else {
            showToast(message: "Plan cannot be empty ⚠️")
            return
        }
        
        endCall?.subjective = subjective
        endCall?.objective = objective
        endCall?.assessment = assessment
        endCall?.plan = plan
        
        addEndCallDiagnosis(model: endCall!)
        
    }
    
}

extension EndCallTwoVC {
    
    //AddEndCallDiagnosis API
    func addEndCallDiagnosis(model: EndCallModel) {
        
        let request = MeftiiRequests.AddEndCallDiagnosis(model: model)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            print(resp)
            
            if code == 0 {
                self?.showToast(message: "Success")
            }
            
            }, failureBlock: { [weak self] (error) in
                self?.showToast(message: error.localizedDescription)
                self?.isLoadingFinished()
        })
        
    }
    
}

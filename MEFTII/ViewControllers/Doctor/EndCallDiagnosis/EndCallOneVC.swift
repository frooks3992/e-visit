//
//  EndCallOneVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 13/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class EndCallOneVC: BaseViewController {

    @IBOutlet weak var diagnosisField: MFTextField!
    @IBOutlet weak var tableVw: UITableView!
    
    var medicine: String!
    var dosage: String!
    var directions: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func nextButtonTapped(_ sender: MFButton) {
        
        let diagnosis = diagnosisField.text ?? ""
        
        let cells = tableVw.visibleCells
        
        for cell in cells {
            if let mfCell = cell as? MFTableViewCell {
//                guard mfCell.dosageDp?.selectedValue?.count ?? 0 > 0 else {
//                    showToast(message: "Dosage cannot be empty ⚠️")
//                    return
//                }
                
                dosage = mfCell.dosageDp?.selectedValue ?? ""
            }
        }
        
        guard diagnosis.count > 0 else {
            showToast(message: "Diagnosis cannot be empty ⚠️")
            return
        }
        
        guard medicine.count > 0 else {
            showToast(message: "Medicine cannot be empty ⚠️")
            return
        }
        
        guard directions.count > 0 else {
            showToast(message: "Directions cannot be empty ⚠️")
            return
        }
        
        var prescription = Prescriptions()
        prescription.medicine = medicine
        prescription.dosage = dosage
        prescription.direction = directions
        
        let endCall = EndCallModel()
        endCall.diagnosis = diagnosis
        endCall.prescriptions = [prescription]
        
        let vc = EndCallTwoVC.instantiateFromStoryboard()
        vc.endCall = endCall
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func addMedicineTapped(_ sender: MFButton) {
        
    }
    
}

extension EndCallOneVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "staticCell") as? MFTableViewCell else { return UITableViewCell() }
        
        cell.medicineField?.delegate = self
        cell.medicineField?.tag = 1
        cell.directionsField?.delegate = self
        cell.directionsField?.tag = 2
        
//        cell.reuseIdentifier = nil
        
        
        return cell
    }
    
    
    
}

extension EndCallOneVC : UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField) {
        
        switch textField.tag {
        case 1:
            medicine = textField.text
        case 2:
            directions = textField.text
        default:
            print("Check tag")
        }
        
    }
}

//
//  reason_PickerVC.swift
//  MEFTII
//
//  Created by Awais Malik on 13/04/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import UIKit

class reason_PickerVC: BaseViewController {

    @IBOutlet var Tv: UITableView!
       
       @IBOutlet var TitleTf: UILabel!
       @IBOutlet var SearchTF: UITextField!
       var Pickertype:Int?
       var ReasonsSyptom_MedicalCond:[Condition]!
       var SelectedSymptom:[String] = []
        var searchTimer: Timer?
       var SelectedCondition:String!
       var index: Int!
       var symptomsList:[Condition] = []
       var Filteredsymptoms:[Condition] = []
       var symptomsIds:[Int]=[]
       var ConditionId:[Int]=[]
       var isEmergency = false
       override func viewDidLoad() {
           super.viewDidLoad()
           updateViews()
        SearchTF.delegate = self
           Tv.tableFooterView = UIView()
//        if SelectedSymptom.first == "" {
//            symptomsList.removeAll()
//        }
           // Do any additional setup after loading the view.
       }
       
       
       func updateViews(){
           switch Pickertype {
           case 2:
               TitleTf.text! = "Symptoms"
             
               getSymptomsList(type: 2)
              // ReasonsSyptom_MedicalCond = Constants.symptoms
           default:
               TitleTf.text! = "Medical conditions"
                  Tv.allowsMultipleSelection = false
               getSymptomsList(type: 1)
               //ReasonsSyptom_MedicalCond = Constants.conditions
           }
       }
       
       
       @IBAction func BackPressed(_ sender: Any) {
           self.dismiss(animated: true, completion: nil)
           
       }
       
       
       
       @IBAction func EditingChanged(_ sender: Any) {
           
           
           let text = SearchTF.text!
           
           if text == "" {
               
               switch Pickertype {
               case 2:
                   ReasonsSyptom_MedicalCond = symptomsList
               default:
                   
                   ReasonsSyptom_MedicalCond = symptomsList
               }
           }
           else {
               
               switch Pickertype {
               case 2:
                Filteredsymptoms = symptomsList.filter{($0.name.contains(text))}
               default:
                Filteredsymptoms = symptomsList.filter{($0.name.contains(text))}
               }
               
               
           }
           
           Tv.reloadData()
           
       }
       
       
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           
        if let vc = segue.destination as? AppointmentFourVC {
            
            vc.symptoms = SelectedSymptom
            vc.SelectedCondition = SelectedCondition
            vc.symptomsIds = symptomsIds
            vc.ConditionId = ConditionId
            
        }
           
       }

}

extension reason_PickerVC: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        Filteredsymptoms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "city_stateCell", for: indexPath) as! City_StateCell
        
        cell.Lbl.text! = Filteredsymptoms[indexPath.row].name
        //cell.Img.tag = indexPath.row
        switch Pickertype {
        case 2:
            if self.SelectedSymptom.contains(Filteredsymptoms[indexPath.row].name){
                cell.Img.image = UIImage(named: "selected")
                index = indexPath.row
            }else{
                cell.Img.image = nil
            }
        default:
            if self.SelectedCondition == Filteredsymptoms[indexPath.row].name{
                index = indexPath.row
                cell.Img.image = UIImage(named: "selected")
            }else{
                cell.Img.image = nil
            }
        }
        //cell.Img.image = nil
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       
       // let cell = Tv.cellForRow(at: indexPath) as! City_StateCell
        
        //cell.Img.image = UIImage(named: "selected")
        index = indexPath.row
        switch Pickertype {
        case 2:
            
            if Filteredsymptoms[indexPath.row].name == "Other" {
                
                SelectedSymptom.removeAll()
                symptomsIds.removeAll()
                symptomsIds.append(Filteredsymptoms[indexPath.row].id)
                SelectedSymptom.append(Filteredsymptoms[indexPath.row].name)
            
                Tv.reloadData()
            }
            
            else{
            
            if SelectedSymptom.contains(Filteredsymptoms[indexPath.row].name) {
                
              symptomsIds =  symptomsIds.filter({$0 != Filteredsymptoms[indexPath.row].id})
                SelectedSymptom = SelectedSymptom.filter({$0 != Filteredsymptoms[indexPath.row].name})
            }
            else{
                SelectedSymptom.append(Filteredsymptoms[indexPath.row].name)
                symptomsIds.append(Filteredsymptoms[indexPath.row].id)
            }
            }
           // SelectedSymptom =
           
        default:
            SelectedCondition = Filteredsymptoms[indexPath.row].name
           // isEmergency = Filteredsymptoms[indexPath.row].isEmergency
            ConditionId.removeAll()
            ConditionId.append(Filteredsymptoms[indexPath.row].id)
        }
        
        if Filteredsymptoms[indexPath.row].isEmergency == true {
        isEmergency = Filteredsymptoms[indexPath.row].isEmergency
        }
        print(SelectedSymptom)
        print(symptomsIds)
        Tv.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}

extension reason_PickerVC{
    
    
    
      //MARK: - GetstatesList API
    func getSymptomsList(type:Int) {
            
        let request = MeftiiRequests.getsymptomsList(type:type)
            
         //   self.isLoading()
            request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
                
               // self?.isLoadingFinished()
                
                print(code)
              
                if code == 0 {
                    var key = ""
                    switch type {
                    case 2:
                        
                        key = "symptomsList"
                    default:
                        key = "medicalConditionsList"
                    }
                    let data = resp["data"] as! [String:Any]
                    let cities = data["\(key)"] as! [[String:Any]]
                    self!.symptomsList.removeAll()
                    self!.Filteredsymptoms.removeAll()
                    for city in cities {
                        
                        let value = Condition.init(json: city)
                        print(value.id)
                        print(value.name)
                        self!.symptomsList += [value]
                        
                    }
              
                    self!.Filteredsymptoms = self!.symptomsList
                    
                    if self!.symptomsList.count > 0 {
                        self!.Tv.isHidden = false
                    }
                    else{
                        self!.Tv.isHidden = false
                    }
                    
                    self!.Tv.reloadData()

                }
                
                }, failureBlock: { [weak self] (error) in
                   // self?.showToast(message: error.localizedDescription)
                    
                    self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                      
                                                            })
                   // self?.isLoadingFinished()
            })
            
        }
    
    
    
}


extension reason_PickerVC : UITextFieldDelegate {
    
    @objc func textFieldDidEditingChanged(_ textField: UITextField) {
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }
        searchTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text ?? "", repeats: false)
    }
    
    @objc func searchForKeyword(_ timer: Timer) {
        let keyword = ((timer.userInfo as? String) ?? "").lowercased()
        if keyword.count > 0 {
            Filteredsymptoms = Filteredsymptoms.filter({ ($0.name.lowercased()).contains(keyword) }) ?? []
        }
        else {
            Filteredsymptoms = symptomsList
        }
        Tv.reloadData()
    }
    
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
           
           
           if textField.text!.containsWhitespace{
               
            if string == " " && textField.text?.last == " "{
                     return false
               }
    
        }
        return true
    }
    
}

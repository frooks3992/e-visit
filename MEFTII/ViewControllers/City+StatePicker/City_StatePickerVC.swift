//
//  City_StatePickerVC.swift
//  MEFTII
//
//  Created by Farhan on 30/03/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import UIKit


class City_StatePickerVC: BaseViewController {
    @IBOutlet var Tv: UITableView!
    
    @IBOutlet var TitleTf: UILabel!
    @IBOutlet var SearchTF: searchTextField!
    var Pickertype:Int?
    var Filteredcities:[City] = []
    var statesList:[City] = []
    var SelectedCity:String!
    var SelectedState:String!
    var selectedId:Int!
    var selectedCityId:Int!
    var selectedStateId:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        updateViews()
        Tv.tableFooterView = UIView()
        SearchTF.delegate = self
        SearchTF.valueType = .fullName
        
        SearchTF.bottomBorder = UIView()
        // Do any additional setup after loading the view.
    }
    
    
    func updateViews(){
        switch Pickertype {
        case 2:
            TitleTf.text! = "State"
            GetstatesList(type: 1, state_id: 0)
        default:
            TitleTf.text! = "City"
            
            if selectedId != 0 {
            GetstatesList(type: 0, state_id: selectedId)
            }
            
            else{
                GetstatesList(type: 0, state_id: DataManager.shared.currentPatient?.state?.id ?? 0)
            }
        }
    }
    
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func EditingChanged(_ sender: Any) {
        
        
        let text = SearchTF.text!
        
        if text == "" {
            
            switch Pickertype {
            case 2:
                
                Filteredcities = statesList
            default:
                
                Filteredcities = statesList
            }
        }
        else {
            
            switch Pickertype {
            case 2:
                Filteredcities = statesList.filter{($0.name.contains(text))}
            default:
                Filteredcities = statesList.filter{($0.name.contains(text))}
            }
            
            
        }
        
        Tv.reloadData()
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
    }
    
    
}


extension City_StatePickerVC: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Filteredcities.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "city_stateCell", for: indexPath) as! City_StateCell
        
        
        switch Pickertype {
        case 2:
            if SelectedState.count > 0{
                
                 (SelectedState == Filteredcities[indexPath.row].name) ?
                    (cell.Img.image = UIImage(named: "selected")):
                    (cell.Img.image = nil)
                }
                
            
        case 1:
            if SelectedCity.count > 0{
                
                if SelectedCity == Filteredcities[indexPath.row].name {
                    
                    cell.Img.image = UIImage(named: "selected")
                }
                else {
                    
                    cell.Img.image = nil
                }
            }
        default:
            cell.Img.image = nil
        }
        
        
        cell.Lbl.text! = Filteredcities[indexPath.row].name
        //cell.Img.tag = indexPath.row
        
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let cell = Tv.cellForRow(at: indexPath) as! City_StateCell
        
        cell.Img.image = UIImage(named: "selected")
        switch Pickertype {
        case 2:
           
            SelectedState = Filteredcities[indexPath.row].name
            selectedId = Filteredcities[indexPath.row].id
           // DataManager.shared.currentPatient?.state?.name = SelectedState
           // DataManager.shared.currentPatient?.state?.id = selectedStateId
            selectedStateId = selectedId
            
            print(SelectedState)
            
        default:
            SelectedCity = Filteredcities[indexPath.row].name
            selectedCityId = Filteredcities[indexPath.row].id
           // DataManager.shared.currentPatient?.city?.name = SelectedCity
            //DataManager.shared.currentPatient?.city?.id = selectedCityId
            
            print(SelectedState)
            
        }
        
        print(SelectedState!)
        Tv.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}


extension City_StatePickerVC{
    
    
    
    //MARK: - GetstatesList API
    func GetstatesList(type:Int,state_id:Int) {
        
        let request = MeftiiRequests.getstatesList(type:type,state_id:state_id)
        
        //   self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
             self?.isLoadingFinished()
            
            print(code)
            
            if code == 0 {
                var key = ""
                switch type {
                case 1:
                    key = "states"
                default:
                    key = "cities"
                }
                let data = resp["data"] as! [String:Any]
                let cities = data["\(key)"] as! [[String:Any]]
                self!.statesList.removeAll()
                self!.Filteredcities.removeAll()
                for city in cities {
                    
                    let value = City.init(json: city)
                    print(value.id)
                    print(value.name)
                    self!.statesList += [value]
                    
                }
                
                self!.Filteredcities = self!.statesList
                
                
                if self!.Filteredcities.count > 0 {
                    
                    self!.Tv.isHidden = false
                }
                else{
                    self!.Tv.isHidden = true
                }
                
                self!.Tv.reloadData()
                
            }
            
            }, failureBlock: { [weak self] (error) in
               // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                  
                                                        })
                // self?.isLoadingFinished()
        })
        
    }
    
    
    
}

extension City_StatePickerVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField.text!.containsWhitespace{
                 
              if string == " " && textField.text?.last == " "{
                       return false
                 }

          }
        
        
        
        // Verify all the conditions
        if let sdcTextField = textField as? searchTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return true
    }
}




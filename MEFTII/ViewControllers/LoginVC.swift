//
//  LoginVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 24/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginVC: BaseViewController {
    
    @IBOutlet weak var userNameField: MFTextField!
    @IBOutlet weak var passwordField: MFTextField!
    
    var isDoctor = false
    var patient_p: Profile_p!
    var doctor_p: Profile_d!
    
    var biometricEnabled = false
    var address : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordField.delegate = self
        // Do any additional setup after loading the view.
        
        
        
        if isDoctor {
            biometricEnabled = DataManager.shared.currentDoctor?.isFingerPrintOn ?? false
            //  userNameField.text = DataManager.shared.currentDoctor?.username
            address = DataManager.shared.currentDoctor?.street
        }
        else {
            biometricEnabled = DataManager.shared.currentPatient?.isFingerPrintOn ?? false
            // userNameField.text = DataManager.shared.currentPatient?.username
            address = DataManager.shared.currentPatient?.street
        }
        
        if let unam = DataManager.shared.userName {
            // userNameField.text = unam
        }
        
        
        if biometricEnabled {
            var authError: NSError?
            if LAContext().canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
                authenticateWithTouchID()
            }
            else {
                self.biometricEnabled = false
                print(authError?.localizedDescription)
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func backButtonPressed(_ sender: UIButton) {
        DataManager.shared.restLoginData()
        super.backButtonPressed(sender)
    }
    
    @IBAction func loginButtonPressed(_ sender: MFButton) {
        
        userNameField.resignFirstResponder()
        passwordField.resignFirstResponder()
        
        let userName = userNameField.text ?? ""
        let password = passwordField.text ?? ""
        
        guard userName.count > 0 else {
            
            self.showAlertWith(title: "Warning", message: "Email cannot be Empty", buttonText: "OK", callBack: {
                //                    let vc  = LoginVC.instantiateFromStoryboard()
                //  self?.navigationController?.popViewController(animated: true)
            })
            return
        }
        
        guard userName.isValidEmail() else {
            
            self.showAlertWith(title: "Warning", message: "Enter valid email", buttonText: "OK", callBack: {
                //                    let vc  = LoginVC.instantiateFromStoryboard()
                //  self?.navigationController?.popViewController(animated: true)
            })
            return
        }
        
        guard password.count > 0 else {
            //showToast(message: "Password cannot be empty ⚠️")
            self.showAlertWith(title: "Warning", message: "Password is mandatory", buttonText: "OK", callBack: {
                //                    let vc  = LoginVC.instantiateFromStoryboard()
                //  self?.navigationController?.popViewController(animated: true)
            })
            return
        }
        address = password
        if isDoctor {
            doctorlogin(userName: userName, password: password)
            
        }
        else {
            patientlogin(userName: userName, password: password)
        }
        //  loginAPI(userName: userName, password: password)
        
        
    }
    
    @IBAction func forgotPassButtonPressed(_ sender: MFButton) {
        
        let vc  = ForgotPassVC.instantiateFromStoryboard()
        vc.isDoctor = isDoctor
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func signUpButtonPressed(_ sender: MFButton) {
        
        if isDoctor {
            let vc  = SignUpDoctorVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let vc  = SignUpPatientVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
        
    }
    
    @IBAction func logoutDoctor(segue:UIStoryboardSegue){}
    @IBAction func logoutPatient(segue:UIStoryboardSegue){}
    
    func authenticateWithTouchID() {
        
        guard let pass = address else {
            self.biometricEnabled = false
            return
        }
        
        guard let usename = userNameField.text, usename.count > 0 else {
            self.biometricEnabled = false
            return
        }
        
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = "Retry"
        
        var authError: NSError?
        let reasonString = "to login application"
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
                
                if success {
                    DispatchQueue.main.async {
                        self.loginAPI(userName: usename, password: pass)
                    }
                    
                } else {
                    //TODO: User did not authenticate successfully, look at error and take appropriate action
                    guard let error = evaluateError else {
                        self.biometricEnabled = false
                        return
                    }
                    
                    if error._code == LAError.userFallback.rawValue {
                        self.authenticateWithTouchID()
                        
                    }
                    
                    
                    //print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                    
                    //TODO: If you have choosen the 'Fallback authentication mechanism selected' (LAError.userFallback). Handle gracefully
                    
                }
            }
        }
        else {
            localAuthenticationContext.invalidate()
        }
    }
    
    func loginAPI(userName:String, password:String) {
        
        
        
        let request = MeftiiRequests.loginApi(userName: userName, password: password)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            if code != 0 {
                
                self!.showAlertWith(title: "Failure", message: desc, buttonText: "Ok", callBack: {
                    
                })
            }
            let role = resp["role"] as? String ?? ""
            
            if role == "doctor" {
                DataManager.shared.setUserLoggedIn(userName: userName, role: role)
                self?.getDoctorProfile(userName: userName)
                
            }
            else if role == "patient" {
                DataManager.shared.setUserLoggedIn(userName: userName, role: role)
                self?.fetchPatientAndAllDoctors()
            }
            else {
                
            }
            
            
            }, failureBlock: { [weak self] (error) in
                
                self!.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                    //                    let vc  = LoginVC.instantiateFromStoryboard()
                    //  self?.navigationController?.popViewController(animated: true)
                })
                //self?.showToast(message: error.localizedDescription)
                self?.isLoadingFinished()
        })
        
    }
    func doctorlogin(userName:String, password:String) {
        
        
        //  UserDefaults.standard.set(userName, forKey: "doctor_email")
        DataManager.shared.setcurrentEmail(email: userName)
        DataManager.shared.setcurrentPass(pass: password)
        
        let request = MeftiiRequests.loginDoctor(userName: userName, password: password)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            if code == 0{
                
                
                let data = resp["data"] as! [String:Any]
                
                if data["id"] as! Int == 0 {
                    
                    self?.isLoadingFinished()
                    self!.showAlertWith(title: "Failure", message: desc, buttonText: "Ok", callBack: {
                        
                    })
                    //  self?.showToast(message: "Wrong Email or Password")
                    
                }
                    
                else {
                    
                    let patient = data
                    do {
                        let profile = try Profile_d2.init(from: patient)
                        
                        DataManager.shared.setCity(city: profile.city!.name!)
                        DataManager.shared.setState(state: profile.state!.name!)
                        
                        DataManager.shared.saveLoggedInDoctor(profile: profile)
                        
                        let vc = HomeDoctorVC.instantiateFromStoryboard()
                        self?.navigationController?.pushViewController(vc, animated: true)
                        PushNotificationManager.shared.registerForPushNotifications()
                    }
                    catch {
                        
                    }
                }
                
                
                
            }
            else {
                
                self!.showAlertWith(title: "Failure", message: desc, buttonText: "Ok", callBack: {
                    
                })
                
                
            }
            //  let role = resp["role"] as? String ?? ""
            
            //              if role == "doctor" {
            //                  DataManager.shared.setUserLoggedIn(userName: userName, role: role)
            //                  self?.getDoctorProfile(userName: userName)
            //
            //              }
            //   else if role == "patient" {
            DataManager.shared.setUserLoggedIn(userName: userName, role: "doctor")
            // self?.fetchPatientAndAllDoctors()
            // }
            
            
            
            }, failureBlock: { [weak self] (error) in
                self!.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                    
                })
                self?.isLoadingFinished()
        })
        
    }
    func patientlogin(userName:String, password:String) {
        // UserDefaults.standard.set(userName, forKey: "patient_email")
        
        DataManager.shared.setcurrentEmail(email: userName)
        DataManager.shared.setcurrentPass(pass: password)
        let request = MeftiiRequests.loginPatient(userName: userName, password: password)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            if code == 0{
                
                
                
                let data = resp["data"] as! [String:Any]
                
                if data["id"] as! Int == 0 {
                    
                    self?.isLoadingFinished()
                    self!.showAlertWith(title: "Failure", message: desc, buttonText: "Ok", callBack: {
                        
                    })
                }
                    
                else {
                    let patient = data
                    do {
                        let profile = try Profile_p2.init(from: patient)
                        //                              profile.isFingerPrintOn = self?.biometricEnabled
                        //                              profile.addressTwo = self?.address
                        
                        
                        DataManager.shared.saveLoggedInPatient(profile: profile)
                        let vc  = WarningVC.instantiateFromStoryboard()
                        self?.navigationController?.pushViewController(vc, animated: true)
                        PushNotificationManager.shared.registerForPushNotifications()
                    }
                    catch {
                        
                    }
                    
                }
                
                
                
                
            }
            else {
                
                
                self!.showAlertWith(title: "Failure", message: desc, buttonText: "Ok", callBack: {
                    
                })
            }
            //  let role = resp["role"] as? String ?? ""
            
            //              if role == "doctor" {
            //                  DataManager.shared.setUserLoggedIn(userName: userName, role: role)
            //                  self?.getDoctorProfile(userName: userName)
            //
            //              }
            //   else if role == "patient" {
            DataManager.shared.setUserLoggedIn(userName: userName, role: "patient")
            // self?.fetchPatientAndAllDoctors()
            // }
            
            
            
            }, failureBlock: { [weak self] (error) in
                self!.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                    
                })
                self?.isLoadingFinished()
        })
        
    }
    
    //getDoctorProfile API
    func getDoctorProfile(userName:String) {
        
        let request = MeftiiRequests.getDoctorProfile(userName: userName)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            print(resp)
            
            if code == 0 {
                if let someObj = resp["doctorProfile"] {
                    
                    do {
                        let data = try JSONSerialization.data(withJSONObject: someObj!, options: .prettyPrinted)
                        let profile = try JSONDecoder().decode(Profile_d2.self, from: data)
                        profile.isFingerPrintOn = self?.biometricEnabled
                        profile.street = self?.address
                        DataManager.shared.setCity(city: profile.city!.name!)
                        DataManager.shared.setState(state: profile.state!.name!)
                        DataManager.shared.saveLoggedInDoctor(profile: profile)
                        
                        let vc = HomeDoctorVC.instantiateFromStoryboard()
                        self?.navigationController?.pushViewController(vc, animated: true)
                        PushNotificationManager.shared.registerForPushNotifications()
                    }
                    catch {
                        
                    }
                }
            }
            
            }, failureBlock: { [weak self] (error) in
               // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                                    
                                                    })
                self?.isLoadingFinished()
        })
        
    }
    
    //Fetch Patient Profile and then fetch all doctors
    func fetchPatientAndAllDoctors()  {
        
        let userName = DataManager.shared.userName ?? ""
        let request = MeftiiRequests.getPatientProfile(username: userName)
        
        self.isLoading()
        
        request.start(vc: nil, successBlock: { [weak self] (code, desc, resp) in
            
            self?.isLoadingFinished()
            
            let data = resp["patientProfile"]
            do {
                let profile = try Profile_p2(from: data!!)
                profile.isFingerPrintOn = self?.biometricEnabled
                profile.street = self?.address
                DataManager.shared.saveLoggedInPatient(profile: profile)
            }
            catch {
                
            }
            
            
            
            let regst = MeftiiRequests.getAllDoctorList(userName: userName)
            regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
                
                if let someObj = respTwo["doctorProfiles"] {
                    
                    do {
                        let data = try JSONSerialization.data(withJSONObject: someObj!, options: .prettyPrinted)
                        //                        let arr = try JSONDecoder().decode([DoctorProfile].self, from: data)
                        //                        DataManager.shared.setAllDoctorsProfile(arr)
                    }
                    catch {
                        
                    }
                }
                
                let vc  = WarningVC.instantiateFromStoryboard()
                self?.navigationController?.pushViewController(vc, animated: true)
                PushNotificationManager.shared.registerForPushNotifications()
                
                }, failureBlock: nil)
            
            
            
            
            }, failureBlock: { [weak self] (error) in
               // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                                    
                                                    })
                self?.isLoadingFinished()
        })
        
    }
}

extension LoginVC:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                           replacementString string: String) -> Bool
    {
        let maxLength = 25
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

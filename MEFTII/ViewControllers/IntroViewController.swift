//
//  IntroViewController.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 20/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class IntroViewController: BaseViewController {

    @IBOutlet weak var steppedVw: MFSteppedView!
    @IBOutlet weak var btnNext: MFButton!
    @IBOutlet weak var btnPrv: MFButton!
    @IBOutlet weak var headingImage: UIImageView!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    var currentStep = 0
    
    var timer : Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        moveToPage(0)
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self,
                                            selector: #selector(self.leftSwipe(_:)), userInfo: nil, repeats: true)
        }
        DataManager.shared.setIntroductionVcShown()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        
    }
    
    
    
    

    @IBAction func leftSwipe(_ sender: Any?) {
        
        let newStep = currentStep + 1
        if newStep >= 3 {
            btnNextPressed(UIButton())
            return
        }
        if newStep < 0 {
            return
        }
        
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self,
                                            selector: #selector(self.leftSwipe(_:)), userInfo: nil, repeats: true)
        }
        
        moveToPage(newStep)
    }
    
    @IBAction func rightSwipe(_ sender: UISwipeGestureRecognizer) {
        
        let newStep = currentStep - 1
        if newStep >= 3 {
            return
        }
        if newStep < 0 {
            return
        }
        
        if timer != nil {
            timer.invalidate()
            timer = nil
        }
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 3.0, target: self,
                                            selector: #selector(self.leftSwipe(_:)), userInfo: nil, repeats: true)
        }
        
        moveToPage(newStep)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func btnNextPressed(_ sender: UIButton) {
        
        let vc = ChoosePatientDoctorVC.instantiateFromStoryboard()
        vc.nextVcIsLogin = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnPrvPressed(_ sender: Any) {
        //goto signup vc
        //goto chose patient doctor vc
        
        let vc = ChoosePatientDoctorVC.instantiateFromStoryboard()
        vc.nextVcIsLogin = false
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    func moveToPage(_ no:Int) -> Void {

        if no > currentStep {
            headingImage.addTransition(0.4, fromLeft: false)
            lblHeading.addTransition(0.4, fromLeft: false)
            lblDescription.addTransition(0.4, fromLeft: false)
        }
        else {
            headingImage.addTransition(0.4, fromLeft: true)
            lblHeading.addTransition(0.4, fromLeft: true)
            lblDescription.addTransition(0.4, fromLeft: true)
        }
        
        btnNext.addNextArrow = true
        btnNext.setTitle("SKIP", for: .normal)
        btnPrv.isHidden = true
        
        currentStep = no
        steppedVw.moveTo(Step: currentStep)
        
        var desc = "or a psychologist from the comfort of your home."
        var hed = "See a Doctor"
        var img = "intro1"
        switch currentStep {
        case 0:
            desc = "or a psychologist from the comfort of your home."
            hed = "See a Doctor"
            img = "intro1"
        case 1:
            desc = "matching your symptoms."
            hed = "Get a Board Certified Doctor"
            img = "intro2"
        default:
            desc = "or refill at the pharmacy of your choice."
            hed = "Get Your Prescription"
            img = "intro3"
        }
        
        lblHeading.text = hed
        lblDescription.text = desc
        headingImage.image = UIImage(named: img)
        
    }
}

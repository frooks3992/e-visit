//
//  FindDoctorMainVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 07/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class FindDoctorMainVC: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func byNamePressed(_ sender: UIButton) {
        let vc = FindDoctorMapVC.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bySpecialityPressed(_ sender: UIButton) {
        let vc = FindDoctorMapVC.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func inviteDoctoryPressed(_ sender: UIButton) {
        
    }
    

}

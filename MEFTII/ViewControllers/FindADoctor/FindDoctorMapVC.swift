//
//  FindDoctorMapVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 07/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class FindDoctorMapVC: BaseViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var headingLabel: UILabel!
    var selectedDoctor: Profile_d3!
    var searchTimer: Timer?
    
    //let locationManager = CLLocationManager()
    var doctorlist: [Profile_d3] = []
    var docArrayToShow: [Profile_d3] = []
    var onCallDoctorId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      //  tableVw.isHidden = true
        self.searchField.delegate = self
        self.searchField.addTarget(self, action: #selector(textFieldDidEditingChanged(_:)), for: .editingChanged)
        
        tableVw.tableFooterView = UIView()
        /*
        //Mark: - Authorization
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        mapVw.delegate = self
        mapVw.mapType = MKMapType.standard
        mapVw.showsUserLocation = true
        */
        //Show doctors on map
        
//        if let _ = onCallDoctorId {
//            headingLabel.text = "On Call Doctor"
//            //fetchOnCallDoctorsAndReload()
//             fetchAllDoctorsAndReload()
//        }
//        else if let docList = DataManager.shared.allDoctors {
//            doctorlist = Array(docList)
//            docArrayToShow = doctorlist
//        }
//        else {
            fetchAllDoctorsAndReload()
      //  }
        
        tableVw.reloadData()
 
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? ViewDoctorVC {
            
            vc.doctor = selectedDoctor
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    /*
    func reloadPinsOnMap() {
        
        for ann in mapVw.annotations {
            mapVw.removeAnnotation(ann)
        }
        
        var counter = 0
        var lat : Double = 0.0
        var long : Double = 0.0
        for doctor in docArrayToShow {
            
            let annotation = CustomPointAnnotation()
            
            if counter == 0 {
                lat = 33.684422
                long = 73.047882
            } else if counter == 1 {
                lat = 24.860735
                long = 67.001137
            }
            else {
                lat = Double(doctor.latitude ?? "x") ?? lat + 2
                long = Double(doctor.longitude ?? "x") ?? long + 2
            }
            
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            annotation.doctorProfile = doctor
            
            mapVw.addAnnotation(annotation)
            
            counter += 1
            
        }
        
    }
    
    */
    
    
    func fetchAllDoctorsAndReload() {
        
        self.isLoading()
        let regst = MeftiiRequests.getaddeddoctors(userName: DataManager.shared.currentPatientId )
        regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            
            
            self?.isLoadingFinished()
            if let someObj = respTwo["data"] {
                
                do {
                    let data = try JSONSerialization.data(withJSONObject: someObj!["doctorProfileList"]!, options: .prettyPrinted)
                    let arr = try JSONDecoder().decode([Profile_d3].self, from: data)
                    self?.docArrayToShow = arr
                    self?.doctorlist = arr
                  //  DataManager.shared.setAllDoctorsProfile(arr)
                    
                }
                catch {
                    
                }
                
                if self!.docArrayToShow.count > 0 {
                    self!.tableVw.isHidden = false
                }
                else {
                     self!.tableVw.isHidden = true
                }
                
                self?.tableVw.reloadData()
            }
            
        }, failureBlock: nil)
    }
//
//    func fetchOnCallDoctorsAndReload() {
//        let regst = MeftiiRequests.getOnCallDoctors(for: onCallDoctorId ?? "", patient: DataManager.shared.userName ?? "" )
//        regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
//            if let someObj = respTwo["doctorProfiles"] {
//                do {
//                    let data = try JSONSerialization.data(withJSONObject: someObj!, options: .prettyPrinted)
//                    let arr = try JSONDecoder().decode([DoctorProfile].self, from: data)
//                    self?.doctorlist = arr
//                    self?.docArrayToShow = arr
//                }
//                catch {
//
//                }
//                self?.tableVw.reloadData()
//            }
//        }, failureBlock: nil)
//    }
    
}

extension FindDoctorMapVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docArrayToShow.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFTableViewCell else {
            return UITableViewCell()
        }
        
        let doctor = docArrayToShow[indexPath.row]
        
        let urlll = URL(string: doctor.profilePicture ?? "")
        cell.imgProfile?.sd_setImage(with: urlll, placeholderImage: setDummyImage(gender: doctor.gender!))
                 
        
        cell.lblName.text = "Dr. \(doctor.completeName)"
        cell.lblValue?.text = doctor.speciality
        cell.ratingVw?.rating = 0
        cell.lblUnreadCount?.text = "(0)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let doctor = docArrayToShow[indexPath.row]
        selectedDoctor = doctor

//        if let _ = onCallDoctorId {
//            if let docterUserName = doctor.firstName {
//
//                let realm = try! Realm()
//                let results = realm.objects(MWConversation.self).filter("conversationId == %@", docterUserName)
//
//                if let convo = results.first {
//
//                    let chatViewController = ChatDetailVC(conversation: convo)
//                    navigationController?.pushViewController(chatViewController, animated: true)
//
//                }
//                else {
//                    let conversation = MWConversation()
//
//                    var uName = docterUserName.lowercased()
//                    uName = uName.components(separatedBy: CharacterSet.letters.inverted).joined()
//                    conversation.conversationId = uName
//                    let name = doctor.completeName.capitalized
//                    conversation.displayName = name
//
//                    try! realm.write {
//                        realm.add(conversation, update: .all)
//                    }
//
//                    let chatViewController = ChatDetailVC(conversation: conversation)
//                    navigationController?.pushViewController(chatViewController, animated: true)
//                }
//
//            }
//        } else {
            let viewDoctorVC = ViewDoctorVC.instantiateFromStoryboard()
          viewDoctorVC.doctor = doctor
        viewDoctorVC.isAppointment = true
            self.navigationController?.pushViewController(viewDoctorVC, animated: true)
           
        
        
       // }
        
    }
}





extension FindDoctorMapVC : UITextFieldDelegate {
    
    @objc func textFieldDidEditingChanged(_ textField: UITextField) {

        // if a timer is already active, prevent it from firing
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }

        // reschedule the search: in 1.0 second, call the searchForKeyword method on the new textfield content
        searchTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text ?? "", repeats: false)
    }

    @objc func searchForKeyword(_ timer: Timer) {

        // retrieve the keyword from user info
        let keyword = ((timer.userInfo as? String) ?? "").lowercased()

        print("Searching for keyword \(keyword)")
        
        if keyword.count > 0 {
//            docArrayToShow = DataManager.shared.allDoctors?.filter({ ($0.completeName.lowercased()).contains(keyword) }) ?? []
        }
        else {
            docArrayToShow = doctorlist
        }
        
        tableVw.reloadData()
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.containsWhitespace{
                    
                 if string == " " && textField.text?.last == " "{
                          return false
                    }
         
             }
        
        return true
    }
    
}

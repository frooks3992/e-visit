//
//  ViewDoctorVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 11/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import RealmSwift

class ViewDoctorVC: BaseViewController {
    
    var doctorProfile: Profile_d3?
    var isOnCallDoc: Bool = false
    @IBOutlet weak var chatBtn: MFButton!
    @IBOutlet weak var appointmentBtn: MFButton!
    
    @IBOutlet weak var callBtn: MFButton!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var profileImgVw: UIImageView!
    @IBOutlet weak var lblDoctorName: UILabel!
    var doctor: Profile_d3!
    var isAppointment: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        setupViews()
    }
    
    func setupViews() {
        
       
        guard let doctorProfile = doctor else { return }
        
        if doctor.urgentCallFlag == "1" {
            
            chatBtn.isHidden = true
            
        }
        else{
            callBtn.isHidden = true
        }
        
        
        lblDoctorName.text = doctor.firstName
        
        let isFav = DataManager.shared.isDoctorFavorite(doctorProfile.id ?? -1)
        
        if isFav {
            favBtn.tag = 78
            favBtn.setImage(UIImage(named: "heart_filled"), for: .normal)
        }
        else {
            favBtn.tag = 77
            favBtn.setImage(UIImage(named: "heart_empty"), for: .normal)
        }
        
        if isOnCallDoc {
            callBtn.color = UIColor.MFRed
            callBtn.setTitle("Set Timing", for: .normal)
            
            if !isAppointment{
                callBtn.isHidden = true
                appointmentBtn.isHidden = true
                
            }
        }
        
        if let pic = doctorProfile.profilePicture {
            profileImgVw.sd_setImage(with: URL(string: (pic)), placeholderImage: setDummyImage(gender: doctorProfile.gender!))
        }
     
    }
    
    @IBAction func message_Pressed(_ sender: MFButton) {
        
    }
    
    @IBAction func MakeAppointment(_ sender: Any) {
             let vc = PatientAppointmentVC.instantiateFromStoryboard("Schedule")
        vc.doctorId = doctor.id
          self.navigationController?.pushViewController(vc, animated: true)
        
       // performSegue(withIdentifier: "toSchedule", sender: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
               
             let destinationVc = segue.destination as! PatientAppointmentVC
              
        destinationVc.doctorId = doctor.id
         }
      
    
    
    @IBAction func call_Pressed(_ sender: MFButton) {
        
        if isOnCallDoc {
            
            let chatViewController = SetOnCallDocTimingVC.instantiateFromStoryboard()
            chatViewController.onCallDoctor = doctorProfile
            navigationController?.pushViewController(chatViewController, animated: true)
            
        }
        else if let docterUserName = doctorProfile?.firstName {
            
            let realm = try! Realm()
            let results = realm.objects(MWConversation.self).filter("conversationId == %@", docterUserName)
            
            if let convo = results.first {
                
                let chatViewController = ChatDetailVC(conversation: convo)
                navigationController?.pushViewController(chatViewController, animated: true)
                
            }
            else {
                let conversation = MWConversation()
                
                var uName = docterUserName.lowercased()
                uName = uName.components(separatedBy: CharacterSet.letters.inverted).joined()
                conversation.conversationId = uName
                let name = (doctorProfile?.completeName ?? "").capitalized
                conversation.displayName = name
                
                try! realm.write {
                    realm.add(conversation, update: .all)
                }
                
                let chatViewController = ChatDetailVC(conversation: conversation)
                navigationController?.pushViewController(chatViewController, animated: true)
            }
            
            
        }
        
    }
    
    @IBAction func favorite_Pressed(_ sender: UIButton) {
        
        if sender.tag == 77 {
            sender.tag = 78
            sender.setImage(UIImage(named: "heart_filled"), for: .normal)
            DataManager.shared.markDoctorFavorite(doctorProfile?.id ?? -1)
        }
        else {
            sender.tag = 77
            sender.setImage(UIImage(named: "heart_empty"), for: .normal)
            DataManager.shared.removeFavoriteDoctor(doctorProfile?.id ?? -1)
        }
        
    }
    
    func getTimingsString(timing:Timings) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "h:mm a"
        var to = ""
        var from = ""
        
        let apiFormat = DateFormatter()
        apiFormat.dateFormat = "yyyy-MM-dd HH:mm:ss"
        apiFormat.dateFormat = "HH:mm"
        
        if let toDate = apiFormat.date(from: timing.to ?? "") {
            to = formatter.string(from: toDate)
        }
        
        if let fromDate = apiFormat.date(from: timing.from ?? "") {
            from = formatter.string(from: fromDate)
        }
        
        return "\(from) - \(to)"
        
    }

    
    
}

extension ViewDoctorVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 4
        }
//        else if section == 1 {
//            if doctorProfile?.reviews?.count == 0 {
//                return 1
//            } else {
//                guard let doctorProfile = doctorProfile, let reviews = doctorProfile.reviews else { return 1 }
//                return reviews.count
//            }
//        }
//
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        else if section == 1 {
            return 70
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
          let myLabel = UILabel()
            myLabel.frame = CGRect(x: 25, y: 20, width: 320, height: 20)
            myLabel.font = UIFont.AppBoldFont(ofSize: 20)
            myLabel.text = "Reviews"

            let headerView = UIView()
            headerView.addSubview(myLabel)

            return headerView
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
     //   (view as! UITableViewHeaderFooterView).tintColor = .white
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as? MFTableViewCell else {
                return UITableViewCell()
            }
            
            if indexPath.row == 0 {
                cell.lblName.text = "Speciality"
                cell.lblValue?.text = doctor?.speciality ?? " "
                
                return cell
            } else if indexPath.row == 1 {
                
                guard let starCell = tableView.dequeueReusableCell(withIdentifier: "starsCell") as? MFTableViewCell else {
                    return UITableViewCell()
                }
                starCell.ratingVw?.rating = 2
                starCell.lblName.text = "Rating"
                
                return starCell
//            } else if indexPath.row == 2 {
//                cell.lblName.text = "Availability"
//                cell.lblValue?.text = " "
////                if let timng = doctorProfile?.timings, timng.count > 0 {
////                    cell.lblValue?.text = getTimingsString(timing: timng[0])
////                }
                
                return cell
            } else if indexPath.row == 2 {
                cell.lblName.text = "Cost per session"
                cell.lblValue?.text = "$" + "50" ?? " "
                
                return cell
            } else if indexPath.row == 3 {
                cell.lblName.text = "Gender"
                cell.lblValue?.text = doctor?.gender ?? " "
                
                return cell
            } else if indexPath.row == 4 {
                guard let buttonCell = tableView.dequeueReusableCell(withIdentifier: "buttonCell") as? MFTableViewCell, !isOnCallDoc else {
                    return UITableViewCell()
                }
                
                buttonCell.makeAppointmentTapped = {
                    let appointmentDateTimeVC = AppointmentDateTimeVC.instantiateFromStoryboard()
                    appointmentDateTimeVC.doctor = self.doctorProfile
                    self.navigationController?.pushViewController(appointmentDateTimeVC, animated: true)
                }
                
                return buttonCell
            }
            
        case 1:
            guard let reviewCell = tableView.dequeueReusableCell(withIdentifier: "reviewCell") as? MFTableViewCell else {
                return UITableViewCell()
            }
            
//            if let doctorProfile = doctorProfile, let reviews = doctorProfile.reviews {
//                if reviews.count == 0 {
//                    reviewCell.lblName.text = "No reviews"
//                    reviewCell.lblValue?.text = " "
//
//                    return reviewCell
//                }
//            }
            
//
//            reviewCell.lblName.text = doctorProfile?.reviews?[indexPath.row].patientName
//            reviewCell.lblValue?.text = doctorProfile?.reviews?[indexPath.row].reviewText
            
            return reviewCell
            
        default:
            print("Section does not exist")
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
    }
}







//
//  RateDoctorVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 15/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import FloatRatingView

class RateDoctorVC: BaseViewController {

    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var reviewField: MFTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    @IBAction func submitTapped(_ sender: MFButton) {
        
        let reviewText = reviewField.text ?? ""
        
        guard reviewText.count > 0 else {
            showToast(message: "Review cannot be empty ⚠️")
            return
        }
        
        var review = Review()
        review.reviewText = reviewText
        review.patientName = DataManager.shared.currentPatient?.firstName ?? ""
        review.docid = DataManager.shared.currentDoctorId
        
        updateDoctorReviewRating(userName: DataManager.shared.userName ?? "", rating: Int(ratingView?.rating ?? 0), review: review)
        
    }
    
    //updateDoctorReviewRating API
    func updateDoctorReviewRating(userName: String, rating:Int, review: Review) {
        
        let request = MeftiiRequests.updateDoctorReviewRating(userName: userName, rating: rating, review: review)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            print(resp)
            
            if code == 0 {
                self?.showToast(message: "Success")
            }
            
            }, failureBlock: { [weak self] (error) in
                self?.showToast(message: error.localizedDescription)
                self?.isLoadingFinished()
        })
        
    }

}

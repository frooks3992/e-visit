//
//  HomePatientVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 02/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import SDWebImage

class HomePatientVC: BaseViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblProfileName: UILabel!
    @IBOutlet weak var lblHeading: UILabel!
    @IBOutlet weak var sideMenuTableVw: UITableView!
    @IBOutlet weak var constraintSideMenu: NSLayoutConstraint!
    @IBOutlet weak var constraintTransparentVwWidth: NSLayoutConstraint!
    @IBOutlet weak var appointmentTableVw: UITableView!
    @IBOutlet weak var sideMenu: UIView!
    
    var leftMenuOpen = false
    
    @IBOutlet weak var PlaceholderLbl: UILabel!
    var Appointments = [appoitments]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
  sideMenu.isHidden = true
        sideMenuTableVw.removeExtraRows()
        
        // Do any additional setup after loading the view.
        let nib = UINib(nibName: "AppointmentHeaderView", bundle: nil)
        appointmentTableVw.register(nib, forHeaderFooterViewReuseIdentifier: "header")
        let firstName = (DataManager.shared.currentPatient?.firstName ?? "").capitalized
        let completeName = (DataManager.shared.currentPatient?.completeName ?? "").capitalized
        lblHeading.text = "Hi \(firstName)!"
        lblProfileName.text = "\(completeName)"
        
//        let urlll = URL(string: (DataManager.shared.currentPatient?.profilePicture)!))
    

        
        
        homePatientApiCalls()
    }
    
    func homePatientApiCalls() {
        
        let id = DataManager.shared.currentPatientId
        
        let apointmentRqst = MeftiiRequests.getpatientappointments(patientId: DataManager.shared.currentPatientId)
        
        self.isLoading()
        
        apointmentRqst.start(vc: nil, successBlock: { [weak self] (code, desc, resp) in
            
            if let Obj = resp["data"] {
                           
                           do {
                               let someObj = Obj!["appointmentList"]
                                   
                               
                               
                               let data = try JSONSerialization.data(withJSONObject: someObj!, options: .prettyPrinted)
                            let arr = try JSONDecoder().decode([appoitments].self, from: data)
                             
                               self?.Appointments = arr
                               
                            
                        
                           }
                           catch {
                               
                           }
                
                
                if self!.Appointments.count > 0 {
                    self?.appointmentTableVw.isHidden = false
                    self!.PlaceholderLbl.isHidden = true
                }
                else{
                    self?.appointmentTableVw.isHidden = true
                    self!.PlaceholderLbl.isHidden = false
                }
                           
                           self?.appointmentTableVw.reloadData()
                       }
            
            
            
            
            
            
            
            
//            let obj = resp["data"] as? [String:AnyObject]
//            do {
//                let data = try JSONSerialization.data(withJSONObject: obj!["appointmentList"]!, options: .prettyPrinted)
//                let arr = try JSONDecoder().decode([appointments].self, from: data)
//                self?.appointments = arr
//
//            }
//            catch {
//
//            }
            
            /*
            
            for app in self?.appointments ?? [] {
                
                if let doc = DataManager.shared.allDoctors?.first(where: { (appoint) -> Bool in
                    appoint.user_id == app.doctorId
                }) {
                    app.doctorName = "\(doc.firstname ?? "") \(doc.lastname ?? "")"
                    app.doctorImage = Constants.imageAddress + "profile/" + (doc.username ?? "") + ".jpg"
                }
            }
            */
            
            self?.appointmentTableVw.reloadData()
            self?.isLoadingFinished()
            
        }, failureBlock: {[weak self] (error) in
            self?.isLoadingFinished()
           // self?.showToast(message: error.localizedDescription)
            self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                     
                                     })
            
        })
        
    }
    
    @IBAction func fromappointmentSent(segue:UIStoryboardSegue){
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let pic = DataManager.shared.currentPatient!.profilePicture {
            let urlll = URL(string: (pic))
            imgProfile.sd_setImage(with: URL(string: (DataManager.shared.currentPatient?.profilePicture)!), placeholderImage: setDummyImage(gender: (DataManager.shared.currentPatient!.gender)!))
         }
        
        else{
            imgProfile.image = setDummyImage(gender: (DataManager.shared.currentPatient!.gender!))
        }
    }
    
    
    @IBAction func notificationPressed(_ sender: UIButton) {
        let vc = UrgentCallSentRequestsVC.instantiateFromStoryboard("UrgentCall")
              
                  self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    @IBAction func seeADoctorPressed(_ sender: UIButton) {
        
        let vc = FindDoctorMapVC.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func menuPressed(_ sender: UIButton) {
        showSideMenu(animated:true)
    }
    
    @IBAction func crossPressed(_ sender: UIButton) {
        hideSideMeny(animated: true)
    }
    
    @IBAction func transparentViewTapped(_ sender: Any) {
        hideSideMeny(animated: true)
    }
    
    @objc func cellButtonPressed(_ sender: UIButton) {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.locale = NSLocale(localeIdentifier: "en") as Locale
        dateFormatter1.dateFormat = Constants.kHomeDateFormate
        
        let row = sender.tag - 10
        let appointment = Appointments[row]
        /*
        var shouldStartCall = false
        if let appointmentDate = dateFormatter1.date(from: appointment.from ?? "") {
            if appointmentDate.timeIntervalSinceNow < 3600 {
                shouldStartCall = true
            }
        }
        
        if appointment.status == "1" {
            if shouldStartCall {
                
                showAlertWith(title: "Are you sure to start call?", message: "", buttonText: "YES", callBack: {
                    
                    
                }, leftButtonText: "NO", leftCallBack: {
                    
                })
                
            }
        }
        
        */
        
        if let docterUserName = appointment.firstName {
           
            
            let reuser = docterUserName + String(Date().timeIntervalSinceNow)
            
            let tokenRqst = MeftiiRequests.getTokensForCall(sender: DataManager.shared.userName ?? "", receiver: reuser)
            
            tokenRqst.start(vc: self, successBlock: { (code, desc, resp) in
                if code == 0 {
                    let senderToken = resp["senderToken"] as? String
                    let receiverToken = resp["receiverToken"] as? String
                    let roomName = resp["roomName"] as? String
                    
                    if let sToken = senderToken,  let rToken = receiverToken, let rName = roomName {
                        self.initiateCall(toDoctor: docterUserName, receiverTkn: rToken, sendrTkn: sToken, room: rName)
                    }
                }
            }, failureBlock: nil)
        }
    }
    
    func gotoPage(indexNumber:Int) {
        
        switch indexNumber {
        case 0:
            hideSideMeny(animated: false)
            let vc = ProfilePatientVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            break
//        case 1:
//            hideSideMeny(animated: false)
//            let vc = MessagesVC.instantiateFromStoryboard()
//            self.navigationController?.pushViewController(vc, animated: true)
//            break
        case 1:
            hideSideMeny(animated: false)
            let vc = OnCallDoctorsVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 2:
            hideSideMeny(animated: false)
            let vc = FindDoctorMapVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 3:
            hideSideMeny(animated: false)
//            let vc = PickPharmacyVC.instantiateFromStoryboard()
            let vc = UrgentCallAddedDoctors.instantiateFromStoryboard("UrgentCall")
            
            self.navigationController?.pushViewController(vc, animated: true)
            break
            
//        case 4:
//            hideSideMeny(animated: false)
//            //            let vc = PickPharmacyVC.instantiateFromStoryboard()
//           // let vc = UrgentCallAddedDoctors.instantiateFromStoryboard("UrgentCall")
//
////            self.navigationController?.pushViewController(vc, animated: true)
//            break
//        case 5:
//            hideSideMeny(animated: false)
//            let vc = SessionHistoryVC.instantiateFromStoryboard()
//            self.navigationController?.pushViewController(vc, animated: true)
//            break
//        case 6:
//           hideSideMeny(animated: false)
////            let vc = ReportsVC.instantiateFromStoryboard()
////            self.navigationController?.pushViewController(vc, animated: true)
//            break
        case 4:
            hideSideMeny(animated: false)
            DataManager.shared.logoutCurrentUser(isForceLogout: true)
           // performSegue(withIdentifier: "logoutPatient", sender: nil)
            break
            
        default:
            
            hideSideMeny(animated: true)
        }
        
    }
    
    func hideSideMeny(animated:Bool) {
        sideMenu.isHidden = true
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.constraintTransparentVwWidth.constant = 0
                self.constraintSideMenu.constant = 0
                self.view.layoutIfNeeded()
            }) { (completed) in
                self.leftMenuOpen = false
                self.sideMenu.layer.masksToBounds = true
            }
        }
        else {
            self.sideMenu.layer.masksToBounds = true
            self.constraintTransparentVwWidth.constant = 0
            self.constraintSideMenu.constant = 0
            self.leftMenuOpen = false
        }
        
    }
    
    func showSideMenu(animated:Bool) {
        sideMenu.isHidden = false
        sideMenu.addRightShadow()
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.constraintTransparentVwWidth.constant = 500
                self.constraintSideMenu.constant = 300
                self.view.layoutIfNeeded()
            }) { (completed) in
                self.leftMenuOpen = true
            }
        }
        else {
            self.constraintTransparentVwWidth.constant = 500
            self.constraintSideMenu.constant = 300
            self.leftMenuOpen = true
            self.view.layoutIfNeeded()
        }
    }
    
    
}

extension HomePatientVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView === appointmentTableVw {
            return tableView.dequeueReusableHeaderFooterView(withIdentifier: "header")
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView === appointmentTableVw {
            return 50
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView === appointmentTableVw {
            return Appointments.count
        }
        else if tableView === sideMenuTableVw  {
            return 5
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFTableViewCell else {
            return UITableViewCell()
        }
        
        let row = indexPath.row
        if tableView === appointmentTableVw {
            
            let appointment = Appointments[row]
            
            
            let urlll = URL(string: (appointment.profilePicture) ?? "")
            
            
            cell.imgProfile?.sd_setImage(with: urlll, placeholderImage: nil)
            cell.lblDateTime?.text = appointment.timeSlot! + "\n" + appointment.date!
            cell.lblName.text = "Dr. " + (appointment.firstName ?? "").capitalized
            cell.btnCall?.tag = 10 + row
            cell.btnCall?.addTarget(self, action: #selector(cellButtonPressed(_:)), for: .touchUpInside)
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.locale = NSLocale(localeIdentifier: "en") as Locale
            dateFormatter1.dateFormat = "h:mm a yyyy/MM/dd"
            
            var showCallButton = false
            
            if let appointmentDate = dateFormatter1.date(from: appointment.date ?? "") {
                if (appointmentDate.timeIntervalSinceNow / 1000) < 3600 {
                    showCallButton = true
                }
            }
            let status = (appointment.status ?? "").lowercased()
            if status == "approved" || status == "1"{
                if showCallButton {
                    cell.btnCall?.setTitle("Call", for: UIControl.State.normal)
                
                    cell.btnCall?.isFilled = true
                    cell.btnCall?.color = UIColor.MFLightBlue
                }
                else {
                    cell.btnCall?.setTitle("Approved", for: UIControl.State.normal)
                    cell.btnCall?.isFilled = false
                    cell.btnCall?.color = UIColor.MFGreen
                }
            }
            else {
                cell.btnCall?.setTitle("Pending", for: UIControl.State.normal)
                cell.btnCall?.isFilled = false
                cell.btnCall?.color = UIColor.MFLightBlue
            }
            
        }
        else if tableView === sideMenuTableVw  {
            cell.btnCall?.isHidden = true
            if row == 0 {
                cell.lblName.text = "Profile"
            }
                //            else if row == 1 {
                //                cell.lblName.text = "Messages"
                //                cell.btnCall?.isHidden = false
                //            }
            else if row == 1 {
                cell.lblName.text = "Your Doctors"
            }
            else if row == 2 {
                cell.lblName.text = "Appointments"
            }
            else if row == 3 {
                cell.lblName.text = "Urgent Call"
            }
                
//            else if row == 4 {
//                cell.lblName.text = "Pharmacy"
//            }
//            else if row == 5 {
//                cell.lblName.text = "Session History"
//            }
//            else if row == 6 {
//                cell.lblName.text = "Invite People"
//            }
//            else if row == 7 {
//                cell.lblName.text = "Contact Us"
//            }
//            else if row == 8 {
//                cell.lblName.text = "Help"
//            }
            else if row == 4 {
                cell.lblName.text = "Logout"
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if tableView === sideMenuTableVw {
            gotoPage(indexNumber:indexPath.row)
        }
    }
}


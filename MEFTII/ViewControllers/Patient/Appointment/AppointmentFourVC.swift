//
//  AppointmentThreeVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 13/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class AppointmentFourVC: BaseViewController {

    @IBOutlet weak var symptomsField: MFTextField!
    @IBOutlet weak var conditionsField: MFTextField!
    @IBOutlet weak var txtSymptomsOtherField: MFTextField!
    @IBOutlet weak var txtOtherField: MFTextField!
    var SelectedPickerType:Int!
    var idx: Int?
    var appointment: Appointment?
    var symptoms:[String]=[]
    var symptomsIds:[Int]=[]
    var ConditionId:[Int]=[]
    var SelectedCondition :String!
    var docId:Int!
    var appointmentSlotId:Int!
   
   
    @IBOutlet weak var isEmergencyView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isEmergencyView.isHidden = true
        txtOtherField.isHidden = true
        txtSymptomsOtherField.isHidden = true
        /*symptomsField.dataStrings = Constants.symptoms.map{ $0.name ?? "" }
        
        conditionsField.dataStrings = Constants.conditions.map{ $0.name ?? "" }
        
        
        conditionsField.closure = { [weak self]  index,  value in
                self?.txtOtherField.isHidden = false
            }
            else {
                self?.txtOtherField.isHidden = true
            }
        }
        
        symptomsField.closure = { [weak self]  index,  value in
            if value.lowercased().contains("other") {
                self?.txtSymptomsOtherField.isHidden = false
            }
            else {
                self?.txtSymptomsOtherField.isHidden = true
            }
        }*/
    }
    
    
    @IBAction func closeEmergencyView(_ sender: Any) {
        
        hideShowEmergency(sett: true)
    }
    
    
    @IBAction func unwindFromReasonPicker(_ sender: UIStoryboardSegue){
        
          if sender.source is reason_PickerVC{
              
              if let senderVC = sender.source as? reason_PickerVC {
                
                senderVC.symptomsIds = symptomsIds
                senderVC.ConditionId = ConditionId
                  print(symptomsIds)
                print(ConditionId)
                
                if senderVC.isEmergency != nil{
                           
                                 hideShowEmergency(sett: !senderVC.isEmergency)
                                }
                                
                            
                
                
                
                if senderVC.SelectedSymptom != nil{
                    
                    symptoms = senderVC.SelectedSymptom
                    symptomsField.text =
                        senderVC.SelectedSymptom.joined(separator: ", ")
                    if senderVC.SelectedSymptom.contains("Other"){
                        self.txtSymptomsOtherField.isHidden = false
                    }else{
                        self.txtSymptomsOtherField.isHidden = true
                    }
                    print("Symptoms====\n======\n\(senderVC.SelectedSymptom)")
                    //CityTF.text = senderVC.SelectedSymptom
                  }
                  
                if senderVC.SelectedCondition != nil{
                    conditionsField.text = senderVC.SelectedCondition
                    if senderVC.SelectedCondition.lowercased().contains("other"){
                        self.txtOtherField.isHidden = false
                    }else{
                        self.txtOtherField.isHidden = true
                    }
                     // StateTF.text = senderVC.SelectedCondition
                  }
                  
              }
              
          }
          
      }

    
func hideShowEmergency(sett:Bool){
    
    
    self.isEmergencyView.isHidden = sett
}

    @IBAction func City_StatePressed(_ sender: UIButton) {
        SelectedPickerType  = sender.tag
        performSegue(withIdentifier: "ToReason_Picker", sender: SelectedPickerType)
    }
    
    
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        let symptoms = symptomsField.text ?? ""
        let conditions = conditionsField.text ?? ""
        
        if (symptoms != "") || (conditions != "") {
            
        }
        else {
            
            self.showAlertWith(title: "Warning", message: "Please choose a symptom or condition", buttonText: "Ok", callBack: {
                                         
                                     })
           // showToast(message: "Please choose symtom or condition ⚠️")
            return
        }
        
//        if let idx = symptomsField.selectedIndex {
//            appointment?.symptoms = Constants.symptoms[idx]
//        }
//
//        if let idx = conditionsField.selectedIndex {
//            appointment?.condition = Constants.conditions[idx]
//        }

        if let idx = self.idx {
            appointment?.symptoms = Constants.symptoms[idx]
        }
        
        if let idx =  self.idx {
            appointment?.condition = Constants.conditions[idx]
        }
       
//        let appointmentOneVC = AppointmentOneVC.instantiateFromStoryboard()
//        appointmentOneVC.appointment = appointment
//        navigationController?.pushViewController(appointmentOneVC, animated: true)
        
        performSegue(withIdentifier: "toVitalSigns", sender: nil)
    }
 
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if let destinationVc = segue.destination as? reason_PickerVC{
        destinationVc.Pickertype = SelectedPickerType
        
        destinationVc.symptomsIds = symptomsIds
        destinationVc.ConditionId = ConditionId
        

            switch SelectedPickerType {
            case 2:
                destinationVc.SelectedSymptom = symptoms
                  destinationVc.SelectedCondition = conditionsField.text
            default:
                destinationVc.SelectedCondition = conditionsField.text
                  destinationVc.SelectedSymptom = symptoms
            }
        
        }
        
        if let destinationVc2 = segue.destination as? AppointmentOneVC{
            destinationVc2.docId = docId
            destinationVc2.appointmentSlotId = appointmentSlotId
            destinationVc2.symptomsIds = symptomsIds
            destinationVc2.ConditionId = ConditionId
        }
        
    }
}

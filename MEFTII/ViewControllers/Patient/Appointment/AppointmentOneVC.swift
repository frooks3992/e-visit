//
//  AppointmentOneVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 13/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class AppointmentOneVC: BaseViewController, UITextFieldDelegate {

    var appointment: Appointment!
    
    @IBOutlet weak var temperatureField: CustomTextField!
    @IBOutlet weak var respiratoryRateField: UITextField!
    @IBOutlet weak var heartRateField: UITextField!
    @IBOutlet weak var oxygenSaturationField: UITextField!
    var symptomsIds:[Int]=[]
    var ConditionId:[Int]=[]
    
  
    
    var docId:Int!
    var appointmentSlotId:Int!
    override func viewDidLoad() {
        super.viewDidLoad()
      
        print(["sympton":symptomsIds])
        print(["condtionid":ConditionId])
        temperatureField.delegate = self
        temperatureField.keyboardType = .numbersAndPunctuation
        respiratoryRateField.keyboardType = .numbersAndPunctuation
        oxygenSaturationField.keyboardType = .numbersAndPunctuation
        heartRateField.keyboardType = .numbersAndPunctuation
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? AppointmentTwoVC {
        
            vc.docId = docId
            vc.appointmentSlotId = appointmentSlotId
            vc.symptomsIds = symptomsIds
            vc.ConditionId = ConditionId
            vc.temperature = temperatureField.text ?? ""
            vc.respRate = respiratoryRateField.text ?? ""
            vc.heartRate = heartRateField.text ?? ""
            vc.OxyRate = oxygenSaturationField.text ?? ""
        }
        
    }
    
    
    
    
    @IBAction func nextButtonTapped(_ sender: Any) {
        
//        appointment.temperature = temperatureField.text ?? ""
//        appointment.respirationRate = respiratoryRateField.text ?? ""
//        appointment.heartRate = heartRateField.text ?? ""
//        appointment.oxygenSaturation = oxygenSaturationField.text ?? ""
        
//        let appointmentTwoVC = AppointmentTwoVC.instantiateFromStoryboard()
//        appointmentTwoVC.appointment = appointment
//        navigationController?.pushViewController(appointmentTwoVC, animated: true)
        
        performSegue(withIdentifier: "toReports", sender: nil)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if let char = string.cString(using: String.Encoding.utf8) {
               let isBackSpace = strcmp(char, "\\b")
               if (isBackSpace == -92) {
                  return true
               }
           }
                  let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string) as String
              if let num = Float(newText), num >= 0.0 && num <= 110.0 {
                      return true
                  } else {
                      return false
                  }
              }
    
    
}



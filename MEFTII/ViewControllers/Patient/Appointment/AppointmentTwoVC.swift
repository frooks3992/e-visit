//
//  AppointmentTwoVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 13/09/2019.
//  Copyright © 2019 Mian Waqas Umar. All rights reserved.
//

import UIKit

class AppointmentTwoVC: BaseViewController {
    
    var appointment: Appointment!

    var labReports: [UIImage] = []
    var pictures: [UIImage] = []
    
    var attachReportController: UIImagePickerController!
    var attachPictureController: UIImagePickerController!
    var symptomsIds:[Int]=[]
    var ConditionId:[Int]=[]
    var temperature:String?
    var respRate:String?
    var heartRate:String?
    var OxyRate:String?
    var uploadedReports:[String] = []
    var docId:Int!
    var appointmentSlotId:Int!
    
   
    
    
    @IBOutlet weak var TV: UITableView!
    
    @IBOutlet weak var picturesCollectionView: UICollectionView!
    @IBOutlet weak var reportsCollectionView: UICollectionView!
    
    var filePickerType = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        
        print(["sympton":symptomsIds])
         print(["condtionid":ConditionId])
         print(["temp":temperature!])
         print(["respRate":respRate!])
         print(["heartRate":heartRate!])
         print(["oxyRate":OxyRate!])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Constants.selctedFileNameArray.removeAll()
        Constants.selctedFileImagesArray.removeAll()
    }
    
    
    
    
    
    @IBAction func attachPicture(_ sender: CustomButton) {
        
        attachPictureController = UIImagePickerController()
        attachPictureController.delegate = self
        attachPictureController.sourceType = .photoLibrary
        present(attachPictureController, animated: true)
        
    }
    
    @IBAction func attachReport(_ sender: CustomButton) {
       
        self.filePickerType = sender.tag
        attachReportController = UIImagePickerController()
        attachReportController.delegate = self
        attachReportController.sourceType = .photoLibrary
        present(attachReportController, animated: true)
        
    }
    
    @IBAction func submitTapped(_ sender: Any) {
//        regiterAppointment(userName: DataManager.shared.userName ?? "", appointment: appointment)
        self.isLoading()
        
        labReports.forEach { (report) in
            
            processImage(image: report)
        }
        
       
       
    }
    
    
    @IBAction func skipPressed(_ sender: Any) {
        self.isLoading()
        self.InsertAppointment(appointmentSlotId: self.appointmentSlotId, temperature: self.temperature!, respiratoryRate:self.respRate!, heartRate: self.heartRate!, oxygenSaturation: self.OxyRate!, commaSeperatedLabReportsURLs: "", symptomsList: self.symptomsIds, medicalConditionList: self.ConditionId)
    }
    
    
}

extension AppointmentTwoVC: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == reportsCollectionView {
            return labReports.count
        } else if collectionView == picturesCollectionView {
            return pictures.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let reportCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: ReportCell.self), for: indexPath) as? ReportCell else { return UICollectionViewCell() }
        
        if collectionView == reportsCollectionView {
            reportCell.reportImageView.image = labReports[indexPath.row]
        } else if collectionView == picturesCollectionView {
            reportCell.reportImageView.image = pictures[indexPath.row]
        }
        
        return reportCell
    }
    
    
    
}

extension AppointmentTwoVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let widthHeight = (collectionView.bounds.width / 3.0) - 4
        
        return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
}



extension AppointmentTwoVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        dismiss(animated: true, completion: nil)

        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        let information = info[UIImagePickerController.InfoKey.imageURL] as! URL
        if picker == attachReportController {
            labReports.append(image)
            Constants.selctedFileNameArray.append(information.lastPathComponent)
            //Constants.selctedFileImagesArray.append(image)
           // reportsCollectionView.insertItems(at: [IndexPath(row: labReports.count - 1, section: 0)])
        } else if picker == attachPictureController {
            pictures.append(image)
            Constants.selctedFileImagesArray.append(image)
            //picturesCollectionView.insertItems(at: [IndexPath(row: pictures.count - 1, section: 0)])
        }
        self.TV.reloadData()
    }

}

extension AppointmentTwoVC {
    
    func regiterAppointment(userName: String, appointment: Appointment) {
        
        let request = MeftiiRequests.regiterAppointment(username: userName, appointment: appointment)
        
        self.showLoadingView()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.hideLoadingView()
            print(resp)
            
            if code == 0 {
                
                
                self?.showAlertWith(title: "Success", message: desc, buttonText: "Ok", callBack: {
                     let vc = self?.navigationController!.viewControllers[1]
                                                          self?.navigationController?.popToViewController(vc!, animated: true)
                                         })
                
                
//                self?.showToast(message: "Request processed successfully.")
//                let vc = self?.navigationController!.viewControllers[1]
//                self?.navigationController?.popToViewController(vc!, animated: true)
            }
            
            }, failureBlock: { [weak self] (error) in
              //  self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
        
                                    })
                self?.hideLoadingView()
        })
        
    }
}

extension AppointmentTwoVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 3{
            return 135
        }else if indexPath.section == 2 || indexPath.section == 0{
            return 70
        }
        
        return 60
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1{
            return Constants.selctedFileNameArray.count
        }else if section == 3{
            return 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "attatchFileCell", for: indexPath)
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "reportNameCell", for: indexPath) as! MFTableViewCell
            cell.btndelete!.tag = indexPath.row
            cell.setProfileCell(label: "", value: Constants.selctedFileNameArray[indexPath.row], row: indexPath.row)
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "attatchPictureCell", for: indexPath)
            return cell
        }else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "reportImageCell", for: indexPath) as! ReportsTableViewCell
            cell.reloadAllData()
            return cell
        }

        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1{
             return 25
        }
        return 15
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let views = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        return views
    }
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let header = view as? UITableViewHeaderFooterView {
            header.backgroundView?.backgroundColor = UIColor.green
            header.textLabel?.textColor = UIColor.yellow
        }
    }
    
    @IBAction func deleteFileName(_ sender: UIButton){
        Constants.selctedFileNameArray.remove(at: sender.tag)
        self.TV.reloadData()
    }
    
    
}

extension AppointmentTwoVC {
    
    func processImage(image:UIImage){
          
        
              
              let img = Base64Image.shared.imageToBase64(imageToDecode: image)
              self.uploadImage(image: img, useCase: "report", userType: "patient")
              
        
          
          
          
      }
    
    
    func uploadImage(image:String,useCase:String,userType:String) {
           
           
           let request = MeftiiRequests.uploadImage(image: image, useCase: useCase, userType: userType)
           
           //   self.isLoading()
           request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
               
               
               if code == 0 {
                
                
                   
                   let data = resp["data"] as! [String:Any]
                   let image = data["imgUrl"] as! String
                   print(image)
                   
                   DataManager.shared.currentDoctor?.profilePicture = image
                  // self!.signUpVc?.newDoctor.profilePicture = image
                self!.uploadedReports.append(image)
                
                if self!.labReports.count == self!.uploadedReports.count {
                    let patientId = DataManager.shared.currentPatientId
                    let resp = self!.uploadedReports.joined(separator: ",")
                    self!.InsertAppointment(appointmentSlotId: self!.appointmentSlotId, temperature: self!.temperature!, respiratoryRate:self!.respRate!, heartRate: self!.heartRate!, oxygenSaturation: self!.OxyRate!, commaSeperatedLabReportsURLs: resp, symptomsList: self!.symptomsIds, medicalConditionList: self!.ConditionId)
                                     }
                   
                
               }
               
               }, failureBlock: { [weak self] (error) in
                
                self?.isLoadingFinished()
                  // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                       
                                                   })
                   // self?.isLoadingFinished()
           })
           
        print(uploadedReports)
       
       }
       
       
}

extension AppointmentTwoVC{
    //MARK: - GetstatesList API
    func InsertAppointment(appointmentSlotId:Int,temperature:String,respiratoryRate:String,heartRate:String,oxygenSaturation:String,commaSeperatedLabReportsURLs:String,symptomsList:[Int],medicalConditionList:[Int]) {
          
        let request = MeftiiRequests.addAppointment(appointmentId:appointmentSlotId,temperature:temperature,respiratoryRate:respiratoryRate,heartRate:heartRate,oxygenSaturation:oxygenSaturation,commaSeperatedLabReportsURLs:commaSeperatedLabReportsURLs,symptomsList:symptomsList,medicalConditionList:medicalConditionList)
        
      
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
              
            self?.isLoadingFinished()
         
            print(code)

         if code == 0 {
            
            
            self?.showAlertWith(title: "Success", message: desc, buttonText: "Ok", callBack: {
                
                self?.performSegue(withIdentifier: "AppointmentToHome", sender: nil)
                   
                                               })
            
           // self?.showToast(message: "Appointment success")
           
           
            
            
           
                      }
            
            }, failureBlock: { [weak self] (error) in
                
                self?.isLoadingFinished()
                        // self!.successView.isHidden = false
               // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                 
                                                             })
                // self?.isLoadingFinished()
        })
        
    }
}

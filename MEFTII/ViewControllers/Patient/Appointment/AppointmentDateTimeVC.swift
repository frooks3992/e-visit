//
//  AppointmentDateTimeVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 13/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import RealmSwift

class AppointmentDateTimeVC: BaseViewController {
    
    @IBOutlet weak var dateLabel: MFTextField!
    @IBOutlet weak var slotField: MFDropDownField!
    @IBOutlet weak var btnYourDoctor: MFButton!
    @IBOutlet weak var btnOnCallDoctor: MFButton!
    @IBOutlet weak var onCallDoctorView: UIView?
    @IBOutlet weak var doneButton: UIButton?
    @IBOutlet weak var slotOverlayButton: UIButton?
    
    
    var doctor : Profile_d3?
    var isUnscheduleEnable = false
    let dateFormatter = DateFormatter()
    var dateStr = ""
    
    var slotsArr: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        isUnscheduleEnable = (doctor?.unscheduledCalls ?? "false").lowercased().contains("t")
        dateFormatter.dateFormat = "yyyy-MM-dd"
        //2019-08-23 11:30:20
        btnYourDoctor.isOutlineButton = true
        btnOnCallDoctor.isOutlineButton = true
        onCallDoctorView?.isHidden = true
        slotOverlayButton?.isHidden = !isUnscheduleEnable
        
        
    }
    
    @IBAction func dateButtonPressed(_ sender: Any) {
        
        let minDate = Date(timeIntervalSinceReferenceDate: 599582088)
        
        self.showChoseDateDialog(minDate: minDate, maxDate: nil) { [weak self] (date, strDate) in
            self?.dateLabel.text = strDate
            if let date = date {
                
                self?.dateStr = self?.dateFormatter.string(from: date) ?? ""
                
                self?.getAvailableSlotsForDate(self?.dateStr ?? "", userName: DataManager.shared.userName ?? "", docId: "\(self?.doctor?.id ?? 0)")
            }
        }
        
        
    }
    
    @IBAction func slotOverlayButtonPressed(_ sender: Any) {
        
        self.showChoseDateDialog(minDate: nil, maxDate: nil, isForTime: true) { [weak self] (date, strDate) in
            self?.dateLabel.text = strDate
            if let date = date {
                
                let apiFormat = DateFormatter()
                apiFormat.dateFormat = "HH:mm"
                let slotStr = apiFormat.string(from: date)
                self?.slotField.dataStrings = [slotStr]
                self?.slotField.selectedIndex = 0
                
                self?.checkSelectedSlotAvailableOrNot(slot: slotStr)
            }
        }
        
        
    }
    
    @IBAction func yourDoctorPressed(_ sender: Any) {
        
        btnYourDoctor.isOutlineButton = false
        btnOnCallDoctor.isOutlineButton = true
        
        btnOnCallDoctor.setNeedsLayout()
        btnYourDoctor.setNeedsLayout()
        btnYourDoctor?.superview?.layoutSubviews()
        
        if let docterUserName = doctor?.firstName {
            
            let realm = try! Realm()
            let results = realm.objects(MWConversation.self).filter("conversationId == %@", docterUserName)
            
            if let convo = results.first {
                
                let chatViewController = ChatDetailVC(conversation: convo)
                navigationController?.pushViewController(chatViewController, animated: true)
                
            }
            else {
                let conversation = MWConversation()
                
                var uName = docterUserName.lowercased()
                uName = uName.components(separatedBy: CharacterSet.letters.inverted).joined()
                conversation.conversationId = uName
                let name = (doctor?.completeName ?? "").capitalized
                conversation.displayName = name
                
                try! realm.write {
                    realm.add(conversation, update: .all)
                }
                
                let chatViewController = ChatDetailVC(conversation: conversation)
                navigationController?.pushViewController(chatViewController, animated: true)
            }
            
            
        }
        
    }
    
    @IBAction func onCallDoctorPressed(_ sender: Any) {
        
        btnYourDoctor.isOutlineButton = true
        btnOnCallDoctor.isOutlineButton = false
        
        btnOnCallDoctor.setNeedsLayout()
        btnYourDoctor.setNeedsLayout()
        btnYourDoctor?.superview?.layoutSubviews()
        
        let vc = FindDoctorMapVC.instantiateFromStoryboard()
        vc.onCallDoctorId = String(doctor?.id ?? 0)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        
        let date = dateLabel.text ?? ""
        
        guard date.count > 0 else {
            showToast(message: "Date cannot be empty ⚠️")
            return
        }
        
        guard let slot = slotField.selectedValue else {
            showToast(message: "Please select a slot ⚠️")
            return
        }
        
        let seprators = slot.filter( { $0 == ":"} )
        if seprators.count > 1 {
            dateStr = dateStr + " " + slot
        }
        else if seprators.count > 0 {
            dateStr = dateStr + " " + slot
        }
        else {
            dateStr = dateStr + " " + slot + "00:00"
        }
        
        
        let appointment = Appointment()
        appointment.doctorId = doctor?.id ?? 0
        appointment.patientId = DataManager.shared.currentPatient?.id ?? 0
        appointment.from = dateStr
        
        let appointmentFourVC = AppointmentFourVC.instantiateFromStoryboard()
        appointmentFourVC.appointment = appointment
        
        navigationController?.pushViewController(appointmentFourVC, animated: true)
    }
    
    func checkSelectedSlotAvailableOrNot(slot:String) {
        if let _ = slotsArr.first(where: { $0.contains(slot) }) {
            onCallDoctorView?.isHidden = true
            doneButton?.isHidden = false
        }
        else {
            onCallDoctorView?.isHidden = false
            doneButton?.isHidden = true
        }
    }
    
    
    //Available slots API
    func getAvailableSlotsForDate(_ date:String, userName:String, docId: String ) {
        
        //let request = MeftiiRequests.getAvailableSlotsForDate(date, userName: userName, docId: docId)
        let request = MeftiiRequests.getAvailableSlotsForDate(date, userName: userName, docId: "2")
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            if code == 0 {
                let data = resp["availableSlots"]
                do {
                    let data = try JSONSerialization.data(withJSONObject: data!!, options: .prettyPrinted)
                    self?.slotsArr = try JSONDecoder().decode([String].self, from: data)
                    if let arr = self?.slotsArr, arr.count > 0 {
                        self?.slotField.dataStrings = arr
                    }
                }
                catch {
                    print("In fetch patient catch block")
                }
            }
            
            
            }, failureBlock: { [weak self] (error) in
                self?.showToast(message: error.localizedDescription)
                self?.isLoadingFinished()
        })
        
    }
    
}

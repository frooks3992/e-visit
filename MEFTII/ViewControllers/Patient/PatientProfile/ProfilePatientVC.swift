//
//  ProfilePatientVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 07/09/2019.
//  Copyright © 2019 Mian Waqas Umar. All rights reserved.
//

import UIKit

class ProfilePatientVC: BaseViewController {
    
    @IBOutlet weak var btnEdit: MFButton!
    @IBOutlet weak var btnNext: MFButton!
    @IBOutlet weak var lblSteppedLabel: UILabel!
    @IBOutlet weak var steppedVw: MFSteppedView!
    @IBOutlet weak var contraintTableVwBottom: NSLayoutConstraint!
    
    var pageViewController: UIPageViewController!
    
    var editPressed = false
    var crrentStep : Int = 0
    
    var profileObj = (DataManager.shared.currentPatient ?? Profile_p2()).replica()
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        return [self.newProfileViewController("One"),
                self.newProfileViewController("Two")]
    }()
    
    private func newProfileViewController(_ name: String) -> UIViewController {
        let vc =  UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "PatientProfile\(name)VC")
        if let vcc = (vc as? PatientProfileOneVC) {
            vcc.profileVc = self
        }
        else if let vcc = (vc as? PatientProfileTwoVC) {
            vcc.profileVc = self
        }
        
        return vc
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        contraintTableVwBottom.constant = 20
        // btnNext.isHidden = true
       removeSwipeGesture()
        
        btnEdit.setTitle("EDIT", rightImage: nil, leftImage: UIImage(named: "edit"), for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: NSNotification.Name(rawValue: "HeathAndPolicyNotForPatient"), object: nil)
        
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchPatientProfile()
    }
    
    func removeSwipeGesture(){
        for view in self.pageViewController!.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
    
    @IBAction override func backButtonPressed(_ sender: UIButton) {
        
        if crrentStep == 0 {
            self.navigationController?.popViewController(animated: true)
            
        }
        else {
            
            moveToStep(step:crrentStep - 1)
            
            pageViewController?.setViewControllers([orderedViewControllers[crrentStep - 1]], direction: .reverse, animated: true, completion: { (sts) in
                self.crrentStep = self.crrentStep - 1
            })
            
        }
        
        
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        moveToStep(step:crrentStep + 1)
        
        pageViewController?.setViewControllers([orderedViewControllers[crrentStep + 1]], direction: .forward, animated: true, completion: { (sts) in
            self.crrentStep = self.crrentStep + 1
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if let vc = segue.destination as? UIPageViewController {
            vc.delegate = self
            vc.dataSource = self
            self.pageViewController = vc
            
            if let firstViewController = orderedViewControllers.first {
                vc.setViewControllers([firstViewController],
                                      direction: .forward,
                                      animated: true,
                                      completion: nil)
            }
            
        }
    }
    
    @IBAction func btnNext_Pressed(_ sender: MFButton) {
        
        updatePatientProfile()
        
    }
    
    @IBAction func btnEdit_Pressed(_ sender: MFButton) {
        DataManager.shared.isPatientUpdating = true
        if self.crrentStep == 0{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpPatientVC") as! SignUpPatientVC
            SignUpPatientVC.isUpdateProfile = true
            DataManager.shared.isPatientUpdating = true
            SignUpPatientVC.isUpdateHealthandPolicy = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else if self.crrentStep == 1{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpPatientVC") as! SignUpPatientVC
            SignUpPatientVC.isUpdateProfile = true
            DataManager.shared.isPatientUpdating = true
            SignUpPatientVC.isUpdateHealthandPolicy = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        //        editPressed = true
        //        btnEdit.isHidden = true
        //        btnNext.isHidden = false
        //        contraintTableVwBottom.constant = 90
        //        self.view.setNeedsLayout()
        //        self.view.layoutIfNeeded()
        //
        //        if let vcc = (orderedViewControllers[0] as? PatientProfileOneVC) {
        //            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpPatientVC") as! SignUpPatientVC
        //                   SignUpPatientVC.isUpdateProfile = true
        //                   self.navigationController?.pushViewController(vc, animated: true)
        //           // vcc.reloadAllPage()
        //        }
        //        if let vcc = (orderedViewControllers[1] as? PatientProfileTwoVC) {
        //            vcc.reloadAllPage()
        //        }
        
    }
    
    func moveToStep(step:Int) {
        //        lblSteppedLabel.text = step == 0 ? "Health & Legel Info":"Basic Info"
        //        steppedVw.moveTo(Step: step)
    }
    
    func reloadProfile() {
        
        editPressed = false
        btnEdit.isHidden = false
        // btnNext.isHidden = true
        contraintTableVwBottom.constant = 20
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        if let vcc = (orderedViewControllers[0] as? PatientProfileOneVC) {
            vcc.reloadAllPage()
        }
        if let vcc = (orderedViewControllers[1] as? PatientProfileTwoVC) {
            vcc.reloadAllPage()
        }
        
    }
    
    
}



//MARK:- UpdatePatientProfile API
extension ProfilePatientVC {
    
    func updatePatientProfile() {
        
        //        let request = MeftiiRequests.updatePatientProfile(profileObj)
        //        self.isLoading()
        //
        //        request.start(vc: nil, successBlock: { [weak self] (code, desc, resp) in
        //            self?.isLoadingFinished()
        //
        //            if code == 0 {
        //                self?.fetchPatientProfile()
        //                self?.showToast(message: "Updated successfully!")
        //            }
        //            print(resp)
        //        }) { [weak self] (error) in
        //            self?.isLoadingFinished()
        //            print(error)
        //        }
    }
    
    func fetchPatientProfile()  {
        
         profileObj = (DataManager.shared.currentPatient ?? Profile_p2()).replica()
        
       // DataManager.shared.isPatientUpdating = false
        //        let userName = DataManager.shared.currentPatient?.username ?? ""
        //        let request = MeftiiRequests.getPatientProfile(username: userName)
        //
        //        self.isLoading()
        //
        //        request.start(vc: nil, successBlock: { [weak self] (code, desc, resp) in
        //            self?.isLoadingFinished()
        //
        //            let data = resp["patientProfile"]
        //            do {
        //                let profile = try PatientProfile(from: data!!)
        //                DataManager.shared.saveLoggedInPatient(profile: profile)
        //                self?.profileObj = profile
        //                self?.reloadProfile()
        //            }
        //            catch {
        //                print("In fetch patient catch block")
        //            }
        //        }) { [weak self] (error) in
        //            self?.isLoadingFinished()
        //            print(error)
        //        }
        
    }
    
}

extension ProfilePatientVC : UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        guard completed else { return }
        let vc = pageViewController.viewControllers!.first!
        
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: vc) else {
            return
        }
        crrentStep = viewControllerIndex
        moveToStep(step:viewControllerIndex)
    }
    
}

extension ProfilePatientVC : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
}


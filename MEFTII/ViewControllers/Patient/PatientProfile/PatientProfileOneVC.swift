//
//  PatientProfileOneVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 29/11/2019.
//  Copyright © 2019 Mian Waqas Umar. All rights reserved.
//

import UIKit

class PatientProfileOneVC: UIViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    
    var profileVc:ProfilePatientVC?
    
    var editPressed : Bool {
        get {
            if let vc = profileVc {
                return vc.editPressed
            }
            return false
        }
    }
    
    var profileImage : UIImage?
    
    let dateFormatter = DateFormatter()
    
    var birthDateString = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableVw.rowHeight = UITableView.automaticDimension
        tableVw.estimatedRowHeight = 80
        
        dateFormatter.dateFormat = "MM-dd-yyyy"
        
        let imageVw = UIImageView()
        let urlll = URL(string: Constants.getProfileImageUrl(userName: (profileVc?.profileObj.firstName ?? "")))
        imageVw.sd_setImage(with: urlll) { (image, error, cacheType, url) in
            self.profileImage = #imageLiteral(resourceName: "home_pic_4")
            self.tableVw.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
        }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadAllPage()
        let imageVw = UIImageView()
        let urlll = URL(string: Constants.getProfileImageUrl(userName: (profileVc?.profileObj.firstName ?? "")))
        imageVw.sd_setImage(with: urlll) { (image, error, cacheType, url) in
            self.profileImage = #imageLiteral(resourceName: "home_pic_4")
            self.tableVw.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
        }
        
        if let date = dateFormatter.date(from: profileVc?.profileObj.dateOfBirth ?? "") {
            let formater = DateFormatter()
            formater.dateFormat = Constants.kHomeDateFormate
            birthDateString = formater.string(from: date)
        }
        
        tableVw.reloadData()
    }
    
    

    func reloadAllPage()  {
        if tableVw != nil {
            tableVw.reloadData()
        }
    }

}

extension PatientProfileOneVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
   
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        dismiss(animated: true, completion: nil)
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        profileImage = image

        if let imageData = image.pngData() {
            profileVc?.profileObj.profilePicture = imageData.base64EncodedString(options: .lineLength64Characters)
        }

        tableVw.reloadRows(at: [IndexPath(row: 4, section: 0)], with: .automatic)
    }
    
}


extension PatientProfileOneVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 120
        }
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
         let row = indexPath.row
        
        if row == 0{
            let celll = tableView.dequeueReusableCell(withIdentifier: "pictureCell") as! MFTableViewCell
           // celll.lblName.text = "Picture"
            celll.imgProfile?.sd_setImage(with: URL(string: (profileVc?.profileObj.profilePicture ?? "")), placeholderImage: setDummyImage(gender: (profileVc?.profileObj.gender!)!))
            celll.imgProfile?.isUserInteractionEnabled = self.editPressed
            celll.imageTapped = {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = .photoLibrary
                
                self.present(imagePickerController, animated: true, completion: nil)
            }
            celll.bottomLineView?.isHidden = self.editPressed
            celll.strongLineView?.isHidden = !self.editPressed
            celll.setNeedsLayout()
            return celll
        }
        else if row == 1{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.firstName ?? "") \(profileVc?.profileObj.lastName ?? "")", row: row)
            return cell
        }
//        else if row == 2{
//             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
//            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.completeName ?? "")", row: row)
//            return cell
//        }
        else if row == 2{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.gender ?? "Male")", row: row)
            return cell
        }
        else if row == 3{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.email ?? "")", row: row)
            return cell
        }
        else if row == 4{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.phone ?? "")", row: row)
            return cell
        }
        else if row == 5{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.dateOfBirth ?? "")", row: row)
            return cell
        }
        else if row == 6{
             let cell = tableView.dequeueReusableCell(withIdentifier: "normalCell") as! MFTableViewCell
            
            if let state = DataManager.shared.currentPatient?.state {
                
                cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.street ?? ""), \(profileVc?.profileObj.city?.name ?? ""), \(profileVc?.profileObj.state?.name ?? ""), \(profileVc?.profileObj.zipCode ?? "")", row: row)
            }
            else {
                
                cell.setProfileCell(label: "", value: "\(profileVc?.profileObj.street ?? ""), \(DataManager.shared.getCity()), \(DataManager.shared.getState() ), \(profileVc?.profileObj.zipCode ?? "")", row: row)
            }
            
            
           
            return cell
        }
        else if row == 7{
             let cell = tableView.dequeueReusableCell(withIdentifier: "heathLegalInfoId") as! MFTableViewCell
            return cell
        }
        else if row == 8 || row == 9{
             let cell = tableView.dequeueReusableCell(withIdentifier: "FingerPrintId") as! MFTableViewCell
            if row == 9{
                cell.setProfileCell(label: "", value: "Facial Recognition", row: row)
            }
    
            return cell
        }
        
        
         
         return UITableViewCell()
        
//        let idfier = pictureCell ? "editableCell":"normalCell"
//
//        guard let cell =  tableView.dequeueReusableCell(withIdentifier: idfier) as? MFTableViewCell else {
//            return UITableViewCell()
//        }
//
//
//
//        cell.textField?.isUserInteractionEnabled = true
//        cell.textField?.isEnabled = editPressed
//        cell.textField?.textFieldDelegate = self
//
//        if row == 0 {
//            cell.setProfileCell(label: "First Name", value:profileVc?.profileObj.firstname, row: row)
//        }
//        else if row == 1 {
//            cell.setProfileCell(label: "Last Name", value:profileVc?.profileObj.lastname, row: row)
//        }
//        else if row == 2 {
//            cell.setProfileCell(label: "Username", value:profileVc?.profileObj.username, row: row)
//        }
//        else if row == 3 {
//
//            if editPressed {
//                let dropCell =  tableView.dequeueReusableCell(withIdentifier: "editableDropCell") as! MFTableViewCell
//                dropCell.lblName.text = "Gender"
//                dropCell.dosageDp?.selectedValue = profileVc?.profileObj.gender
//                dropCell.dosageDp?.dataStrings = Constants.genders.map{ $0.name ?? "" }
//                dropCell.dosageDp?.placeholderText = ""
//                dropCell.dosageDp?.closure = { (index,value) in
//                    self.profileVc?.profileObj.gender = value
//                }
//
//                return dropCell
//            }
//            else {
//                cell.lblName.text = "Gender"
//                cell.lblValue?.text = profileVc?.profileObj.gender
//            }
//        }
//        else if row == 4 {
//            let celll = tableView.dequeueReusableCell(withIdentifier: "pictureCell") as! MFTableViewCell
//            celll.lblName.text = "Picture"
//            celll.imgProfile?.image = self.profileImage
//            celll.imgProfile?.isUserInteractionEnabled = self.editPressed
//            celll.imageTapped = {
//                let imagePickerController = UIImagePickerController()
//                imagePickerController.delegate = self
//                imagePickerController.sourceType = .photoLibrary
//
//                self.present(imagePickerController, animated: true, completion: nil)
//            }
//            celll.bottomLineView?.isHidden = self.editPressed
//            celll.strongLineView?.isHidden = !self.editPressed
//            celll.setNeedsLayout()
//            return celll
//        }
//        else if row == 5 {
//            cell.setProfileCell(label: "Date of Birth", value:self.birthDateString, row: row)
//            cell.textField?.isEnabled = false
//            cell.textField?.isUserInteractionEnabled = false
//        }
//        else if row == 6 {
//            cell.setProfileCell(label: "Address", value:profileVc?.profileObj.address?.street, row: row)
//        }
//        else if row == 7 {
//
//            if editPressed {
//                let dropCell =  tableView.dequeueReusableCell(withIdentifier: "editableDropCell") as! MFTableViewCell
//                dropCell.lblName.text = "City"
//                dropCell.dosageDp?.selectedValue = profileVc?.profileObj.address?.city
//                dropCell.dosageDp?.dataStrings = Constants.cities.map{ $0.name ?? "" }
//                dropCell.dosageDp?.placeholderText = ""
//                dropCell.dosageDp?.closure = { (index,value) in
//                    self.profileVc?.profileObj.address?.city = value
//                }
//                return dropCell
//            }
//            else {
//                cell.lblName.text = "City"
//                cell.lblValue?.text = profileVc?.profileObj.address?.city
//
//            }
//        }
//        else if row == 8 {
//
//
//            if editPressed {
//                let dropCell =  tableView.dequeueReusableCell(withIdentifier: "editableDropCell") as! MFTableViewCell
//                dropCell.lblName.text = "State"
//                dropCell.dosageDp?.selectedValue = profileVc?.profileObj.address?.state
//                dropCell.dosageDp?.dataStrings = Constants.states.map{ $0.name ?? "" }
//                dropCell.dosageDp?.placeholderText = ""
//                dropCell.dosageDp?.closure = { (index,value) in
//                    self.profileVc?.profileObj.address?.state = value
//                }
//                return dropCell
//            }
//            else {
//                cell.lblName.text = "State"
//                cell.lblValue?.text = profileVc?.profileObj.address?.state
//            }
//        }
//        else if row == 9 {
//            cell.setProfileCell(label: "Zip Code", value:profileVc?.profileObj.address?.zipcode, row: row)
//        }
//
//        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableVw.deselectRow(at: indexPath, animated: false)
        
        let rowInd = indexPath.row
        if rowInd == 7{
           let name = Notification.Name("HeathAndPolicyNotForPatient")
           NotificationCenter.default.post(name: name, object: nil)
        }
        if editPressed {
            if indexPath.row == 3 {
                profileVc?.showChoseDateDialog(minDate: nil, maxDate: Date(), callBack: { (date, string) in
                    self.birthDateString = string ?? ""
                    tableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
                })
            }
        }
    }
    
    
}

extension PatientProfileOneVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            profileVc?.profileObj.firstName = textField.text
        case 1:
            profileVc?.profileObj.lastName = textField.text
        case 2:
            profileVc?.profileObj.firstName = textField.text
        case 6:
            profileVc?.profileObj.street = textField.text //Address textField
        case 9:
            profileVc?.profileObj.zipCode = textField.text
        default:
            print("Check textField tag")
        }
    }
    
}

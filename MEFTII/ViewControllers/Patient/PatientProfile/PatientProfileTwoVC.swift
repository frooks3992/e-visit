//
//  PatientProfileTwoVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 29/11/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class PatientProfileTwoVC: UIViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    
    weak var profileVc:ProfilePatientVC?
    
    var editPressed :Bool {
        get {
            if let vc = profileVc {
                return vc.editPressed
            }
            return false
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        DataManager.shared.isLegalInfoUpdating = true
        // Do any additional setup after loading the view.
        tableVw.rowHeight = UITableView.automaticDimension
        tableVw.estimatedRowHeight = 70
        tableVw.removeExtraRows()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableVw.reloadData()
    }
    
    func reloadAllPage()  {
        if tableVw != nil {
            tableVw.reloadData()
        }
    }
    
    @objc func toggleFingerPrint(_ sender: UISwitch) {
        
        DataManager.shared.setFingerPrintOnOff(isOn: sender.isOn)
        
    }
    
}

extension PatientProfileTwoVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let idfier = editPressed ? "editableCell":"normalCell"
        
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: idfier) as? MFTableViewCell else {
            return UITableViewCell()
        }
        
        let row = indexPath.row
        
        cell.textField?.isEnabled = editPressed
        cell.textField?.textFieldDelegate = self
        
        if row == 0 {
            var names:[String] = []
           let foods =  profileVc?.profileObj.foodAndDrugAllergies
            
            foods?.forEach({ (food) in
                
                let x = food.name
                names.append(x ?? "")
            })
            
            let foodNames = names.joined(separator: ",")
           
            cell.setProfileCell(label: "Food & Druf Allergies", value: foodNames, row: row)
        }
        else if row == 1 {
            cell.setProfileCell(label: "Employer", value:profileVc?.profileObj.employer, row: row)
        }
        else if row == 2 {
            cell.setProfileCell(label: "Insurance Name", value:profileVc?.profileObj.primaryInsuredName, row: row)
        }
        else if row == 3 {
            cell.setProfileCell(label: "Policy Number", value:profileVc?.profileObj.policyNumber, row: row)
        }
        else if row == 4 {
            cell.setProfileCell(label: "Insurance Company", value:profileVc?.profileObj.insuranceCompany, row: row)
        }
        else if row == 5 {
            
            
            
               cell.setProfileCell(label: "Copay $", value:profileVc?.profileObj.copay, row: row)
//            let priceCell = tableView.dequeueReusableCell(withIdentifier: "callsCell") as! MFTableViewCell
//            priceCell.lblName.text = "FingerPrint Login"
//            priceCell.toggleSw?.isOn = DataManager.shared.currentPatient?.isFingerPrintOn ?? false
//            priceCell.toggleSw?.addTarget(self, action: #selector(toggleFingerPrint(_:)), for: .valueChanged)
//            return priceCell
        }
            
            else if row == 6 {
                cell.setProfileCell(label: "Social Security Code", value:profileVc?.profileObj.socialSecurityNumber, row: row)
            }
        else {
            return UITableViewCell()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    

}


extension PatientProfileTwoVC : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {

        case 0:
            let v = textField.text
        case 1:
            profileVc?.profileObj.employer = textField.text
        case 2:
            profileVc?.profileObj.primaryInsuredName = textField.text
        case 3:
            profileVc?.profileObj.policyNumber = textField.text
        case 4:
            profileVc?.profileObj.insuranceCompany = textField.text
        default:
            print("Check textField tag")
        }
    }
    
}


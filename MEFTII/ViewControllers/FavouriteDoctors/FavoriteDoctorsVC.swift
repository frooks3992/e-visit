//
//  FavoriteDoctorsVC.swift
//  MEFTII
//
//  Created by Schaheer Saleem on 10/12/19.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class FavoriteDoctorsVC: BaseViewController {

    @IBOutlet weak var tableVw: UITableView!
    
    
    var favDoctors = [Profile_d3]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let favDocIds = DataManager.shared.favoriteDoctors
        
//        favDoctors = DataManager.shared.allDoctors?.filter({ (profile) -> Bool in
//            return favDocIds.contains((profile.id ?? -1))
//        }) ?? []
        
        tableVw.reloadData()
    }
    

}

extension FavoriteDoctorsVC : UITableViewDelegate, UITableViewDataSource {

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favDoctors.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFTableViewCell else {
            return UITableViewCell()
        }
        
        
        let row = indexPath.row
        cell.lblUnreadCount?.isHidden = true
        let doc = favDoctors[row]
        
        let urlll = URL(string: Constants.getProfileImageUrl(userName: (doc.firstName ?? "")))
        cell.imgProfile?.sd_setImage(with: urlll, completed: nil)
        
        cell.lblName.text = "Dr. \(doc.completeName)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = ViewDoctorVC.instantiateFromStoryboard()
        vc.doctorProfile = favDoctors[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
}

//
//  PickPharmacyVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 07/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class PickPharmacyVC: BaseViewController {

    @IBOutlet weak var mapVw: MKMapView!
    @IBOutlet weak var searchField: UITextField!
    
    let locationManager = CLLocationManager()
    var allPharmcies = [Pharmacies]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Mark: - Authorization
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()

        mapVw.delegate = self
        mapVw.mapType = MKMapType.standard
        mapVw.showsUserLocation = true

        // Do any additional setup after loading the view.
        let request = MeftiiRequests.getPharmacyList()
        self.isLoading()
        
        request.start(vc: nil, successBlock: { [weak self] (code, desc, resp) in
            self?.isLoadingFinished()
            if code == 0 {
                if let someObj = resp["pharmacies"] {
                    
                    do {
                        let data = try JSONSerialization.data(withJSONObject: someObj!, options: .prettyPrinted)
                        let arr = try JSONDecoder().decode([Pharmacies].self, from: data)
                        self?.allPharmcies = arr
                        
                        //reload map pins
                        self?.addPharmacyPinsToMap()
                    }
                    catch {
                        
                    }
                }
            }
            
        }) { [weak self] (error) in
            self?.isLoadingFinished()
            print(error)
        }
        
        
    }
    
    func addPharmacyPinsToMap() {
        //Show pharmacy on map
        var counter = 0
        var lat = ""
        var long = ""
        for pharmacy in allPharmcies {
            
            if counter == 2 {
                return
            }
            
            let annotation = CustomPointAnnotation()
            //            guard let var = doctor.latitude, let long = doctor.longitude else { return }
            if counter == 0 {
                lat = "33.684422"
                long = "73.047882"
            } else if counter == 1 {
                lat = "24.860735"
                long = "67.001137"
            }
            
            annotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: Double(lat)!)!, longitude: CLLocationDegrees(exactly: Double(long)!)!)
            annotation.pharmacy = pharmacy
            
            mapVw.addAnnotation(annotation)
            counter += 1
        }
    }

}

extension PickPharmacyVC : CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let center = CLLocationCoordinate2D(latitude: 35.689949, longitude: 139.697576)

        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.025, longitudeDelta: 0.025))

        mapVw.setRegion(region, animated: true)

        let pointAnnotation = CustomPointAnnotation()
        //pointAnnotation.pinCustomImageName = "Pokemon Pin"
        pointAnnotation.coordinate = center
        pointAnnotation.title = "POKéSTOP"
        pointAnnotation.subtitle = "Pick up some Poké Balls"

        let pinAnnotationView = MKPinAnnotationView(annotation: pointAnnotation, reuseIdentifier: "pharmacy_pin")
        mapVw.addAnnotation(pinAnnotationView.annotation!)

    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }

}

extension PickPharmacyVC : MKMapViewDelegate {

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let reuseIdentifier = "pharmacy_pin"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)

        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = false
        } else {
            annotationView?.annotation = annotation
        }

        let customPointAnnotation = annotation as! CustomPointAnnotation
        //        annotationView?.image = UIImage(named: customPointAnnotation.pinCustomImageName)

        let pharmacyName = "Health\nPharmacy"
        let customAnnotationView = CustomAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier, name: pharmacyName)
        customAnnotationView.image = UIImage(named: customPointAnnotation.pharmacyPinCustomImageName)

        return customAnnotationView
    }

    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
//        let viewDoctorVC = ViewDoctorVC.instantiateFromStoryboard()
//        viewDoctorVC.doctorProfile = ((view as! CustomAnnotationView).annotation as! CustomPointAnnotation).doctorProfile
//        self.navigationController?.pushViewController(viewDoctorVC, animated: true)
    }

}

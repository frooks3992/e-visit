//
//  ViewController.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 10/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class SplashVC: BaseViewController {
    
    var viewAppearingFirstTime = true

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        let request = MeftiiRequests.getDropDownList()
//
      DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
//
//            request.start(vc: self, successBlock: { [weak self] (code, desc, response) in
//                do {
//                    let lists = try DropDownLists(from: response)
//                    Constants.setDropDownLists(lists:lists)
        self.moveToNextVc()
//                }
//                catch {
//
//                }
//                }, failureBlock: nil)
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if viewAppearingFirstTime {
            viewAppearingFirstTime = false
        }
        else {
            moveToNextVc()
        }
    }
    
    
    func moveToNextVc() {
        
        let role = DataManager.shared.currentRole ?? ""
        let userName = DataManager.shared.userName ?? ""
        
        if role == "doctor", let doctor = DataManager.shared.currentDoctor {
            let isFingerOn = doctor.isFingerPrintOn ?? false
            if isFingerOn {
                gotoLoginVC()
            }
            else {
                let vc = HomeDoctorVC.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
                PushNotificationManager.shared.registerForPushNotifications()
            }
        }
        else if role == "patient", let patient = DataManager.shared.currentPatient {
            let isFingerOn = patient.isFingerPrintOn ?? false
            if isFingerOn {
                gotoLoginVC()
            }
            else {
                
                let vc = HomePatientVC.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
//                let request = MeftiiRequests.getAllDoctorList(userName: userName)
//                request.start(vc: self, successBlock: { [weak self] (code, desc, resp) in
//                    let someObj = resp["doctorProfiles"]
//                    do {
//                        let data = try JSONSerialization.data(withJSONObject: someObj!!, options: [])
////                        let arr = try JSONDecoder().decode([DoctorProfile].self, from: data)
////                        DataManager.shared.setAllDoctorsProfile(arr)
//                    }
//                    catch (let error){
//                        print(error)
//                    }
//                    let vc = HomePatientVC.instantiateFromStoryboard()
//                    self?.navigationController?.pushViewController(vc, animated: true)
//                    }, failureBlock: nil)
                PushNotificationManager.shared.registerForPushNotifications()
            }
        }
        else {
            gotoLoginVC()
        }
        
    }
    
    
    func gotoLoginVC() {
        
        if DataManager.shared.showIntroductionVc {
            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "IntroViewController") {
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
        else {
            
            if let role = DataManager.shared.currentRole {
                
                let patientDoctorVc = ChoosePatientDoctorVC.instantiateFromStoryboard()
                let vc = LoginVC.instantiateFromStoryboard()
                 vc.isDoctor = role == "doctor"
                self.navigationController?.pushViewController(patientDoctorVc, animated: false)
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
            else {
                let vc = ChoosePatientDoctorVC.instantiateFromStoryboard()
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }


}


//
//  ChoosePatientDoctorVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 24/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

class ChoosePatientDoctorVC: BaseViewController {
    
    var nextVcIsLogin = true //Or SignUpVC
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        
        
    }
    
    @IBAction func doctorButtonPressed(_ sender: Any) {
        
        if nextVcIsLogin {
            
            let vc = LoginVC.instantiateFromStoryboard()
            vc.isDoctor = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let vc  = SignUpDoctorVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func patientButtonPressed(_ sender: MFButton) {
        
        if nextVcIsLogin {
           
            let vc = LoginVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else {
            let vc  = SignUpPatientVC.instantiateFromStoryboard()
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    

    @IBAction func backtoChooseUserType(segue: UIStoryboardSegue){
        
    }

}

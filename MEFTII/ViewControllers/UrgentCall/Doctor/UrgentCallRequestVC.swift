//
//  UrgentCallRequestsVC.swift
//  MEFTII
//
//  Created by Farhan on 09/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import UIKit

import UIKit


class UrgentCallRequestVC: BaseViewController {

    @IBOutlet weak var PlaceholderLbl: UILabel!
    @IBOutlet weak var Tv: UITableView!
    
     var Sentrequests:[UrgentCallRequests] = []
       @IBOutlet weak var successView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Tv.removeExtraRows()
        Tv.estimatedRowHeight = 105
             Tv.rowHeight = UITableView.automaticDimension
        fetchrequests(doctorId: DataManager.shared.currentDoctorId)
        // Do any additional setup after loading the view.
        successView.isHidden = true
    }
    

    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension UrgentCallRequestVC{
    
    func fetchrequests(doctorId:Int) {
           
           self.isLoading()
        let regst = MeftiiRequests.getsentpatientrequests(doctorId: doctorId )
           regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
               
               self?.isLoadingFinished()
            var reqs:[UrgentCallRequests] = []
               if let data = respTwo["data"] as? [String:Any] {
                       let requests = data["urgentCallRequestList"] as? [[String:Any]] ?? []
                   do {
                    
                    if let message = data["message"] as? String {
                                         
                                         self!.PlaceholderLbl.text = message
                                     }
                                     else {
                                         
                                          self!.PlaceholderLbl.text = "No urgent call requests found"
                                     }
                       
                    requests.forEach { (request) in
                    reqs.append(UrgentCallRequests.init(json: request))
                        
                    }
                   
                    self!.Sentrequests = reqs
                    
                    if self!.Sentrequests.count > 0 {
                                                                    
                                                                    self!.Tv.isHidden = false
                                                                }
                                                                else{
                                                                    self!.Tv.isHidden = true
                                                                }
                                                     
                    
                    
                    
                    self!.Tv.reloadData()
                    
                   }
                   catch {
                       print()
                   }
                   
                   
               }
               
               }, failureBlock: nil)
       }
    
    
    func acceptdeclinerequest(requestId:Int,status:String,patientId:Int,doctorId:Int,doctorName:String) {
              
         self.isLoading()
           let regst = MeftiiRequests.acceptdeclinerequest(requestId: requestId, status: status, patientId: patientId, doctorId: doctorId, doctorName: doctorName)
              regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
                  
                  self?.isLoadingFinished()
               
                   if codeTwo == 0 {
                      
                   self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Ok", callBack: {
                                
                    self?.navigationController?.popViewController(animated: true)
                                                           })
                    //self!.showAlertWithMsg(Msg: "Request " + status + " successfully")
                      
                  }
                
                   else{
                    
                //    self!.showToast(message: "An error occured. Pls try again")
                    
                    self?.showAlertWith(title: "Error", message: descTwo, buttonText: "Ok", callBack: {
                                                     
                                                                              })
                }
                  
                  }, failureBlock: nil)
          }
    
    @IBAction func acceptPressed(_ sender: UIButton) {
        
        
        let idx = sender.tag
               let cell = Tv.cellForRow(at: IndexPath(item: sender.tag, section: 0)) as! urgentCallRequestCell
               
        let req = Sentrequests[idx]
               
        acceptdeclinerequest(requestId: req.requestId, status: "accepted", patientId: req.patientId, doctorId: req.doctorId, doctorName: req.doctorName)
          
    }
    
    func showAlertWithMsg(Msg:String){
        
        successView.isHidden = false
        // create the alert
               let alert = UIAlertController(title: "", message: Msg, preferredStyle: UIAlertController.Style.alert)

               // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "DONE", style: .destructive, handler: { (UIAlertAction) in
            
            self.successView.isHidden = true
        }))

               // show the alert
    
               self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func declinePressed(_ sender: UIButton) {
        
        let idx = sender.tag
                      let cell = Tv.cellForRow(at: IndexPath(item: sender.tag, section: 0)) as! urgentCallRequestCell
                      
               let req = Sentrequests[idx]
                      
               acceptdeclinerequest(requestId: req.requestId, status: "rejected", patientId: req.patientId, doctorId: req.doctorId, doctorName: req.doctorName)
        
    }
}

extension UrgentCallRequestVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Sentrequests.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "urgentCallRequestCell", for: indexPath) as! urgentCallRequestCell
        
        let status = Sentrequests[indexPath.row].status
        cell.acceptBtn.tag = indexPath.row
        cell.declineBtn.tag = indexPath.row
        
         
        
        if status == "NEW" {
            
            cell.txt.text =  Sentrequests[indexPath.row].patientName + " is requesting urgent virtual visit."
        
            
        }
        else{
            cell.txt.text = status
           
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
       }
       
       func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
       }
}

//
//  UrgentCallSentRequests.swift
//  MEFTII
//
//  Created by Farhan on 13/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import UIKit

class UrgentCallSentRequestsVC: BaseViewController {

    @IBOutlet weak var Tv: UITableView!
    @IBOutlet weak var placeholderLbl: UILabel!
    
     var Sentrequests:[UrgentCallRequests] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        Tv.removeExtraRows()
        Tv.estimatedRowHeight = 70
             Tv.rowHeight = UITableView.automaticDimension
        fetchrequests(patientId: DataManager.shared.currentPatientId)
        // Do any additional setup after loading the view.
        
    }
    

    
    @IBAction func backPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension UrgentCallSentRequestsVC{
    
    func fetchrequests(patientId:Int) {
           
           self.isLoading()
        let regst = MeftiiRequests.getsentrequests(patientId: patientId )
           regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
               
               self?.isLoadingFinished()
            var reqs:[UrgentCallRequests] = []
               if let data = respTwo["data"] as? [String:Any]{
                   
                if let message = data["message"] as? String {
                                     
                                     self!.placeholderLbl.text = message
                                 }
                                 else {
                                     
                                      self!.placeholderLbl.text = "No urgent call requests found"
                                 }
                let requests = data["urgentCallRequestList"] as? [[String:Any]] ?? []
                
                   do {
                       
                    requests.forEach { (request) in
                    reqs.append(UrgentCallRequests.init(json: request))
                        
                    }
                   
                    self!.Sentrequests = reqs
                    
                    
                    if self!.Sentrequests.count > 0 {
                                                   
                                                   self!.Tv.isHidden = false
                                               }
                                               else{
                                                   self!.Tv.isHidden = true
                                               }
                                    
                    
                    
                    
                    self!.Tv.reloadData()
                    
                   }
                   catch {
                       print()
                   }
                   
                   
               }
               
               }, failureBlock: nil)
       }
}

extension UrgentCallSentRequestsVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Sentrequests.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "urgentCallRequestCell", for: indexPath) as! urgentCallRequestCell
        
        let status = Sentrequests[indexPath.row].status
       
        
        if status == "NEW" {
            
            cell.txt.text = "You have sent a request to a doctor."
        cell.acceptBtn.isHidden = true
        }
        else{
            cell.txt.text = status
            cell.acceptBtn.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
       }
       
       func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
           return UITableView.automaticDimension
       }
}

//
//  URgentCallAddedDoctors.swift
//  MEFTII
//
//  Created by Farhan on 12/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//


import Foundation
import UIKit
import RealmSwift

class UrgentCallAddedDoctors: BaseViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var headingLabel: UILabel!
    var yourDocName:String!
    var yourDocId:Int!
    var yourDocSpeciality:String!
    var searchTimer: Timer?
    var isUrgentCall:String!
    var DocPicture:String!
    var DocGender:String!
    
    @IBOutlet weak var PlaceholderLbl: UILabel!
    //let locationManager = CLLocxationManager()
    var doctorlist: [Profile_d3] = []
    var docArrayToShow: [Profile_d3] = []
    var onCallDoctorId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchField.delegate = self
        self.searchField.addTarget(self, action: #selector(textFieldDidEditingChanged(_:)), for: .editingChanged)
        
        tableVw.tableFooterView = UIView()
        /*
        //Mark: - Authorization
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        mapVw.delegate = self
        mapVw.mapType = MKMapType.standard
        mapVw.showsUserLocation = true
        */
        //Show doctors on map
            fetchAllDoctorsAndReload()
//        if let _ = onCallDoctorId {
//            headingLabel.text = "On Call Doctor"
//            //fetchOnCallDoctorsAndReload()
//
//        }
//        else if let docList = DataManager.shared.allDoctors {
//            doctorlist = Array(docList)
//            docArrayToShow = doctorlist
//        }
//        else {
//            fetchAllDoctorsAndReload()
//        }
        
        tableVw.reloadData()
 
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? PatientUrgentCallRequestVC {
            
            vc.DocName = yourDocName
            vc.DocId = yourDocId
            vc.DocSpeciality = yourDocSpeciality
            vc.DocPicture = DocPicture
            vc.DocGender = DocGender
            vc.isUrgentCall = isUrgentCall
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    /*
    func reloadPinsOnMap() {
        
        for ann in mapVw.annotations {
            mapVw.removeAnnotation(ann)
        }
        
        var counter = 0
        var lat : Double = 0.0
        var long : Double = 0.0
        for doctor in docArrayToShow {
            
            let annotation = CustomPointAnnotation()
            
            if counter == 0 {
                lat = 33.684422
                long = 73.047882
            } else if counter == 1 {
                lat = 24.860735
                long = 67.001137
            }
            else {
                lat = Double(doctor.latitude ?? "x") ?? lat + 2
                long = Double(doctor.longitude ?? "x") ?? long + 2
            }
            
            annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
            annotation.doctorProfile = doctor
            
            mapVw.addAnnotation(annotation)
            
            counter += 1
            
        }
        
    }
    
    */
    
    
    
    
    @IBAction override func backButtonPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func fetchAllDoctorsAndReload() {
        
        self.isLoading()
        let regst = MeftiiRequests.getaddeddoctors(userName: DataManager.shared.currentPatientId )
        regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            
              self?.isLoadingFinished()
         
            
            if let someObj = respTwo["data"] {
                
                if let dt = respTwo["data"] as? [String:AnyObject]{
                                     if let message = dt["message"] as? String {
                                                                                    
                                                                                    self!.PlaceholderLbl.text = message
                                                                                }
                                                                                else {
                                                                                    
                                                                                     self!.PlaceholderLbl.text = "No doctors found"
                                                                                }
                
                }
                do {
                    let data = try JSONSerialization.data(withJSONObject: someObj!["doctorProfileList"]!, options: .prettyPrinted)
                    let arr = try JSONDecoder().decode([Profile_d3].self, from: data)
                    self?.docArrayToShow = arr
                    self?.doctorlist = arr
                    
                 
                    }
                 
                    
                    
                  //  DataManager.shared.setAllDoctorsProfile(arr)
                
                catch {
                    
                }
                
                self?.tableVw.reloadData()
            }
            
        }, failureBlock: nil)
    }
//
//    func fetchOnCallDoctorsAndReload() {
//        let regst = MeftiiRequests.getOnCallDoctors(for: onCallDoctorId ?? "", patient: DataManager.shared.userName ?? "" )
//        regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
//            if let someObj = respTwo["doctorProfiles"] {
//                do {
//                    let data = try JSONSerialization.data(withJSONObject: someObj!, options: .prettyPrinted)
//                    let arr = try JSONDecoder().decode([DoctorProfile].self, from: data)
//                    self?.doctorlist = arr
//                    self?.docArrayToShow = arr
//                }
//                catch {
//
//                }
//                self?.tableVw.reloadData()
//            }
//        }, failureBlock: nil)
//    }
    
}

extension UrgentCallAddedDoctors : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docArrayToShow.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFTableViewCell else {
            return UITableViewCell()
        }
        
        let doctor = docArrayToShow[indexPath.row]
        
          let urlll = URL(string: doctor.profilePicture ?? "")
        cell.imgProfile?.sd_setImage(with: urlll, placeholderImage: setDummyImage(gender: doctor.gender!))
        
        cell.lblName.text = "Dr. \(doctor.completeName)"
        cell.lblValue?.text = doctor.speciality
        cell.ratingVw?.rating = 0
        cell.lblUnreadCount?.text = ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let doctor = docArrayToShow[indexPath.row]
        
        isUrgentCall = doctor.urgentCallFlag
        yourDocName = "Dr. " + doctor.firstName!
        yourDocId = doctor.id
        yourDocSpeciality = "Speciality: " + doctor.speciality!
        DocPicture = doctor.profilePicture
        DocGender = doctor.gender
        
        
        performSegue(withIdentifier: "toPatientUrgentCallVC", sender: nil)

//        if let _ = onCallDoctorId {
//            if let docterUserName = doctor.firstName {
//
//                let realm = try! Realm()
//                let results = realm.objects(MWConversation.self).filter("conversationId == %@", docterUserName)
//
//                if let convo = results.first {
//
//                    let chatViewController = ChatDetailVC(conversation: convo)
//                    navigationController?.pushViewController(chatViewController, animated: true)
//
//                }
//                else {
//                    let conversation = MWConversation()
//
//                    var uName = docterUserName.lowercased()
//                    uName = uName.components(separatedBy: CharacterSet.letters.inverted).joined()
//                    conversation.conversationId = uName
//                    let name = doctor.completeName.capitalized
//                    conversation.displayName = name
//
//                    try! realm.write {
//                        realm.add(conversation, update: .all)
//                    }
//
//                    let chatViewController = ChatDetailVC(conversation: conversation)
//                    navigationController?.pushViewController(chatViewController, animated: true)
//                }
//
//            }
//        } else {
//            let viewDoctorVC = ViewDoctorVC.instantiateFromStoryboard()
//          viewDoctorVC.doctorProfile = doctor
//            self.navigationController?.pushViewController(viewDoctorVC, animated: true)
//        }
        
    }
}





extension UrgentCallAddedDoctors : UITextFieldDelegate {
    
    @objc func textFieldDidEditingChanged(_ textField: UITextField) {

        // if a timer is already active, prevent it from firing
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }

        // reschedule the search: in 1.0 second, call the searchForKeyword method on the new textfield content
        searchTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text ?? "", repeats: false)
    }

    @objc func searchForKeyword(_ timer: Timer) {

        // retrieve the keyword from user info
        let keyword = ((timer.userInfo as? String) ?? "").lowercased()

        print("Searching for keyword \(keyword)")
        
        if keyword.count > 0 {
            docArrayToShow = docArrayToShow.filter({ ($0.completeName.lowercased()).contains(keyword) }) ?? []
        }
        else {
            docArrayToShow = doctorlist
        }
        
        tableVw.reloadData()
        
    }
    
}

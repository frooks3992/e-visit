//
//  PatientUrgentCallRequestVC.swift
//  MEFTII
//
//  Created by Farhan on 12/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import UIKit
import SDWebImage
class PatientUrgentCallRequestVC: BaseViewController {
    
    
    var DocName:String!
    var DocId:Int!
    var DocSpeciality:String!
    var DocGender: String!
    var DocPicture:String!
    var OnCallDocId:Int!
    var isUrgentCall:String!
    var OnCallDocPicture:String!
    @IBOutlet weak var yourDocName: UILabel!
    
    
    @IBOutlet weak var yourDocSpeciality: UILabel!
    @IBOutlet weak var yourDocBtn: MFButton!
    @IBOutlet weak var yourDocImg: UIImageView!
    
    @IBOutlet weak var OnCallDocName: UILabel!
    
    @IBOutlet weak var OnCallDocSpeciality: UILabel!
    @IBOutlet weak var OnCallDocBtn: MFButton!
    
    
    @IBOutlet weak var OnCallDocImg: UIImageView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    var doctorlist: [Profile_d2] = []
    var docArrayToShow: [Profile_d2] = []
    
    @IBOutlet weak var successView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        OnCallDocBtn?.addTarget(self, action: #selector(sendRequestPressed(_:)), for: .touchUpInside)
        HideShowSuccessView()
        OnCallDocImg.isHidden = true
        // Do any additional setup after loading the view.
        isUrgentCall = "1"
    }
    
    
    func setupView(){
        
        self.yourDocName.text! = DocName
        self.yourDocSpeciality.text! = DocSpeciality
        OnCallDocBtn.isHidden =  !OnCallDocBtn.isHidden
        fetchDoctors(docId: DocId)
        
        let urlll = URL(string: DocPicture ?? "")

        yourDocImg?.sd_setImage(with: urlll
            , placeholderImage: setDummyImage(gender: ""))
        
        if isUrgentCall == "0" {
            titleLbl.text = "Urgent Request"
        }
        
        else if isUrgentCall == "1" {
            
             titleLbl.text = "Urgent Call"
        }
      
        
      //  if let urgentCall = 
        
    }
    
    func HideShowSuccessView(isHidden:Bool = true){
        
        successView.isHidden = isHidden
        
        if !isHidden {
         // create the alert
                   let alert = UIAlertController(title: "", message: "Your request has been sent. You will be notified once accepted or declined", preferredStyle: UIAlertController.Style.alert)

                   // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "DONE", style: .destructive, handler: { (UIAlertAction) in
                
                self.HideShowSuccessView()
            }))

                   // show the alert
                   self.present(alert, animated: true, completion: nil)
               }
        
    }
    
    @IBAction func backpressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func updateOncallBtn(isUrgent:Int) {
        
        
        if OnCallDocName.text?.count == 0 {
            OnCallDocBtn.isHidden = true
        }
        
        print(isUrgent)
        switch isUrgent {
        case 0:
            titleLbl.text = "Urgent Request"
            OnCallDocBtn.backgroundColor = .MFBlue
            OnCallDocBtn.setTitle("Message", for: .normal)
        default:
            
            titleLbl.text = "Urgent Call"
            OnCallDocBtn.backgroundColor = .MFDarkRed
            OnCallDocBtn.setTitle("Call", for: .normal)
            
        }
        
    }
    
    
    @objc func sendRequestPressed(_ sender: UIButton) {
        
        
        sendRequest(doctorId: OnCallDocId, patientId: DataManager.shared.currentPatientId, patientName: DataManager.shared.currentPatient?.completeName ?? "")
      }
      

    
}

extension PatientUrgentCallRequestVC {
    
    
    func fetchDoctors(docId:Int) {
        
        self.isLoading()
        let regst = MeftiiRequests.getUrgentaddeddoc(userName: DocId )
        regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            
            self?.isLoadingFinished()
            
            if let someObj = respTwo["data"] as? [String:Any] {
                
                do {
                    
                    if let name = someObj["firstName"] as? String {
                        self?.OnCallDocImg.isHidden = false

                         self?.OnCallDocName.text! = "Dr. " +  name
                    }
                    
                    else {
                        self?.OnCallDocImg.isHidden = true
                      self?.OnCallDocName.text! = ""
                         self!.OnCallDocBtn.isHidden = true
                    }
                    
                    if let speciality = someObj["speciality"] as? String {
                        self?.OnCallDocSpeciality.text! = "Speciality: " + speciality
                    }
                    else{
                        self?.OnCallDocSpeciality.text! = ""
                        
                        
                    }
                    
                   
                    
                    
                    if let urgentCall = someObj["isUrgentcall"] as? Int {
                        self!.OnCallDocBtn.isHidden = false
                          self!.updateOncallBtn(isUrgent: urgentCall)
                    }
                    else {
                        self!.OnCallDocBtn.isHidden = true
                    }
                    
                    self!.OnCallDocId  =  someObj["doctorId"] as? Int ?? 0
                    
                    
                    if let profilePic = someObj["profilePicture"] as? String {
                        
                        self!.OnCallDocImg.isHidden = false
                        self!.OnCallDocImg.sd_setImage(with: URL(string: profilePic))
                        
                       // let urlll = URL(string: profilePic)

                         //    OnCallDocImg?.sd_setImage(with: urlll
                            //     , placeholderImage: setDummyImage(gender: ""))
                        
                    }
                    
                    else{
                        self!.OnCallDocImg.isHidden = true
                    }
                    
                  
                  
                    
                }
                catch {
                    print()
                }
                
                
            }
            
            else{
                self!.OnCallDocImg.isHidden = true
               
            }
            
            }, failureBlock: nil)
    }
    
    
    //Send request
    
    func sendRequest(doctorId:Int,patientId:Int,patientName:String) {
           
           self.isLoading()
           let regst = MeftiiRequests.sendrequest(doctorId: doctorId, patientId: patientId, patientName: patientName)
           regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
               
               self?.isLoadingFinished()
               
            
               print(codeTwo)
                if codeTwo == 0{
                    
                   // self!.showToast(message: "Request sent")
                   // self!.HideShowSuccessView(isHidden: false)
              
                    
                    self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Ok", callBack: {
                                         
                        self?.navigationController?.popViewController(animated: true)
                                                                                  })
                
               }
            
                else{
                    self?.showAlertWith(title: "Failure", message: descTwo, buttonText: "Ok", callBack: {
                                                                               
                                                                                                     })
            }
            
               
               
               }, failureBlock: nil)
       }
       
    
}

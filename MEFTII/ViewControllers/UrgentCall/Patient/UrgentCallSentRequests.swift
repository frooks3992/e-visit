//
//  UrgentCallSentRequests.swift
//  MEFTII
//
//  Created by Farhan on 13/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import UIKit

class UrgentCallSentRequestsVC: BaseViewController {

    @IBOutlet weak var Tv: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    



}

extension UrgentCallSentRequestsVC{
    
    func fetchrequests(patientId:Int) {
           
           self.isLoading()
        let regst = MeftiiRequests.getsentrequests(patientId: patientId )
           regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
               
               self?.isLoadingFinished()
            var reqs:[UrgentCallSentRequestsVC] = []
               if let requests = respTwo["data"] as? [String:Any] {
                   
                   do {
                       
                    requests.forEach { (request) in
                        reqs.append(UrgentCallSentRequestsVC.in)
                        
                    }
                    
                   }
                   catch {
                       print()
                   }
                   
                   
               }
               
               }, failureBlock: nil)
       }
}

extension UrgentCallSentRequestsVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "urgentCallRequestCell", for: indexPath) as! urgentCallRequestCell
        
        
        cell.txt = ""
        cell.acceptBtn.isHidden = true
    }
}

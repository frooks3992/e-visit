//
//  SignUpPatientVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 27/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import DropDown
import CropViewController
import IQKeyboardManagerSwift

class SignUpPatientVC: BaseViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var nextBtn: MFButton!
    @IBOutlet weak var steppedLabel: UILabel!
    @IBOutlet weak var steppedVw: MFSteppedView!
    @IBOutlet weak var steppedVw2: MFSteppedView!
    
    @IBOutlet weak var backBtn: MFButton!
    
    @IBOutlet weak var back2Btn: MFButton!
      @IBOutlet weak var topCons: NSLayoutConstraint!
    
    @IBOutlet weak var saveBtn: MFButton!
    
    var isSavePressed:Bool = false
    static var isUpdateProfile = false
    static var isUpdateHealthandPolicy = false
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        
//        if DataManager.shared.isPatientUpdating && DataManager.shared.isLegalInfoUpdating{
//            return [self.newSignUpViewController("Three")]
//        }
        
        
         if DataManager.shared.isPatientUpdating {
            return [self.newSignUpViewController("One"),
                    self.newSignUpViewController("Two"),
                    self.newSignUpViewController("Three")]
        }
            
        else{
            return [self.newSignUpViewController("One"),
                    self.newSignUpViewController("Two"),
                    self.newSignUpViewController("Three"),
                    self.newSignUpViewController("Four")]
        }
        
        
    }()
    
    var pageViewController : UIPageViewController?
    
    var crrentStep = 0
    
    var newDoctor = Profile_p()
    var newDoctor2 = DataManager.shared.currentPatient
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveBtn.isHidden = true
        
        setupIfUpdating()
        
        // Do any additional setup after loading the view.
        newDoctor = Profile_p()
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(PatientUploaded), name: Notification.Name("PatientImageUploaded"), object: nil)
        nc.addObserver(self, selector: #selector(showLoading), name: Notification.Name("showLoaderP"), object: nil)
    }
    
    deinit {
        
        
        (orderedViewControllers[0] as? SignUpPatientOneVC)?.signUpVc = nil
        (orderedViewControllers[1] as? SignUpPatientTwoVC)?.signUpVc = nil
        (orderedViewControllers[2] as? SignUpPatientThreeVC)?.signUpVc = nil
        
        if !(DataManager.shared.isPatientUpdating){
            (orderedViewControllers[3] as? SignUpPatientFourVC)?.signUpVc = nil
        }
        
    }
    
    func setupIfUpdating(){
        
        (DataManager.shared.isPatientUpdating) ? (titleLbl.text! = "Profile") : (titleLbl.text! = "Sign Up")
        
        if DataManager.shared.isPatientUpdating{
            saveBtn.isHidden = false
            steppedVw.isHidden = true
            self.steppedVw2.totalSteps =  3
            if SignUpPatientVC.isUpdateHealthandPolicy{
              
                
                  backBtn.isHidden = true
                back2Btn.isHidden = false
                                            moveToStep(step:crrentStep + 2)
                //                           backBtn.isHidden = true
                                            steppedVw.isHidden = true
                                            steppedVw2.isHidden = true
                                        steppedLabel.isHidden = true
                topCons.constant = -40
                                           UIView.animate(withDuration: 0) {
                                               self.view.layoutIfNeeded()
                                           }
                                           
                                           titleLbl.font = UIFont.AppSemiBoldFont(ofSize: 24)
                                           titleLbl.text = "Health & Legal Info"
                pageViewController?.setViewControllers([orderedViewControllers[crrentStep + 2]], direction: .reverse, animated: true, completion: { (sts) in
                    self.crrentStep = self.crrentStep + 2
                })
            }
        }
            
        else{
            steppedVw2.isHidden = true
            steppedVw.isHidden = false
        }
        // steppedVw.isHidden = true
    }
    
    @IBAction func back2BtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    private func newSignUpViewController(_ name: String) -> UIViewController {
        
        let vc =  UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "SignUpPatient\(name)VC")
        if let vcc = (vc as? SignUpPatientOneVC) {
            vcc.signUpVc = self
        }
        else if let vcc = (vc as? SignUpPatientTwoVC) {
            vcc.signUpVc = self
        }
        else if let vcc = (vc as? SignUpPatientThreeVC) {
            vcc.signUpVc = self
        }
        else if let vcc = (vc as? SignUpPatientFourVC) {
            vcc.signUpVc = self
        }
        
        return vc
        
    }
    
    @objc func PatientUploaded(){
        let data = DataManager.shared.currentPatient
        
        //DataManager.shared.saveLoggedInDoctor(profile: data!)
        
        self.updatePatientProfile(data!)
    }
    
    @objc func showLoading(){
        
        self.isLoading()
    }
    
    @IBAction func savePressed(_ sender: Any) {
        
        isSavePressed = true
        
        prepareUpdateRequest(isUpdate: true)
    }

    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let vc = segue.destination as? UIPageViewController {
            vc.delegate = nil
            vc.dataSource = nil
            self.pageViewController = vc
            
            if let firstViewController = orderedViewControllers.first {
                vc.setViewControllers([firstViewController],
                                      direction: .forward,
                                      animated: true,
                                      completion: nil)
            }
        }
        if let destinationVc = segue.destination as? OTPAuthenticationVC{
            destinationVc.msisdn = self.newDoctor.phone
            destinationVc.userType = "patient"
        }
        
        
    }
    
    func prepareUpdateRequest(isUpdate:Bool = false){
        let firstVC =  orderedViewControllers[0] as! SignUpPatientOneVC
        let secondVC =  orderedViewControllers[1] as! SignUpPatientTwoVC
        let thirdVC =  orderedViewControllers[2] as! SignUpPatientThreeVC
        
        if crrentStep < 3 {
            
            if crrentStep == 0 {
                if !(firstVC).fillDataInProfileObject(isUpdate: isUpdate) {
                    return
                }
            }
            else if crrentStep == 1 {
                if !(secondVC).fillDataInProfileObject() {
                    return
                }
            }
            else if crrentStep == 2 {
                if !(thirdVC).fillDataInProfileObject() {
                    return
                }
            }
            
            
            
            
        }
        
        
        if  crrentStep != 0 {
            
            let data = DataManager.shared.currentPatient
            
            
            self.updatePatientProfile(data!)
        }
    }
    @IBAction override func backButtonPressed(_ sender: UIButton) {
        
        if crrentStep == 0 {
            self.navigationController?.popViewController(animated: true)
            
        }
        else {
            
            moveToStep(step:crrentStep - 1)
            
            pageViewController?.setViewControllers([orderedViewControllers[crrentStep - 1]], direction: .reverse, animated: true, completion: { (sts) in
                self.crrentStep = self.crrentStep - 1
            })
            
        }
        
        
    }
    
    @IBAction func nextButtonPressed(_ sender: MFButton) {
        
        let firstVC =  orderedViewControllers[0] as! SignUpPatientOneVC
        let secondVC =  orderedViewControllers[1] as! SignUpPatientTwoVC
        let thirdVC =  orderedViewControllers[2] as! SignUpPatientThreeVC
        
        if crrentStep < 3 {
            
            if crrentStep == 0 {
                if !(firstVC).fillDataInProfileObject() {
                    return
                }
            }
            else if crrentStep == 1 {
                if !(secondVC).fillDataInProfileObject() {
                    return
                }
            }
            else if crrentStep == 2 {
                if !(thirdVC).fillDataInProfileObject() {
                    return
                }
            }
            
            if SignUpPatientVC.isUpdateProfile{
                if crrentStep == 2{
                    thirdVC.fillDataInProfileObject()
                    prepareUpdateRequest()
                }
            }
            if !SignUpPatientVC.isUpdateProfile || crrentStep != 2{
                moveToStep(step:crrentStep + 1)
                
                
                
                pageViewController?.setViewControllers([orderedViewControllers[crrentStep + 1]], direction: .forward, animated: true, completion: { (sts) in
                    self.crrentStep = self.crrentStep + 1
                })
            }
            
            
            //
            //            moveToStep(step:crrentStep + 1)
            //
            //
            //
            //            pageViewController?.setViewControllers([orderedViewControllers[crrentStep + 1]], direction: .forward, animated: true, completion: { (sts) in
            //                self.crrentStep = self.crrentStep + 1})
            
            
        }
        else {
            
            //            (orderedViewControllers[0] as? SignUpPatientOneVC)?.fillDataInProfileObject()
            //            (orderedViewControllers[1] as? SignUpPatientTwoVC)?.fillDataInProfileObject()
            //            (orderedViewControllers[2] as? SignUpPatientThreeVC)?.fillDataInProfileObject()
            if (orderedViewControllers[3] as? SignUpPatientFourVC)?.fillDataInProfileObject() ?? false {
            
            let phone = newDoctor.phone ?? ""
            let email = newDoctor.email ?? ""
            let password = newDoctor.password ?? ""
            let isConfimed = (orderedViewControllers[3] as? SignUpPatientFourVC)?.confirmed
            
            guard phone.count > 0 && phone.count == 14 else {
                //showToast(message: "Phone cannot be empty ⚠️")
                return
            }
            
            guard email.count > 0 && email.isValidEmail() else {
                //showToast(message: "Email cannot be empty ⚠️")
                return
            }
            
            guard password.count > 0 && password.isValidPassword() else {
                //showToast(message: "Password cannot be empty ⚠️")
                return
            }
            
            if isConfimed ?? false {
                
            } else {
                //showToast(message: "Please accept terms of use ⚠️")
                return
            }
            
            
            performSegue(withIdentifier: "toOTPVerification", sender: nil)
            
            
            }
        }
        
    }
    
    @IBAction func unwindFromOTP(_ sender: UIStoryboardSegue){
        
        if sender.source is OTPAuthenticationVC{
            
            if let senderVC = sender.source as? OTPAuthenticationVC {
                
                if senderVC.verified{
                    //  CityTF.hasArrow = true
                    registerPatientProfile(newDoctor)
                    
                }
                    
                else{
                    
                }
                
                
            }
            
        }
        
    }
    
    
    func moveToStep(step:Int) {
        var label = "Basic Info"
        switch step {
        case 2:
            if DataManager.shared.isPatientUpdating {
                nextBtn.setTitle("SAVE", for: UIControl.State.normal)
            }
            else {
                nextBtn.setTitle("NEXT", for: UIControl.State.normal)
            }
            label = "Health & Legal Info"
        case 3:
            nextBtn.setTitle("SUBMIT", for: UIControl.State.normal)
            label = "Security Info"
        default:
            nextBtn.setTitle("NEXT", for: UIControl.State.normal)
            label = "Basic Info"
        }
        steppedLabel.text = label
        
        if DataManager.shared.isPatientUpdating {
            steppedVw2.moveTo(Step: step)
            
            
            print(step)
        }
        else{
            steppedVw.moveTo(Step: step)
        }
    }
    
    
    
    //MARK: - RegisterPatientProfile API
    func registerPatientProfile(_ profile: Profile_p) {
        DataManager.shared.setcurrentEmail(email: profile.email!)
        let request = MeftiiRequests.registerPatientApi(patientProfile: profile)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            if code == 0 {
              //  self?.showToast(message: "Signed up successfully!")
                self?.showAlertWith(title: "Success", message: desc, buttonText: "Okay", callBack: {
                    //                    let vc  = LoginVC.instantiateFromStoryboard()
                    self?.performSegue(withIdentifier: "patientToChooseVC", sender: nil)
                })
            }
            
            }, failureBlock: { [weak self] (error) in
              //  self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                               
                                                        })
                self?.isLoadingFinished()
        })
        
    }
    
    //MARK: - RegisterPatientProfile API
    func updatePatientProfile(_ profile: Profile_p2) {
        DataManager.shared.setcurrentEmail(email: profile.email!)
        
        self.isLoading()
 
   
        
        
        
        let request = MeftiiRequests.updatePatientApi(patientProfile: profile)
        
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            if code == 0 {
                
//                DataManager.shared.patientlogin(userName: DataManager.shared.getcurrentEmail(), password: DataManager.shared.getcurrentPass())
                let data = resp["data"] as? [String:Any] ?? [:]
                            let patient = data
                            do {
                                let profile = try Profile_p2.init(from: patient)
                                DataManager.shared.setCity(city: (profile.city?.name!)!)
                                DataManager.shared.setState(state: (profile.state?.name!)!)
                                
                                DataManager.shared.saveLoggedInPatient(profile: profile)

                            }
                            catch {
                                
                            }
                
                //  self?.showToast(message: "profile updated successfully!")
                self?.showAlertWith(title: "Success", message: "Profile updated successfully!", buttonText: "Okay", callBack: {
                    //                    let vc  = LoginVC.instantiateFromStoryboard()
                    self?.navigationController?.popViewController(animated: true)
                })
            }
            
            }, failureBlock: { [weak self] (error) in
              //  self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                               
                                                        })
                self?.isLoadingFinished()
        })
        
    }
    //
}


//MARK: -UIPageViewControllerDelegate

extension SignUpPatientVC : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        guard completed else { return }
        let vc = pageViewController.viewControllers!.first!
        
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: vc) else {
            return
        }
        crrentStep = viewControllerIndex
        moveToStep(step:viewControllerIndex)
    }
    
    
}


extension SignUpPatientVC : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of:viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of:viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
}

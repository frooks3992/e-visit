//
//  SignUpDoctorThreeVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 26/11/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class SignUpPatientThreeVC: UIViewController {
    
    
    @IBOutlet weak var txtEmployer: CustomTextField!
    @IBOutlet weak var dpFoodDrugAllergies: CustomTextField!
    @IBOutlet weak var txtInsuranceName: CustomTextField!
    @IBOutlet weak var txtPolicyNumber: CustomTextField!
    @IBOutlet weak var txtInsuranceCompany: CustomTextField!
    @IBOutlet weak var txtCopay: CustomTextField!
    @IBOutlet weak var txtSocialSecurityNumber: CustomTextField!
    
       @IBOutlet var TextFields: [CustomTextField]!
    weak var signUpVc : SignUpPatientVC?
    
    @IBOutlet var AlleryTF: CustomTextField!
    var SelectedAllergyId:[Int] = []
    var SelectedAllergyName:[String] = []
    var allfoodsAllergies: [foodDrugAllergies] = []
     var allergyxFood:[String] = []
      var profileData  = (DataManager.shared.currentPatient ?? Profile_p2()).replica()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        AlleryTF.hasArrow = true
        txtEmployer.delegate = self
        txtInsuranceName.delegate = self
        txtEmployer.valueType = .alphaNumericSpace
        txtEmployer.maxLength = 25
        txtInsuranceName.valueType = .fullName
        txtInsuranceName.maxLength = 25
        
        txtPolicyNumber.delegate = self
        txtPolicyNumber.valueType = .alphaNumeric
        txtPolicyNumber.maxLength = 15
        
        txtInsuranceCompany.delegate = self
        txtInsuranceCompany.valueType = .alphaNumericSpace
        txtInsuranceCompany.maxLength = 25
        
        txtCopay.delegate = self
        txtCopay.valueType = .onlyNumbers
        txtCopay.maxLength = 3
        txtSocialSecurityNumber.delegate = self
        txtSocialSecurityNumber.valueType = .onlyNumbers
        txtSocialSecurityNumber.maxLength = 4
        
        setUpIfUpdating()
        // Do any additional setup after loading the view.
        
      //  dpFoodDrugAllergies.dataStrings = Constants.allergies.map{ $0.name ?? "" }
    }
    
    
   
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    func setUpIfUpdating(){
        
        if DataManager.shared.isPatientUpdating {
            let profileData  = (DataManager.shared.currentPatient ?? Profile_p2()).replica()
            
            
            
            let foodDrug = profileData.foodAndDrugAllergies
            var foodAndDrug = ""
            foodDrug?.forEach({ (x) in
                
                if foodAndDrug.count > 0 {
                      foodAndDrug.append("," + x.name!)
                }
                else{
                       foodAndDrug.append(x.name!)
                }
              
            })
            
            AlleryTF.text! = foodAndDrug
            txtEmployer.text! = profileData.employer!
            txtInsuranceName.text! = profileData.primaryInsuredName!
            txtPolicyNumber.text! = profileData.policyNumber!
            txtInsuranceCompany.text! = profileData.insuranceCompany!
            txtCopay.text! = profileData.copay!
            txtSocialSecurityNumber.text! = profileData.socialSecurityNumber!
            
            
        }
    }
    
    
    
    @IBAction func unwindFromAllergiesPicker(_ sender: UIStoryboardSegue){
         
         if sender.source is FoodAndAllergiesPicker{
             
             if let senderVC = sender.source as? FoodAndAllergiesPicker {
                 
                 if senderVC.SelectedAllergy != nil{
                   //  CityTF.hasArrow = true
                   
             
                    allfoodsAllergies = profileData.foodAndDrugAllergies ?? []
                   var foods = profileData.foodAndDrugAllergies
                    
                    foods?.forEach({ (food) in
                        
                        allergyxFood.append(food.name ?? "")
                        
                    })
                   
                    
                    AlleryTF.text! = allergyxFood.joined(separator: ", ")
                    SelectedAllergyId = senderVC.SelectedAllergyId
                    SelectedAllergyName = senderVC.SelectedAllergyName
                    AlleryTF.hasError = false
                    
                    
//                    if SelectedAllergyId.count > 0 {
//                    for i in 0...SelectedAllergyId.count-1 {
//
//                        let food = foodDrugAllergies.init(id: SelectedAllergyId[i], name: SelectedAllergyName[i])
//                        allfoodsAllergies.append(food)
//
//                    }
                    
                }
                    
                    print("x")
                    
                 }
                 
             }
             
         }
         
     
     
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
          
          let destinationVc = segue.destination as! FoodAndAllergiesPicker
         
        destinationVc.SelectedAllergy = AlleryTF.text!
         let profileData  = (DataManager.shared.currentPatient ?? Profile_p2()).replica()
        destinationVc.foodAndDrugAllergies = profileData.foodAndDrugAllergies
         destinationVc.SelectedAllergyId = SelectedAllergyId
         destinationVc.SelectedAllergyName = SelectedAllergyName
          
          
          
      }
    
    
       @IBAction func EditingChanged(_ sender: CustomTextField) {
           
//           switch sender.tag {
//           case 2:
//               txtAddress.hasError = false
//           default:
//                 txtZipCode.hasError = false
//           }
//
        sender.hasError = false
           
           
       }
    
    func checkemptyFields(){
         // var empty = [UITextField]()
          for textField in TextFields {
              
              if textField.text == ""{
                
              
                
                  textField.hasError = true
              }
                
            if ((textField.tag == 6) && (textField.text!.count < 4)) {
                textField.hasError = true
            }
              else {
                  // textField.hasArrow = true
              }
                 
          }
        
      }
    
    @discardableResult
    func fillDataInProfileObject() -> Bool {
        
        
        guard let allergy = AlleryTF.text, allergy.count > 0 else {
            checkemptyFields()
            return false
        }
        
        
          (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.foodAndDrugAllergies = allfoodsAllergies) :(  signUpVc?.newDoctor.foodAndDrugAllergies = SelectedAllergyId)
       
        
        guard let employer = txtEmployer.text, employer.count > 0 else {
            checkemptyFields()
            return false
        }
       
         (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.employer = employer) :( signUpVc?.newDoctor.employer = employer )
        
        guard let insuranceName = txtInsuranceName.text, insuranceName.count > 0 else {
            checkemptyFields()
            return false
        }
      
           (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.primaryInsuredName = insuranceName) :(   signUpVc?.newDoctor.primaryInsuredName = insuranceName)
        
        guard let policyNumber = txtPolicyNumber.text, policyNumber.count > 0 else {
            checkemptyFields()
            return false
        }
      

        (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.policyNumber = policyNumber) :(signUpVc?.newDoctor.policyNumber = policyNumber)
        
        
        guard let ssn = txtSocialSecurityNumber.text, ssn.count > 0 && ssn.count == 4 else {
            checkemptyFields()
            return false
        }
       // signUpVc?.newDoctor.socialSecurityNumber = ssn
          (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.socialSecurityNumber = ssn) :(signUpVc?.newDoctor.socialSecurityNumber = ssn)
        
        guard let copay = txtCopay.text, copay.count > 0 else {
            checkemptyFields()
            return false
        }
       // signUpVc?.newDoctor.copay = copay
          (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.copay = copay) :(signUpVc?.newDoctor.copay = copay)
        guard let insuranceCom = txtInsuranceCompany.text, insuranceCom.count > 0 else {
            checkemptyFields()
            return false
        }
       // signUpVc?.newDoctor.insuranceCompany = insuranceCom
          (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.insuranceCompany = insuranceCom) :(signUpVc?.newDoctor.insuranceCompany = insuranceCom)
       // signUpVc?.newDoctor.allergy = dpFoodDrugAllergies.selectedValue
        
        return true
    
    }


}

extension SignUpPatientThreeVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField.text!.containsWhitespace{
                           
                        if string == " " && textField.text?.last == " "{
                                 return false
                           }

                       }
        // Verify all the conditions
        if let sdcTextField = textField as? CustomTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return true
    }
}

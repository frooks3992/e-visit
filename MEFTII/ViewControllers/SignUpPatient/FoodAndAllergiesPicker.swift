//
//  City_StatePickerVC.swift
//  MEFTII
//
//  Created by Farhan on 30/03/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import UIKit


class FoodAndAllergiesPicker: BaseViewController {
    @IBOutlet var Tv: UITableView!
    
    @IBOutlet var TitleTf: UILabel!
    @IBOutlet var SearchTF: searchTextField!
   
    var Filteredallergies:[Food] = []
    var FoodAndAllergiesList:[Food] = []
    var SelectedAllergy:String!
    var SelectedState:String!
    var SelectedAllergyId:[Int]!
    var SelectedAllergyName:[String]!
    var foodAndDrugAllergies: [foodDrugAllergies]!
    
    var profileData  = (DataManager.shared.currentPatient ?? Profile_p2()).replica()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SearchTF.delegate = self
        SearchTF.valueType = .fullName
        
       getFoodAndAllergies()
        Tv.tableFooterView = UIView()
        foodAndDrugAllergies = profileData.foodAndDrugAllergies
        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func EditingChanged(_ sender: Any) {
        
        
        let text = SearchTF.text!
        
        if text == "" {
            
            Filteredallergies = FoodAndAllergiesList
        }
        else {
            Filteredallergies = FoodAndAllergiesList.filter{($0.name.contains(text))}
            
            
        }
        
        Tv.reloadData()
        
    }
    @IBAction func BackOeDonePressed(_ sender: Any) {
           
        
        
           performSegue(withIdentifier: "BackFromAllergiesPicker", sender: nil)
       }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
    }
    
    
}


extension FoodAndAllergiesPicker: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        Filteredallergies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "city_stateCell", for: indexPath) as! City_StateCell
        
        
            if SelectedAllergyId.count > 0{
                           
                }
                               
                
        
        let foods = profileData.foodAndDrugAllergies
        
        foods?.forEach({ (food) in
            
            if food.id == Filteredallergies[indexPath.row].id {
                
                
            cell.Img.image = UIImage(named: "selected")
                         }
                                      
                         else {
                                        
                                        cell.Img.image = nil
                                    }
        })
        
//        if SelectedAllergyId.contains(Filteredallergies[indexPath.row].id){
//                    
//                      cell.Img.image = UIImage(named: "selected")
//                }
//                             
//                else {
//                               
//                               cell.Img.image = nil
//                           }
//                       
      
        cell.Lbl.text! = Filteredallergies[indexPath.row].name
        //cell.Img.tag = indexPath.row
        
      
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let id = Filteredallergies[indexPath.row].id
         let name = Filteredallergies[indexPath.row].name
        
         SelectedAllergyId = SelectedAllergyId.filter{($0 != id)}
        SelectedAllergyName = SelectedAllergyName.filter{($0 != name)}
        
       foodAndDrugAllergies = foodAndDrugAllergies.filter{($0.id != id)}
        print(SelectedAllergyName!)
        print(SelectedAllergyId!)
        Tv.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let cell = Tv.cellForRow(at: indexPath) as! City_StateCell
        
        cell.Img.image = UIImage(named: "selected")
    
        if SelectedAllergyId.contains((Filteredallergies[indexPath.row].id)) {
            
        }
        else {
           
            
            let allergy = foodDrugAllergies.init(id: Filteredallergies[indexPath.row].id, name: Filteredallergies[indexPath.row].name)
            
            foodAndDrugAllergies.append(allergy)
      
            profileData.foodAndDrugAllergies = foodAndDrugAllergies
            SelectedAllergyId.append((Filteredallergies[indexPath.row].id))
            SelectedAllergyName.append((Filteredallergies[indexPath.row].name))
        }
        
        print(SelectedAllergyName!)
        print(SelectedAllergyId!)
      //  Tv.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}

extension FoodAndAllergiesPicker{
    
    
        
        
        
          //MARK: - GetstatesList API
        func getFoodAndAllergies() {
                
            let request = MeftiiRequests.getFoodAndAllergies()
                
             //   self.isLoading()
                request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
                    
                   // self?.isLoadingFinished()
                    
                    print(code)
                  
                    if code == 0 {
                        
                        }
                        let data = resp["data"] as! [String:Any]
                        let cities = data["foodAndDrugAllergies"] as! [[String:Any]]
                    self?.Filteredallergies.removeAll()
                     self?.FoodAndAllergiesList.removeAll()
                        for city in cities {
                            
                            let value = Food.init(json: city)
                            print(value.id)
                            print(value.name)
                            self!.FoodAndAllergiesList += [value]
                            
                        }
                  
                        self!.Filteredallergies = self!.FoodAndAllergiesList
                        
                    
                    if self!.Filteredallergies.count > 0 {
                                   
                                   self!.Tv.isHidden = false
                               }
                               else{
                                   self!.Tv.isHidden = true
                               }
                    
                    
                        self!.Tv.reloadData()

                    
                    
                    }, failureBlock: { [weak self] (error) in
                       // self?.showToast(message: error.localizedDescription)
                        self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                                       
                                                                })
                        // self?.hideLoadingVi()
                })
                
            }
        
    
    
}

extension FoodAndAllergiesPicker: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text!.containsWhitespace{
                 
              if string == " " && textField.text?.last == " "{
                       return false
                 }

          }
        // Verify all the conditions
        if let sdcTextField = textField as? searchTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return true
    }
}

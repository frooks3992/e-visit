//
//  SignUpPatientOneVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 26/11/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import CropViewController


class SignUpPatientOneVC: BaseViewController {
    
    @IBOutlet weak var txtFirstName: CustomTextField!
    @IBOutlet weak var txtLastName: CustomTextField!
    
    @IBOutlet var firstNameError: UIImageView!
    
    @IBOutlet var firstNameUnderLine: UIView!
    @IBOutlet var lastNameError: UIImageView!
    
    @IBOutlet var lastNameUnderLine: UIView!
    
    @IBOutlet weak var dpGender: UIView!
    
    @IBOutlet var errorGender: UIImageView!
    var isGenderSelected = false
    @IBOutlet weak var imgPhotoVw: UIImageView!
    @IBOutlet weak var btnAddImage: UIButton!
    
    @IBOutlet var removePicBtn: UIButton!
    weak var signUpVc : SignUpPatientVC?
    var selectedGender :String!
    @IBOutlet var maleBtn: UIButton!
    
    @IBOutlet var femaleBtn: UIButton!
    @IBOutlet var otherBtn: UIButton!
    
    @IBOutlet var genderTitle: UILabel!
    
    @IBOutlet var pictureTitle: UILabel!
     var isSavePressed:Bool = false
    
    var uploaded = false
    var uploadedURL = ""
    var source : UIImagePickerController.SourceType!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if DataManager.shared.isPatientUpdating {
            let profileData  = (DataManager.shared.currentPatient ?? Profile_p2()).replica()
            
            txtFirstName.text! = profileData.firstName!
            txtLastName.text! = profileData.lastName!
            imgPhotoVw.sd_setImage(with: URL(string: (profileData.profilePicture)!), placeholderImage: nil)
            let gender = profileData.gender!
            
            switch gender {
            case "Male":
                selectedGender = "Male"
                reloadBtns(Btn: maleBtn)
            case "Female":
                selectedGender = "Male"
                reloadBtns(Btn: femaleBtn)
                
            default:
                selectedGender = "Other"
                reloadBtns(Btn: otherBtn)
            }
            
        }
        
        
        updateFirstNameView(value: true)
        updateLastNameView(value: true)
        updateGender(value: true)
        txtFirstName.delegate = self
        txtLastName.delegate = self
        txtFirstName.valueType = .fullName
        txtFirstName.maxLength = 25
        txtLastName.valueType = .fullName
        txtLastName.maxLength = 25
        // Do any additional setup after loading the view.
        removePicBtn.isHidden = true
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [4, 4]
        yourViewBorder.frame = imgPhotoVw.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(roundedRect: imgPhotoVw.bounds, cornerRadius: 60).cgPath
        imgPhotoVw.layer.addSublayer(yourViewBorder)
        
        // dpGender.dataStrings = Constants.genders.map{ $0.name ?? "" }
        
    }
    
    
    
    func updateFirstNameView(value:Bool){
        
        firstNameError.isHidden = value
        firstNameUnderLine.isHidden = value
    }
    
    func updateLastNameView(value:Bool){
        
        lastNameError.isHidden = value
        lastNameUnderLine.isHidden = value
    }
    
    func updateGender(value:Bool){
        errorGender.isHidden = value
    }
    func showGenderError(){
        if isGenderSelected {
            updateGender(value: true)
        }
    }
    @IBAction func removePicture(_ sender: Any) {
        imgPhotoVw.image = nil
        removePicBtn.isHidden = true
    }
    
    @IBAction func btnClicked(_ sender: UIButton) {
        genderTitle.textColor = .black
        updateGender(value: true)
        switch sender.tag {
        case 1:
            selectedGender = "Male"
            //            maleBtn.setTitleColor(.white, for: .normal)
            //            maleBtn.backgroundColor = #colorLiteral(red: 0.9050329328, green: 0.2997670174, blue: 0.2668333054, alpha: 1)
            //            femaleBtn.backgroundColor = .clear
            //            otherBtn.backgroundColor = .clear
            
            reloadBtns(Btn: maleBtn)
        case 2:
            selectedGender = "Female"
            reloadBtns(Btn: femaleBtn)
            //            femaleBtn.backgroundColor = #colorLiteral(red: 0.9050329328, green: 0.2997670174, blue: 0.2668333054, alpha: 1)
            //            maleBtn.backgroundColor = .clear
        //            otherBtn.backgroundColor = .clear
        default:
            
            selectedGender = "Other"
            reloadBtns(Btn: otherBtn)
            //        femaleBtn.backgroundColor = .clear
            //            maleBtn.backgroundColor = .clear
            //           otherBtn.backgroundColor = #colorLiteral(red: 0.9050329328, green: 0.2997670174, blue: 0.2668333054, alpha: 1)
        }
        
    }
    
    func reloadBtns(Btn: UIButton){
        let Btns = [maleBtn,femaleBtn,otherBtn]
        Btn.setTitleColor(.white, for: .normal)
        Btn.backgroundColor = #colorLiteral(red: 0.9050329328, green: 0.2997670174, blue: 0.2668333054, alpha: 1)
        for i  in Btns {
            
            if Btn != i {
                
                i!.setTitleColor(#colorLiteral(red: 0.9050329328, green: 0.2997670174, blue: 0.2668333054, alpha: 1), for: .normal)
                i!.backgroundColor = .clear
            }
        }
        
    }
    
    
    
    @IBAction func FNChanged(_ sender: UITextField) {
        
        switch sender.tag {
        case 1:
            
            
            
            if txtFirstName.text != ""{
                //firstNameUnderLine.isHidden = false
                // firstNameError.backgroundColor = UIColor.MFBlack60
                txtFirstName.hasError = false
                
            }
            
        default:
            if txtLastName.text != ""{
                updateLastNameView(value: true)
                txtLastName.hasError = false
            }
        }
    }
    
    
    
    @IBAction func imagePickerPressed(_ sender: UIButton) {
        PresentActionSheet()
        
    }
    
    @discardableResult
    func fillDataInProfileObject(isUpdate:Bool=false) -> Bool {
        
        
          isSavePressed = isUpdate
        
        guard let firstName = txtFirstName.text, firstName.count > 0 else {
            
            txtFirstName.hasError = true
            if txtLastName.text! == ""{
                txtLastName.hasError = true
                if selectedGender != nil {
                    updateGender(value: true)
                    
                }
                else{
                    updateGender(value: false)
                    
                }
                
            }
            
            
            return false
        }
        
        (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.firstName = firstName) :(signUpVc?.newDoctor.firstName = firstName)
        
        // signUpVc?.newDoctor.firstName = firstName
        
        guard let lastName = txtLastName.text, lastName.count > 0 else {
            
            updateLastNameView(value: false)
            txtLastName.hasError = true
            if !isGenderSelected{
                updateGender(value: false)
            }
            
            return false
        }
        
        (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.lastName = lastName) :(signUpVc?.newDoctor.lastName = lastName)
        //  signUpVc?.newDoctor.lastName = lastName
        
        guard let gender = selectedGender, gender.count > 0 else {
            updateGender(value: false)
            
            return false
        }
        
        // signUpVc?.newDoctor.gender = selectedGender
        
        (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.gender = selectedGender) :(signUpVc?.newDoctor.gender = selectedGender)
        
//        if let imageData = imgPhotoVw.image?.pngData() {
//            signUpVc?.newDoctor.profilePicture = imageData.base64EncodedString(options: .lineLength64Characters)
//        }
//
        
//        (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.isPregnant = "false") :(signUpVc?.newDoctor.isPregnant = "false")
        //signUpVc?.newDoctor.isPregnant = "false"
        
        if (uploaded == false){
            
            if imgPhotoVw.image != nil {
                
                processImage()
                
            }
                
            else{
                
                if isSavePressed{
                    NotificationCenter.default.post(name: Notification.Name("PatientImageUploaded"), object: nil)
                }
                
                
            }
          
        }
        else {
            
            (DataManager.shared.isPatientUpdating) ?  (DataManager.shared.currentPatient!.profilePicture = uploadedURL) :(signUpVc?.newDoctor.profilePicture = uploadedURL )
            // signUpVc?.newDoctor.profilePicture = uploadedURL
        }
        
        
        return true
        
    }
    
    
    func processImage(){
        
        
        if let image = imgPhotoVw.image{
            
            let img = Base64Image.shared.imageToBase64(imageToDecode: image)
            self.uploadImage(image: img, useCase: "profile", userType: "patient")
            
        }
        
        
        
    }
    
}

extension SignUpPatientOneVC : CropViewControllerDelegate {
    
    func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        self.uploaded = false
        self.imgPhotoVw.image = image
        
        pictureTitle.textColor = .black
        //processImage()
        //removePicBtn.isHidden = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didFinishCancelled cancelled: Bool) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    func PresentActionSheet(){
        
        let messageFont = [NSAttributedString.Key.font: UIFont(name: "Montserrat", size: 16.0)!,NSAttributedString.Key.foregroundColor: UIColor.black]
        let messageAttrString = NSMutableAttributedString(string: "Select Profile", attributes: messageFont)
        
        
        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        alert.setValue(messageAttrString, forKey: "attributedMessage")
        alert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { (_) in
            self.pickFromCamera(source: 1)
        }))
        
        alert.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: { (_) in
            self.pickFromCamera(source: 2)              }))
        
        if imgPhotoVw.image != nil {
            alert.addAction(UIAlertAction(title: "Delete Photo", style: .destructive, handler: { (_) in
                self.imgPhotoVw.image = nil
                self.uploaded = false
                self.pictureTitle.textColor = .lightGray
                // self.removePicBtn.isHidden = true
            }))
        }
        
        
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            print("User click Delete button")
        }))
        
        
        
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        alert.view.superview?.isUserInteractionEnabled = true
        alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.alertControllerBackgroundTapped)))
    }
    
    
    
    @objc func alertControllerBackgroundTapped()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}



//MARK: -UIImagePickerControllerDelegate
extension SignUpPatientOneVC :UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func pickFromCamera(source:Int){
        
        let picker = UIImagePickerController()
        
        if source != 1{
            
            picker.sourceType = .photoLibrary
        }
        else {
            picker.sourceType = .camera
        }
        picker.delegate = self
        self.present(picker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        //imageView.contentMode = .scaleAspectFit
        //imageView.image = chosenImage.resizeImage(image: chosenImage, targetSize: CGSize(width: 3024 , height: 4032))
        //dismiss(animated:true, completion: nil)
        self.dismiss(animated: false) {
            
            let cropViewController = CropViewController(croppingStyle: .circular, image: chosenImage)
            cropViewController.delegate = self
            self.present(cropViewController, animated: true, completion: nil)
            
        }
    }
    
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension SignUpPatientOneVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text!.containsWhitespace{
                           
                        if string == " " && textField.text?.last == " "{
                                 return false
                           }

                       }
        // Verify all the conditions
        if let sdcTextField = textField as? CustomTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return true
    }
}



extension SignUpPatientOneVC{
    
    
    //MARK: - GetstatesList API
    func uploadImage(image:String,useCase:String,userType:String) {
        
        let nc = NotificationCenter.default
              
               if DataManager.shared.isPatientUpdating && isSavePressed{
                    nc.post(name: Notification.Name("showLoaderP"), object: nil)
              }
        
        let request = MeftiiRequests.uploadImage(image: image, useCase: useCase, userType: userType)
        
        //   self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            
            if code == 0 {
                
                let data = resp["data"] as! [String:Any]
                let image = data["imgUrl"] as! String
                print(image)
                self?.uploaded = true
                 DataManager.shared.currentPatient?.profilePicture = image
                self!.signUpVc?.newDoctor.profilePicture = image
                self!.uploadedURL = image
                
                if DataManager.shared.isPatientUpdating && self!.isSavePressed{
                
                  nc.post(name: Notification.Name("PatientImageUploaded"), object: nil)
                  }
                
            }
            
            }, failureBlock: { [weak self] (error) in
               // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                               
                                                        })
                // self?.isLoadingFinished()
        })
        
    }
    
    
    
    
}

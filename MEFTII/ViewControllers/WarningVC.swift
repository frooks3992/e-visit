//
//  WarningVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 27/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class WarningVC: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func continueButtonPressed(_ sender: MFButton) {
        
        let vc  = HomePatientVC.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    

}

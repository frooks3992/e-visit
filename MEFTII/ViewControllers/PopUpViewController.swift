//
//  PopUpViewController.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 03/09/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

typealias AlertButtonCallback = () -> Void

class PopUpViewController: UIViewController {

    @IBOutlet weak var popUpMessage: UILabel!
    @IBOutlet weak var popUpTitle: UILabel!
    @IBOutlet weak var stackVw: UIStackView!
    @IBOutlet weak var centerView: UIView!
    
    var popTitle:String = ""
    var popMessage:String = ""
    var leftButtonTitle:String?
    var leftButtonCallback : AlertButtonCallback?
    var rightButtonTitle:String = "OK"
    var rightButtonCallback : AlertButtonCallback?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.modalTransitionStyle = .crossDissolve
        
        popUpMessage.text = popMessage
        popUpTitle.text = popTitle
        
        centerView.clipsToBounds = true
        centerView.layer.masksToBounds = false
        centerView.layer.cornerRadius = 15.0
        centerView.layer.shadowRadius = 6
        centerView.layer.shadowOpacity = 0.5
        
        if let btnTitle = leftButtonTitle {
            let leftButton = MFButton(type: UIButton.ButtonType.system)
            leftButton.titleSize = 15
          //  leftButton.isOutlineButton = true
            leftButton.tag = 777
            leftButton.addTarget(self, action: #selector(buttonPressed(_:)), for: UIControl.Event.touchUpInside)
            leftButton.adjustsButtonLayout()
            stackVw.addArrangedSubview(leftButton)
            leftButton.setTitle(btnTitle, for: UIControl.State.normal)
            
            
        }
        
        let rightButton = MFButton(type: UIButton.ButtonType.system)
        rightButton.titleSize = 15
        rightButton.addTarget(self, action: #selector(buttonPressed(_:)), for: UIControl.Event.touchUpInside)
        rightButton.adjustsButtonLayout()
        stackVw.addArrangedSubview(rightButton)
        rightButton.setTitle(rightButtonTitle, for: UIControl.State.normal)
        
        self.view.setNeedsDisplay()
        self.view.layoutIfNeeded()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for vw in stackVw.arrangedSubviews {
            if let btn = vw as? MFButton {
                btn.adjustsButtonLayout()
            }
        }
    }
 
    @objc func buttonPressed(_ sender: MFButton) {
        
        let callBack : AlertButtonCallback? = sender.tag == 777 ? leftButtonCallback:rightButtonCallback
        
        self.dismiss(animated: true, completion: {
            callBack?()
        })
        
    }
    



}

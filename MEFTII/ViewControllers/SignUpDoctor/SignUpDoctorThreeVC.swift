//
//  SignUpDoctorThreeVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 26/11/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class SignUpDoctorThreeVC: UIViewController {
    
    @IBOutlet weak var txtDeaNumber: CustomTextField!
    @IBOutlet weak var txtSpeciality: CustomTextField!
    @IBOutlet weak var txtNationalProviderId: CustomTextField!
    @IBOutlet weak var txtComapanyHospital: CustomTextField!
    @IBOutlet weak var txtStateLicenseNumber: CustomTextField!
    @IBOutlet weak var txtSocialSecurityNumber: CustomTextField!
    
    weak var signUpVc : SignUpDoctorVC?
     @IBOutlet var TextFields: [CustomTextField]!
var  isdoctorUpdating = DataManager.shared.isDoctorUpdating
    override func viewDidLoad() {
        super.viewDidLoad()

        txtDeaNumber.delegate = self
        txtDeaNumber.valueType = .alphaNumeric
        txtDeaNumber.maxLength = 9
        txtNationalProviderId.delegate = self
        txtNationalProviderId.valueType = .onlyNumbers
        txtNationalProviderId.maxLength = 10
        txtComapanyHospital.delegate = self
        txtComapanyHospital.valueType = .alphaNumeric
        txtComapanyHospital.maxLength = 25
        
        txtStateLicenseNumber.delegate = self
        txtStateLicenseNumber.valueType = .alphaNumeric
        txtStateLicenseNumber.maxLength = 10
        
        txtSpeciality.delegate = self
        txtSpeciality.valueType = .fullName
        txtSpeciality.maxLength = 25
        txtSocialSecurityNumber.delegate = self
         txtSocialSecurityNumber.valueType = .onlyNumbers
          txtSocialSecurityNumber.maxLength = 4
        setUpIfUpdating()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setUpIfUpdating(){
        
        if isdoctorUpdating {
            let profileData  = (DataManager.shared.currentDoctor ?? Profile_d2()).replica()
            
            txtDeaNumber.text! = profileData.deaNumber!
            txtSpeciality.text! = profileData.speciality!
            txtNationalProviderId.text! = profileData.nationalProviderIdentifierNumber!
            txtComapanyHospital.text! = profileData.hospitalAffiliation!
            txtStateLicenseNumber.text! = profileData.stateLicenseNumber!
            txtSocialSecurityNumber.text! = profileData.socialSecurityNumber!
            
       
        }
    }
    
           @IBAction func EditingChanged(_ sender: CustomTextField) {
               
    //           switch sender.tag {
    //           case 2:
    //               txtAddress.hasError = false
    //           default:
    //                 txtZipCode.hasError = false
    //           }
    //
            sender.hasError = false
               
               
           }
        
    
    
     func checkemptyFields(){
          // var empty = [UITextField]()
           for textField in TextFields {
               
               if textField.text == "" {
                  // empty.append(textField)
                   
                  // if (textField.tag == 3) || (textField.tag == 4) {
                   
                //   }
                   // textField.hasArrow = false
                   textField.hasError = true
               }
                
                if ((textField.tag == 5) && (textField.text!.count < 4)) {
                               textField.hasError = true
                           }
               else {
                   // textField.hasArrow = true
               }
                  
           }
         
       }
    
    @discardableResult
    func fillDataInProfileObject() -> Bool {
        
        guard let dea = txtDeaNumber.text, dea.count > 0 else {
             checkemptyFields()
            return false
        }
          (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.deaNumber = dea) :(signUpVc?.newDoctor.deaNumber = dea)
        //signUpVc?.newDoctor.deaNumber = dea
        
        guard let speciality = txtSpeciality.text, speciality.count > 0 else {
             checkemptyFields()
            return false
        }
        
         (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.speciality = speciality) :(signUpVc?.newDoctor.speciality = speciality)
       // signUpVc?.newDoctor.speciality = speciality
        
        guard let npi = txtNationalProviderId.text, npi.count > 0 else {
             checkemptyFields()
            return false
        }
        
         (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.nationalProviderIdentifierNumber = npi) :(signUpVc?.newDoctor.nationalProviderIdentifierNumber = npi)
       // signUpVc?.newDoctor.nationalProviderIdentifierNumber = npi
        
        guard let company = txtComapanyHospital.text, company.count > 0 else {
             checkemptyFields()
            return false
        }
        
         (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.hospitalAffiliation = company) :(signUpVc?.newDoctor.hospitalAffiliation = company)
       // signUpVc?.newDoctor.hospitalAffiliation = company
        
        guard let ssn = txtSocialSecurityNumber.text, ssn.count > 0 && ssn.count == 4 else {
             checkemptyFields()
            return false
        }
        (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.socialSecurityNumber = ssn) :(signUpVc?.newDoctor.socialSecurityNumber = ssn)
            // signUpVc?.newDoctor.socialSecurityNumber = ssn
        
        guard let stl = txtStateLicenseNumber.text, stl.count > 0 else {
             checkemptyFields()
            return false
        }
            (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.stateLicenseNumber = stl) :(signUpVc?.newDoctor.stateLicenseNumber = stl)
       // signUpVc?.newDoctor.stateLicenseNumber = stl
        
        return true
    
    }

}


extension SignUpDoctorThreeVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField.text!.containsWhitespace{
                      
                   if string == " " && textField.text?.last == " "{
                            return false
                      }

                  }
        // Verify all the conditions
        if let sdcTextField = textField as? CustomTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return true
    }
}

//
//  SignUpDoctorTwoVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 26/11/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class SignUpDoctorTwoVC: UIViewController {
    
    @IBOutlet var CityTF: CustomTextField!
    @IBOutlet var StateTF: CustomTextField!
    
    
    @IBOutlet var TextFields: [CustomTextField]!
    
    @IBOutlet weak var txtAddress: CustomTextField!
    @IBOutlet weak var dpCity: UIView!
    @IBOutlet weak var dpState: UIView!
    @IBOutlet weak var txtZipCode: CustomTextField!
    @IBOutlet weak var txtDateOfBirth: CustomTextField!
    @IBOutlet weak var pregnantViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnPregnant: MFButton!
    @IBOutlet weak var btnNotPregnant: MFButton!
    
    var selectedCity:String!
    var selectedState:String!
    var selectedCityId:Int!
    var selectedStateId:Int!
    weak var signUpVc : SignUpDoctorVC?
    var selectedId = 0
    var SelectedPickerType:Int!
    var  isdoctorUpdating = DataManager.shared.isDoctorUpdating
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        CityTF.hasArrow = true
        StateTF.hasArrow = true
        txtAddress.delegate = self
        txtZipCode.delegate = self
        txtAddress.valueType = .alphaNumericSpace
        txtAddress.maxLength = 35
        txtZipCode.valueType = .onlyNumbers
        txtZipCode.maxLength = 7
        
        
        //  self.isLoaderShow(yes: false)
        
        
        // Do any additional setup after loading the view.
        btnPregnant.isOutlineButton = true
        btnNotPregnant.isOutlineButton = true
        setUpIfUpdating()
        
        //        dpCity.dataStrings = Constants.cities.map{ $0.name ?? "" }
        //        dpState.dataStrings = Constants.states.map{ $0.name ?? "" }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 65.0
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 45.0
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setUpIfUpdating(){
        
        if isdoctorUpdating {
            let profileData  = (DataManager.shared.currentDoctor ?? Profile_d2()).replica()
            
            txtDateOfBirth.text! = profileData.dateOfBirth!
            txtAddress.text! = profileData.street!
            StateTF.text! = profileData.state?.name ?? "LosAngeles"
            CityTF.text! =  profileData.city?.name ?? "San jose"
            txtZipCode.text! = profileData.zipCode!
            
        }
    }
    
    
    @IBAction func dateOfBirthPressed(_ sender: UIButton) {
        
        //        let date = Date(timeIntervalSince1970: -2.113e+9)
        //
        //        signUpVc?.showChoseDateDialog(minDate: date , maxDate: Date()) { [weak self] (date, strDate) in
        //
        //            let gender = (self?.signUpVc?.newDoctor.gender ?? "").lowercased()
        //            var isOver18 = false
        //            if gender.contains("fema") {
        //                if let birthDate = date {
        //                    let calendar = Calendar(identifier: .gregorian)
        //                    let now = Date()
        //                    let ageComponents = calendar.dateComponents([.year], from: birthDate, to: now)
        //                    let age = ageComponents.year ?? 0
        //                    isOver18 = (age >= 13 && age <= 50)
        //                }
        //            }
        //            self?.showHidePregnantView(show:isOver18)
        //
        //            self?.txtDateOfBirth.text = strDate
        //
        //        }
        
        self.showDateDialog()
        
        
        
        
        
    }
    
    
    
    func showDateDialog(){
        
        let myDatePicker: UIDatePicker = UIDatePicker()
        //  myDatePicker.timeZone = NSTimeZone.local
        myDatePicker.datePickerMode = .date
        
        
        let date = Date(timeIntervalSince1970: -2.113e+9)
        myDatePicker.minimumDate = date
        myDatePicker.maximumDate = Date()
        myDatePicker.frame = CGRect(x: 10, y: 10, width: 260, height: 200)
        
    
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
       let constraintHeight = NSLayoutConstraint(
          item: alertController.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute:
          NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 250)
        
        let constraintWidth = NSLayoutConstraint(
           item: alertController.view!, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute:
           NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 280)
        alertController.view.addConstraint(constraintWidth)
       alertController.view.addConstraint(constraintHeight)
        alertController.view.addSubview(myDatePicker)
        let somethingAction = UIAlertAction(title: "Done", style: .destructive) { (UIAlertAction) in
            
            
            
            let gender = (self.signUpVc?.newDoctor.gender ?? "").lowercased()
            var isOver18 = false
            if gender.contains("fema") {
                if let birthDate = myDatePicker.date as Date?{
                    let calendar = Calendar(identifier: .gregorian)
                    let now = Date()
                    let ageComponents = calendar.dateComponents([.year], from: birthDate, to: now)
                    let age = ageComponents.year ?? 0
                    isOver18 = (age >= 13 && age <= 50)
                }
            }
            self.showHidePregnantView(show:isOver18)
            
            let selectedDate = Date_Time.shared.dateFormatter(date: myDatePicker.date)
            self.txtDateOfBirth.text! = selectedDate
            self.txtDateOfBirth.hasError = false
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertController.addAction(cancelAction)
        alertController.addAction(somethingAction)
        
        present(alertController, animated: true, completion:{})
        
    }
    
    
    
    
    @IBAction func EditingChanged(_ sender: UITextField) {
        
        switch sender.tag {
        case 2:
            txtAddress.hasError = false
        default:
            txtZipCode.hasError = false
        }
        
        
        
        
    }
    
    
    @IBAction func unwindFromCityPicker(_ sender: UIStoryboardSegue){
        
        if sender.source is City_StatePickerVC{
            
            if let senderVC = sender.source as? City_StatePickerVC {
                
                if senderVC.SelectedCity != nil{
                    //  CityTF.hasArrow = true
                    CityTF.text = senderVC.SelectedCity
                    selectedCityId = senderVC.selectedCityId
                }
                
                if senderVC.SelectedState != nil{
                    // StateTF.hasArrow = true
                    
                    // StateTF.hasArrow = true
                    if (StateTF.text != senderVC.SelectedState)&&(StateTF.text != ""){
                        CityTF.text = ""}
                    
                    StateTF.text = senderVC.SelectedState
                    selectedId = senderVC.selectedId
                    selectedStateId = senderVC.selectedStateId
                }
                
            }
            
        }
        
    }
    
    @IBAction func City_StatePressed(_ sender: UIButton) {
        
        switch sender.tag {
        case 1:
            CityTF.hasError = false
        default:
            StateTF.hasError = false
        }
        
        SelectedPickerType  = sender.tag
        performSegue(withIdentifier: "ToCity_StatePickerVC2", sender: SelectedPickerType)
    }
    
    
    
    @IBAction func pregnantBtnPressed(_ sender: MFButton) {
        
        btnPregnant.isOutlineButton = false
        btnNotPregnant.isOutlineButton = true
        
        btnNotPregnant.setNeedsLayout()
        btnNotPregnant.setNeedsLayout()
        btnNotPregnant?.superview?.layoutSubviews()
        
    }
    
    @IBAction func notPregnantPressed(_ sender: MFButton) {
        
        btnPregnant.isOutlineButton = true
        btnNotPregnant.isOutlineButton = false
        
        btnNotPregnant.setNeedsLayout()
        btnNotPregnant.setNeedsLayout()
        btnNotPregnant?.superview?.layoutSubviews()
    }
    
    func checkemptyFields(){
        // var empty = [UITextField]()
        for textField in TextFields {
            
            if textField.text == "" {
                // empty.append(textField)
                
                // if (textField.tag == 3) || (textField.tag == 4) {
                
                //   }
                // textField.hasArrow = false
                textField.hasError = true
            }
            else {
                // textField.hasArrow = true
            }
            
        }
        
    }
    
    @discardableResult
    func fillDataInProfileObject() -> Bool {
        
        let add = Address()
        guard let street = txtAddress.text, street.count > 0 else {
            checkemptyFields()
            
            
            return false
        }
        (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.street = street) : (signUpVc?.newDoctor.street = street)
        //  signUpVc?.newDoctor.street = street
        
        guard let city = CityTF.text, city.count > 0 else {
            checkemptyFields()
            return false
        }
        
        (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.city?.name = selectedCity) :(print())
        (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.cityId = selectedCityId) :( signUpVc?.newDoctor.cityId = selectedCityId)
        
        // signUpVc?.newDoctor.cityId = selectedCityId
        checkemptyFields()
        guard let state = StateTF.text, state.count > 0 else {
            checkemptyFields()
            return false
        }
        
        (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.state?.name = selectedState) :(print())
        
        (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.stateId = selectedId) :( signUpVc?.newDoctor.stateId = selectedId)
        
        
        //  signUpVc?.newDoctor.stateId = selectedId
        //add.state = StateTF.text!
        //checkemptyFields()
        guard let zip = txtZipCode.text, zip.count > 0 else {
            checkemptyFields()
            return false
        }
        (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.zipCode = zip) :( signUpVc?.newDoctor.zipCode = zip)
        
        // signUpVc?.newDoctor.zipCode = zip
        
        guard let dob = txtDateOfBirth.text, dob.count > 0 else {
            checkemptyFields()
            return false
        }
        (isdoctorUpdating) ?  (DataManager.shared.currentDoctor!.dateOfBirth = dob) :( signUpVc?.newDoctor.dateOfBirth = dob)
        // signUpVc?.newDoctor.dateOfBirth = dob
        
        
        
        return true
    }
    
    func showHidePregnantView(show:Bool) {
        
        
        UIView.animate(withDuration: 0.33) {
            self.pregnantViewHeight.constant = show ? 0:0
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationVc = segue.destination as! City_StatePickerVC
               destinationVc.Pickertype = SelectedPickerType
               destinationVc.SelectedCity = self.CityTF.text
               destinationVc.SelectedState = self.StateTF.text
               destinationVc.selectedId = self.selectedId
        
        
        
    }
    
}

extension SignUpDoctorTwoVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField.text!.containsWhitespace{
                      
                   if string == " " && textField.text?.last == " "{
                            return false
                      }

                  }
        // Verify all the conditions
        if let sdcTextField = textField as? CustomTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return true
    }
}

//
//  SignUpDoctorFourVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 26/11/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit

class SignUpDoctorFourVC: UIViewController {
    
    
    @IBOutlet var txtPhone: CustomTextField!
    @IBOutlet weak var txtEmail: CustomTextField!
    @IBOutlet weak var txtPassword: CustomTextField!
    @IBOutlet weak var txtConfirmPass: CustomTextField!
    @IBOutlet weak var btnCheckUncheck: UIButton!
    @IBOutlet weak var txtUserName: CustomTextField!
    @IBOutlet var TextFields: [CustomTextField]!
    weak var signUpVc : SignUpDoctorVC?
    var  isdoctorUpdating = DataManager.shared.isDoctorUpdating
    @IBOutlet var agreeHasError: UIImageView!
    
    @IBOutlet weak var agreeLbl: UILabel!
    
    
    var checked : Bool = false
    var confirmed : Bool {
        return btnCheckUncheck.tag == 201
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        agreeHasError.isHidden = true
        txtPhone.delegate = self
        txtEmail.delegate = self
       
        txtPassword.delegate = self
        txtPassword.maxLength = 25
        txtEmail.maxLength = 35
        txtEmail.autocapitalizationType = .none
        txtPhone.valueType = .onlyNumbers
        txtPhone.maxLength = 14
        
        
        // Do any additional setup after loading the view.
        
        btnCheckUncheck.layer.borderColor = UIColor.darkGray.cgColor
        btnCheckUncheck.layer.borderWidth = 1.0
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func checkUncheckPressed(_ sender: UIButton) {
       if btnCheckUncheck.tag == 200 {
                   btnCheckUncheck.tag = 201
                   btnCheckUncheck.layer.borderColor = UIColor.clear.cgColor
                    btnCheckUncheck.backgroundColor = UIColor.MFRed
                        btnCheckUncheck.layer.borderWidth = 1.0
                   btnCheckUncheck.setBackgroundImage(#imageLiteral(resourceName: "ic_agreed"), for: .normal)
                  
                   checked = true
                   agreeHasError.isHidden = true
                    agreeLbl.textColor = .black
               }
               else {
                   btnCheckUncheck.tag = 200
                   btnCheckUncheck.layer.borderColor = UIColor.darkGray.cgColor
                   btnCheckUncheck.backgroundColor = .clear
                        btnCheckUncheck.layer.borderWidth = 1.0
                   btnCheckUncheck.setBackgroundImage(nil, for: .normal)
                   checked = false
               }
        
    }
    func checkemptyFields(){
           // var empty = [UITextField]()
            for textField in TextFields {
                   
                   if textField.text == "" {
                       
                       
                       textField.hasError = true
                   }
                   
                   if textField.tag == 1{
                       
                       
                       (textField.text!.count < 14) ?  (textField.hasError = true) : (textField.hasError = false)
                       
                       
                   }
                   
                   if textField.tag == 2{
                       
                       (textField.text?.isValidEmail())! ? (textField.hasError = false) : (textField.hasError = true)
                   }
                   
                   
                   if textField.tag == 4{
                       
                       (textField.text?.isValidPassword())! ? (textField.hasError = false) : (textField.hasError = true)
                   }
                   
                   if textField.tag == 5{
                       
                       (textField.text?.isValidPassword())! ? (textField.hasError = false) : (textField.hasError = true)
                   }
                   
                   
                   if !checked{
                       
                       agreeHasError.isHidden = false
                       agreeLbl.textColor = .MFDarkRed
                   }
                       
                   else {
                       // textField.hasArrow = true
                   }
                   
               }
           
       }
    
    @IBAction func PhoneNumberFormatting(_ sender: Any) {
        
        
        if txtPhone.text?.count == 1{
            txtPhone.text?.append("-")
        }
        
        
        
    }
    
    
    
    @IBAction func EditingChanged(_ sender: CustomTextField) {
        
     if sender.tag == 2{
             
             (sender.text?.isValidEmail())! ? (sender.hasError = false) : (sender.hasError = true)
         }
         
         
         if sender.tag == 4{
             
            (sender.text?.isValidPassword())! ? (sender.hasError = false) : (sender.hasError = true)
         }
         if sender.tag == 5{

            (sender.text?.isValidPassword())! ? (sender.hasError = false) : (sender.hasError = true)
         }
             
         else{
             
             sender.hasError = false
         }
        
    }
    
    
    func formattedNumber(number: String) -> String {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let mask = "+X (XXX) XXX-XXXX"
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask where index < cleanPhoneNumber.endIndex {
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        return result
    }
    
    
    @discardableResult
    func fillDataInProfileObject() -> Bool {
        
        guard let phone = txtPhone.text, phone.count > 0 && phone.count == 14 else {
            checkemptyFields()
            return false
        }
       signUpVc?.newDoctor.phone = phone
        
        guard let email = txtEmail.text, Constants.isValidEmail(emailStr: email) else {
            //            signUpVc?.showToast(message: "Email is not good ⚠️")
            checkemptyFields()
            return false
        }
        signUpVc?.newDoctor.email = email
        
//        guard let username = txtUserName.text, username.count > 0 else {
//            //            signUpVc?.showToast(message: "Username is not good ⚠️")
//            checkemptyFields()
//            return false
//        }
    //    signUpVc?.newDoctor.username = txtUserName.text
        
      let passOne = txtPassword.text ?? ""
        let passTwo = txtConfirmPass.text ?? ""
        let isvalid = passOne.isValidPassword()
        
        if  passOne == passTwo && isvalid {
            signUpVc?.newDoctor.password = passOne
        }
        else {
            checkemptyFields()
            //            signUpVc?.showToast(message: "Password issue ⚠️")
            return false
        }
        
        if !checked {
              checkemptyFields()
            return false
        }
        
        return true

        
    }
    
}
extension SignUpDoctorFourVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.text!.containsWhitespace{
                      
                   if string == " " && textField.text?.last == " "{
                            return false
                      }

                  }
        
                      
        if textField.tag == 1{
            
            if let char = string.cString(using: String.Encoding.utf8) {
                
                
                switch textField.text?.count {
                case 1,5,9:
                    let isBackSpace = strcmp(char, "\\b")
                    if (isBackSpace == -92) {
                        print("Backspace was pressed")
                    }
                    else{
                        textField.text?.append("-")
                    }
                    
                case 0:
                    
                    textField.text?.append("1-")
                    
                    
                default:
                    print("")
                }
                
                
                //                    q
            }
        }
        
       if textField.tag == 2{
                   
                    let tf = textField as! CustomTextField
                   (textField.text?.isValidEmail())! ? print("valid"):(tf.hasError = true)
               }
               
              
        if textField.tag == 4{
               
               let tf = textField as! CustomTextField
               
             //  (textField.text?.isValidPassword())! ? print("valid"): (tf.hasError = true)
               
           }
           if textField.tag == 5{
               
               let tf = textField as! CustomTextField
               
            //   (textField.text?.isValidPassword())! ? print("valid"): (tf.hasError = true)
               
           }
//        if textField.tag == 5{
//
//                       let tf = textField as! CustomTextField
//
//                       (textField.text?.isValidPassword())! ? print("valid"): (tf.hasError = true)
//
//                   }
        
        // Verify all the conditions
        if let sdcTextField = textField as? CustomTextField {
            return sdcTextField.verifyFields(shouldChangeCharactersIn: range, replacementString: string)
        }
        return true
    }
    
    
    
    
}


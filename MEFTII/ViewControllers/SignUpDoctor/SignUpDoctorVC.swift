//
//  SignUpDoctorVC.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 26/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import UIKit
import DropDown
import CropViewController
import IQKeyboardManagerSwift

class SignUpDoctorVC: BaseViewController {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var nextBtn: MFButton!
    @IBOutlet weak var steppedLabel: UILabel!
    @IBOutlet weak var steppedVw: MFSteppedView!
    @IBOutlet weak var steppedVw2: MFSteppedView!
    @IBOutlet weak var saveBtn: MFButton!
    @IBOutlet weak var backBtn: MFButton!
    var vf = true
    
    @IBOutlet weak var back2Btn: MFButton!
    @IBOutlet weak var topCons: NSLayoutConstraint!
    static var isUpdateDoctorProfile = false
    static var isUpdateDoctorHealthandPolicy = false
    var crrentStep = 0
    var isSavePressed:Bool = false
    var newDoctor = Profile_d()
    
    static var isUpdateProfile = false
    static var isUpdateHealthandPolicy = false
    
    var isdoctorUpdating = DataManager.shared.isDoctorUpdating
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        
        if isdoctorUpdating{
            return [self.newSignUpViewController("One"),
                    self.newSignUpViewController("Two"),
                    self.newSignUpViewController("Three")]
            
        }
            
        else{
            
            return [self.newSignUpViewController("One"),
                    self.newSignUpViewController("Two"),
                    self.newSignUpViewController("Three"),
                    self.newSignUpViewController("Four")]
        }
    }()
    
    var pageViewController : UIPageViewController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupIfUpdating()
        // Do any additional setup after loading the view.
        newDoctor = Profile_d()
      //  back2Btn.isHidden = true
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(DocImageUploaded), name: Notification.Name("DoctorImageUploaded"), object: nil)
        nc.addObserver(self, selector: #selector(showLoading), name: Notification.Name("showLoader"), object: nil)
    }
    
    deinit {
        
        if isdoctorUpdating {
            (orderedViewControllers[0] as? SignUpDoctorOneVC)?.signUpVc = nil
            (orderedViewControllers[1] as? SignUpDoctorTwoVC)?.signUpVc = nil
            (orderedViewControllers[2] as? SignUpDoctorThreeVC)?.signUpVc = nil
        }
//        else {
//            (orderedViewControllers[0] as? SignUpDoctorOneVC)?.signUpVc = nil
//            (orderedViewControllers[1] as? SignUpDoctorTwoVC)?.signUpVc = nil
//            (orderedViewControllers[2] as? SignUpDoctorThreeVC)?.signUpVc = nil
//            (orderedViewControllers[3] as? SignUpDoctorFourVC)?.signUpVc = nil
//        }
        
        if !(DataManager.shared.isDoctorUpdating){
                   (orderedViewControllers[3] as? SignUpPatientFourVC)?.signUpVc = nil
               }
        
        // if isdoctorUpdating{
        
        //        if ((orderedViewControllers[3] as? SignUpDoctorFourVC)?.signUpVc) != nil {
        //            (orderedViewControllers[3] as? SignUpDoctorFourVC)?.signUpVc = nil
        //        }
        //
        
    }
    
    
    private func newSignUpViewController(_ name: String) -> UIViewController {
        let vc =  UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "SignUpDoctor\(name)VC")
        if let vcc = (vc as? SignUpDoctorOneVC) {
            vcc.signUpVc = self
        }
        else if let vcc = (vc as? SignUpDoctorTwoVC) {
            vcc.signUpVc = self
        }
        else if let vcc = (vc as? SignUpDoctorThreeVC) {
            vcc.signUpVc = self
        }
        else if let vcc = (vc as? SignUpDoctorFourVC) {
            vcc.signUpVc = self
        }
        
        return vc
        
    }
    
    
    
    
    @objc func DocImageUploaded(){
        let data = DataManager.shared.currentDoctor
        
        //DataManager.shared.saveLoggedInDoctor(profile: data!)
        
        self.updateDoctorProfile(data!)
    }
    
    @objc func showLoading(){
        
        self.isLoading()
    }
    
    
    @IBAction func back2BtnPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func savePressed(_ sender: Any) {
        isSavePressed = true
        prepareUpdateRequest(isUpdate: true)
    }
    
    func setupIfUpdating(){
        
        if isdoctorUpdating{
            
            steppedVw.isHidden = true
            titleLbl.text! = "Profile"
            saveBtn.isHidden = false
        }
        else{
            steppedVw.isHidden = false
            steppedVw2.isHidden = true
            saveBtn.isHidden = true
            titleLbl.text! = "Sign Up"
        }
        
                 if DataManager.shared.isDoctorUpdating{
                    
                   
                 self.steppedVw.totalSteps =  2
                          if SignUpDoctorVC.isUpdateDoctorHealthandPolicy{
                            backBtn.isHidden = true
                            back2Btn.isHidden = false
                            moveToStep(step:crrentStep + 2)
//                           backBtn.isHidden = true
                            steppedVw.isHidden = true
                            steppedVw2.isHidden = true
                        steppedLabel.isHidden = true
                          topCons.constant = -40
                            UIView.animate(withDuration: 0) {
                                self.view.layoutIfNeeded()
                            }
                            
                            titleLbl.font = UIFont.AppSemiBoldFont(ofSize: 24)
                            titleLbl.text = "Legal Info"
                                pageViewController?.setViewControllers([orderedViewControllers[crrentStep + 2]], direction: .reverse, animated: true, completion: { (sts) in
                                    self.crrentStep = self.crrentStep + 2
                                })
                            }
                        }
//         steppedVw.isHidden = true
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        
        if let vc = segue.destination as? UIPageViewController {
            vc.delegate = nil
            vc.dataSource = nil
            self.pageViewController = vc
            
            if let firstViewController = orderedViewControllers.first {
                vc.setViewControllers([firstViewController],
                                      direction: .forward,
                                      animated: true,
                                      completion: nil)
            }
            
        }
        if let destinationVc = segue.destination as? OTPAuthenticationVC{
            destinationVc.msisdn = self.newDoctor.phone
            //self.newDoctor.p
            
            destinationVc.userType = "doctor"
        }
    }
    
    
    
    func prepareUpdateRequest(isUpdate:Bool = false){
        let firstVC =  orderedViewControllers[0] as! SignUpDoctorOneVC
        let secondVC =  orderedViewControllers[1] as! SignUpDoctorTwoVC
        let thirdVC =  orderedViewControllers[2] as! SignUpDoctorThreeVC
        if crrentStep < 3 {
            
            if crrentStep == 0 {
                if !(firstVC).fillDataInProfileObject(isUpdate: isUpdate) {
                    return
                }
            }
            else if crrentStep == 1 {
                if !(secondVC).fillDataInProfileObject() {
                    return
                }
            }
            else if crrentStep == 2 {
                if !(thirdVC).fillDataInProfileObject() {
                    return
                }
            }
            
        }
        
        
        
        
        // DataManager.shared.saveLoggedInDoctor(profile: data!)
        
        if  crrentStep != 0 {
            
            let data = DataManager.shared.currentDoctor
            
            print(data?.id)
            print(data?.doctorId)
            
            self.updateDoctorProfile(data!)
        }
        
        
        
    }
    
    
    @IBAction override func backButtonPressed(_ sender: UIButton) {
        
        if crrentStep == 0 {
            self.navigationController?.popViewController(animated: true)
            
        }
        else {
            
            moveToStep(step:crrentStep - 1)
            
            pageViewController?.setViewControllers([orderedViewControllers[crrentStep - 1]], direction: .reverse, animated: true, completion: { (sts) in
                self.crrentStep = self.crrentStep - 1
            })
            
        }
        
        
    }
    
    @IBAction func nextButtonPressed(_ sender: MFButton) {
        if crrentStep < 3 {
            
            if crrentStep == 0 {
                if !(orderedViewControllers[0] as! SignUpDoctorOneVC).fillDataInProfileObject() {
                    return
                }
            }
            else if crrentStep == 1 {
                if !(orderedViewControllers[1] as! SignUpDoctorTwoVC).fillDataInProfileObject() {
                    return
                }
            }
            else if crrentStep == 2 {
                if !(orderedViewControllers[2] as! SignUpDoctorThreeVC).fillDataInProfileObject() {
                    return
                }
            }
            
            
            
            if isdoctorUpdating && crrentStep == 2 {
                
                self.prepareUpdateRequest()
                
            }
                
            else{
                moveToStep(step:crrentStep + 1)
                pageViewController?.setViewControllers([orderedViewControllers[crrentStep + 1]], direction: .forward, animated: true, completion: { (sts) in
                    self.crrentStep = self.crrentStep + 1
                })
            }
            
        }
        else {
            
            //            (orderedViewControllers[0] as? SignUpDoctorOneVC)?.fillDataInProfileObject()
            //            (orderedViewControllers[1] as? SignUpDoctorTwoVC)?.fillDataInProfileObject()
            //            (orderedViewControllers[2] as? SignUpDoctorThreeVC)?.fillDataInProfileObject()
            if (orderedViewControllers[3] as? SignUpDoctorFourVC)?.fillDataInProfileObject() ?? false {
            
            let phone = newDoctor.phone ?? ""
            let email = newDoctor.email ?? ""
            let password = newDoctor.password ?? ""
            let isConfimed = (orderedViewControllers[3] as? SignUpDoctorFourVC)?.confirmed
            
            guard phone.count > 0 && phone.count == 14 else {
         //   self.showAlertWith(title: "Warning", message: "Phone cannot be empty", buttonText: "Ok", callBack: {
                                     
                //    })
             //   vf = false
                            return
                        }
            
            guard email.count > 0 && email.isValidEmail() else {
              
               //  vf = false

              //  self.showAlertWith(title: "Warning", message: "Email cannot be empty", buttonText: "Ok", callBack: {
                                                  //  })
                return
            }
            
            guard password.count > 0 && password.isValidPassword() else {
                
                
              //  vf = false

              //  showToast(message: "Password is invalid ⚠️")
              //  self.showAlertWith(title: "Warning", message: "Password is invalid", buttonText: "Ok", callBack: {
                                                                 //  })
                return
            }
            
            
            
            if isConfimed ?? false{
                
                
            } else {
                
               //  vf = false

                
              //  showToast(message: "Please accept terms of use ⚠️")
                self.showAlertWith(title: "Warning", message: "Please accept terms of use", buttonText: "Ok", callBack: {
                                                                                })
                return
            }
            
            
           
                  performSegue(withIdentifier: "toOTPVerification2", sender: nil)
            
            }
            // registerDoctorProfile(newDoctor)
          
        }
        
    }
    
    
    @IBAction func unwindFromOTP2(_ sender: UIStoryboardSegue){
        
        if sender.source is OTPAuthenticationVC{
            
            if let senderVC = sender.source as? OTPAuthenticationVC {
                
                if senderVC.verified{
                    //  CityTF.hasArrow = true
                    registerDoctorProfile(newDoctor)
                    
                }
                    
                else{
                    
                }
                
                
            }
            
        }
        
    }
    
    
    func moveToStep(step:Int) {
        var label = "Basic Info"
        switch step {
        case 2:
            if isdoctorUpdating {
                nextBtn.setTitle("SAVE", for: UIControl.State.normal)
            }
            else{
                nextBtn.setTitle("NEXT", for: UIControl.State.normal)}
            
            label = "Legal Info"
        case 3:
            nextBtn.setTitle("SUBMIT", for: UIControl.State.normal)
            label = "Security Info"
        default:
            nextBtn.setTitle("NEXT", for: UIControl.State.normal)
            label = "Basic Info"
        }
        steppedLabel.text = label
        
        if isdoctorUpdating {
            steppedVw2.moveTo(Step: step)
            
            
            print(step)
        }
        else{
            steppedVw.moveTo(Step: step)
        }
        
    }
    
    //MARK: - RegisterDoctorProfile API
    func registerDoctorProfile(_ profile: Profile_d) {
        
        DataManager.shared.setcurrentEmail(email: profile.email!)
        
        let request = MeftiiRequests.registerDoctorProfile(profile)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            if code == 0 {
               // self?.showToast(message: "Signed up successfully!")
                self?.showAlertWith(title: "Success", message: desc, buttonText: "Okay", callBack: {
                    //                    let vc  = LoginVC.instantiateFromStoryboard()
                 //   self?.navigationController?.popViewController(animated: true)
                    
                    self?.performSegue(withIdentifier: "doctorToChooseVC", sender: nil)
                })
            }
            
            }, failureBlock: { [weak self] (error) in
              //  self?.showToast(message: error.localizedDescription)
              self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                
                                         })
                self?.isLoadingFinished()
        })
        
    }
    
}

//MARK: -UIPageViewControllerDelegate
extension SignUpDoctorVC : UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        
        guard completed else { return }
        let vc = pageViewController.viewControllers!.first!
        
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: vc) else {
            return
        }
        crrentStep = viewControllerIndex
        moveToStep(step:viewControllerIndex)
    }
    
    
}

extension SignUpDoctorVC : UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
}



extension SignUpDoctorVC{
    
    //MARK: - update doctor profile API
    func updateDoctorProfile(_ profile: Profile_d2) {
        DataManager.shared.setcurrentEmail(email: profile.email!)
        self.isLoading()
        let request = MeftiiRequests.updateDoctorApi(patientProfile: profile)
        
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            if code == 0 {
                
                let data = resp["data"] as! [String:Any]
                                let patient = data
                                do {
                                    let profile = try Profile_d2.init(from: patient)
                                    //                              profile.isFingerPrintOn = self?.biometricEnabled
                                    //                              profile.addressTwo = self?.address
                                  DataManager.shared.setCity(city: (profile.city?.name!) ?? "")
                                  DataManager.shared.setState(state: (profile.state?.name!) ?? "")
                                    DataManager.shared.saveLoggedInDoctor(profile: profile)

                                }
                                catch {
                                    
                                }
//
//                DataManager.shared.doctorlogin(userName: DataManager.shared.getcurrentEmail(), password: DataManager.shared.getcurrentPass())
                
                //  self?.showToast(message: "profile updated successfully!")
                self?.showAlertWith(title: "Success", message: "Profile updated successfully!", buttonText: "Ok", callBack: {
                    //                    let vc  = LoginVC.instantiateFromStoryboard()
                    
                    
                    
                    self?.navigationController?.popViewController(animated: true)
                })
            }
            
            }, failureBlock: { [weak self] (error) in
               // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                               
                                                        })
                self?.isLoadingFinished()
        })
        
    }
    //
}


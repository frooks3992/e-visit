//
//  MFNavigationController.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 02/11/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

class MFNavigationController: UINavigationController {
    
    private let callVc : CallViewController
    
    override init(rootViewController: UIViewController) {
        callVc = CallViewController.instantiateFromStoryboard()
        super.init(rootViewController: rootViewController)
        self.isNavigationBarHidden = true
        self.isToolbarHidden = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        callVc = CallViewController.instantiateFromStoryboard()
        super.init(coder: aDecoder)
        self.isNavigationBarHidden = true
        self.isToolbarHidden = true
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        callVc = CallViewController.instantiateFromStoryboard()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.isNavigationBarHidden = true
        self.isToolbarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func presentCallController(forIncoming isIncoming:Bool, roomName:String, token:String) {
        
        let role = DataManager.shared.currentRole ?? ""
        
        callVc.roomName = roomName
        callVc.accessToken = token
        
        if role == "doctor", let _ = DataManager.shared.currentDoctor {
    
            
        }
        else if role == "patient", let _ = DataManager.shared.currentPatient {
    
            
        }
        else {
            return
        }
        
        callVc.modalPresentationStyle = .overFullScreen
        callVc.modalTransitionStyle = .coverVertical
        
        self.present(callVc, animated: true) {
            
            if isIncoming {
                self.callVc.reportIncomingCall(uuid: UUID(), roomName: roomName, completion: nil)
            }
            else {
                self.callVc.performStartCallAction(uuid: UUID(), roomName: roomName)
            }
            
        }
        
        
    }
    
    
    

}

//
//  BaseViewController.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 12/08/2019.
//  Copyright © 2019 MEFTII. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    @IBOutlet weak var apiProgressVw: UIProgressView?
    private var showingLoadingSign = false
    
    lazy var loadingVw : UIView = {
        return Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)![0] as! UIView
    }()
    
    lazy var spinnerView: UIView = {
        let blurView = UIView(frame: view.bounds)
        blurView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        var spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        if #available(iOS 13.0, *) {
           spinner = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        }
        spinner.startAnimating()
        spinner.center = view.center
        blurView.addSubview(spinner)
        return blurView
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        
      isLoaderShow(yes: !showingLoadingSign)
        
      self.view.addSubview(spinnerView)
        spinnerView.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    
    func isLoaderShow(yes:Bool){
        
        if !yes {
            
        }
        else {
            
            showLoader()
        }
        
    }
    
    
    func showLoader(){
        self.view.addSubview(spinnerView)
        DispatchQueue.main.asyncAfter(deadline: .now()+1.5) {
                  self.spinnerView.isHidden = true
    }
}

    @IBAction func backButtonPressed(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func isLoading(){
        
         spinnerView.isHidden = false
    }
    
    func isLoadingFinished(){
          
           spinnerView.isHidden = true
      }
    
    func showAlertWith(title:String, message:String, buttonText:String, callBack:@escaping AlertButtonCallback, leftButtonText:String? = nil, leftCallBack: AlertButtonCallback? = nil) {
        let vc = PopUpViewController.instantiateFromStoryboard()
        vc.popTitle = title
        vc.popMessage = message
        vc.leftButtonTitle = leftButtonText
        vc.rightButtonTitle = buttonText
        vc.rightButtonCallback = callBack
        
        vc.leftButtonTitle = leftButtonText
        vc.leftButtonCallback = leftCallBack
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    
    }
    
    func showChoseDateDialog(minDate:Date?, maxDate:Date?, isForTime:Bool = false, callBack:@escaping ((Date?,String?)->Void)) {
        let vc = SelectDateTimeVC.instantiateFromStoryboard()
      
        if let minDate = minDate {
            vc.minDate = minDate
        }
        
        if let maxDate = maxDate {
            vc.maxDate = maxDate
        }
        if isForTime {
            vc.mode = UIDatePicker.Mode.time
        }
        
        vc.closure = callBack
//        vc.datePickerView.date = Date()
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    
    }
    
    func showLoadingView(progress:Progress? = nil) {
        
//        loadingVw.center = view.center
//
//        view.isUserInteractionEnabled = false
//        if let progresVw = apiProgressVw, let progrs = progress {
//            progresVw.observedProgress = progrs
//        }
//
//        if !showingLoadingSign {
//            UIView.transition(with: self.loadingVw, duration: 0.25, options: [.transitionCrossDissolve], animations: {
//                self.view.addSubview(self.loadingVw)
//            }, completion: { (success) in
//                self.showingLoadingSign = true
//            })
//        }
        
        self.view.addSubview(spinnerView)
        self.showingLoadingSign = true
        
    }
    
    func hideLoadingView() {
//        apiProgressVw?.observedProgress = nil
//        view.isUserInteractionEnabled = true
//
//        if showingLoadingSign {
//            UIView.transition(with: self.loadingVw, duration: 0.25, options: [.transitionCrossDissolve], animations: {
//                self.loadingVw.removeFromSuperview()
//            }, completion: {(success) in
//                self.showingLoadingSign = false
//            })
//        }
//        else {
//            showingLoadingSign = false
//            loadingVw.removeFromSuperview()
//        }
        
        showingLoadingSign = false
        spinnerView.removeFromSuperview()
    }
    
    func initiateCall(toDoctor rUserName:String, receiverTkn:String, sendrTkn:String, room:String) {
        
        PushNotificationManager.shared.sendPushNotification(to: rUserName, body: room, isForCall: true, token: receiverTkn)
        
        if let navVc = self.navigationController as? MFNavigationController {
            
            navVc.presentCallController(forIncoming: false, roomName: room, token: sendrTkn)
            
        }
    }
    
}

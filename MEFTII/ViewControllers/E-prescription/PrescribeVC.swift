//
//  PrescribeVC.swift
//  Doctore E visit
//
//  Created by Awais Malik on 08/06/2020.
//  Copyright © 2020 Awais. All rights reserved.
//

import UIKit

class PrescribeVC: UIViewController {

    
    var selectedTextFields = MFTextField()
    var SelectedPickerType: Int!
    
    
    @IBOutlet weak var txtMedicine: MFTextField!
    @IBOutlet weak var txtDose: MFTextField!
     @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stackViewTopCons: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
              
        txtMedicine.addTarget(self, action: #selector(self.textFieldDidTap(_:)), for: .touchDown)
        txtDose.addTarget(self, action: #selector(self.textFieldDidTap(_:)), for: .touchDown)
        setView()
        
        // Do any additional setup after loading the view.
    }
    
    
    func setView(){
         let device = UIDevice.current.name
         print("device \n" + device)
        switch device {
        
        case "iPhone 4", "iPhone 4s":
            stackView.spacing = 6
            stackViewTopCons.constant = 0
        case "iPhone 5", "iPhone 5c", "iPhone 5s":
            stackView.spacing = 10
        default:
            stackViewTopCons.constant = 34
            stackView.spacing = 20
        }
    }
    

    @objc func textFieldDidTap(_ textField: MFTextField) {
        
        switch textField.tag {
        case 0:
            SelectedPickerType  = textField.tag
            self.selectedTextFields = self.txtMedicine
            performSegue(withIdentifier: "TofilterPicker", sender: SelectedPickerType)
        case 1:
            SelectedPickerType  = textField.tag
            self.selectedTextFields = self.txtDose
            performSegue(withIdentifier: "TofilterPicker", sender: SelectedPickerType)
        default:
            print("default")
        }
        
       
        
    }
    
    
    @IBAction func AddMorePressed(_ sender: Any) {
        
        self.performSegue(withIdentifier: "TofilterPicker", sender: nil)
    }
    
    
    @IBAction func backVC(_ sender: MFButton){
           self.navigationController?.popViewController(animated: true)
       }
    @IBAction func nextVC(_ sender: MFButton){
        let vc = TemplateVC.instantiateFromStoryboard("Eprescription")
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func unwindfilterPicker(_ sender: UIStoryboardSegue){
        
        if sender.source is FilterVC{
            
            if let senderVC = sender.source as? FilterVC {
                
                if senderVC.selectedValu != nil{
                    self.selectedTextFields.text = senderVC.selectedValu
                    self.selectedTextFields = MFTextField()
                    print("Symptoms====\n======\n\(senderVC.selectedValu!)")
                    //CityTF.text = senderVC.SelectedSymptom
                }
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationVc = segue.destination as! FilterVC
        destinationVc.Pickertype = SelectedPickerType
        
        let selectedValue = self.selectedTextFields.text ?? ""
        
        switch self.selectedTextFields.tag {
            case 0:
                destinationVc.selectedValu = selectedValue
                destinationVc.titles = "Medicine"
            case 1:
                destinationVc.selectedValu = selectedValue
                destinationVc.titles = "Dose"
            default:
               print("Defualts")
            }
        
        
        
    }

}

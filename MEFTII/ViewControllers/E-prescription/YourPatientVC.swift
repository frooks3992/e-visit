//
//  YourPatientVC.swift
//  Doctore E visit
//
//  Created by Awais Malik on 08/06/2020.
//  Copyright © 2020 Awais. All rights reserved.
//

import UIKit

class YourPatientVC: UIViewController {
    @IBOutlet weak var Tv: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Tv.removeExtraRows()
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func backVC(_ sender: MFButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @IBAction func prescribePressed(_ sender: Any) {
        
        self.performSegue(withIdentifier: "toPrescribe", sender: nil)
    }
    
    

}

extension YourPatientVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "YourPatientCell", for: indexPath) as! YourPatientCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let delete = UIContextualAction(style: .normal, title: "") { (action, sourceView, completionHandler) in
            print("index path of delete: \(indexPath)")
            completionHandler(true)
        }
        delete.image = #imageLiteral(resourceName: "trash-delete")
//        delete.image?.withTintColor(.white)
        delete.backgroundColor = UIColor(red: 222/255, green: 53/255, blue: 53/255, alpha: 1)
        let swipeActionConfig = UISwipeActionsConfiguration(actions: [delete])
        swipeActionConfig.performsFirstActionWithFullSwipe = false
        return swipeActionConfig
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
       let vc = ePrescriptionVC.instantiateFromStoryboard("Eprescription")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

//
//  FilterVC.swift
//  Doctore E visit
//
//  Created by Awais Malik on 08/06/2020.
//  Copyright © 2020 Awais. All rights reserved.
//

import UIKit

class FilterVC: UIViewController {
    
    @IBOutlet var Tv: UITableView!
    
    @IBOutlet var TitleTf: UILabel!
    @IBOutlet var SearchTF: UITextField!
    
    var Pickertype:Int?
    var selectedValu:String?
    var titles: String!
    var filterArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        updateViews()
        Tv.removeExtraRows()
        Tv.tableFooterView = UIView()
        self.filterArray = Constants.filterArray
        
        self.TitleTf.text = titles
        // Do any additional setup after loading the view.
    }
    
    
//    func updateViews(){
//        switch Pickertype {
//        case 2:
//            TitleTf.text! = "Symptoms"
//            ReasonsSyptom_MedicalCond = Constants.symptoms
//        default:
//            TitleTf.text! = "Medical conditions"
//            ReasonsSyptom_MedicalCond = Constants.conditions
//        }
//    }
    
    
    @IBAction func BackPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    
    @IBAction func EditingChanged(_ sender: Any) {
        
        
        let text = SearchTF.text!
        
        if text == "" {
            self.filterArray = Constants.filterArray
            
        }
        else {
            self.filterArray = Constants.filterArray.filter{($0.contains(text))}
            
        }
        
        Tv.reloadData()
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        
    }
    
}

extension FilterVC: UITableViewDelegate,UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.filterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as! FilterCell
        
        cell.title.text =  self.filterArray[indexPath.row]
        if self.filterArray[indexPath.row] == self.selectedValu{
            cell.img.image = UIImage(named: "selected")
        }else{
             cell.img.image = UIImage(named: "")
        }
        
        //cell.Img.image = nil
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedValu = self.filterArray[indexPath.row]

        Tv.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
}

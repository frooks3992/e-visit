//
//  ePrescriptionVC.swift
//  Doctore E visit
//
//  Created by Awais Malik on 08/06/2020.
//  Copyright © 2020 Awais. All rights reserved.
//

import UIKit

class ePrescriptionVC: UIViewController {
    
    
    //IBOUTLET
    
    @IBOutlet weak var txtPrescribeMeds: MFTextField!
    @IBOutlet weak var txtMyTemplate: MFTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func backpressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func setUpUI(){
        txtPrescribeMeds.addTarget(self, action: #selector(self.textFieldDidTap(_:)), for: .touchDown)
        txtMyTemplate.addTarget(self, action: #selector(self.textFieldDidTap(_:)), for: .touchDown)
    }
    
    
    @objc func textFieldDidTap(_ textField: UITextField) {
        
        switch textField.tag {
        case 0:
            let vc = PrescribeVC.instantiateFromStoryboard("Eprescription")
            self.navigationController?.pushViewController(vc, animated: true)
        case 1:
            let vc = MedicineVC.instantiateFromStoryboard("Eprescription")
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            print("default")
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  TemplateVC.swift
//  Doctore E visit
//
//  Created by Awais Malik on 08/06/2020.
//  Copyright © 2020 Awais. All rights reserved.
//

import UIKit

class TemplateVC: UIViewController {

    
    @IBOutlet weak var templateTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

  
    
    @IBAction func backVC(_ sender: MFButton){
        self.navigationController?.popViewController(animated: true)
    }

}


extension TemplateVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 190
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TemplateCell", for: indexPath) as! TemplateCell
        cell.tag = indexPath.row
        cell.cellViewleadingConst.constant = 0
        cell.btnChecked.tag = indexPath.row
        cell.btnChecked.addTarget(self, action: #selector(TemplateVC.checkButtonAction(_:)), for: .touchUpInside)
        cell.btndelete.tag = indexPath.row
         cell.btnEdit.tag = indexPath.row
         cell.btndelete.addTarget(self, action: #selector(TemplateVC.deleteButtonAction(_:)), for: .touchUpInside)
        cell.btnEdit.addTarget(self, action: #selector(TemplateVC.editButtonAction(_:)), for: .touchUpInside)
        
        let swipleft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipleft.direction = .left
        cell.addGestureRecognizer(swipleft)
        
        let swipright = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipright.direction = .right
        cell.addGestureRecognizer(swipright)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.hideShowSideView(isShow: false, row: indexPath.row)
    }
    
    
    @objc func handleGesture(gesture: UISwipeGestureRecognizer) -> Void {
        let tag = gesture.view?.tag ?? 0
      
     
        if gesture.direction == .right {
           self.hideShowSideView(isShow: false, row: tag)
       }
       else if gesture.direction == .left {
            self.hideShowSideView(isShow: true, row: tag)
        }
    }
    
    func hideShowSideView(isShow: Bool, row: Int) {
        let indexPath = IndexPath(row: row, section: 0)
        let cell = self.templateTableView.cellForRow(at: indexPath) as! TemplateCell
        if isShow{
            UIView.animate(withDuration: 0.33, animations: {
                      if cell.cellViewleadingConst.constant != -67{
                           cell.cellViewleadingConst.constant = -67
                        }
                         self.view.layoutIfNeeded()
                     })
        }else{
           UIView.animate(withDuration: 0.33, animations: {
                 if cell.cellViewleadingConst.constant != 0{
                   cell.cellViewleadingConst.constant = 0
                  }
                self.view.layoutIfNeeded()
            })
        }
            
        
        
    }
    
    
    @objc func checkButtonAction(_ sender: UIButton!)
    {
        print("Button is working")
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.templateTableView.cellForRow(at: indexPath) as! TemplateCell
        if cell.btnChecked.imageView?.image == UIImage(named: "unchecked"){
            cell.btnChecked.setImage(UIImage(named: "checked"), for: .normal)
        }else{
            cell.btnChecked.setImage(UIImage(named: "unchecked"), for: .normal)
        }
    }
    
    
    @objc func editButtonAction(_ sender: UIButton!)
       {
        self.hideShowSideView(isShow: false, row: sender.tag)
       }
    
    @objc func deleteButtonAction(_ sender: UIButton!)
       {
            self.hideShowSideView(isShow: false, row: sender.tag)
           
       }
    
}

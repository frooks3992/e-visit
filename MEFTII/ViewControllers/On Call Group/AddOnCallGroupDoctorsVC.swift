//
//  AddOnCallGroupDoctorsVC.swift
//  MEFTII
//
//  Created by Farhan on 05/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import UIKit

class AddOnCallGroupDoctorsVC: BaseViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var searchField: UITextField!
    
    var searchTimer: Timer?
    var doctorlist: [suggestedDoctors] = []
    var docArrayToShow: [suggestedDoctors] = []
    var newAdded:[Int] = []
    var status: [String:Bool] = [:]
    var selectedDocId:Int!
    var selectedDocsArray: [[String:Any]] = []
    var isOldGroup:Bool = false
    var group: Group!
    var groupId:Int!
    var docId:Int!
    @IBOutlet weak var PlaceholderLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchField.delegate = self
        self.searchField.addTarget(self, action: #selector(textFieldDidEditingChanged(_:)), for: .editingChanged)
        fetchAllDoctorsAndReload()
              tableVw.reloadData()
        print(groupId)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
      
    }
    
    
    func validateCells(rows:Int){
        //  status.removeAll()
        
        
    }
    
    @IBAction func backfromOncallGroupDocSchedule(_ sender: UIStoryboardSegue){
        
        if sender.source is AddOnCallDocScheduleVC{
            
            if let senderVC = sender.source as? AddOnCallDocScheduleVC {
                
                if senderVC.selectedDoctors.count>0{
                    
                   // selectedDocsArray = senderVC.selectedDoctors
                    groupId = senderVC.groupId
                   // selectedDocId = sendVC.selectedDocId
                    print(selectedDocsArray)
                    fetchAllDoctorsAndReload()
                          tableVw.reloadData()
                }
                
                
                
            }
            
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVC = segue.destination as? AddOnCallDocScheduleVC{
            
            destinationVC.selectedDoctors = selectedDocsArray
            destinationVC.doctorId = selectedDocId
            destinationVC.groupId = groupId
        }
    }
    
    
    
    
    
    
    
    @IBAction func savePressed(_ sender: Any) {
        
        if !isOldGroup{
            if selectedDocsArray.count > 0 {
                addDoctors(groupName: group.groupName, speciality: group.speciality, startTime: group.startTime, endTime: group.endTime, doctorId: DataManager.shared.currentDoctorId, doctors: selectedDocsArray)
            }
        }
            
        else{
            
            if selectedDocsArray.count > 0 {
             
                addonCallGroupDoctors(groupId: groupId, doctorId: selectedDocId, schedulesDates: selectedDocsArray)
                
                
            }
            
            performSegue(withIdentifier: "fromAddedDocToGetGroups", sender: nil)
           // self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    
    func addDoctors(groupName: String, speciality: String, startTime: String, endTime: String, doctorId: Int, doctors: [[String:Any]]) {
        
        self.isLoading()
        let request = MeftiiRequests.addgroup(groupName: groupName, speciality: speciality, startTime: startTime, endTime: endTime, doctorId: doctorId, doctors: doctors)
        request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            
            print(respTwo)
            self?.isLoadingFinished()
            if codeTwo == 0{
               // self!.showToast(message: "Doctors Added")
                
             self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Ok", callBack: {
                                          //                    let vc  = LoginVC.instantiateFromStoryboard()
//                                          self?.navigationController?.popViewController(animated: true)
                
                self!.performSegue(withIdentifier: "fromAddedDocToGetGroups", sender: nil)
                                      })
            }
            
            }, failureBlock: nil)
    }
    
    
    
    func fetchAllDoctorsAndReload() {
        
         self.isLoading()
        let request = MeftiiRequests.getOnCallSuggestedDocs(userName: DataManager.shared.currentDoctorId)
        
       
        request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            
            
            print(codeTwo)
            print(respTwo)
            
            if codeTwo == 0{
                if let someObj = respTwo["data"] {
                    
                    if let dt = respTwo["data"] as? [String:AnyObject] {
                        if let message = dt["message"] as? String {
                                                                           
                                                                           self!.PlaceholderLbl.text = message
                                                                       }
                                                                       else {
                                                                           
                                                                            self!.PlaceholderLbl.text = "No urgent call requests found"
                                                                       }
                                     
                        
                    }

                    
                    do {
                        let data = try JSONSerialization.data(withJSONObject: respTwo["data"]!!, options:[])
                        let arr = try JSONDecoder().decode([suggestedDoctors].self, from: data)
                        self?.docArrayToShow = arr
                        self?.doctorlist = arr
                        
                        print(self!.docArrayToShow.count)
                        self?.validateCells(rows: self!.docArrayToShow.count)
                    }
                    catch {
                        
                    }
                    
                    if self!.docArrayToShow.count > 0 {
                        self!.tableVw.isHidden = false
                    }
                    else{
                        self!.tableVw.isHidden = true
                    }
                    
                    self?.tableVw.reloadData()
                }
            }
            
            }, failureBlock: nil)
    }
    
    
    
    func addOnCallDoctor(_ onCallDocId:String) {
        let regst = MeftiiRequests.addOnCallDoctor(for: String(DataManager.shared.currentDoctorId), onCallDocId: onCallDocId)
        regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            //self?.showToast(message: descTwo)
            
            self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Ok", callBack: {
                                         //                    let vc  = LoginVC.instantiateFromStoryboard()
                                         self?.navigationController?.popViewController(animated: true)
                                     })
            
            
            
            }, failureBlock: nil)
    }
    
    
    @objc func addDoctorButtonPressed(_ sender: UIButton) {
        let idx = sender.tag
        let cell = tableVw.cellForRow(at: IndexPath(item: sender.tag, section: 0)) as! MFTableViewCell
        
        let docToAdd = docArrayToShow[idx]
        
        let id = docToAdd.doctorId!
        
        selectedDocId = id
        
        let urlll = URL(string: docToAdd.profilePicture ?? "")
                 cell.imgProfile?.sd_setImage(with: urlll, placeholderImage: nil)
        
        
        if newAdded.contains(id){
            
            cell.btnCall?.backgroundColor = UIColor.MFBlue
            cell.btnCall?.setTitle("Add", for: .normal)
            newAdded = newAdded.filter{($0 != id)}
            
        }
            
        else{
            cell.btnCall?.backgroundColor = UIColor.MFDarkRed
            cell.btnCall?.setTitle("Added", for: .normal)
            newAdded.append(id)
            
        }
        
        
        
        print(newAdded)
        
        performSegue(withIdentifier: "OnCallDoctorGroupSchedule", sender: nil)
    }
    

    
}



extension AddOnCallGroupDoctorsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docArrayToShow.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFTableViewCell else {
            return UITableViewCell()
        }
        
        let doctor = docArrayToShow[indexPath.row]
        
        let urlll = URL(string: doctor.profilePicture ?? "")
        cell.imgProfile?.sd_setImage(with: urlll, completed: nil)
        
        cell.lblName.text = "Dr. " + doctor.firstName!
        cell.lblValue?.text = "Specialty: " + (doctor.speciality ?? "")
        cell.btnCall?.tag = indexPath.row
        cell.btnCall?.addTarget(self, action: #selector(addDoctorButtonPressed(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}





extension AddOnCallGroupDoctorsVC : UITextFieldDelegate {
    
    @objc func textFieldDidEditingChanged(_ textField: UITextField) {
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }
        searchTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text ?? "", repeats: false)
    }
    
    @objc func searchForKeyword(_ timer: Timer) {
        let keyword = ((timer.userInfo as? String) ?? "").lowercased()
        
        if keyword.count > 0 {
            
            docArrayToShow = doctorlist.filter{($0.firstName!.lowercased()).contains(keyword)}
        }
        else {
            docArrayToShow = doctorlist
            validateCells(rows: docArrayToShow.count)
        }
        
        tableVw.reloadData()
        
    }
    
}

extension AddOnCallGroupDoctorsVC {
    
    func addonCallGroupDoctors(groupId: Int, doctorId: Int, schedulesDates: [[String:Any]]) {
              let request = MeftiiRequests.addonCallGroupDoctors(groupId: groupId, doctorId: doctorId, schedulesDates: schedulesDates)
        self.isLoading()
               request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
                   
                self?.isLoadingFinished()
                   print(codeTwo)
                   print(respTwo)
                   
                   if codeTwo == 0{
                   
                       // self!.showToast(message: "Doctor added Successfully")
                    
                    
                    self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Ok", callBack: {
                                                           //                    let vc  = LoginVC.instantiateFromStoryboard()
//                                                           self?.navigationController?.popViewController(animated: true)
                        self!.performSegue(withIdentifier: "fromAddedDocToGetGroups", sender: nil)
                                                       })
                    
                     
                   }
                   
                   }, failureBlock: nil)
           }
          
           
       
}

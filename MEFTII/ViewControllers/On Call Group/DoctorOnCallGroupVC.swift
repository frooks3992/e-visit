//
//  DoctorOnCallGroupVC.swift
//  MEFTII
//
//  Created by Farhan on 05/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//


import Foundation
import UIKit

class DoctorOnCallGroupVC: BaseViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var AddButton: UIButton!
    @IBOutlet weak var btnEdit: MFButton!
    
    var searchTimer: Timer?
    var doctorlist: [Group] = []
    var docArrayToShow: [Group] = []
    var doctorId:Int!
    var groupId:Int!
    
    @IBOutlet weak var editBtnWidth: NSLayoutConstraint!
    var groupIdToDelete:Int = 0
    var selectedGroup:Group!
    var isEdit:Bool = false
    override func viewDidLoad() {
        
        if let docId = DataManager.shared.currentDoctorId as? Int {
            
            if docId == 0 {
                doctorId = 5
            }
            else{
                
                doctorId = docId
            }
            
        }
        
        super.viewDidLoad()
        tableVw.tableFooterView = UIView()
        self.searchField.delegate = self
        self.searchField.addTarget(self, action: #selector(textFieldDidEditingChanged(_:)), for: .editingChanged)
        btnEdit.setTitle("EDIT", rightImage: nil, leftImage: UIImage(named: "edit"), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        isEdit = false
        docArrayToShow.removeAll()
        
        doctorlist.removeAll()
        fetchOnCallDoctorsAndReload()
        
        tableVw.reloadData()
    }
    
    
    func fetchOnCallDoctorsAndReload() {
        
        self.isLoading()
        let request = MeftiiRequests.getoncallgroups(doctorId: doctorId)
        request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            
            
            print(codeTwo)
            print(respTwo)
            self?.isLoadingFinished()
            if codeTwo == 0{
                
                
                if let someObj = respTwo["data"] {
                    
                    do {
                        if let data = someObj as? [String:Any] {
                            
                            let group = Group.init(json: data)
                            print(group.groupId)
                            self?.docArrayToShow.append(group)
                            self?.doctorlist = self!.docArrayToShow
                            
                            print(self!.docArrayToShow.count)
                            
                            if ((self?.docArrayToShow.count)! > 0) {
                                
                                self!.editBtnWidth.constant = 70
                                
                                UIView.animate(withDuration: 0.5) {
                                    self?.view.layoutIfNeeded()
                                }
                            }
                                
                            else{
                                self!.editBtnWidth.constant = 0
                                
                                UIView.animate(withDuration: 0.0) {
                                    self?.view.layoutIfNeeded()
                                }
                                
                            }
                        }
                        
                        
                    }
                    catch {
                        
                    }
                    
                    
                    
                    self?.tableVw.reloadData()
                }
            }
            
            }, failureBlock: nil)
    }
    
    @IBAction func fromAddedDocToGetGroups(segue:UIStoryboardSegue){}
    
    
    func deleteOnCallDoctor(_ groupId:Int) {
        self.isLoading()
        let regst = MeftiiRequests.deleteOnCallGroup(groupId: groupId)
        regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            //self?.showToast(message: descTwo)
            self?.isLoadingFinished()
            self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Okay", callBack: {
                //                    let vc  = LoginVC.instantiateFromStoryboard()
                //  self?.navigationController?.popViewController(animated: true)
            })
            
            }, failureBlock: nil)
    }
    
    
    @IBAction func addDoctorButtonPressed(_ sender: UIButton) {
        
        let vc = AddOnCallDoctorsVC.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func editPressed(_ sender: Any) {
        
        isEdit  = !isEdit
        
        tableVw.reloadData()
        
    }
    
    
    @IBAction func editOrDeletePressed(_ sender: UIButton) {
        
        
        if sender.tag  < 100 {
            let group = self.docArrayToShow[sender.tag]
            self.selectedGroup = group
            
            isEdit = false
            tableVw.reloadData()
            self.performSegue(withIdentifier: "toOnCallUpdateGroup", sender: nil)
        }
            
        else {
            
            let docToRemove = self.docArrayToShow[sender.tag-100]
            if let idx = self.docArrayToShow.firstIndex(of: docToRemove) {
                self.docArrayToShow.remove(at: idx)
            }
            if let idx = self.doctorlist.firstIndex(of: docToRemove) {
                self.doctorlist.remove(at: idx)
            }
            tableVw.beginUpdates()
            tableVw.deleteRows(at: [IndexPath(item: sender.tag-100, section: 0)], with: .automatic)
            tableVw.endUpdates()
            
            if docArrayToShow.count == 0 {
                self.editBtnWidth.constant = 0
                
                UIView.animate(withDuration: 0.0) {
                    self.view.layoutIfNeeded()
                }
            }
            self.deleteOnCallDoctor(docToRemove.groupId)
            
        }
        
        isEdit = false
        
        tableVw.reloadData()
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if let destinationVc = segue.destination as? OnCallGroupDoctorsVC{
            
            destinationVc.groupId = groupId
            
        }
        
        if let destinationVc2 = segue.destination as? OnCallUpdateGroup {
            
            destinationVc2.selectedGroup = selectedGroup
        }
        
    }
    
}

extension DoctorOnCallGroupVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docArrayToShow.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFOnCallGroupCell else {
            return UITableViewCell()
        }
        
        let doctor = docArrayToShow[indexPath.row]
        
        // let urlll = URL(string: Constants.getProfileImageUrl(userName: (doctor.groupName ?? "")))
        // cell.imgProfile?.sd_setImage(with: urlll, completed: nil)
        
        cell.title?.text = doctor.groupName
        cell.des?.text = doctor.speciality
        cell.timing?.text = doctor.timing
        cell.editBTn.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row + 100
        
        if isEdit {
            cell.leftConstraint.constant = 0
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
            
        }
        else {
            
            cell.leftConstraint.constant = -67
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        //        var deleteAction = UITableViewRowAction(style: .default, title: "Delete") {action,s  in
        //
        //            let docToRemove = self.docArrayToShow[indexPath.row]
        //            if let idx = self.docArrayToShow.firstIndex(of: docToRemove) {
        //                self.docArrayToShow.remove(at: idx)
        //                    }
        //            if let idx = self.doctorlist.firstIndex(of: docToRemove) {
        //                self.doctorlist.remove(at: idx)
        //                    }
        //                    tableView.beginUpdates()
        //                    tableView.deleteRows(at: [indexPath], with: .automatic)
        //                    tableView.endUpdates()
        //
        //
        //            self.deleteOnCallDoctor(docToRemove.groupId)
        //
        //        }
        //
        //        var editAction = UITableViewRowAction(style: .normal, title: "Edit") {action,s  in
        //
        //            let group = self.docArrayToShow[indexPath.row]
        //            self.selectedGroup = group
        //            self.performSegue(withIdentifier: "toOnCallUpdateGroup", sender: nil)
        //        }
        //
        return []
    }
    
    //    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    //        if editingStyle == .delete {
    //
    //
    //
    //            let docToRemove = docArrayToShow[indexPath.row]
    //            if let idx = docArrayToShow.firstIndex(of: docToRemove) {
    //                docArrayToShow.remove(at: idx)
    //            }
    //            if let idx = doctorlist.firstIndex(of: docToRemove) {
    //                doctorlist.remove(at: idx)
    //            }
    //            tableView.beginUpdates()
    //            tableView.deleteRows(at: [indexPath], with: .automatic)
    //            tableView.endUpdates()
    //
    //
    //            deleteOnCallDoctor(docToRemove.groupId)
    //
    //        } else if editingStyle == .insert {
    //            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    //
    //
    //            print("Hello")
    //        }
    //
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = ViewDoctorVC.instantiateFromStoryboard()
        // vc.doctorProfile = docArrayToShow[indexPath.row]
        // vc.isOnCallDoc = true
        let doctor = docArrayToShow[indexPath.row]
        groupId = doctor.groupId
        selectedGroup = doctor
        
        
        if !isEdit {
            performSegue(withIdentifier: "toOnCallGroupdocs", sender: nil)
        }
            
        else{
            
            //  performSegue(withIdentifier: "toOnCallUpdateGroup", sender: nil)
        }
        //        navigationController?.pushViewController(vc, animated: true)
    }
}





extension DoctorOnCallGroupVC : UITextFieldDelegate {
    
    @objc func textFieldDidEditingChanged(_ textField: UITextField) {
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }
        searchTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text ?? "", repeats: false)
    }
    
    @objc func searchForKeyword(_ timer: Timer) {
        let keyword = ((timer.userInfo as? String) ?? "").lowercased()
        if keyword.count > 0 {
            docArrayToShow = doctorlist.filter({ ($0.groupName.lowercased()).contains(keyword) }) ?? []
        }
        else {
            docArrayToShow = doctorlist
        }
        tableVw.reloadData()
    }
    
}


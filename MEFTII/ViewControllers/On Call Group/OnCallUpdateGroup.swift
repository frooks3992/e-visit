//
//  OnCallUpdateGroup.swift
//  MEFTII
//
//  Created by Farhan on 06/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import UIKit

class OnCallUpdateGroup: BaseViewController {
    
    @IBOutlet weak var groupNameTf: CustomTextField!
    
    @IBOutlet weak var specialityTf: CustomTextField!
    

    @IBOutlet weak var startTimeTF: CustomTextField!
    
    @IBOutlet weak var endTimeTF: CustomTextField!
    var groupId:Int!
    var selectedGroup:Group!
    var group:Group!
    var isEdited:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        group = selectedGroup
        groupNameTf.text = selectedGroup.groupName
        specialityTf.text = "Speciality: " + selectedGroup.speciality
        startTimeTF.text = selectedGroup.startTime
        endTimeTF.text = selectedGroup.endTime
        groupId = selectedGroup.groupId
        // Do any additional setup after loading the view.
    }
    

    @IBAction func BackPressed(_ sender: UIButton) {
            
          self.navigationController?.popViewController(animated: true)
        }
        
      @IBAction func showPicker(_ sender: UIButton) {
            
            showDateDialog(type: sender.tag)
        }
     
    
    
    
    @IBAction func editingStarted(_ sender: Any) {
        
          isEdited  = true
           
          
    }
    
    
    
    
    @IBAction func updateBtnPressed(_ sender: Any) {
        
        if isEdited  {
            
            let group = Group.init(groupId: groupId, groupName: groupNameTf.text!, speciality: specialityTf.text!, startTime: startTimeTF.text!, endTime: endTimeTF.text!, doctorId: 0, timing: "")
            
            updateGroup(group: group)
            
        }
    }
    
 
}

extension OnCallUpdateGroup{
    func showDateDialog(type:Int){
          
          let myDatePicker: UIDatePicker = UIDatePicker()
          //  myDatePicker.timeZone = NSTimeZone.local
          myDatePicker.datePickerMode = .time
          
          
          let date = Date(timeIntervalSince1970: -2.113e+9)
          myDatePicker.minimumDate = date
          
          myDatePicker.maximumDate = Date()
          
          myDatePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 180)
          let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
          alertController.view.addSubview(myDatePicker)
          let somethingAction = UIAlertAction(title: "Done", style: .destructive) { (UIAlertAction) in
            self.isEdited  = true
              // self.oldDate = myDatePicker.date
              let timeFormatter = DateFormatter()
              timeFormatter.dateFormat = "h:mm a" // set this date format string
              let time = timeFormatter.string(from: myDatePicker.date)
            
            let d2 = myDatePicker.date.addingTimeInterval(43200)
            let time2 = timeFormatter.string(from: d2)
              
              switch type {
              case 1:
                  
                 
                  self.startTimeTF.text =  time
                   self.endTimeTF.text =  time2
                
                
              default:
                 // self.endTimeTF.text =  time
                print()
              }
              
              
          }
          let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
          alertController.addAction(cancelAction)
          alertController.addAction(somethingAction)
          
          present(alertController, animated: true, completion:{})
          
      }
}

extension OnCallUpdateGroup{
    
    func updateGroup(group:Group) {
        let request = MeftiiRequests.updategroup(groupName: group.groupName, speciality: group.speciality, startTime: group.startTime, endTime: group.endTime, groupId: group.groupId)
        self.isLoading()
         request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
             
            self?.isLoadingFinished()
             print(codeTwo)
             print(respTwo)
             
             if codeTwo == 0{
               
            //  self!.showToast(message: "Group Updated")
                
               
                
                
                self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Okay", callBack: {
                                             //                    let vc  = LoginVC.instantiateFromStoryboard()
                                             self?.navigationController?.popViewController(animated: true)
                                         })
               
             }
             
             }, failureBlock: nil)
     }
     
}

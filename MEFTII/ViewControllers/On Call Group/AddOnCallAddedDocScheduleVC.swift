//
//  AddOnCallDocScheduleVC.swift
//  MEFTII
//
//  Created by Farhan on 05/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//


import UIKit
import FSCalendar
class AddOnCallAddedDocScheduleVC: BaseViewController {
    
    
    @IBOutlet weak var nextBtn: MFButton!
    @IBOutlet weak var calendarView: FSCalendar!
    var startTime:String!
    var endTime:String!
    var dateSelected:String!
    var datesSelected:[String] = []
    var doctorId:Int!
    var dates : [DaysColorsList] = []
    var selectedDoctors: [String:Any] = [:]
    var groupId:Int!
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd/MM/YYYY"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendarView.collectionView.register(UINib(nibName: "CalenderCell", bundle: nil), forCellWithReuseIdentifier: "CalenderCell")
        
        //                DispatchQueue.main.asyncAfter(deadline: .now()+3) {
        //        }
        //            self.endTime = (self.dates.first)!
        //            self.startTime = (self.dates.last)!
        
        self.startTime = "14/04/2020"
        self.endTime = "16/04/2020"
        
        self.GetdDatesColor(groupId: 1, doctorId: 1, startTime: startTime, endTime: endTime)
        
//        nextBtn.isHidden = true
        calendarView.appearance.headerTitleFont = .AppMediumFont(ofSize: 18)
        calendarView.appearance.weekdayFont = .AppMediumFont(ofSize: 14)
        calendarView.appearance.headerDateFormat = "MMMM YYYY"
        calendarView.collectionView.isMultipleTouchEnabled = true
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        
        
        if nextBtn.titleLabel?.text == "SKIP"{
            self.navigationController?.popViewController(animated: true)
            
        }
        
        //        let vc = SelectTimeVC.instantiateFromStoryboard("Schedule")
        //        self.navigationController?.pushViewController(vc, animated: true)
        var scheduledDates = [[String:Any]]()
        if datesSelected.count > 0 {
            
            datesSelected.forEach { (date) in
                
                let schedulesDate = ["date":date]
                
                scheduledDates.append(schedulesDate)
                
                
            }
            
            let doctorData = ["groupId":groupId!,"doctorId":doctorId!,"startDate":startTime!,"endDate":endTime!,"schedule":scheduledDates] as [String : Any]
            
            selectedDoctors = doctorData
            updategroupdocschedule(params: doctorData)
            
        }
        
        performSegue(withIdentifier: "backfromOncallGroupDocSchedule", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destinationVc = segue.destination as! AddOnCallGroupDoctorsVC
        
        // destinationVc.selectedDocsArray = selectedDoctors
        // destinationVc.end = endTime
        destinationVc.selectedDocId = doctorId
        destinationVc.groupId = groupId
        
    }
    
    
    //MARK:- Calendar next and previous actions
    @IBAction func calendarPreviousTapped(_ sender: Any) {
        dates.removeAll()
        let prevMonth = Calendar.current.date(byAdding: .month, value: -1, to: calendarView.currentPage)
        calendarView.setCurrentPage(prevMonth!, animated: true)
    }
    
    @IBAction func calendarNextTapped(_ sender: Any) {
        dates.removeAll()
        let nextMonth = Calendar.current.date(byAdding: .month, value: 1, to: calendarView.currentPage)
        calendarView.setCurrentPage(nextMonth!, animated: true)
    }
    
}

extension AddOnCallAddedDocScheduleVC: FSCalendarDelegate, FSCalendarDataSource {
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        return isDateEnabled(date) ? .black : .lightGray
    }
    
    
    
    private func isDateEnabled(_ date: Date, at monthPosition: FSCalendarMonthPosition = .current) -> Bool {
        
        //        if dates.count > 0 {
        //
        //            let date = self.dateFormatter.string(from: date)
        //
        //            for currentDate in dates {
        //
        //                if currentDate.date == date {
        //
        //                    return true
        //                }
        //                else{
        //
        //                    return false
        //                }
        //            }
        //
        //
        //        }
        //
        //        else{
        //
        //            return false
        //        }
        
        return false
    }
    
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        
        nextBtn.setTitle("SAVE", for: .normal)
        //         endTime = (dates.first)!
        //         startTime = (dates.last)!
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        // again convert your date to string
        let myStringafd = formatter.string(from: date)
        
        dateSelected = myStringafd
        print("selcted")
        
        if datesSelected.contains(dateSelected)
        {
            
        }
        else{
            datesSelected.append(dateSelected)
        }
        
        print(datesSelected)
        
        nextBtn.isHidden = false
    }
    
    func calendar(_ calendar: FSCalendar, didDeselect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        // again convert your date to string
        let myStringafd = formatter.string(from: date)
        
        dateSelected = myStringafd
        
        
        if datesSelected.contains(dateSelected)
        {
            datesSelected = datesSelected.filter{($0 != dateSelected)}
        }
        else{
            datesSelected.append(dateSelected)
        }
        print(datesSelected)
    }
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    
    func calendar(_ calendar: FSCalendar, willDisplay cell: FSCalendarCell, for date: Date, at monthPosition: FSCalendarMonthPosition) {
        cell.titleLabel.font = UIFont(name: "Montserrat-Medium", size: 15.0)
        
        
        for currentDate in dates {
            print(currentDate.date)
            print(self.dateFormatter.string(from: date))
            if currentDate.date == self.dateFormatter.string(from: date){
                
                print(currentDate.color.cgColor)
                cell.shapeLayer.fillColor = currentDate.color.cgColor
                cell.titleLabel.textColor = UIColor.white
                
            }
            
            
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/YYYY"
        // again convert your date to string
        let myStringafd = formatter.string(from: date)
        
        // dates.append(myStringafd)
        // print(monthPosition.rawValue)
    }
    
    
    
}



extension AddOnCallAddedDocScheduleVC{
    //MARK: - GetstatesList API
    func GetdDatesColor(groupId: Int,doctorId:Int,startTime:String,endTime:String) {
        
        let request = MeftiiRequests.getgroupdoctorschedule(groupId: groupId, doctorId: doctorId, startDate: startTime, endDate: endTime)
        
        self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            if code == 0 {
                
                let data = resp["data"] as! [[String:Any]]
                //                let datess = data["scheduleDaysColorsList"] as! [[String:Any]]
                //
                //
                //                for dt in datess {
                //
                //                    self?.dates.append(DaysColorsList.init(json: dt))
                //                }
                
                self!.calendarView.collectionView.reloadData()
                self?.isLoadingFinished()
            }
            
            }, failureBlock: { [weak self] (error) in
                // self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                    
                })
                // self?.isLoadingFinished()
        })
        
    }
}
extension AddOnCallAddedDocScheduleVC{
    //MARK: - GetstatesList API
    func GetSlotst() {
        
        let request = MeftiiRequests.getscheduledatescolor(doctorId: doctorId, startDate: "14/04/2020", endDate: "16/04/2020")
        
        //  let request = MeftiiRequests.getavailableappointmentdates(doctorId: doctorId, startDate: startTime, endDate: endTime)
        
        //   self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            if code == 0 {
                
                let data = resp["data"] as! [String:Any]
                let dates = data["scheduleDaysColorsList"] as! [String]
                
                print(dates)
                
            }
            
            }, failureBlock: { [weak self] (error) in
                // self?.showToast(message: error.localizedDescription)
                // self?.isLoadingFinished()
                
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                    
                })
        })
        
    }
    
    func updategroupdocschedule(params:[String:Any]) {
        
        let request = MeftiiRequests.updategroupdocschedule(params:params)
        
        //  let request = MeftiiRequests.getavailableappointmentdates(doctorId: doctorId, startDate: startTime, endDate: endTime)
        
        //   self.isLoading()
        request.start(vc: nil, successBlock: { [weak self] (code, desc,resp)  in
            
            self?.isLoadingFinished()
            
            if code == 0 {
                
//                self?.showToast(message: "Schedule updated successfully")
                
                self?.showAlertWith(title: "Success", message: desc, buttonText: "Ok", callBack: {
                    self?.navigationController?.popViewController(animated: true)
                })
                
                
            }
            
            }, failureBlock: { [weak self] (error) in
                //self?.showToast(message: error.localizedDescription)
                self?.showAlertWith(title: "Failure", message: error.localizedDescription, buttonText: "Ok", callBack: {
                    self?.navigationController?.popViewController(animated: true)
                })
                // self?.isLoadingFinished()
        })
        
    }
}


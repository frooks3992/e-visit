//
//  OnCallGroupDoctorsVC.swift
//  MEFTII
//
//  Created by Farhan on 05/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import Foundation
import UIKit

class OnCallGroupDoctorsVC: BaseViewController {
    
    @IBOutlet weak var tableVw: UITableView!
    @IBOutlet weak var searchField: UITextField!
    
    var searchTimer: Timer?
    var doctorlist: [GroupDoc] = []
    var docArrayToShow: [GroupDoc] = []
    var newAdded:[Int] = []
    var status: [String:Bool] = [:]
    
    @IBOutlet weak var PlaceholderLbl: UILabel!
    var groupId:Int!
    var docId:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.searchField.delegate = self
        self.searchField.addTarget(self, action: #selector(textFieldDidEditingChanged(_:)), for: .editingChanged)
        
        //  fetchAllDoctorsAndReload()
        //tableVw.reloadData()
        
    }
    
    
    func validateCells(rows:Int){
        //  status.removeAll()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // if tableVw.numberOfRows(inSection: 0) > 0{
        fetchAllDoctorsAndReload()
        tableVw.reloadData()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        print()
    }
    
    @IBAction func addDocPressed(_ sender: Any) {
        performSegue(withIdentifier: "toAddDocsTogroup", sender: nil)
    }
    
    
    
    @IBAction func savePressed(_ sender: Any) {
        
        if newAdded.count > 0 {
            addDoctors()
        }
        
        
        
        
        
        // addonCallGroupDoctors
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVc = segue.destination as? AddOnCallGroupDoctorsVC {
            
            destinationVc.isOldGroup = true
            destinationVc.groupId = groupId
            //  destinationVc.docId = docId
        }
        
        if let destinationVc = segue.destination as? AddOnCallAddedDocScheduleVC {
            
            
            destinationVc.groupId = groupId
            destinationVc.doctorId = docId
        }
    }
    
    
    
    func addDoctors() {
        
        self.isLoading()
        let request = MeftiiRequests.addDoctors(userName: DataManager.shared.currentPatientId, doctorIdsList: newAdded)
        request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            self?.isLoadingFinished()
            print(respTwo)
            
            if codeTwo == 0{
                // self!.showToast(message: "Doctors Added Succesfully")
                
                
                
                self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Ok", callBack: {
                    //                    let vc  = LoginVC.instantiateFromStoryboard()
                    self?.navigationController?.popViewController(animated: true)
                })
            }
            
            }, failureBlock: nil)
    }
    
    
    
    func fetchAllDoctorsAndReload() {
        self.isLoading()
        let request = MeftiiRequests.onCallGroupDoctors(groupId: groupId)
        request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            
            self?.isLoadingFinished()
            print(codeTwo)
            print(respTwo)
            
            if codeTwo == 0{
                if let someObj = respTwo["data"] {
                    
                    
                    if let dt = respTwo["data"] as? [String:Any] {
                        if let message = dt["message"] as? String {
                            
                            self!.PlaceholderLbl.text = message
                        }
                        else {
                            
                            self!.PlaceholderLbl.text = "No group doctors found"
                        }
                        
                        
                    }
                        
                    else{
                        self!.PlaceholderLbl.text = "No group doctors found"
                    }
                    
                    
                    
                    
                    
                    
                    
                    
                    var docArray = [GroupDoc]()
                    do {
                        let data = someObj as! [[String:Any]]
                        
                        
                        data.forEach { (groupDoc) in
                            
                            docArray.append(GroupDoc.init(json: groupDoc))
                        }
                        self?.docArrayToShow.removeAll()
                        self?.doctorlist.removeAll()
                        
                        self?.docArrayToShow = docArray
                        self?.doctorlist = docArray
                        
                        print(self!.docArrayToShow.count)
                        self?.validateCells(rows: self!.docArrayToShow.count)
                    }
                    
                    if self!.docArrayToShow.count > 0 {
                        
                        self!.tableVw.isHidden = false
                    }
                    else{
                        self!.tableVw.isHidden = true
                    }
                    
                    self?.tableVw.reloadData()
                }
            }
            
            }, failureBlock: nil)
    }
    
    
    @IBAction func unwindFromAddOnCallGroupDocs(_ sender: UIStoryboardSegue){
        
        if let vc = sender.destination as? AddOnCallGroupDoctorsVC{
            
            groupId = vc.groupId
            fetchAllDoctorsAndReload()
            tableVw.reloadData()
        }
        
    }
    
    
    func addOnCallDoctor(_ groupId:Int) {
        let regst = MeftiiRequests.onCallGroupDoctors(groupId: groupId)
        regst.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            //   self?.showToast(message: descTwo)
            
            
            self?.showAlertWith(title: "Success", message: descTwo, buttonText: "OK", callBack: {
                //                    let vc  = LoginVC.instantiateFromStoryboard()
                // self?.navigationController?.popViewController(animated: true)
            })
            
            
            
            
            }, failureBlock: nil)
    }
    
    
    @objc func addDoctorButtonPressed(_ sender: UIButton) {
        let idx = sender.tag
        let cell = tableVw.cellForRow(at: IndexPath(item: sender.tag, section: 0)) as! MFTableViewCell
        
        let docToAdd = docArrayToShow[idx]
        let urlll = URL(string: docToAdd.profilePicture ?? "")
        cell.imgProfile?.sd_setImage(with: urlll, placeholderImage: nil)
        let id = docToAdd.doctorId
        docId = id
        
        if newAdded.contains(id){
            
            cell.btnCall?.backgroundColor = UIColor.MFBlue
            cell.btnCall?.setTitle("Add", for: .normal)
            newAdded = newAdded.filter{($0 != id)}
            
        }
            
        else{
            cell.btnCall?.backgroundColor = UIColor.MFDarkRed
            cell.btnCall?.setTitle("Added", for: .normal)
            newAdded.append(id)
            
        }
        
        //addOnCallDoctor(String(docToAdd.id ?? 0))
        
        print(newAdded)
        
        performSegue(withIdentifier: "toAddedDocGroupSchedule", sender: nil)
        
    }
    
}

extension OnCallGroupDoctorsVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return docArrayToShow.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as? MFTableViewCell else {
            return UITableViewCell()
        }
        
        let doctor = docArrayToShow[indexPath.row]
        
        let urlll = URL(string: doctor.profilePicture)
        cell.imgProfile?.sd_setImage(with: urlll, completed: nil)
        
        cell.lblName.text = "Dr. " + doctor.firstName
        cell.lblValue?.text = "Specialty: " + doctor.speciality
        cell.btnCall?.tag = indexPath.row
        
        //        if newAdded.count > 0 {
        //
        //            if newAdded.contains(doctor.id!){
        //
        //                cell.btnCall?.backgroundColor = UIColor.MFDarkRed
        //                cell.btnCall?.setTitle("Added", for: .normal)
        //            }
        //
        ////            else{
        ////                cell.btnCall?.backgroundColor = UIColor.MFBlue
        ////                cell.btnCall?.setTitle("Add", for: .normal)
        ////            }
        //
        //        }
        
        
        cell.btnCall?.addTarget(self, action: #selector(addDoctorButtonPressed(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    //    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    //        var deleteAction = UITableViewRowAction(style: .default, title: "Delete") {action,s  in
    //
    //
    //            let doc = self.docArrayToShow[indexPath.row]
    //
    //            self.DeleteDoctor(groupId: self.groupId , doctorId: doc.doctorId,Index:indexPath.row)
    //
    //
    //
    //        }
    //
    //        return [deleteAction]
    //    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
        -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                // delete the item here
                
                
                self.showAlertWith(title: "Confirmation required", message: "Are you sure you want to delete Group Dr." + "\(self.docArrayToShow[indexPath.row].firstName)" + "\(self.docArrayToShow[indexPath.row].lastName)", buttonText: "YES", callBack: {
                    let docToRemove = self.docArrayToShow[indexPath.row]
                    if let idx = self.docArrayToShow.firstIndex(of: docToRemove) {
                        self.docArrayToShow.remove(at: idx)
                    }
                    if let idx = self.doctorlist.firstIndex(of: docToRemove) {
                        
                        
                        
                        //  doctorlist.remove(at: idx)
                    }
                    tableView.beginUpdates()
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                    tableView.endUpdates()
                    
                    
                    self.DeleteDoctor(groupId: self.groupId , doctorId: docToRemove.doctorId,Index:indexPath.row)
                    
                }, leftButtonText: "NO") {
                    print("cancel")
                    completionHandler(true)
                }
                
            }
            deleteAction.image = #imageLiteral(resourceName: "trash-delete")
            deleteAction.backgroundColor = UIColor(red: 222/255, green: 53/255, blue: 53/255, alpha: 1)
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            return configuration
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}






extension OnCallGroupDoctorsVC : UITextFieldDelegate {
    
    @objc func textFieldDidEditingChanged(_ textField: UITextField) {
        if searchTimer != nil {
            searchTimer?.invalidate()
            searchTimer = nil
        }
        searchTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(searchForKeyword(_:)), userInfo: textField.text ?? "", repeats: false)
    }
    
    @objc func searchForKeyword(_ timer: Timer) {
        let keyword = ((timer.userInfo as? String) ?? "").lowercased()
        
        if keyword.count > 0 {
            
            docArrayToShow = doctorlist.filter{($0.firstName.lowercased()).contains(keyword)}
        }
        else {
            docArrayToShow = doctorlist
            validateCells(rows: docArrayToShow.count)
        }
        
        tableVw.reloadData()
        
    }
    
}


extension OnCallGroupDoctorsVC{
    
    func DeleteDoctor(groupId:Int,doctorId:Int,Index:Int) {
        let request = MeftiiRequests.deletegroupDocs(groupId: groupId,doctorId: doctorId)
        self.isLoading()
        request.start(vc: self, successBlock: { [weak self] (codeTwo, descTwo, respTwo) in
            
            
            print(codeTwo)
            print(respTwo)
            self?.isLoadingFinished()
            if codeTwo == 0{
                
                //   self!.showToast(message: "Doctor deleted Successfully")
                
                //                if Index != 0 {
                //                self!.docArrayToShow.remove(at: Index)
                //
                //                }
                
                
                self?.showAlertWith(title: "Success", message: descTwo, buttonText: "Ok", callBack: {
                    
                })
                
                self?.tableVw.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    self?.isLoadingFinished()
                    self?.navigationController?.popViewController(animated: true)}
                
            }
            
            }, failureBlock: nil)
    }
    
    
    
}

//
//  OnCallAddNewGroup.swift
//  MEFTII
//
//  Created by Farhan on 05/05/2020.
//  Copyright © 2020 MEFTII. All rights reserved.
//

import UIKit

class OnCallAddNewGroup: BaseViewController {
    @IBOutlet weak var groupNameTF: CustomTextField!
    @IBOutlet weak var specialityTF: CustomTextField!
    
    @IBOutlet weak var startTimeTF: CustomTextField!
    @IBOutlet weak var endTimeTF: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func showPicker(_ sender: UIButton) {
        
        showDateDialog(type: sender.tag)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destinationVc = segue.destination as? AddOnCallGroupDoctorsVC {
            
            destinationVc.group = Group.init(groupId: 0, groupName: groupNameTF.text!, speciality: specialityTF.text!, startTime: startTimeTF.text!, endTime: endTimeTF.text!, doctorId: 5, timing: "")
            
        }
    }
    
    
      @IBAction func BackPressed(_ sender: UIButton) {
          
        self.navigationController?.popViewController(animated: true)
      }
      
    
    
    func showDateDialog(type:Int){
        
        let myDatePicker: UIDatePicker = UIDatePicker()
        //  myDatePicker.timeZone = NSTimeZone.local
        myDatePicker.datePickerMode = .time
        
        
        let date = Date(timeIntervalSince1970: -2.113e+9)
        myDatePicker.minimumDate = date
        
        myDatePicker.maximumDate = Date()
        
        myDatePicker.frame = CGRect(x: 0, y: 15, width: 270, height: 180)
        let alertController = UIAlertController(title: "\n\n\n\n\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
        alertController.view.addSubview(myDatePicker)
        let somethingAction = UIAlertAction(title: "Done", style: .destructive) { (UIAlertAction) in
            
            // self.oldDate = myDatePicker.date
          let timeFormatter = DateFormatter()
                         timeFormatter.dateFormat = "h:mm a" // set this date format string
                         let time = timeFormatter.string(from: myDatePicker.date)
                       
                       let d2 = myDatePicker.date.addingTimeInterval(43200)
                       let time2 = timeFormatter.string(from: d2)
            
            switch type {
            case 1:
                
                
               self.startTimeTF.text =  time
                                self.endTimeTF.text =  time2
            default:
              print()
            }
            
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alertController.addAction(cancelAction)
        alertController.addAction(somethingAction)
        
        present(alertController, animated: true, completion:{})
        
    }
    
}

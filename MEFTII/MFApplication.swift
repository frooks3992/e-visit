//
//  MFApplication.swift
//  MEFTII
//
//  Created by Mian Waqas Umar on 02/02/2020.
//  Copyright © 2020 Mian Waqas Umar. All rights reserved.
//

import Foundation
import UIKit

class MFApplication: UIApplication {
    
    override func sendEvent(_ event: UIEvent) {
        if event.type == .touches {
            TimeOutManager.shared.resetTimer()
        }
        super.sendEvent(event)
    }
    
    override func sendAction(_ action: Selector, to target: Any?, from sender: Any?, for event: UIEvent?) -> Bool {
        print("FILE= \(NSStringFromSelector(action)) METHOD=\(String(describing: target!)) SENDER=\(String(describing: sender))")
        TimeOutManager.shared.resetTimer()
        return super.sendAction(action, to: target, from: sender, for: event)
    }
}

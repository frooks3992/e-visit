//
//  ROCConfig.swift
//  Pods
//
//  Created by Maximilian Alexander on 3/20/17.
//
//

import UIKit

public struct ROCConfig {
    
    public struct Colors {
        public static var blueColor = UIColor(hexString: "#59569e")
        public static var lightGrayColor = UIColor(hexString: "#faf9f9")
    }
    
}
